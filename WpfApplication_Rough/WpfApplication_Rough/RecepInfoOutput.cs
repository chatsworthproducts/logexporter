﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using CPI.eConnect.Logging.eConnect;

namespace ConsoleApplication_LogBintoCSV
{
    public class RecepInfoOutput
    {
        public static void RecepInfoCSVOutput(ReadingStateInfo logInfo, string outputFilePath) 
        {
            foreach (KeyValuePair<string, PDUInfoEntry> entry in logInfo.PDUInfoIndex)
            {
                PDUInfoEntry test = logInfo.PDUInfoIndex[entry.Key];

                try
                {
                    using (StreamWriter w = new StreamWriter(outputFilePath, true))
                    {
                        for (int i = 0; i < test.RecepData.Count; i++)
                        {
                            RecepInfo currentEntry = test.RecepData[i];

                            ushort voltage = currentEntry.Voltage;
                            int j = i;
                            while (voltage == 0)
                            {
                                voltage = test.RecepData[j].Voltage;
                                j--;
                            }

                            string line = currentEntry.TimeStamp + ", " + currentEntry.Outlet + ", " + currentEntry.MAC + ", " +
                                String.Format("{0:0.00}", currentEntry.Current/100.0) + " A, " + String.Format("{0:0.0}", voltage / 10.0) + " V, " +
                                String.Format("{0:0.00}", currentEntry.Power/1000.0) + " kW, " + String.Format("{0:0.00}", currentEntry.Energy / 360000.0) + " kWh";

                            if (currentEntry.Current != 65535)
                                w.WriteLine(line);
                        }
                    }
                }
                catch (Exception e)
                {
                    //System.Console.WriteLine("SOMETHING WENT WRONG");
                }
            }
        }
    }
}
