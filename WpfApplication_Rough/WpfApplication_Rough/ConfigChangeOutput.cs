﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using CPI.eConnect.Logging.eConnect;

namespace ConsoleApplication_LogBintoCSV
{
    public class ConfigChangeOutput
    {

        private static string NewValue(ConfigChange currentEntry)
        {
            string newValue = "";

            if (currentEntry.Size == 1)
                newValue = currentEntry.NewValue[0] + "";

            //Numeric New Value
            if (currentEntry.Size == 2)
            {
                double value = (currentEntry.NewValue[0] << 8) + (currentEntry.NewValue[1]);

                if (currentEntry.ConfigVariable.ToString().Contains("VOLTS"))
                    value /= 10.0;
                if (currentEntry.ConfigVariable.ToString().Contains("CUR") || currentEntry.ConfigVariable.ToString().Contains("ENV"))
                    value /= 100.0;

                newValue = value + "";
            }
                
            //IPv4 Address
            if (currentEntry.Size == 4)
                newValue = currentEntry.NewValue[0] + "." + currentEntry.NewValue[1] + "." + currentEntry.NewValue[2] + "." + currentEntry.NewValue[3];

            //IPv6 Address
            if (currentEntry.Size == 16)
            {
                for (int j = 0; j < currentEntry.Size; j++)
                {
                    newValue += currentEntry.NewValue[j] + ", ";
                }
            }

            //Null Terminated String
            if (currentEntry.Size == 64)
            {
                for (int j = 0; j < currentEntry.NewValue.Length; j++)
                {
                    if (currentEntry.NewValue[j] == '\0')
                        break;
                    else
                        newValue += (char)currentEntry.NewValue[j];
                }
            }

            return newValue;
        }

        private static string Action(ConfigChange currentEntry)
        {
            string action = "";

            switch (currentEntry.ConfigVariable.ToString())
            {
                case("CIRCMAXCURR"):
                    switch(currentEntry.Occurance.ToString())
                    {
                        case("0"): 
                            action = "Set Max Current Alarm Threshold. Branch XY1";
                            break;
                        case("1"): 
                            action = "Set Max Current Alarm Threshold. Branch YZ1";
                            break;
                        case("2"): 
                            action = "Set Max Current Alarm Threshold. Branch ZX1";
                            break;
                        case("3"): 
                            action = "Set Max Current Alarm Threshold. Branch XY2";
                            break;
                        case("4"): 
                            action = "Set Max Current Alarm Threshold. Branch YZ2";
                            break;
                        case("5"): 
                            action = "Set Max Current Alarm Threshold. Branch ZX2";
                            break;
                    }
                    break;
                case("CIRCWARNHICUR"):
                    switch(currentEntry.Occurance.ToString())
                    {
                        case("0"): 
                            action = "Set Max Current Warning Threshold. Branch XY1";
                            break;
                        case("1"): 
                            action = "Set Max Current Warning Threshold. Branch YZ1";
                            break;
                        case("2"): 
                            action = "Set Max Current Warning Threshold. Branch ZX1";
                            break;
                        case("3"): 
                            action = "Set Max Current Warning Threshold. Branch XY2";
                            break;
                        case("4"): 
                            action = "Set Max Current Warning Threshold. Branch YZ2";
                            break;
                        case("5"): 
                            action = "Set Max Current Warning Threshold. Branch ZX2";
                            break;
                    }
                    break;
                case("CIRCWARNLOCUR"):
                    switch(currentEntry.Occurance.ToString())
                    {
                        case("0"): 
                            action = "Set Low Current Warning Threshold. Branch XY1";
                            break;
                        case("1"): 
                            action = "Set Low Current Warning Threshold. Branch YZ1";
                            break;
                        case("2"): 
                            action = "Set Low Current Warning Threshold. Branch ZX1";
                            break;
                        case("3"): 
                            action = "Set Low Current Warning Threshold. Branch XY2";
                            break;
                        case("4"): 
                            action = "Set Low Current Warning Threshold. Branch YZ2";
                            break;
                        case("5"): 
                            action = "Set Low Current Warning Threshold. Branch ZX2";
                            break;
                    }
                    break;
                case("ENVLIMITS"):
                    switch(currentEntry.Occurance.ToString())
                    {
                        case "0":
                            action = "Set Max Temp. Probe 1";
                            break;
                        case "1":
                            action = "Set Min Temp. Probe 1";
                            break;
                        case "4":
                            action = "Set Max Hum. Probe 1";
                            break;
                        case "5":
                            action = "Set Min Hum. Probe 1";
                            break;
                        case "8":
                            action = "Set Max Temp. Probe 2";
                            break;
                        case "9":
                            action = "Set Min Temp. Probe 2";
                            break;
                        case "12":
                            action = "Set Max Hum. Probe 2";
                            break;
                        case "13":
                            action = "Set Min Hum. Probe 2";
                            break;
                    }
                    break;
                case("MAXVOLTS"):
                    switch (currentEntry.Occurance.ToString())
                    {
                        case ("0"):
                            action = "Set Max Voltage Alarm Threshold. Branch XY1";
                            break;
                        case ("1"):
                            action = "Set Max Voltage Alarm Threshold. Branch YZ1";
                            break;
                        case ("2"):
                            action = "Set Max Voltage Alarm Threshold. Branch ZX1";
                            break;
                        case ("3"):
                            action = "Set Max Voltage Alarm Threshold. Branch XY2";
                            break;
                        case ("4"):
                            action = "Set Max Voltage Alarm Threshold. Branch YZ2";
                            break;
                        case ("5"):
                            action = "Set Max Voltage Alarm Threshold. Branch ZX2";
                            break;
                    }
                    break;
                case("MINVOLTS"):
                    switch (currentEntry.Occurance.ToString())
                    {
                        case ("0"):
                            action = "Set Min Voltage Alarm Threshold. Branch XY1";
                            break;
                        case ("1"):
                            action = "Set Min Voltage Alarm Threshold. Branch YZ1";
                            break;
                        case ("2"):
                            action = "Set Min Voltage Alarm Threshold. Branch ZX1";
                            break;
                        case ("3"):
                            action = "Set Min Voltage Alarm Threshold. Branch XY2";
                            break;
                        case ("4"):
                            action = "Set Min Voltage Alarm Threshold. Branch YZ2";
                            break;
                        case ("5"):
                            action = "Set Min Voltage Alarm Threshold. Branch ZX2";
                            break;
                    }
                    break;
                case("PDUDESCR"):
                    action = "Set PDU Description";
                    break;
                case("RECEPMAXCURR"):
                    action = "Set Max Current Alarm on Receptacle " + (currentEntry.Occurance + 1).ToString();
                    break;
                case("RECEPWARNHICURR"):
                    action = "Set Max Current Warning on Receptacle " + (currentEntry.Occurance + 1).ToString();
                    break;
                case("RECEPWARNLOCURR"):
                    action = "Set Min Current Warning on Receptacle " + (currentEntry.Occurance + 1).ToString();
                    break;
                case("SNMPHOSTSV6"):
                    action = "Set PDU Name";
                    break;
                case("DLOGCYCLE"):
                    action = "Set Log Interval";
                    break;
                case("DEVSIP"):
                    action = "Set IPv4 Address";
                    break;
                case("SUBNETMASK"):
                    action = "Set IPv4 Subnet Mask";
                    break;
                case("GATEWAY"):
                    action = "Set IPv4 Gateway";
                    break;
                case("DNSIPS"):
                    switch (currentEntry.Occurance.ToString())
                    {
                        case "0":
                            action = "Set Primary IPv4 DNS Server";
                            break;
                        case "1":
                            action = "Set Secondary IPv4 DNS Server";
                            break;
                    }
                    break;
                case("DEVSIPV6"):
                    action = "Set IPv6 Address";
                    break;
                case("SUBNMIPV6"):
                    action = "Set IPv6 Prefix Length";
                    break;
                case("GATEWAYIPV6"):
                    action = "Set IPv6 Gateway";
                    break;
                case ("DNSIPV6"):
                    switch (currentEntry.Occurance.ToString())
                    {
                        case "0":
                            action = "Set Primary IPv6 DNS Server";
                            break;
                        case "1":
                            action = "Set Secondary IPv6 DNS Server";
                            break;
                    }
                    break;

            }

            return action;
        }

        public static void ConfigChangeCSVOutput(ReadingStateInfo logInfo, string outputFilePath)
        {
            foreach (KeyValuePair<string, PDUInfoEntry> entry in logInfo.PDUInfoIndex)
            {
                PDUInfoEntry test = logInfo.PDUInfoIndex[entry.Key];

                try
                {
                    using (StreamWriter w = new StreamWriter(outputFilePath, true))
                    {
                        for (int i = 0; i < test.ConfigChanges.Count; i++)
                        {
                            ConfigChange currentEntry = test.ConfigChanges[i];

                            string action = Action(currentEntry);

                            if (action == "")
                                action = currentEntry.ConfigVariable.ToString();

                            string line = currentEntry.TimeStamp + ", " + currentEntry.MAC + ", " + currentEntry.UserId + ", " + 
                                currentEntry.PDUInterface + ", " + action + ", " + NewValue(currentEntry);
                            w.WriteLine(line);
                        }
                    }
                }
                catch (Exception e){}
            }
        }
    }
}
