﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using ConsoleApplication_LogBintoCSV;
using CPI.eConnect.Logging.eConnect;

namespace WpfApplication_Rough
{
    /// <summary>
    /// Interaction logic for ExportWindow.xaml
    /// </summary>
    public partial class ExportWindow : Window
    {
        string reportToRun = "";
        string relativePath;
        System.Collections.IList selectedFiles;
        Dictionary<string, ReadingStateInfo> logInfo;

        public ExportWindow()
        {
            InitializeComponent();
        }

        public void Show(string report, string relPath, System.Collections.IList logFiles, Dictionary<string, ReadingStateInfo> readings)
        {
            
            reportToRun = report;
            relativePath = relPath;
            selectedFiles = logFiles;
            logInfo = readings;
            this.Show();
        }

        private void BranchReport(string exportFile)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(exportFile))
                {
                    writer.WriteLine("TimeStamp, MAC, Branch, Current, Voltage, Power, PowerFactor, Energy");
                }
                foreach (var file in selectedFiles)
                {
                    BranchOutput.BranchCSVOutput(logInfo[file.ToString()], exportFile);
                }                                 
            }
            catch (Exception e) { }
        }

        private void ReceptacleReport(string exportFile)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(exportFile))
                {
                    writer.WriteLine("TimeStamp, Outlet, MAC, Current, Voltage, Power, Energy");
                }
                foreach (var file in selectedFiles)
                {
                    RecepInfoOutput.RecepInfoCSVOutput(logInfo[file.ToString()], exportFile);
                }    
            }
            catch (Exception e) { }
        }

        private void EnvironmentalReport(string exportFile)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(exportFile))
                {
                    writer.WriteLine("TimeStamp, MAC, Temp. 1, Humidity1, Temp. 2, Humidity 2");
                }
                foreach (var file in selectedFiles)
                {
                    EnvironInfoOutput.EnvironInfoCSVOutput(logInfo[file.ToString()], exportFile);
                }    
            }
            catch (Exception e) { }
        }

        private void AccessReport(string exportFile)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(exportFile))
                {
                    writer.WriteLine("TimeStamp, MAC, User ID, PDU Interface, Action, New Value");
                }
                foreach (var file in selectedFiles)
                {
                    ConfigChangeOutput.ConfigChangeCSVOutput(logInfo[file.ToString()], exportFile);
                    UserChangeOutput.UserChangeCSVOutput(logInfo[file.ToString()], exportFile);
                }    
            }
            catch (Exception e) { }
        }

        private void BranchAlarmReport(string exportFile)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(exportFile))
                {
                    writer.WriteLine("TimeStamp, MAC, Branch, High Voltage Alarm, Low Voltage Alarm, Critical High Current, Warning High Current, Warning Low Current");
                }
                foreach (var file in selectedFiles)
                {
                    AlarmOutput.AlarmCSVOutput(logInfo[file.ToString()], exportFile, "Branch");
                }
            }
            catch (Exception e) { }
        }

        private void ReceptacleAlarmReport(string exportFile)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(exportFile))
                {
                    writer.WriteLine("TimeStamp, MAC, Receptacle, High Current Alarm, High Current Warning, Low Current Warning");
                }
                foreach (var file in selectedFiles)
                {
                    AlarmOutput.AlarmCSVOutput(logInfo[file.ToString()], exportFile, "Receptacle");
                }
            }
            catch (Exception e) { }
        }

        private void EnvironmentalAlarmReport(string exportFile)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(exportFile))
                {
                    writer.WriteLine("TimeStamp, MAC, Probe, Max Threshold Alarm, Min Threshold Alarm");
                }
                foreach (var file in selectedFiles)
                {
                    AlarmOutput.AlarmCSVOutput(logInfo[file.ToString()], exportFile, "Environmental");
                }
            }
            catch (Exception e) { }
        }

        private void BrowseExportDirectory_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SaveFileDialog dialog = new SaveFileDialog();
                dialog.DefaultExt = "csv";
                dialog.Filter = "CSV Files | *.csv";
                dialog.AddExtension = true;
                dialog.ShowDialog();

                string selectedPath = dialog.FileName;
                ExportLogDirectoryTextBox.Text = selectedPath;

                switch (reportToRun)
                {
                    case "Branch":
                        BranchReport(selectedPath);
                        break;
                    case "Receptacle":
                        ReceptacleReport(selectedPath);
                        break;
                    case "Environment":
                        EnvironmentalReport(selectedPath);
                        break;
                    case "Access":
                        AccessReport(selectedPath);
                        break;
                    case "Branch Alarm":
                        BranchAlarmReport(selectedPath);
                        break;
                    case "Receptacle Alarm":
                        ReceptacleAlarmReport(selectedPath);
                        break;
                    case "Environmental Alarm":
                        EnvironmentalAlarmReport(selectedPath);
                        break;
                }       
                this.Close();
            }
            catch(Exception except) { }
        }
    }
}
