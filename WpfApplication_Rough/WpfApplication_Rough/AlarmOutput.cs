﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPI.eConnect.Logging.eConnect;

namespace ConsoleApplication_LogBintoCSV
{
    public class AlarmOutput
    {

        //How many receptacles are individually monitored
        static int numOfReceptacles = 64;

        private static string EnvironmentalAlarmLineOutput(AlarmInfo currentEntry, string probe)
        {
            string line = "";

            ushort mask = 0x0000;

            switch (probe)
            {
                case "T1":
                    mask = 0x1000;
                    break;
                case "T2":
                    mask = 0x2000;
                    break;
                case "H1":
                    mask = 0x4000;
                    break;
                case "H2":
                    mask = 0x8000;
                    break;
            }

            if ((currentEntry.AlarmsHigh & mask) != 0 ||
                (currentEntry.AlarmsLow & mask) != 0)
            {
                line += currentEntry.TimeStamp + ", " + currentEntry.MAC + ", " + probe + ", ";
                //High Voltage Alarm
                line += ((currentEntry.AlarmsHigh & mask) != 0) ? "1, " : ", ";
                //Low Voltage Alarm
                line += ((currentEntry.AlarmsLow & mask) != 0) ? "1, " : ", ";
            }
            else
                return "";

            return line;
        }

        private static string ReceptacleAlarmLineOutput(AlarmInfo currentEntry, short receptacle)
        {
            string line = "";

            //Get the receptacle Bit Mask
            ulong mask = (ulong)0x00000001 << (receptacle - 1);

            if ((currentEntry.RecepAlarmsHigh & mask) != 0 ||
                (currentEntry.RecepWarningHigh & mask) != 0 ||
                (currentEntry.RecepWarningLow & mask) != 0)
            {
                line += currentEntry.TimeStamp + ", " + currentEntry.MAC + ", " + receptacle + ", ";
                //High Voltage Alarm
                line += ((currentEntry.RecepAlarmsHigh & mask) != 0) ? "1, " : ", ";
                //Low Voltage Alarm
                line += ((currentEntry.RecepWarningHigh & mask) != 0) ? "1, " : ", ";
                //High Current Alarm
                line += ((currentEntry.RecepWarningLow & mask) != 0) ? "1, " : " ";
            }
            else
                return "";

            return line;
        }

        private static string BranchAlarmLineOutput(AlarmInfo currentEntry, string branch)
        {
            string line = "";
            ushort voltMask = 0x0000;
            ushort curMask = 0x0000;
            //Set the BitMask to use
            switch (branch)
            {
                case "XY1":
                    voltMask = 0x0040;
                    curMask = 0x0001;
                    break;
                case "YZ1":
                    voltMask = 0x0080;
                    curMask = 0x0002;
                    break;
                case "ZX1":
                    voltMask = 0x0100;
                    curMask = 0x0004;
                    break;
                case "XY2":
                    voltMask = 0x0200;
                    curMask = 0x0008;
                    break;
                case "YZ2":
                    voltMask = 0x0400;
                    curMask = 0x0010;
                    break;
                case "ZX2":
                    voltMask = 0x0800;
                    curMask = 0x0020;
                    break;
            }

            //Check if there are any alarms present
            if ((currentEntry.AlarmsHigh & voltMask) != 0 ||
                (currentEntry.AlarmsHigh & curMask) != 0 ||
                (currentEntry.AlarmsLow & voltMask) != 0 ||
                (currentEntry.AlarmsLow & curMask) != 0 ||
                (currentEntry.WarningsHigh & voltMask) != 0 ||
                (currentEntry.WarningsHigh & curMask) != 0 ||
                (currentEntry.WarnsingLow & voltMask) != 0 ||
                (currentEntry.WarnsingLow & curMask) != 0)
            //We have Alarms. Need to build the line for the CSV file
            {
                line += currentEntry.TimeStamp + ", " + currentEntry.MAC + ", " + branch + ", ";
                //High Voltage Alarm
                line += ((currentEntry.AlarmsHigh & voltMask) != 0) ? "1, " : ", ";
                //Low Voltage Alarm
                line += ((currentEntry.AlarmsLow & voltMask) != 0) ? "1, " : ", ";
                //High Current Alarm
                line += ((currentEntry.AlarmsHigh & curMask) != 0) ? "1, " : ", ";
                //Warning High Current
                line += ((currentEntry.WarningsHigh & curMask) != 0) ? "1, " : ", ";
                //Warning Low Current
                line += ((currentEntry.WarnsingLow & curMask) != 0) ? "1 " : " ";
            }
            //No Alarms
            else
                return "";

            return line;
        }

        static string convertRecAlarm(ulong code)
        {
            string alarms = "";

            for (int i = 0; i < numOfReceptacles; i++)
            {
                ulong mask = (ulong)Math.Pow(2, i);

                if ((code & mask) != 0)
                {
                    if (alarms == "")
                        alarms = (i + 1) + " ";
                    else
                        alarms += " | " + (i + 1);
                }
            }
            return alarms;
        }

        static string convertAlarm(uint code)
        {
            string alarms = "";

            if ((code & 0x0001) != 0)
            {
                if (alarms == "")
                    alarms = "XY1 Cur";
                else
                    alarms += " | XY1 Cur";
            }
            if ((code & 0x0002) != 0)
            {
                if (alarms == "")
                    alarms = "YZ1 Cur";
                else
                    alarms += " | YZ1 Cur";
            }
            if ((code & 0x0004) != 0)
            {
                if (alarms == "")
                    alarms = "ZX1 Cur";
                else
                    alarms += " | ZX1 Cur";
            }
            if ((code & 0x0008) != 0)
            {
                if (alarms == "")
                    alarms = "XY2 Cur";
                else
                    alarms += " | XY2 Cur";
            }
            if ((code & 0x0010) != 0)
            {
                if (alarms == "")
                    alarms = "YZ2 Cur";
                else
                    alarms += " | YZ2 Cur";
            }
            if ((code & 0x0020) != 0)
            {
                if (alarms == "")
                    alarms = "ZX2 Cur";
                else
                    alarms += " | ZX2 Cur";
            }
            if ((code & 0x0040) != 0)
            {
                if (alarms == "")
                    alarms = "XY1 Volt";
                else
                    alarms += " | XY1 Volt";
            }
            if ((code & 0x0080) != 0)
            {
                if (alarms == "")
                    alarms = "YZ1 Volt";
                else
                    alarms += " | YZ1 Volt";
            }
            if ((code & 0x0100) != 0)
            {
                if (alarms == "")
                    alarms = "ZX1 Volt";
                else
                    alarms += " | ZX1 Volt";
            }
            if ((code & 0x0200) != 0)
            {
                if (alarms == "")
                    alarms = "XY2 Volt";
                else
                    alarms += " | XY2 Volt";
            }
            if ((code & 0x0400) != 0)
            {
                if (alarms == "")
                    alarms = "YZ2 Volt";
                else
                    alarms += " | YZ2 Volt";
            }
            if ((code & 0x0800) != 0)
            {
                if (alarms == "")
                    alarms = "ZX2 Volt";
                else
                    alarms += " | ZX2 Volt";
            }
            if ((code & 0x1000) != 0)
            {
                if (alarms == "")
                    alarms = "T1";
                else
                    alarms += " | T1";
            }
            if ((code & 0x2000) != 0)
            {
                if (alarms == "")
                    alarms = "T2";
                else
                    alarms += " | T2";
            }
            if ((code & 0x4000) != 0)
            {
                if (alarms == "")
                    alarms = "H1";
                else
                    alarms += " | H1";
            }
            if ((code & 0x8000) != 0)
            {
                if (alarms == "")
                    alarms = "H2";
                else
                    alarms += " | H2";
            }
            return alarms;
        }

        //Full combined Report
        public static void AllAlarmCSVOutput(ReadingStateInfo logInfo, string outputFilePath)
        {
            foreach (KeyValuePair<string, PDUInfoEntry> entry in logInfo.PDUInfoIndex)
            {
                PDUInfoEntry test = logInfo.PDUInfoIndex[entry.Key];

                try
                {
                    using (StreamWriter w = new StreamWriter(outputFilePath, true))
                    {
                        for (int i = 0; i < test.Alarms.Count; i++)
                        {
                            AlarmInfo currentEntry = test.Alarms[i];

                            string alarmsHigh = convertAlarm(currentEntry.AlarmsHigh);
                            string alarmsLow = convertAlarm(currentEntry.AlarmsLow);
                            string warningsHigh = convertAlarm(currentEntry.WarningsHigh);
                            string warningsLow = convertAlarm(currentEntry.WarnsingLow);
                            string recepAlarmsLow = convertRecAlarm(currentEntry.RecepAlarmsLow);
                            string recepAlarmsHigh = convertRecAlarm(currentEntry.RecepAlarmsHigh);
                            string recepWarningLow = convertRecAlarm(currentEntry.RecepWarningLow);
                            string recepWarningHigh = convertRecAlarm(currentEntry.RecepWarningHigh);

                            string line = currentEntry.TimeStamp + ", " + currentEntry.MAC + ", " + alarmsHigh + ", " + alarmsLow + ", " + warningsHigh + ", " +
                                warningsLow + ", " + recepAlarmsHigh + ", " + recepAlarmsLow + ", " + recepWarningHigh + ", " + recepWarningLow;
                            w.WriteLine(line);
                            //w.Flush();         
                        }
                    }
                }
                catch (Exception e)
                {
                    //System.Console.WriteLine("SOMETHING WENT WRONG");
                }
            }
        }

        public static void AlarmCSVOutput(ReadingStateInfo logInfo, string outputFilePath, string report)
        {
            foreach (KeyValuePair<string, PDUInfoEntry> entry in logInfo.PDUInfoIndex)
            {
                PDUInfoEntry test = logInfo.PDUInfoIndex[entry.Key];

                try
                {
                    switch (report)
                    {
                        case "Branch":
                            using (StreamWriter w = new StreamWriter(outputFilePath, true))
                            {                                
                                string line = "";

                                for (int i = 0; i < test.Alarms.Count; i++)
                                {
                                    AlarmInfo currentEntry = test.Alarms[i];
                                    //XY1
                                    line = BranchAlarmLineOutput(currentEntry, "XY1");
                                    if (line != "")
                                        w.WriteLine(line);
                                    //YZ1
                                    line = BranchAlarmLineOutput(currentEntry, "YZ1");
                                    if (line != "")
                                        w.WriteLine(line);
                                    //ZX1
                                    line = BranchAlarmLineOutput(currentEntry, "ZX1");
                                    if (line != "")
                                        w.WriteLine(line);
                                    //XY2
                                    line = BranchAlarmLineOutput(currentEntry, "XY2");
                                    if (line != "")
                                        w.WriteLine(line);
                                    //YZ2
                                    line = BranchAlarmLineOutput(currentEntry, "YZ2");
                                    if (line != "")
                                        w.WriteLine(line);
                                    //ZX2
                                    line = BranchAlarmLineOutput(currentEntry, "ZX2");
                                    if (line != "")
                                        w.WriteLine(line);
                                }
                            }
                            break;
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        case "Receptacle":
                            using (StreamWriter w = new StreamWriter(outputFilePath, true))
                            {                                
                                for (int i = 0; i < test.Alarms.Count; i++)
                                {
                                    AlarmInfo currentEntry = test.Alarms[i];
                                    for (short r = 0; r < 64; r++)
                                    {
                                        string line = ReceptacleAlarmLineOutput(currentEntry, (short)(r + 1));
                                        if (line != "")
                                            w.WriteLine(line);
                                    }
                                }
                            }
                            break;
                        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        case "Environmental":
                            using (StreamWriter w = new StreamWriter(outputFilePath, true))
                            {                                
                                for (int i = 0; i < test.Alarms.Count; i++)
                                {
                                    AlarmInfo currentEntry = test.Alarms[i];
                                    //Temperature 1 Probe
                                    string line = EnvironmentalAlarmLineOutput(currentEntry, "T1");
                                    if (line != "")
                                        w.WriteLine(line);
                                    //Temperature 2 Probe
                                    line = EnvironmentalAlarmLineOutput(currentEntry, "T2");
                                    if (line != "")
                                        w.WriteLine(line);
                                    //Humidity 1 Probe
                                    line = EnvironmentalAlarmLineOutput(currentEntry, "H1");
                                    if (line != "")
                                        w.WriteLine(line);
                                    //Humidity 2 Probe
                                    line = EnvironmentalAlarmLineOutput(currentEntry, "H2");
                                    if (line != "")
                                        w.WriteLine(line);
                                }
                            }
                            break;

                    }
                }
                catch (Exception e)
                {
                    //System.Console.WriteLine("SOMETHING WENT WRONG");
                }
            }
        }
    }
}
