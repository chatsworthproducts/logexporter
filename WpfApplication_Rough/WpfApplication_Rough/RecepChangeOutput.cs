﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using CPI.eConnect.Logging.eConnect;

namespace ConsoleApplication_LogBintoCSV
{
    public class RecepChangeOutput
    {
        public static void RecepChangeCSVOutput(ReadingStateInfo logInfo, string outputFilePath)
        {
            foreach (KeyValuePair<string, PDUInfoEntry> entry in logInfo.PDUInfoIndex)
            {
                PDUInfoEntry test = logInfo.PDUInfoIndex[entry.Key];

                try
                {
                    using (StreamWriter w = new StreamWriter(outputFilePath, true))
                    {
                        //w.WriteLine("TimeStamp, MAC, User ID, PDU Interface, Action, Status Map, Reset Map");
                        //w.Flush();

                        for (int i = 0; i < test.RecepChanges.Count; i++)
                        {
                            RecepChange currentEntry = test.RecepChanges[i];
                            string line = currentEntry.TimeStamp + ", " + currentEntry.MAC + ", " + currentEntry.UserId + ", " + currentEntry.PDUInterface + ", " +
                                currentEntry.Action + ", " + currentEntry.StatusMap + ", " + currentEntry.ResetMap;
                            w.WriteLine(line);
                            //w.Flush();         
                        }
                    }
                }
                catch (Exception e)
                {
                    //System.Console.WriteLine("SOMETHING WENT WRONG");
                }
            }
        }
    }
}
