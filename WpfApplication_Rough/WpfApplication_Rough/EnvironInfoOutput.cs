﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using CPI.eConnect.Logging.eConnect;

namespace ConsoleApplication_LogBintoCSV
{
    public class EnvironInfoOutput
    {
        public static void EnvironInfoCSVOutput(ReadingStateInfo logInfo, string outputFilePath)
        {
            foreach (KeyValuePair<string, PDUInfoEntry> entry in logInfo.PDUInfoIndex)
            {
                PDUInfoEntry test = logInfo.PDUInfoIndex[entry.Key];

                try
                {
                    using (StreamWriter w = new StreamWriter(outputFilePath, true))
                    {
                        ushort temp1, temp2, humid1, humid2;
                        double displayTemp1, displayTemp2, displayHumid1, displayHumid2;
                        string dT1, dT2, dH1, dH2, line;
                        line = dT1 = dT2 = dH1 = dH2 = "";

                        temp1 = temp2 = humid1 = humid1 = 0;
                        displayTemp1 = displayTemp2 = displayHumid1 = displayHumid2 = 0.0;

                        for (int i = 0; i < test.EnvironmentData.Count; i++)
                        {
                            EnvironInfo currentEntry = test.EnvironmentData[i];

                            switch (i % 4)
                            {
                                case 0:
                                    temp1 = currentEntry.Value;
                                    displayTemp1 = temp1 / 100.0;
                                    dT1 = String.Format("{0:0.00}", displayTemp1) + " F";
                                    break;
                                case 1:
                                    humid1 = currentEntry.Value;
                                    displayHumid1 = humid1 / 100.0;
                                    dH1 = String.Format("{0:0.00}", displayHumid1) + "%";
                                    break;
                                case 2:
                                    temp2 = currentEntry.Value;
                                    displayTemp2 = temp2 / 100.0;
                                    dT2 = String.Format("{0:0.00}", displayTemp2) + " F";
                                    break;
                                case 3:                                   
                                    humid2 = currentEntry.Value;
                                    displayHumid2 = humid2 / 100.0;
                                    dH2 = String.Format("{0:0.00}", displayHumid2) + "%";

                                    line = currentEntry.TimeStamp + ", " + currentEntry.MAC + ", " + dT1 + ", " + dH1 + ", " + dT2 + ", " + dH2;
                                    w.WriteLine(line);
                                    temp1 = temp2 = humid1 = humid1 = 0;
                                    break;
                            }                          
                        }
                    }
                }
                catch (Exception e)
                {
                    //System.Console.WriteLine("SOMETHING WENT WRONG");
                }
            }
        }
    }
}
