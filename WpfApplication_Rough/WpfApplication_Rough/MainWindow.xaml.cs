﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using CPI.eConnect.Logging.eConnect;

namespace WpfApplication_Rough
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static string outputFile = "C:\\Users\\baldwin_m\\Documents\\Random Notes\\Feature and Bug Testing\\BinToCSV\\GUI_Testing.txt";
        
        //To Pass when running reports
        string selectedPath;
        Dictionary<string, ReadingStateInfo> readingStates;
        List<string> sortedFileSelections;

        public MainWindow()
        {            
            readingStates = new Dictionary<string, ReadingStateInfo>();
            sortedFileSelections = new List<string>();
            InitializeComponent();
        }

        private void AvailableLogFiles_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                //Add to the readingStates Dictionary
                if(e.AddedItems.Count > 0)
                {
                    LogReader reader = new LogReader( selectedPath + "\\" + e.AddedItems[0].ToString());
                    ReadingStateInfo logInfo = new ReadingStateInfo(reader);

                    try
                    {
                        while (true)
                        {
                            reader.ReadEntry();
                        }
                    }
                    catch (System.IO.EndOfStreamException)
                    {
                        // not really an error, we're just done reading the file.
                    }
                    finally
                    {
                        reader.Dispose();
                    }
                    string file = e.AddedItems[0].ToString();
                    readingStates.Add(file, logInfo);

                    sortedFileSelections = readingStates.Keys.ToList<string>();
                    sortedFileSelections.Sort();

                }
                //Remove from ReadingStates Dictionary
                if(e.RemovedItems.Count > 0)
                {
                    string file = e.RemovedItems[0].ToString();
                    readingStates.Remove(file);
                }
            }
            catch (Exception except) 
            {
                using (StreamWriter writer = new StreamWriter(outputFile, true))
                {
                    writer.WriteLine("Here's what went wrong...");
                    writer.WriteLine(except.ToString());
                }
            }
        }

        private void BrowseWorkingDirectory_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
                dialog.ShowDialog();
                selectedPath = dialog.SelectedPath;

                WorkingLogDirectoryTextBox.Text = selectedPath;

                AvailableLogFiles.Items.Clear();

                IEnumerable<string> listOfFiles = Directory.EnumerateFiles(selectedPath, "*.dat*");

                short count = 0;

                foreach (string file in listOfFiles)
                {
                    string fileName = file.Remove(0, selectedPath.Length + 1);
                    AvailableLogFiles.Items.Add(fileName);
                    count++;
                }

                if (count == 0)
                    AvailableLogFiles.Items.Add("No Log Files Found!");

            }
            catch(Exception except) { }
        }

        private void BranchMetricsReport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ExportWindow win1 = new ExportWindow();
                win1.Show("Branch", selectedPath, sortedFileSelections, readingStates);
            }
            catch (Exception except) { }
        }

        private void ReceptacleMetricsReport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ExportWindow win1 = new ExportWindow();
                win1.Show("Receptacle", selectedPath, sortedFileSelections, readingStates);
            }
            catch (Exception except) { }
        }

        private void EnvironmentalMetricsReport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ExportWindow win1 = new ExportWindow();
                win1.Show("Environment", selectedPath, sortedFileSelections, readingStates);
            }
            catch (Exception except) { }
        }

        private void AccessAuditReport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ExportWindow win1 = new ExportWindow();
                win1.Show("Access", selectedPath, sortedFileSelections, readingStates);
            }
            catch (Exception except) { }
        }

        private void BranchAlarmsReport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ExportWindow win1 = new ExportWindow();
                win1.Show("Branch Alarm", selectedPath, sortedFileSelections, readingStates);
            }
            catch (Exception except) { }
        }

        private void ReceptacleAlarmsReport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ExportWindow win1 = new ExportWindow();
                win1.Show("Receptacle Alarm", selectedPath, sortedFileSelections, readingStates);
            }
            catch (Exception except) { }
        }

        private void EnvironmentalAlarmsReport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ExportWindow win1 = new ExportWindow();
                win1.Show("Environmental Alarm", selectedPath, sortedFileSelections, readingStates);
            }
            catch (Exception except) { }
        }
    }

}
