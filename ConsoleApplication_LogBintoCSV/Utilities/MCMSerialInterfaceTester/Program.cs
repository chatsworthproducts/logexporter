﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPI.eConnect.Communications;

namespace MCMSerialInterfaceTester
{
    class Program
    {
        static void Main(string[] args)
        {
            MCMSerialController controller = new MCMSerialController();

            controller.NewData += new EventHandler<OnSerialDataEventArgs>(MCMResponseHandler);
            controller.PortName = "COM1";
            controller.Connect();

            controller.Login
                    .SetParameter("user", string.Empty)
                    .SetParameter("password", string.Empty)
                    .Execute();

            controller.SetAdvandedMode
                    .Execute();
            
            controller.GetMetrics
                .SetParameter("pdu index", 0)
                .SetParameter("port", "0xff")
                .SetParameter("metric", Metric.ALL.ToString())
                .Execute();

            controller.Logout
                    .Execute();

            System.Console.ReadLine();
            controller.Close();
        }

        static void MCMResponseHandler(object sender, OnSerialDataEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(e.Data);
        }
    }
}
