﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPI.eConnect.API;

namespace PDUEnumerator
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, string> properties = new Dictionary<string, string>();
            properties.Add("Public.DeviceManager.SNMPVersion", "SNMPv1v2c");
            properties.Add("Public.DeviceManager.SNMPReadCommunity", "public");
            properties.Add("Public.DeviceManager.SNMPWriteCommunity", "private");

            List<PDU> foundPdus = PDUSearcher.FindWithSNMP("192.168.140.1", "192.168.140.254", properties);

            Console.WriteLine("Found {0} PDUS.", foundPdus.Count);
        }
    }
}
