﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPI.eConnect.Communications;
using CPI.eConnect.Programmer;

namespace MCMDevProgrammer
{
    class Program
    {
        static string _Sequence = string.Empty;
        static string COMPort = string.Empty;
        static string ServerIP = string.Empty;
        static string MCMIP = string.Empty;
        static int MDMID = -1;

        // MCMDevProgrammer -port [COM Port] -serverip [server ip] -mcmip [MCM IP] -mcmid [MCMID]
        static void Main(string[] args)
        {
            ProcessArgs(args);
            if( ValidateArgs() == false)
            {
                Console.WriteLine("Please correct the issues above before trying again.");
                return;
            }

            MCMBiosController controller;

            controller = new MCMBiosController();
            controller.PortName = COMPort;
            controller.NewData += new EventHandler<OnSerialDataEventArgs>(MCMResponseHandler);
            controller.Connect();

            if (ProgramSBF(controller) != 0)
            {
                controller.Close();
                return;
            }

            if (ProgramBIOS(controller) != 0)
            {
                controller.Close();
                return;
            }

            if (ProgramBoot(controller) != 0)
            {
                controller.Close();
                return;
            }

            if(ProgramFactoryDefaults(controller) != 0)
            {
                controller.Close();
                return;
            }

            controller.Close();
        }

        static bool ValidateArgs()
        {
            bool returnValue = true;
            if( COMPort == string.Empty )
            {
                returnValue = false;
                Console.WriteLine("\tPlease specify a COM Port for communication to the MCM.");
            }

            if (ServerIP == string.Empty)
            {
                returnValue = false;
                Console.WriteLine("\tPlease specify a TFTP server address.");
            }

            if (MCMIP == string.Empty)
            {
                returnValue = false;
                Console.WriteLine("\tPlease specify a MCM IP address.");
            }

            if (MDMID == -1)
            {
                returnValue = false;
                Console.WriteLine("\tPlease specify a MCM ID number.");
            }

            return returnValue;
        }

        static void ProcessArgs(string[] args)
        {
            for(int i = 0; i < args.Length; i++)
            {
                switch(args[i])
                {
                    case "-port":
                        COMPort = args[i + 1];
                        i++;
                        break;
                    case "-serverip":
                        ServerIP = args[i + 1];
                        i++;
                        break;
                    case "-mcmip":
                        MCMIP = args[i + 1];
                        i++;
                        break;
                    case "-mcmid":
                        MDMID = int.Parse(args[i + 1]);
                        i++;
                        break;
                }
            }
        }

        static int ProgramFactoryDefaults(MCMBiosController controller)
        {
            int returnValue = 0;
            FactoryDefaultsProgrammer programmer = new FactoryDefaultsProgrammer();
            _Sequence = "Setting Factory Defaults";
            programmer.StatusEvent += new EventHandler<ProgressStatusEventArgs>(ProgressHandler);
            programmer.Setup(controller);
            returnValue = programmer.Execute();
            programmer.StatusEvent -= new EventHandler<ProgressStatusEventArgs>(ProgressHandler);
            programmer.Dispose();

            return returnValue;
        }

        static int ProgramBoot(MCMBiosController controller)
        {
            int returnValue = 0;
            BootProgrammer programmer = new BootProgrammer();
            _Sequence = "Booting MCM";
            programmer.StatusEvent += new EventHandler<ProgressStatusEventArgs>(ProgressHandler);
            programmer.Setup(controller);
            returnValue = programmer.Execute(true);
            programmer.StatusEvent -= new EventHandler<ProgressStatusEventArgs>(ProgressHandler);
            programmer.Dispose();

            return returnValue;
        }

        static int ProgramSBF(MCMBiosController controller)
        {
            int returnValue = 0;
            SerialBootProgrammer programmer = new SerialBootProgrammer();
            _Sequence = "Programming Boot Flash";
            programmer.StatusEvent += new EventHandler<ProgressStatusEventArgs>(ProgressHandler);
            programmer.Setup(controller);
            returnValue = programmer.Execute();
            programmer.StatusEvent -= new EventHandler<ProgressStatusEventArgs>(ProgressHandler);
            programmer.Dispose();

            return returnValue;
        }

        static int ProgramBIOS(MCMBiosController controller)
        {
            int returnValue = 0;
            BiosProgrammer programmer = new BiosProgrammer();
            _Sequence = "Programming BIOS";
            programmer.StatusEvent += new EventHandler<ProgressStatusEventArgs>(ProgressHandler);
            programmer.Setup(controller);
            returnValue = programmer.Execute();
            programmer.StatusEvent -= new EventHandler<ProgressStatusEventArgs>(ProgressHandler);
            programmer.Dispose();
            
            return returnValue;
        }

        static void ProgressHandler(object sender, ProgressStatusEventArgs e)
        {
            Console.WriteLine("{3} - {0}/{1} - {2}", e.CurrentStep, e.TotalSteps, e.Message, _Sequence);
            System.Diagnostics.Debug.WriteLine(string.Format("{3} - {0}/{1} - {2}", e.CurrentStep, e.TotalSteps, e.Message, _Sequence));
        }

        static void MCMResponseHandler(object sender, OnSerialDataEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(e.Data);
        }
    }
}
