﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDULogConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            CPI.eConnect.Logging.eConnect.LogReader reader = new CPI.eConnect.Logging.eConnect.LogReader(args[0]);
            CPI.eConnect.Logging.eConnect.ReadingStateInfo state = new CPI.eConnect.Logging.eConnect.ReadingStateInfo(reader);

            try
            {
                while (true)
                {
                    reader.ReadEntry();
                }
            }
            catch (System.IO.EndOfStreamException)
            {
                // not really an error, we're just done reading the file.
            }
            finally
            {
                reader.Dispose();
            }
        }
    }
}
