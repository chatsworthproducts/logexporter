﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using CPI.eConnect.Communications;

namespace CPI.eConnect.OpsLibrary
{
    public class OpsSequence : IDisposable
    {
        public Dictionary<string, object> Parameters { get; private set; }
        public List<IPDUCommand> CommandSequence { get; private set; }
        public IMCMChannel MCMConnection { get; set; }

        public OpsSequence(IMCMChannel connection)
        {
            this.MCMConnection = connection;
            this.Parameters = new Dictionary<string, object>();
            this.CommandSequence = new List<IPDUCommand>();
        }

        public void Execute()
        {            
            foreach(IPDUCommand cmd in CommandSequence)
            {
                List<string> keys = new List<string>(cmd.Parameters.Keys);
                foreach(string paramName in keys)
                {
                    if(this.Parameters.ContainsKey(paramName))
                    {
                        cmd.Parameters[paramName] = this.Parameters[paramName];
                    }
                }

                bool ret = cmd.Execute();
                if(ret == false)
                {
                    throw new Exception();
                }
            }
        }

        // Sequence Line:
        //   0     1         2              3        4
        //  cmd [paramName=paramValue], [paramName=paramValue]
        public static OpsSequence LoadSequenceFromStream(StreamReader sr, IMCMChannel connection)
        {
            List<string> sequenceCommandString = new List<string>();
            OpsSequence sequence = new OpsSequence(connection);

            while (sr.EndOfStream == false)
            {
                sequenceCommandString.Add(sr.ReadLine());
            }

            foreach (string cmdStr in sequenceCommandString)
            {
                string[] parts = cmdStr.Split(' ', ',', '=');
                IPDUCommand cmd = (IPDUCommand)sequence.MCMConnection.GetType().GetProperty(parts[0]).GetValue(sequence.MCMConnection);
                for (int i = 1; i < parts.Length - 1; i += 2)
                {
                    string paramName = parts[i].Trim('[', ']', ' ');
                    string paramValue = parts[i + 1].Trim('[', ']', ' ');
                    sequence.Parameters.Add(paramName, paramValue);
                    cmd.Parameters.Add(paramName, paramValue);
                }

                sequence.CommandSequence.Add(cmd);
            }

            return sequence;
        }

        // Sequence Line:
        //   0     1         2              3        4
        //  cmd [paramName=paramValue], [paramName=paramValue]
        public static OpsSequence LoadSequence(string fileName, IMCMChannel connection)
        {            
            using(StreamReader sr = new StreamReader(fileName))
            {
                return OpsSequence.LoadSequenceFromStream(sr, connection);
            }
        }

        public static void SaveSequence(string fileName, OpsSequence seq)
        {

        }

        public void Dispose()
        {
            foreach (IDisposable cmd in CommandSequence)
            {
                cmd.Dispose();
            }
        }
    }
}
