﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPI.eConnect.Communications;

namespace CPI.eConnect.OpsLibrary
{
    public static class OpsFactory
    {
        public static OpsSequence CreateSequence(string sequenceName, IMCMChannel connection)
        {
            System.IO.Stream s = 
                typeof(OpsFactory).Assembly.GetManifestResourceStream(
                    string.Format("CPI.eConnect.OpsLibrary.Sequences.{0}.txt", sequenceName));

            System.IO.StreamReader sr = new System.IO.StreamReader(s);

            return OpsSequence.LoadSequenceFromStream(sr, connection);
        }
    }
}
