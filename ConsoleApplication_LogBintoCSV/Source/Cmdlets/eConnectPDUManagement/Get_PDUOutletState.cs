﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eConnectPDUManagement.SupportScripts
{
    [System.Management.Automation.Cmdlet(System.Management.Automation.VerbsCommon.Get, "PDUOutletState")]
    public class Get_PDUOutletState : System.Management.Automation.PSCmdlet
    {
        [System.Management.Automation.Parameter(Position = 0, Mandatory = true)]
        public string GatewayIPAddress;

        [System.Management.Automation.Parameter(Position = 1, Mandatory = true)]
        public string GatewaySelector;

        [System.Management.Automation.Parameter(Position = 2, Mandatory = true)]        
        public int OutletNumber;

        protected override void ProcessRecord()
        {
            base.ProcessRecord();

            Dictionary<string, string> properties = new Dictionary<string, string>();
            properties.Add("Public.DeviceManager.SNMPVersion", "SNMPv1v2c");
            properties.Add("Public.DeviceManager.SNMPReadCommunity", "public");
            properties.Add("Public.DeviceManager.SNMPWriteCommunity", "private");

            List<string> oidList = new List<string>();
            Helpers.OIDBuilder oidBuild = new Helpers.OIDBuilder(this.GatewaySelector);
            string recepControlOID = oidBuild.BuildOID("1.3.6.1.4.1.30932.1.1.4.1.{0}", this.OutletNumber);

            oidList.Add(recepControlOID); // recept. Control

            IDictionary<string, string> snmpData = CPI.Common.SNMP.SNMPDotNetLib.GetSNMPData(GatewayIPAddress, oidList, properties);

            Helpers.SNMPDictionary dict = new Helpers.SNMPDictionary(snmpData);
            SupportObjects.OutletState outletValue = dict.GetOIDValueAsOutletState(recepControlOID);
            if(outletValue == SupportObjects.OutletState.NotAvailable)
            {
                this.WriteWarning("PDU does not have outlet control.");
            }
            else            
            {
                this.WriteObject(outletValue);
            }
        }
    }
}
