﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using CPI.eConnect.API;

namespace eConnectPDUManagement
{
    [System.Management.Automation.Cmdlet(System.Management.Automation.VerbsCommon.Find, "NetworkPDUs")]
    public class Find_NetworkPDUs : System.Management.Automation.PSCmdlet
    {
        [System.Management.Automation.Parameter(Position = 0, Mandatory = true)]
        public string StartIPAddress;

        [System.Management.Automation.Parameter(Position = 1, Mandatory = true)]
        public string EndIPAddress;

        protected override void ProcessRecord()
        {
            base.ProcessRecord();

            Dictionary<string, string> properties = new Dictionary<string, string>();
            properties.Add("Public.DeviceManager.SNMPVersion", "SNMPv1v2c");
            properties.Add("Public.DeviceManager.SNMPReadCommunity", "public");
            properties.Add("Public.DeviceManager.SNMPWriteCommunity", "private");

            List<PDU> foundPdus = PDUSearcher.FindWithSNMP(StartIPAddress, EndIPAddress, properties);
            this.WriteObject(foundPdus);
        }
    }
}
