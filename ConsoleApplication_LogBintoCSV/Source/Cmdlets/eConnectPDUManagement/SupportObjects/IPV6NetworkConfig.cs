﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eConnectPDUManagement.SupportObjects
{
    public class IPV6NetworkConfig
    {
        public bool ipV6Enable { get; set; }
        public bool linkLocalEnable { get; set; }
        public bool globalAddrEnable { get; set; }
        public string pduIPv6 { get; set; }
        public bool autoIPv6 { get; set; }
        public bool autoDNSv6 { get; set; }
        public string subnetMaskv6 { get; set; }
        public string gatewayv6 { get; set; }
        public string dns1v6 { get; set; }
        public string dns2v6 { get; set; }
        public string dns3v6 { get; set; }
    }
}
