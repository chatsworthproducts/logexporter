﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eConnectPDUManagement.SupportObjects
{
    public class IPV4NetworkConfig
    {
        public bool ipv4Enable { get; set; }
        public string pduIP { get; set; }
        public bool autoIP { get; set; }
        public bool autoDNS { get; set; }
        public string subnetMask { get; set; }
        public string gateway { get; set; }
        public string dns1 { get; set; }
        public string dns2 { get; set; }
        public string dns3 { get; set; }
    }
}
