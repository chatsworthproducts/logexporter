﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eConnectPDUManagement.SupportObjects
{
    public class LDAPAccessControlConfig
    {
        public bool enableADAuth { get; set; }
        public string adServer { get; set; }
        public string adDomain { get; set; }
        public int adPort { get; set; }
        public int adResvPort { get; set; }
    }
}
