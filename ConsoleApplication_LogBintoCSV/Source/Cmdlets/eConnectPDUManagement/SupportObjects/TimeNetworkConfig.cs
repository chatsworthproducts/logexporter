﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eConnectPDUManagement.SupportObjects
{
    public class TimeNetworkConfig
    {
        public string rfcTimeServer { get; set; }
        public string ntpTimeServer { get; set; }
    }
}
