﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eConnectPDUManagement.SupportObjects
{
    public class HTTPInterfaceConfig
    {
        public bool enableWeb { get; set; }
        public bool httpEnable { get; set; }
        public bool httpsEnable { get; set; }
        public int httpPort { get; set; }
        public int httpsPort { get; set; }
    }
}
