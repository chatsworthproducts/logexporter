﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eConnectPDUManagement.SupportObjects
{
    public class LoggingConfig
    {
        public bool logEnable { get; set; }
        public int logInterval { get; set; }
        public int logDifference { get; set; }
        public bool netservdLog { get; set; }
        public bool autonetLog { get; set; }
        public bool stdmeterdLog { get; set; }
        public bool groupmeterdLog { get; set; }
        public bool alarmdLog { get; set; }
        public bool userlogindLog { get; set; }
        public bool usersetupdLog { get; set; }
        public bool fwupdatedLog { get; set; }
        public bool setupchangedLog { get; set; }
        public bool recepchangedLog { get; set; }
        public bool pduopchangedLog { get; set; }
        public int dLogCycle { get; set; }
        public int dLogFullWarnLevel { get; set; }
        public string netLogServer { get; set; }
        public int netdLogPort { get; set; }
        public string netdLogUser { get; set; }
        public string netdLogPasswd { get; set; }
        public string netdLogDestdir { get; set; }
        public string netdLogOptions { get; set; }

    }
}
