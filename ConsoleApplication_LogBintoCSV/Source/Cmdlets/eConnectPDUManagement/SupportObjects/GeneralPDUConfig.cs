﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eConnectPDUManagement.SupportObjects
{
    public class GeneralPDUConfig
    {
        public bool inputTop { get; set; }
        public bool celsiusTemp { get; set; }
        public bool shareRole { get; set; }
        public bool missingPDUNotify { get; set; }
        public bool roleChangeNotify { get; set; }
        public int dispTimeout { get; set; }
        public int loginTimeout { get; set; }
        public int dispBrightness { get; set; }
    }
}
