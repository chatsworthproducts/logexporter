﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eConnectPDUManagement.SupportObjects
{
    public enum OutletState
    {
        Off = 0,
        On = 1,
        Reset = 2,
        NotAvailable = 0xff
    }
}
