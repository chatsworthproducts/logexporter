﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eConnectPDUManagement.SupportObjects
{
    public class SMTPInterfaceConfig
    {
        public bool enableSMTP { get; set; }
        public int smtpSendPort { get; set; }
        public int smtpReceivePort { get; set; }
        public string smtpServer { get; set; }
        public string smtpUserName { get; set; }
        public string smtpUserPasswd { get; set; }
        public string smtpEmailAddr { get; set; }
        public string smtpEmailDestination1 { get; set; }
        public string smtpEmailDestination2 { get; set; }
        public string smtpEmailDestination3 { get; set; }
        public string smtpServOptions { get; set; }
    }
}
