﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eConnectPDUManagement.SupportObjects
{
    public class SNMPInterfaceConfig
    {
        public bool snmpEnable { get; set; }
        public bool snmpHostOnly { get; set; }
        public string snmpHost1 { get; set; }
        public string snmpHost2 { get; set; }
        public string snmpHost3 { get; set; }
        public string snmpHostIPv61 { get; set; }
        public string snmpHostIPv62 { get; set; }
        public string snmpHostIPv63 { get; set; }
        public int snmpQueryPort { get; set; }
        public int snmpTrapPort { get; set; }
        public string snmpReadComm { get; set; }
        public string snmpWriteComm { get; set; }
        public string snmpUSMuserName { get; set; }
        public int snmpSecurityLevel { get; set; }
        public int snmpAuthAlgo { get; set; }
        public int snmpPrivAlgo { get; set; }
        public string snmpAuthPasswd { get; set; }
        public string snmpPrivPasswd { get; set; }
        public string snmpContextName { get; set; }
        public string snmpEngineID { get; set; }
        public string snmpLocAuthKey { get; set; }
        public string snmpLocPrivKey { get; set; }
        public string snmpTrapHost1 { get; set; }
        public string snmpTrapHost2 { get; set; }
        public string snmpTrapHost3 { get; set; }
        public string snmpTrapHostIPv61 { get; set; }
        public string snmpTrapHostIPv62 { get; set; }
        public string snmpTrapHostIPv63 { get; set; }
    }
}
