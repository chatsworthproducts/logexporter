﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eConnectPDUManagement.SupportObjects
{
    public class SSHInterfaceConfig
    {
        public bool telnetEnable { get; set; }
        public bool sshEnable { get; set; }
        public int sshPort { get; set; }
        public int telnetPort { get; set; }
    }
}
