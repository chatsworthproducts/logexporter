﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eConnectPDUManagement.SupportObjects
{
    public class RadiusAccessControlConfig
    {
        public bool enableRadiusAuth { get; set; }
        public string radiusServer { get; set; }
        public string radiusSHSecret { get; set; }
        public string radiusNASServer { get; set; }
        public int radiusPort { get; set; }
        public int nasPort { get; set; }
    }
}
