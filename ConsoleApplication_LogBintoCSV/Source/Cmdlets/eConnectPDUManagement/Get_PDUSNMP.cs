﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eConnectPDUManagement
{
    [System.Management.Automation.Cmdlet(System.Management.Automation.VerbsCommon.Get, "PDUSNMP")]
    public class Get_PDUSNMP : System.Management.Automation.PSCmdlet
    {
        [System.Management.Automation.Parameter(Position = 0, Mandatory = true)]
        public string GatewayIPAddress;

        [System.Management.Automation.Parameter(Position = 1, Mandatory = true)]
        public string GatewaySelector;

        [System.Management.Automation.Parameter(Position = 2, Mandatory = true)]
        public string OID;

        protected override void ProcessRecord()
        {
            base.ProcessRecord();

            Dictionary<string, string> properties = new Dictionary<string, string>();
            properties.Add("Public.DeviceManager.SNMPVersion", "SNMPv1v2c");
            properties.Add("Public.DeviceManager.SNMPReadCommunity", "public");
            properties.Add("Public.DeviceManager.SNMPWriteCommunity", "private");

            List<string> getData = new List<string>();
            Helpers.OIDBuilder oidBuild = new Helpers.OIDBuilder(this.GatewaySelector);
            string requestedOID = oidBuild.BuildOID(this.OID);

            getData.Add(requestedOID);

            IDictionary<string, string> snmpData = CPI.Common.SNMP.SNMPDotNetLib.GetSNMPData(GatewayIPAddress, getData, properties);

            Helpers.SNMPDictionary dict = new Helpers.SNMPDictionary(snmpData);
            this.WriteObject(dict.GetOIDValueAsString(requestedOID));
        }
    }
}
