﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eConnectPDUManagement
{
    [System.Management.Automation.Cmdlet(System.Management.Automation.VerbsCommon.New, "PDUConfigObject")]
    public class New_PDUConfigObject : System.Management.Automation.PSCmdlet
    {
        [System.Management.Automation.Parameter(Position = 0, Mandatory = true)]
        public string ObjectName;

        protected override void ProcessRecord()
        {
            base.ProcessRecord();
                        
            switch(this.ObjectName)
            {
                case "GeneralPDUConfig":
                    this.WriteObject(new SupportObjects.GeneralPDUConfig());
                    break;
                case "HTTPInterfaceConfig":
                    this.WriteObject(new SupportObjects.HTTPInterfaceConfig());
                    break;
                case "IPV4NetworkConfig":
                    this.WriteObject(new SupportObjects.IPV4NetworkConfig());
                    break;
                case "IPV6NetworkConfig":
                    this.WriteObject(new SupportObjects.IPV6NetworkConfig());
                    break;
                case "LDAPAccessControlConfig":
                    this.WriteObject(new SupportObjects.LDAPAccessControlConfig());
                    break;
                case "LoggingConfig":
                    this.WriteObject(new SupportObjects.LoggingConfig());
                    break;
                case "RadiusAccessControlConfig":
                    this.WriteObject(new SupportObjects.RadiusAccessControlConfig());
                    break;
                case "SMTPInterfaceConfig":
                    this.WriteObject(new SupportObjects.SMTPInterfaceConfig());
                    break;
                case "SNMPInterfaceConfig":
                    this.WriteObject(new SupportObjects.SNMPInterfaceConfig());
                    break;
                case "SSHInterfaceConfig":
                    this.WriteObject(new SupportObjects.SSHInterfaceConfig());
                    break;
                case "TimeNetworkConfig":
                    this.WriteObject(new SupportObjects.TimeNetworkConfig());
                    break;
            }
        }
    }
}
