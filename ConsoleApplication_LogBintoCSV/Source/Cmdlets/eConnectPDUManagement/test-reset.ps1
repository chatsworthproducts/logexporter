﻿
function Wait-PDUReboot($pduIp)
{
	while( (Test-Connection -ComputerName $pduIp -Quiet -Count 2) -eq $true )
	{
		Start-Sleep -Seconds 1
	}

	while( (Test-Connection -ComputerName $pduIp -Quiet -Count 2) -eq $false )
	{
		Start-Sleep -Seconds 1
	}

	Invoke-WebRequest -Uri http://$pduip/login.lp -Method Get
}


Reset-PDU admin admin 192.168.10.5
Wait-PDUReboot -pduip 192.168.10.5
Write-Host "PDU 192.168.10.5 has completed the reset process."