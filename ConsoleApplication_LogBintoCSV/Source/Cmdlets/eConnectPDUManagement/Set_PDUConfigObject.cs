﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eConnectPDUManagement.SupportObjects;

namespace eConnectPDUManagement
{
    [System.Management.Automation.Cmdlet(System.Management.Automation.VerbsCommon.Set, "PDUConfigObject")]
    public class Set_PDUConfigObject : System.Management.Automation.PSCmdlet
    {
        [System.Management.Automation.Parameter(Position = 0, Mandatory = true)]
        public string IPAddress;

        [System.Management.Automation.Parameter(Position = 1, Mandatory = true)]
        public string ConfigType;

        [System.Management.Automation.Parameter(Position = 3, Mandatory = true)]
        public object ConfigObject;

        protected override void ProcessRecord()
        {
            base.ProcessRecord();

            switch(this.ConfigType)
            {
                case "GeneralPDUConfig":
                    this.ProcessGeneralPDUConfig();
                    break;
                case "HTTPInterfaceConfig":
                    this.ProcessHTTPInterfaceConfig();
                    break;
                case "IPV4NetworkConfig":
                    this.ProcessIPV4NetworkConfig();
                    break;
                case "IPV6NetworkConfig":
                    this.ProcessIPV6NetworkConfig();
                    break;
                case "LDAPAccessControlConfig":
                    this.ProcessLDAPAccessControlConfig();
                    break;
                case "LoggingConfig":
                    this.ProcessLoggingConfig();
                    break;
                case "RadiusAccessControlConfig":
                    this.ProcessRadiusAccessControlConfig();
                    break;
                case "SMTPInterfaceConfig":
                    this.ProcessSMTPInterfaceConfig();
                    break;
                case "SNMPInterfaceConfig":
                    this.ProcessSNMPInterfaceConfig();
                    break;
                case "SSHInterfaceConfig":
                    this.ProcessSSHInterfaceConfig();
                    break;
                case "TimeNetworkConfig":
                    this.ProcessTimeNetworkConfig();
                    break;
            }
        }

        private void ProcessGeneralPDUConfig()
        {
            System.Management.Automation.PSObject configPSObj = (System.Management.Automation.PSObject)this.ConfigObject;
            GeneralPDUConfig config = (GeneralPDUConfig)configPSObj.BaseObject;
            if(config == null)
            {
                this.WriteError(new System.Management.Automation.ErrorRecord(
                        new ArgumentException("Configuration object is not of type GeneralPDUConfig.","ConfigObject"),
                        "error", System.Management.Automation.ErrorCategory.InvalidArgument, this));
                return;
            }
            List<KeyValuePair<string, object>> setData = new List<KeyValuePair<string, object>>();
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.6.1.0", ConvertToSNMPString(config.inputTop)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.6.2.0", ConvertToSNMPString(config.celsiusTemp)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.6.3.0", ConvertToSNMPString(config.shareRole)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.6.4.0", ConvertToSNMPString(config.missingPDUNotify)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.6.5.0", ConvertToSNMPString(config.roleChangeNotify)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.6.6.0", config.dispTimeout));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.6.7.0", config.loginTimeout));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.6.8.0", config.dispBrightness));

            this.SetSNMPData(setData);
        }

        private void ProcessHTTPInterfaceConfig()
        {
            HTTPInterfaceConfig config = (HTTPInterfaceConfig)this.ConfigObject;
            if (config == null)
            {
                this.WriteError(new System.Management.Automation.ErrorRecord(
                        new ArgumentException("Configuration object is not of type GeneralPDUConfig.", "ConfigObject"),
                        "error", System.Management.Automation.ErrorCategory.InvalidArgument, this));
                return;
            }
            List<KeyValuePair<string, object>> setData = new List<KeyValuePair<string, object>>();
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.2.1", ConvertToSNMPString(config.enableWeb)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.2.2", ConvertToSNMPString(config.httpEnable)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.2.3", ConvertToSNMPString(config.httpsEnable)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.2.4", config.httpPort));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.2.5", config.httpsPort));

            this.SetSNMPData(setData);
        }

        private void ProcessIPV4NetworkConfig()
        {
            IPV4NetworkConfig config = (IPV4NetworkConfig)this.ConfigObject;
            if (config == null)
            {
                this.WriteError(new System.Management.Automation.ErrorRecord(
                        new ArgumentException("Configuration object is not of type GeneralPDUConfig.", "ConfigObject"),
                        "error", System.Management.Automation.ErrorCategory.InvalidArgument, this));
                return;
            }
            List<KeyValuePair<string, object>> setData = new List<KeyValuePair<string, object>>();
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.1.1.1", ConvertToSNMPString(config.ipv4Enable)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.1.1.2", config.pduIP));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.1.1.3", ConvertToSNMPString(config.autoIP)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.1.1.4", ConvertToSNMPString(config.autoDNS)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.1.1.5", config.subnetMask));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.1.1.6", config.gateway));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.1.1.7", config.dns1));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.1.1.8", config.dns2));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.1.1.9", config.dns3));

            this.SetSNMPData(setData);
        }

        private void ProcessIPV6NetworkConfig()
        {
            IPV6NetworkConfig config = (IPV6NetworkConfig)this.ConfigObject;
            if (config == null)
            {
                this.WriteError(new System.Management.Automation.ErrorRecord(
                        new ArgumentException("Configuration object is not of type GeneralPDUConfig.", "ConfigObject"),
                        "error", System.Management.Automation.ErrorCategory.InvalidArgument, this));
                return;
            }
            List<KeyValuePair<string, object>> setData = new List<KeyValuePair<string, object>>();
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.1.2.1", ConvertToSNMPString(config.ipV6Enable)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.1.2.2", ConvertToSNMPString(config.linkLocalEnable)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.1.2.3", ConvertToSNMPString(config.globalAddrEnable)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.1.2.4", config.pduIPv6));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.1.2.5", ConvertToSNMPString(config.autoIPv6)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.1.2.6", ConvertToSNMPString(config.autoDNSv6)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.1.2.7", config.subnetMaskv6));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.1.2.8", config.gatewayv6));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.1.2.9", config.dns1v6));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.1.2.10", config.dns2v6));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.1.2.11", config.dns3v6));

            this.SetSNMPData(setData);
        }
        
        private void ProcessLDAPAccessControlConfig()
        {
            LDAPAccessControlConfig config = (LDAPAccessControlConfig)this.ConfigObject;
            if (config == null)
            {
                this.WriteError(new System.Management.Automation.ErrorRecord(
                        new ArgumentException("Configuration object is not of type GeneralPDUConfig.", "ConfigObject"),
                        "error", System.Management.Automation.ErrorCategory.InvalidArgument, this));
                return;
            }
            List<KeyValuePair<string, object>> setData = new List<KeyValuePair<string, object>>();
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.4.1", ConvertToSNMPString(config.enableADAuth)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.4.2", config.adServer));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.4.3", config.adDomain));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.4.4", config.adPort));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.4.5", config.adResvPort));

            this.SetSNMPData(setData);
        }

        private void ProcessLoggingConfig()
        {
            LoggingConfig config = (LoggingConfig)this.ConfigObject;
            if (config == null)
            {
                this.WriteError(new System.Management.Automation.ErrorRecord(
                        new ArgumentException("Configuration object is not of type GeneralPDUConfig.", "ConfigObject"),
                        "error", System.Management.Automation.ErrorCategory.InvalidArgument, this));
                return;
            }
            List<KeyValuePair<string, object>> setData = new List<KeyValuePair<string, object>>();
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.3.1", ConvertToSNMPString(config.logEnable)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.3.2", config.logInterval));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.3.3", config.logDifference));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.3.4", ConvertToSNMPString(config.netservdLog)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.3.5", ConvertToSNMPString(config.autonetLog)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.3.6", ConvertToSNMPString(config.stdmeterdLog)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.3.7", ConvertToSNMPString(config.groupmeterdLog)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.3.8", ConvertToSNMPString(config.alarmdLog)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.3.9", ConvertToSNMPString(config.userlogindLog)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.3.10", ConvertToSNMPString(config.usersetupdLog)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.3.11", ConvertToSNMPString(config.fwupdatedLog)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.3.12", ConvertToSNMPString(config.setupchangedLog)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.3.13", ConvertToSNMPString(config.recepchangedLog)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.3.14", ConvertToSNMPString(config.pduopchangedLog)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.3.15", config.dLogCycle));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.3.16", config.dLogFullWarnLevel));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.3.17", config.netLogServer));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.3.18", config.netdLogPort));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.3.19", config.netdLogUser));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.3.20", config.netdLogPasswd));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.3.21", config.netdLogDestdir));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.3.22", config.netdLogOptions));

            this.SetSNMPData(setData);
        }

        private void ProcessRadiusAccessControlConfig()
        {
            RadiusAccessControlConfig config = (RadiusAccessControlConfig)this.ConfigObject;
            if (config == null)
            {
                this.WriteError(new System.Management.Automation.ErrorRecord(
                        new ArgumentException("Configuration object is not of type GeneralPDUConfig.", "ConfigObject"),
                        "error", System.Management.Automation.ErrorCategory.InvalidArgument, this));
                return;
            }
            List<KeyValuePair<string, object>> setData = new List<KeyValuePair<string, object>>();
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.4.2.1", ConvertToSNMPString(config.enableRadiusAuth)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.4.2.2", config.radiusServer));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.4.2.3", config.radiusSHSecret));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.4.2.4", config.radiusNASServer));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.4.2.5", config.radiusPort));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.4.2.6", config.nasPort));

            this.SetSNMPData(setData);
        }

        private void ProcessSMTPInterfaceConfig()
        {
            SMTPInterfaceConfig config = (SMTPInterfaceConfig)this.ConfigObject;
            if (config == null)
            {
                this.WriteError(new System.Management.Automation.ErrorRecord(
                        new ArgumentException("Configuration object is not of type GeneralPDUConfig.", "ConfigObject"),
                        "error", System.Management.Automation.ErrorCategory.InvalidArgument, this));
                return;
            }
            List<KeyValuePair<string, object>> setData = new List<KeyValuePair<string, object>>();
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.4.1", ConvertToSNMPString(config.enableSMTP)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.4.2", config.smtpSendPort));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.4.3", config.smtpReceivePort));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.4.4", config.smtpServer));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.4.5", config.smtpUserName));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.4.6", config.smtpUserPasswd));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.4.7", config.smtpEmailAddr));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.4.8", config.smtpEmailDestination1));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.4.9", config.smtpEmailDestination2));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.4.10", config.smtpEmailDestination3));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.4.11", config.smtpServOptions));

            this.SetSNMPData(setData);
        }

        private void ProcessSNMPInterfaceConfig()
        {
            SNMPInterfaceConfig config = (SNMPInterfaceConfig)this.ConfigObject;
            if (config == null)
            {
                this.WriteError(new System.Management.Automation.ErrorRecord(
                        new ArgumentException("Configuration object is not of type GeneralPDUConfig.", "ConfigObject"),
                        "error", System.Management.Automation.ErrorCategory.InvalidArgument, this));
                return;
            }
            List<KeyValuePair<string, object>> setData = new List<KeyValuePair<string, object>>();
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.1.1", ConvertToSNMPString(config.snmpEnable)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.1.2.1", ConvertToSNMPString(config.snmpHostOnly)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.1.2.2", config.snmpHost1));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.1.2.3", config.snmpHost2));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.1.2.4", config.snmpHost3));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.1.2.5", config.snmpHostIPv61));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.1.2.6", config.snmpHostIPv62));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.1.2.7", config.snmpHostIPv63));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.1.3", config.snmpQueryPort));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.1.4", config.snmpTrapPort));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.1.5", config.snmpReadComm));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.1.6", config.snmpWriteComm));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.1.7", config.snmpUSMuserName));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.1.8", config.snmpSecurityLevel));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.1.9", config.snmpAuthAlgo));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.1.10", config.snmpPrivAlgo));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.1.11", config.snmpAuthPasswd));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.1.12", config.snmpPrivPasswd));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.1.13", config.snmpContextName));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.1.14", config.snmpEngineID));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.1.15", config.snmpLocAuthKey));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.1.16", config.snmpLocPrivKey));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.1.17.1", config.snmpTrapHost1));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.1.17.2", config.snmpTrapHost2));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.1.17.3", config.snmpTrapHost3));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.1.17.4", config.snmpTrapHostIPv61));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.1.17.5", config.snmpTrapHostIPv62));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.1.17.6", config.snmpTrapHostIPv63));

            this.SetSNMPData(setData);
        }

        private void ProcessSSHInterfaceConfig()
        {
            SSHInterfaceConfig config = (SSHInterfaceConfig)this.ConfigObject;
            if (config == null)
            {
                this.WriteError(new System.Management.Automation.ErrorRecord(
                        new ArgumentException("Configuration object is not of type GeneralPDUConfig.", "ConfigObject"),
                        "error", System.Management.Automation.ErrorCategory.InvalidArgument, this));
                return;
            }
            List<KeyValuePair<string, object>> setData = new List<KeyValuePair<string, object>>();
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.3.1", ConvertToSNMPString(config.telnetEnable)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.3.2", ConvertToSNMPString(config.sshEnable)));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.3.3", config.sshPort));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.2.3.4", config.telnetPort));

            this.SetSNMPData(setData);
        }

        private void ProcessTimeNetworkConfig()
        {
            TimeNetworkConfig config = (TimeNetworkConfig)this.ConfigObject;
            if (config == null)
            {
                this.WriteError(new System.Management.Automation.ErrorRecord(
                        new ArgumentException("Configuration object is not of type GeneralPDUConfig.", "ConfigObject"),
                        "error", System.Management.Automation.ErrorCategory.InvalidArgument, this));
                return;
            }
            List<KeyValuePair<string, object>> setData = new List<KeyValuePair<string, object>>();
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.1.3.1", config.rfcTimeServer));
            setData.Add(new KeyValuePair<string, object>("1.3.6.1.4.1.30932.1.10.1.1.1.3.2", config.ntpTimeServer));

            this.SetSNMPData(setData);
        }

        private void SetSNMPData(List<KeyValuePair<string,object>> setData)
        {
            Dictionary<string, string> properties = new Dictionary<string, string>();
            properties.Add("Public.DeviceManager.SNMPVersion", "SNMPv1v2c");
            properties.Add("Public.DeviceManager.SNMPReadCommunity", "public");
            properties.Add("Public.DeviceManager.SNMPWriteCommunity", "private");
            CPI.Common.SNMP.SNMPDotNetLib.SetSNMPData(this.IPAddress, setData, properties);
        }

        private Int32 ConvertToSNMPString(bool bData)
        {
            return bData == true ? 1 : 0;
        }
    }
}
