﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eConnectPDUManagement.SupportObjects;

namespace eConnectPDUManagement
{
    [System.Management.Automation.Cmdlet(System.Management.Automation.VerbsCommon.Get, "PDUConfigObject")]
    public class Get_PDUConfigObject : System.Management.Automation.PSCmdlet
    {
        [System.Management.Automation.Parameter(Position = 0, Mandatory = true)]
        public string IPAddress;

        [System.Management.Automation.Parameter(Position = 1, Mandatory = true)]
        public string ConfigType;
        
        protected override void ProcessRecord()
        {
            base.ProcessRecord();

            switch (this.ConfigType)
            {
                case "GeneralPDUConfig":
                    this.ProcessGeneralPDUConfig();
                    break;
                case "HTTPInterfaceConfig":
                    this.ProcessHTTPInterfaceConfig();
                    break;
                case "IPV4NetworkConfig":
                    this.ProcessIPV4NetworkConfig();
                    break;
                case "IPV6NetworkConfig":
                    this.ProcessIPV6NetworkConfig();
                    break;
                case "LDAPAccessControlConfig":
                    this.ProcessLDAPAccessControlConfig();
                    break;
                case "LoggingConfig":
                    this.ProcessLoggingConfig();
                    break;
                case "RadiusAccessControlConfig":
                    this.ProcessRadiusAccessControlConfig();
                    break;
                case "SMTPInterfaceConfig":
                    this.ProcessSMTPInterfaceConfig();
                    break;
                case "SNMPInterfaceConfig":
                    this.ProcessSNMPInterfaceConfig();
                    break;
                case "SSHInterfaceConfig":
                    this.ProcessSSHInterfaceConfig();
                    break;
                case "TimeNetworkConfig":
                    this.ProcessTimeNetworkConfig();
                    break;
            }
        }

        private void ProcessGeneralPDUConfig()
        {
            GeneralPDUConfig config = new GeneralPDUConfig();
            
            List<string> getData = new List<string>();
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.6.1");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.6.2");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.6.3");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.6.4");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.6.5");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.6.6");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.6.7");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.6.8");

            IDictionary<string,string> retrievedData = this.GetSNMPData(getData);

            this.WriteVerbose(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.6.1"]);
            this.WriteVerbose(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.6.2"]);
            this.WriteVerbose(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.6.3"]);
            this.WriteVerbose(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.6.4"]);
            this.WriteVerbose(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.6.5"]);
            this.WriteVerbose(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.6.6"]);
            this.WriteVerbose(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.6.7"]);
            this.WriteVerbose(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.6.8"]);

            config.inputTop = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.6.1"]);
            config.celsiusTemp = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.6.2"]);
            config.shareRole = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.6.3"]);
            config.missingPDUNotify = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.6.4"]);
            config.roleChangeNotify = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.6.5"]);
            config.dispTimeout = Convert.ToInt32(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.6.6"]);
            config.loginTimeout = Convert.ToInt32(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.6.7"]);
            config.dispBrightness = Convert.ToInt32(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.6.8"]);

            this.WriteObject(config);
        }

        private void ProcessHTTPInterfaceConfig()
        {
            HTTPInterfaceConfig config = new HTTPInterfaceConfig();
            
            List<string> getData = new List<string>();
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.2.1");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.2.2");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.2.3");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.2.4");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.2.5");

            IDictionary<string, string> retrievedData = this.GetSNMPData(getData);

            config.enableWeb = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.2.1"]); 
            config.httpEnable = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.2.2"]); 
            config.httpsEnable = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.2.3"]); 
            config.httpPort = Convert.ToInt32(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.2.4"]);
            config.httpsPort = Convert.ToInt32(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.2.5"]);

            this.WriteObject(config);
        }

        private void ProcessIPV4NetworkConfig()
        {
            IPV4NetworkConfig config = new IPV4NetworkConfig();
            
            List<string> getData = new List<string>();
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.1.1");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.1.2");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.1.3");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.1.4");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.1.5");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.1.6");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.1.7");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.1.8");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.1.9");

            IDictionary<string, string> retrievedData = this.GetSNMPData(getData);

            config.ipv4Enable = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.1.1"]); 
            config.pduIP = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.1.2"]; 
            config.autoIP = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.1.3"]); 
            config.autoDNS = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.1.4"]); 
            config.subnetMask = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.1.5"];
            config.gateway = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.1.6"];
            config.dns1 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.1.7"];
            config.dns2 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.1.8"];
            config.dns3 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.1.9"];

            this.WriteObject(config);
        }

        private void ProcessIPV6NetworkConfig()
        {
            IPV6NetworkConfig config = new IPV6NetworkConfig();
            
            List<string> getData = new List<string>();
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.2.1");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.2.2");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.2.3");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.2.4");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.2.5");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.2.6");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.2.7");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.2.8");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.2.9");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.2.10");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.2.11");

            IDictionary<string, string> retrievedData = this.GetSNMPData(getData);

            config.ipV6Enable = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.2.1"]); 
            config.linkLocalEnable = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.2.2"]); 
            config.globalAddrEnable = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.2.3"]); 
            config.pduIPv6 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.2.4"];
            config.autoIPv6 = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.2.5"]); 
            config.autoDNSv6 = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.2.6"]); 
            config.subnetMaskv6 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.2.7"];
            config.gatewayv6 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.2.8"];
            config.dns1v6 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.2.9"];
            config.dns2v6 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.2.10"];
            config.dns3v6 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.2.11"];

            this.WriteObject(config);
        }

        private void ProcessLDAPAccessControlConfig()
        {
            LDAPAccessControlConfig config = new LDAPAccessControlConfig();

            List<string> getData = new List<string>();
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.4.1");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.4.2");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.4.3");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.4.4");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.4.5");

            IDictionary<string, string> retrievedData = this.GetSNMPData(getData);

            config.enableADAuth = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.4.1"]);
            config.adServer = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.4.2"];
            config.adDomain = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.4.3"];
            config.adPort = Convert.ToInt32(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.4.4"]);
            config.adResvPort = Convert.ToInt32(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.4.5"]);

            this.WriteObject(config);
        }

        private void ProcessLoggingConfig()
        {
            LoggingConfig config = new LoggingConfig();

            List<string> getData = new List<string>();
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.1");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.2");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.3");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.4");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.5");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.6");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.7");;
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.8");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.9");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.10");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.11");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.12");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.13");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.14");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.15");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.16");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.17");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.18");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.19");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.20");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.21");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.22");

            IDictionary<string, string> retrievedData = this.GetSNMPData(getData);

            config.logEnable = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.1"]);
            config.logInterval = Convert.ToInt32(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.2"]); 
            config.logDifference = Convert.ToInt32(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.3"]); 
            config.netservdLog = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.4"]);
            config.autonetLog = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.5"]);
            config.stdmeterdLog = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.6"]); 
            config.groupmeterdLog = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.7"]); 
            config.alarmdLog = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.8"]);
            config.userlogindLog = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.9"]);
            config.usersetupdLog = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.10"]);
            config.fwupdatedLog = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.11"]);
            config.setupchangedLog = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.12"]);
            config.recepchangedLog = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.13"]);
            config.pduopchangedLog = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.14"]);
            config.dLogCycle = Convert.ToInt32(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.15"]);
            config.dLogFullWarnLevel = Convert.ToInt32(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.16"]);
            config.netLogServer = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.17"];
            config.netdLogPort = Convert.ToInt32(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.18"]);
            config.netdLogUser = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.19"];
            config.netdLogPasswd = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.20"];
            config.netdLogDestdir = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.21"];
            config.netdLogOptions = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.22"];

            this.WriteObject(config);
        }

        private void ProcessRadiusAccessControlConfig()
        {
            RadiusAccessControlConfig config = new RadiusAccessControlConfig();

            List<string> getData = new List<string>();
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.4.2.1");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.4.2.2");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.4.2.3");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.4.2.4");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.4.2.5");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.4.2.6");

            IDictionary<string, string> retrievedData = this.GetSNMPData(getData);

            config.enableRadiusAuth = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.4.2.1"]); 
            config.radiusServer = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.4.2.2"]; 
            config.radiusSHSecret = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.4.2.3"]; 
            config.radiusNASServer = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.4.2.4"]; 
            config.radiusPort = Convert.ToInt32(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.4.2.5"]); 
            config.nasPort = Convert.ToInt32(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.4.2.6"]);

            this.WriteObject(config);
        }

        private void ProcessSMTPInterfaceConfig()
        {
            SMTPInterfaceConfig config = new SMTPInterfaceConfig();

            List<string> getData = new List<string>();
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.4.1");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.4.2");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.4.3");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.4.4");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.4.5");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.4.6");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.4.7");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.4.8");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.4.9");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.4.10");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.4.11");

            IDictionary<string, string> retrievedData = this.GetSNMPData(getData);

            config.enableSMTP = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.4.1"]); 
            config.smtpSendPort = Convert.ToInt32(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.4.2"]); 
            config.smtpReceivePort = Convert.ToInt32(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.4.3"]); 
            config.smtpServer = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.4.4"]; 
            config.smtpUserName = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.4.5"]; 
            config.smtpUserPasswd = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.4.6"]; 
            config.smtpEmailAddr = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.4.7"]; 
            config.smtpEmailDestination1 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.4.8"]; 
            config.smtpEmailDestination2 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.4.9"]; 
            config.smtpEmailDestination3 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.4.10"];
            config.smtpServOptions = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.4.11"];

            this.WriteObject(config);
        }

        private void ProcessSNMPInterfaceConfig()
        {
            SNMPInterfaceConfig config = new SNMPInterfaceConfig();

            List<string> getData = new List<string>();
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.1");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.2.1");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.2.2");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.2.3");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.2.4");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.2.5");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.2.6");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.2.7");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.3");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.4");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.5");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.6");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.7");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.8");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.9");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.10");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.11");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.12");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.13");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.14");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.15");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.16");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.17.1");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.17.2");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.17.3");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.17.4");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.17.5");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.17.6");

            IDictionary<string, string> retrievedData = this.GetSNMPData(getData);

            config.snmpEnable = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.1"]);
            config.snmpHostOnly = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.2.1"]);
            config.snmpHost1 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.2.2"];
            config.snmpHost2 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.2.3"];
            config.snmpHost3 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.2.4"];
            config.snmpHostIPv61 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.2.5"];
            config.snmpHostIPv62 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.2.6"];
            config.snmpHostIPv63 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.2.7"];
            config.snmpQueryPort = Convert.ToInt32(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.3"]);
            config.snmpTrapPort = Convert.ToInt32(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.4"]);
            config.snmpReadComm = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.5"];
            config.snmpWriteComm = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.6"]; 
            config.snmpUSMuserName = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.7"];
            config.snmpSecurityLevel = Convert.ToInt32(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.8"]);
            config.snmpAuthAlgo = Convert.ToInt32(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.9"]);
            config.snmpPrivAlgo = Convert.ToInt32(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.10"]);
            config.snmpAuthPasswd = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.11"];
            config.snmpPrivPasswd = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.12"];
            config.snmpContextName = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.13"]; 
            config.snmpEngineID = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.14"];
            config.snmpLocAuthKey = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.15"]; 
            config.snmpLocPrivKey = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.16"];
            config.snmpTrapHost1 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.17.1"]; 
            config.snmpTrapHost2 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.17.2"];
            config.snmpTrapHost3 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.17.3"];
            config.snmpTrapHostIPv61 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.17.4"];
            config.snmpTrapHostIPv62 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.17.5"];
            config.snmpTrapHostIPv63 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.17.6"];

            this.WriteObject(config);
        }

        private void ProcessSSHInterfaceConfig()
        {
            SSHInterfaceConfig config = new SSHInterfaceConfig();

            List<string> getData = new List<string>();
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.3.1");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.3.2");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.3.3");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.3.4");

            IDictionary<string, string> retrievedData = this.GetSNMPData(getData);

            config.telnetEnable = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.3.1"]);
            config.sshEnable = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.3.2"]);
            config.sshPort = Convert.ToInt32(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.3.3"]);
            config.telnetPort = Convert.ToInt32(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.3.4"]);

            this.WriteObject(config);
        }

        private void ProcessTimeNetworkConfig()
        {
            TimeNetworkConfig config = new TimeNetworkConfig();

            List<string> getData = new List<string>();
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.3.1");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.3.2");

            IDictionary<string, string> retrievedData = this.GetSNMPData(getData);

            config.rfcTimeServer = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.3.1"];
            config.ntpTimeServer = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.3.2"];

            this.WriteObject(config);
        }

        private bool ConvertFromSNMPBool(string value)
        {
            return value == "1" ? true : false;
        }

        private IDictionary<string,string> GetSNMPData(List<string> getData)
        {
            Dictionary<string, string> properties = new Dictionary<string, string>();
            properties.Add("Public.DeviceManager.SNMPVersion", "SNMPv1v2c");
            properties.Add("Public.DeviceManager.SNMPCommunity", "public");
            return CPI.Common.SNMP.SNMPDotNetLib.GetSNMPData(this.IPAddress, getData, properties);
        }                
    }
}
