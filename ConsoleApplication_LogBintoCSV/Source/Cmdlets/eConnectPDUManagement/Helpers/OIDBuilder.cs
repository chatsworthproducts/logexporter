﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eConnectPDUManagement.Helpers
{
    internal class OIDBuilder
    {
        private int _SelectorID;

        public OIDBuilder(string GatewaySelector)
        {
            _SelectorID = Int32.Parse(GatewaySelector);
        }

        public string BuildOID(string baseOID)
        {
            return string.Format("{0}.{1}", baseOID, _SelectorID);
        }

        public string BuildOID(string baseOIDFormat, params object[] values )
        {
            return BuildOID(string.Format(baseOIDFormat, values));
        }

        public static bool IsOIDZeroSuffix(string OIDValue)
        {
            return OIDValue.EndsWith(".0");
        }

        public static string StripZeroSuffix(string OIDValue)
        {
            if( IsOIDZeroSuffix(OIDValue) )
            {
                return OIDValue.TrimEnd(new char[] { '.', '0' });
            }

            return OIDValue;
        }
    }
}
