﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eConnectPDUManagement.Helpers
{
    internal class SNMPDictionary
    {
        private IDictionary<string, string> _SnmpData;

        public SNMPDictionary(IDictionary<string,string> data)
        {
            this._SnmpData = data;
        }

        public bool HasOID(string OIDString)
        {
            return _SnmpData.Keys.Contains(OIDString);
        }

        private string DetermineAccessOID(string OIDString)
        {
            if (HasOID(OIDString) == true)
            {
                return OIDString;
            }

            string strippedOID = OIDBuilder.StripZeroSuffix(OIDString);
            if (HasOID(strippedOID) == true)
            {
                return strippedOID;
            }

            return OIDString;
        }

        public string GetOIDValueAsString(string OIDString)
        {
            string oid = DetermineAccessOID(OIDString);
            if (HasOID(oid) == true)
            {
                return _SnmpData[oid];
            }

            return string.Empty;
        }

        public int GetOIDValueAsInt(string OIDString)
        {
            string oid = DetermineAccessOID(OIDString);
            if (HasOID(oid) == true)
            {
                string valueData = _SnmpData[oid];
                if( valueData.Length != 0 )
                {
                    return int.Parse(valueData);
                }
                return int.MinValue;
            }

            return int.MinValue;
        }

        public bool GetOIDValueAsBool(string OIDString)
        {
            string oid = DetermineAccessOID(OIDString);
            if (HasOID(oid) == true)
            {
                string valueData = _SnmpData[oid];
                if (valueData.Length != 0)
                {
                    return valueData.Length == 0 ? false : (valueData == "1" ? true : false);                    
                }
                return false;
            }

            return false;
        }

        public SupportObjects.OutletState GetOIDValueAsOutletState(string OIDString)
        {
            string oid = DetermineAccessOID(OIDString);
            if (HasOID(oid) == true)
            {
                string valueData = _SnmpData[oid];
                if( valueData.Length != 0 )
                {
                    return valueData == "0" ? SupportObjects.OutletState.Off : SupportObjects.OutletState.On;                    
                }
                return SupportObjects.OutletState.NotAvailable;
            }

            return SupportObjects.OutletState.NotAvailable;
        }
    }
}
