﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPI.eConnect.Communications;

namespace eConnectPDUManagement
{
    [System.Management.Automation.Cmdlet(System.Management.Automation.VerbsCommon.Reset, "PDU")]
    public class Reset_PDU : System.Management.Automation.PSCmdlet
    {
        [System.Management.Automation.Parameter(Position = 0, Mandatory = true)]
        public string LoginName;

        [System.Management.Automation.Parameter(Position = 1, Mandatory = true)]
        public string LoginPassword;

        [System.Management.Automation.Parameter(Position = 2, Mandatory = true)]
        public string IPAddress;

        protected override void ProcessRecord()
        {
            base.ProcessRecord();

            using (var pdu = new CPI.eConnect.Communications.SSHMCMConsole())
            {
                // establish a connection to the pdu
                pdu.Connect(this.IPAddress, 22, this.LoginName, this.LoginPassword);
                
                IPDUCommand cmd = pdu.SetAdvandedMode;
                cmd.Execute();
                DisposeCmd(cmd);

                cmd = pdu.RebootPDU;
                cmd.Parameters["pdu index"] = 0;
                cmd.Execute();
                DisposeCmd(cmd);
            }
        }

        private void DisposeCmd(IPDUCommand cmd)
        {
            IDisposable dispCmd = (IDisposable)cmd;
            if (dispCmd != null)
            {
                dispCmd.Dispose();
            }
        }
    }
}
