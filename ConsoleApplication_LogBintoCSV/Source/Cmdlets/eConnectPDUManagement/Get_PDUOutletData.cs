﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPI.eConnect.API;

namespace eConnectPDUManagement
{
    [System.Management.Automation.Cmdlet(System.Management.Automation.VerbsCommon.Get, "PDUOutletData")]
    public class Get_PDUOutletData : System.Management.Automation.PSCmdlet
    {
        [System.Management.Automation.Parameter(Position = 0, Mandatory = true)]
        public string GatewayIPAddress;

        [System.Management.Automation.Parameter(Position = 1, Mandatory = true)]
        public string GatewaySelector;

        protected override void ProcessRecord()
        {
            base.ProcessRecord();

            List<PDUOutlet> branches = new List<PDUOutlet>();

            Dictionary<string, string> properties = new Dictionary<string, string>();
            properties.Add("Public.DeviceManager.SNMPVersion", "SNMPv1v2c");
            properties.Add("Public.DeviceManager.SNMPReadCommunity", "public");
            properties.Add("Public.DeviceManager.SNMPWriteCommunity", "private");

            List<string> oidList = new List<string>();
            oidList.Add(string.Format("1.3.6.1.4.1.30932.1.1.1.2.{0}", this.GatewaySelector)); // model

            IDictionary<string, string> snmpData = CPI.Common.SNMP.SNMPDotNetLib.GetSNMPData(GatewayIPAddress, oidList, properties);
            eConnectDeviceInfo devInfo = new eConnectDeviceInfo(snmpData[string.Format("1.3.6.1.4.1.30932.1.1.1.2.{0}", this.GatewaySelector)]);
            
            for(int i = 1; i <= devInfo.TotalOutlets; i++ )
            {
                int branchId = GetBranchID(devInfo, i);

                oidList.Clear();
                oidList.Add(string.Format("1.3.6.1.4.1.30932.1.1.3.2.{0}.{1}", branchId, this.GatewaySelector)); // Voltage
                oidList.Add(string.Format("1.3.6.1.4.1.30932.1.1.3.5.{0}.{1}", i, this.GatewaySelector)); // Outlet Current
                oidList.Add(string.Format("1.3.6.1.4.1.30932.1.1.3.12.{0}.{1}", i, this.GatewaySelector)); // Outlet Energy                
                oidList.Add(string.Format("1.3.6.1.4.1.30932.1.1.3.7.{0}.{1}", i, this.GatewaySelector)); // Outlet State
                oidList.Add(string.Format("1.3.6.1.4.1.30932.1.1.3.7.{0}.{1}", i, this.GatewaySelector)); // Outlet Name
                oidList.Add(string.Format("1.3.6.1.4.1.30932.1.1.3.7.{0}.{1}", i, this.GatewaySelector)); // Outlet Description
                oidList.Add(string.Format("1.3.6.1.4.1.30932.1.1.4.1.{0}.{1}", i, this.GatewaySelector)); // Outlet Control
                

                snmpData = CPI.Common.SNMP.SNMPDotNetLib.GetSNMPData(GatewayIPAddress, oidList, properties);
                                
                if(snmpData[string.Format("1.3.6.1.4.1.30932.1.1.3.1.{0}.{1}", i, this.GatewaySelector)].Length != 0)
                {
                    PDUOutlet outlet = new PDUOutlet();
                    outlet.OutletID = i;
                    outlet.OutletName = snmpData[string.Format("1.3.6.1.4.1.30932.1.1.3.7.{0}.{1}", i, this.GatewaySelector)];
                    outlet.OutletDescription = snmpData[string.Format("1.3.6.1.4.1.30932.1.1.3.7.{0}.{1}", i, this.GatewaySelector)];
                    outlet.ControlState = (PDUOutletControlState)Int32.Parse(snmpData[string.Format("1.3.6.1.4.1.30932.1.1.4.1.{0}.{1}", i, this.GatewaySelector)]);
                    outlet.Current = Int32.Parse(snmpData[string.Format("1.3.6.1.4.1.30932.1.1.3.5.{0}.{1}", i, this.GatewaySelector)]) / 100;
                    outlet.Energy = Int64.Parse(snmpData[string.Format("1.3.6.1.4.1.30932.1.1.3.12.{0}.{1}", i, this.GatewaySelector)]) / 360;
                    outlet.Voltage = Int32.Parse(snmpData[string.Format("1.3.6.1.4.1.30932.1.1.3.2.{0}.{1}", branchId, this.GatewaySelector)]) / 100;
                    outlet.Power = outlet.Voltage * outlet.Current;

                    branches.Add(outlet);
                }
                else
                {
                    break;
                }
            }

            this.WriteObject(branches);
        }

        private int GetBranchID(eConnectDeviceInfo devInfo, int outletId)
        {
            for(int i = 1; i <= devInfo.NumberOfBreakers; i++)
            {
                if( devInfo.BranchOutletMap[i].Contains(outletId) == true)
                {
                    return i;
                }
            }

            return 1;
        }
    }
}
