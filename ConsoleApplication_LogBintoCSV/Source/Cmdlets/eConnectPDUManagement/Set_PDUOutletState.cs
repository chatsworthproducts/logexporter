﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eConnectPDUManagement
{
    [System.Management.Automation.Cmdlet(System.Management.Automation.VerbsCommon.Set, "PDUOutletState")]
    public class Set_PDUOutletState : System.Management.Automation.PSCmdlet
    {
        [System.Management.Automation.Parameter(Position = 0, Mandatory = true)]
        public string GatewayIPAddress;

        [System.Management.Automation.Parameter(Position = 1, Mandatory = true)]
        public string GatewaySelector;

        [System.Management.Automation.Parameter(Position = 2, Mandatory = true)]
        public int OutletNumber;

        [System.Management.Automation.Parameter(Position = 3, Mandatory = true)]
        public SupportObjects.OutletState State;

        protected override void ProcessRecord()
        {
            base.ProcessRecord();

            SupportObjects.OutletState initialState = GetOutletState();

            if (initialState == SupportObjects.OutletState.NotAvailable)
            {
                this.WriteObject(this.State);
                return;
            }

            if( initialState == State )
            {
                this.WriteObject(this.State);
                return;
            }

            Dictionary<string, string> properties = new Dictionary<string, string>();
            properties.Add("Public.DeviceManager.SNMPVersion", "SNMPv1v2c");
            properties.Add("Public.DeviceManager.SNMPReadCommunity", "public");
            properties.Add("Public.DeviceManager.SNMPWriteCommunity", "private");
            
            List<KeyValuePair<string, object>> setData = new List<KeyValuePair<string, object>>();
            Helpers.OIDBuilder oidBuild = new Helpers.OIDBuilder(this.GatewaySelector);
            string recepControlOID = oidBuild.BuildOID("1.3.6.1.4.1.30932.1.1.4.1.{0}", this.OutletNumber);

            setData.Add(new KeyValuePair<string, object>(recepControlOID, ConvertFromState(this.State)));
            CPI.Common.SNMP.SNMPDotNetLib.SetSNMPData(this.GatewayIPAddress, setData, properties);

            int timeout = 0;
            while( (GetOutletState() != this.State) &&
                   (timeout < 60))
            {
                System.Threading.Thread.Sleep(1000);
                timeout++;
            }

            if(timeout >= 60)
            {
                this.WriteWarning("Outlet did not respond within the time limit of 60 seconds.");
                return;
            }

            this.WriteObject(this.State);
        }

        private SupportObjects.OutletState GetOutletState()
        {
            Dictionary<string, string> properties = new Dictionary<string, string>();
            properties.Add("Public.DeviceManager.SNMPVersion", "SNMPv1v2c");
            properties.Add("Public.DeviceManager.SNMPReadCommunity", "public");
            properties.Add("Public.DeviceManager.SNMPWriteCommunity", "private");

            List<string> oidList = new List<string>();
            Helpers.OIDBuilder oidBuild = new Helpers.OIDBuilder(this.GatewaySelector);
            string recepControlOID = oidBuild.BuildOID("1.3.6.1.4.1.30932.1.1.4.1.{0}", this.OutletNumber);

            oidList.Add(recepControlOID); // recept. Control

            IDictionary<string, string> snmpData = CPI.Common.SNMP.SNMPDotNetLib.GetSNMPData(GatewayIPAddress, oidList, properties);

            Helpers.SNMPDictionary dict = new Helpers.SNMPDictionary(snmpData);
            SupportObjects.OutletState outletValue = dict.GetOIDValueAsOutletState(recepControlOID);
            return outletValue;
        }

        private int ConvertFromState(SupportObjects.OutletState outletValue)
        {
            return outletValue == SupportObjects.OutletState.Off ? 0 : 1;
        }

        private SupportObjects.OutletState ConvertToState(int outletValue)
        {
            return outletValue == 0 ? SupportObjects.OutletState.Off : SupportObjects.OutletState.On;
        }

        private void SetSNMPData(List<KeyValuePair<string, object>> setData)
        {
            Dictionary<string, string> properties = new Dictionary<string, string>();
            properties.Add("Public.DeviceManager.SNMPVersion", "SNMPv1v2c");
            properties.Add("Public.DeviceManager.SNMPReadCommunity", "public");
            properties.Add("Public.DeviceManager.SNMPWriteCommunity", "private");
            CPI.Common.SNMP.SNMPDotNetLib.SetSNMPData(this.GatewayIPAddress, setData, properties);
        }
    }
}
