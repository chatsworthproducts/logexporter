﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPI.eConnect.API;

namespace eConnectPDUManagement
{
    [System.Management.Automation.Cmdlet(System.Management.Automation.VerbsCommon.Get, "PDUBranchData")]
    public class Get_PDUBranchData : System.Management.Automation.PSCmdlet
    {
        [System.Management.Automation.Parameter(Position = 0, Mandatory = true)]
        public string GatewayIPAddress;

        [System.Management.Automation.Parameter(Position = 1, Mandatory = true)]
        public string GatewaySelector;

        protected override void ProcessRecord()
        {
            base.ProcessRecord();

            List< PDUBranch> branches = new List<PDUBranch>();

            Dictionary<string, string> properties = new Dictionary<string, string>();
            properties.Add("Public.DeviceManager.SNMPVersion", "SNMPv1v2c");
            properties.Add("Public.DeviceManager.SNMPReadCommunity", "public");
            properties.Add("Public.DeviceManager.SNMPWriteCommunity", "private");

            for(int i = 1; i <= 6; i++ )
            {
                List<string> oidList = new List<string>();

                oidList.Add(string.Format("1.3.6.1.4.1.30932.1.1.3.1.{0}.{1}", i, this.GatewaySelector)); // current
                oidList.Add(string.Format("1.3.6.1.4.1.30932.1.1.2.19.{0}.{1}", i, this.GatewaySelector)); // Maxcurrent
                oidList.Add(string.Format("1.3.6.1.4.1.30932.1.1.3.2.{0}.{1}", i, this.GatewaySelector)); // Voltage
                oidList.Add(string.Format("1.3.6.1.4.1.30932.1.1.3.3.{0}.{1}", i, this.GatewaySelector)); // Power
                oidList.Add(string.Format("1.3.6.1.4.1.30932.1.1.3.4.{0}.{1}", i, this.GatewaySelector)); // PowerFactor
                oidList.Add(string.Format("1.3.6.1.4.1.30932.1.1.3.11.{0}.{1}", i, this.GatewaySelector)); // Energy

                IDictionary<string, string> snmpData = CPI.Common.SNMP.SNMPDotNetLib.GetSNMPData(GatewayIPAddress, oidList, properties);
                                
                if(snmpData[string.Format("1.3.6.1.4.1.30932.1.1.3.1.{0}.{1}", i, this.GatewaySelector)].Length != 0)
                {
                    PDUBranch branch = new PDUBranch();
                    branch.BranchId = i;
                    branch.Current = Int32.Parse(    snmpData[string.Format("1.3.6.1.4.1.30932.1.1.3.1.{0}.{1}", i, this.GatewaySelector)]) / 100;
                    branch.MaxCurrent = Int32.Parse( snmpData[string.Format("1.3.6.1.4.1.30932.1.1.2.19.{0}.{1}", i, this.GatewaySelector)]) / 100;
                    branch.Voltage = Int32.Parse(    snmpData[string.Format("1.3.6.1.4.1.30932.1.1.3.2.{0}.{1}", i, this.GatewaySelector)]) / 10;
                    branch.Power = Int32.Parse(      snmpData[string.Format("1.3.6.1.4.1.30932.1.1.3.3.{0}.{1}", i, this.GatewaySelector)]) / 10;
                    branch.PowerFactor = Int32.Parse(snmpData[string.Format("1.3.6.1.4.1.30932.1.1.3.4.{0}.{1}", i, this.GatewaySelector)]) / 100;
                    branch.Energy = Int32.Parse(     snmpData[string.Format("1.3.6.1.4.1.30932.1.1.3.11.{0}.{1}", i, this.GatewaySelector)]) / 360;
                    branches.Add(branch);
                }
                else
                {
                    break;
                }
            }

            this.WriteObject(branches);
        }
    }
}
