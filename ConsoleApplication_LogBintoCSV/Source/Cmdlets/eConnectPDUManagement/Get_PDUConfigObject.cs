﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eConnectPDUManagement.SupportObjects;

namespace eConnectPDUManagement
{
    [System.Management.Automation.Cmdlet(System.Management.Automation.VerbsCommon.Get, "PDUConfigObject")]
    public class Get_PDUConfigObject : System.Management.Automation.PSCmdlet
    {
        [System.Management.Automation.Parameter(Position = 0, Mandatory = true)]
        public string IPAddress;

        [System.Management.Automation.Parameter(Position = 1, Mandatory = true)]
        public string ConfigType;
        
        protected override void ProcessRecord()
        {
            base.ProcessRecord();

            switch (this.ConfigType)
            {
                case "GeneralPDUConfig":
                    this.ProcessGeneralPDUConfig();
                    break;
                case "HTTPInterfaceConfig":
                    this.ProcessHTTPInterfaceConfig();
                    break;
                case "IPV4NetworkConfig":
                    this.ProcessIPV4NetworkConfig();
                    break;
                case "IPV6NetworkConfig":
                    this.ProcessIPV6NetworkConfig();
                    break;
                case "LDAPAccessControlConfig":
                    this.ProcessLDAPAccessControlConfig();
                    break;
                case "LoggingConfig":
                    this.ProcessLoggingConfig();
                    break;
                case "RadiusAccessControlConfig":
                    this.ProcessRadiusAccessControlConfig();
                    break;
                case "SMTPInterfaceConfig":
                    this.ProcessSMTPInterfaceConfig();
                    break;
                case "SNMPInterfaceConfig":
                    this.ProcessSNMPInterfaceConfig();
                    break;
                case "SSHInterfaceConfig":
                    this.ProcessSSHInterfaceConfig();
                    break;
                case "TimeNetworkConfig":
                    this.ProcessTimeNetworkConfig();
                    break;
            }
        }

        private void ProcessGeneralPDUConfig()
        {
            GeneralPDUConfig config = new GeneralPDUConfig();
            
            List<string> getData = new List<string>();
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.6.1.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.6.2.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.6.3.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.6.4.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.6.5.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.6.6.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.6.7.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.6.8.0");

            IDictionary<string,string> retrievedData = this.GetSNMPData(getData);

            config.inputTop = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.6.1.0"]);
            config.celsiusTemp = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.6.2.0"]);
            config.shareRole = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.6.3.0"]);
            config.missingPDUNotify = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.6.4.0"]);
            config.roleChangeNotify = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.6.5.0"]);
            config.dispTimeout = ConvertFromSNMPInt(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.6.6.0"]);
            config.loginTimeout = ConvertFromSNMPInt(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.6.7.0"]);
            config.dispBrightness = ConvertFromSNMPInt(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.6.8.0"]);

            this.WriteObject(config);
        }

        private void ProcessHTTPInterfaceConfig()
        {
            HTTPInterfaceConfig config = new HTTPInterfaceConfig();
            
            List<string> getData = new List<string>();
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.2.1.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.2.2.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.2.3.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.2.4.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.2.5.0");

            IDictionary<string, string> retrievedData = this.GetSNMPData(getData);

            config.enableWeb = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.2.1.0"]);
            config.httpEnable = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.2.2.0"]);
            config.httpsEnable = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.2.3.0"]);
            config.httpPort = ConvertFromSNMPInt(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.2.4.0"]);
            config.httpsPort = ConvertFromSNMPInt(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.2.5.0"]);

            this.WriteObject(config);
        }

        private void ProcessIPV4NetworkConfig()
        {
            IPV4NetworkConfig config = new IPV4NetworkConfig();
            
            List<string> getData = new List<string>();
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.1.1.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.1.2.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.1.3.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.1.4.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.1.5.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.1.6.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.1.7.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.1.8.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.1.9.0");

            IDictionary<string, string> retrievedData = this.GetSNMPData(getData);

            config.ipv4Enable = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.1.1.0"]);
            config.pduIP = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.1.2.0"];
            config.autoIP = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.1.3.0"]);
            config.autoDNS = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.1.4.0"]);
            config.subnetMask = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.1.5.0"];
            config.gateway = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.1.6.0"];
            config.dns1 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.1.7.0"];
            config.dns2 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.1.8.0"];
            config.dns3 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.1.9.0"];

            this.WriteObject(config);
        }

        private void ProcessIPV6NetworkConfig()
        {
            IPV6NetworkConfig config = new IPV6NetworkConfig();
            
            List<string> getData = new List<string>();
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.2.1.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.2.2.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.2.3.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.2.4.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.2.5.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.2.6.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.2.7.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.2.8.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.2.9.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.2.10.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.2.11.0");

            IDictionary<string, string> retrievedData = this.GetSNMPData(getData);

            config.ipV6Enable = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.2.1.0"]); 
            config.linkLocalEnable = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.2.2.0"]); 
            config.globalAddrEnable = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.2.3.0"]); 
            config.pduIPv6 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.2.4.0"];
            config.autoIPv6 = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.2.5.0"]); 
            config.autoDNSv6 = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.2.6.0"]); 
            config.subnetMaskv6 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.2.7.0"];
            config.gatewayv6 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.2.8.0"];
            config.dns1v6 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.2.9.0"];
            config.dns2v6 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.2.10.0"];
            config.dns3v6 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.2.11.0"];

            this.WriteObject(config);
        }

        private void ProcessLDAPAccessControlConfig()
        {
            LDAPAccessControlConfig config = new LDAPAccessControlConfig();

            List<string> getData = new List<string>();
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.4.1.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.4.2.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.4.3.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.4.4.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.4.5.0");

            IDictionary<string, string> retrievedData = this.GetSNMPData(getData);

            config.enableADAuth = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.4.1.0"]);
            config.adServer = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.4.2.0"];
            config.adDomain = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.4.3.0"];
            config.adPort = ConvertFromSNMPInt(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.4.4.0"]);
            config.adResvPort = ConvertFromSNMPInt(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.4.5.0"]);

            this.WriteObject(config);
        }

        private void ProcessLoggingConfig()
        {
            LoggingConfig config = new LoggingConfig();

            List<string> getData = new List<string>();
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.1.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.2.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.3.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.4.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.5.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.6.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.7.0");;
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.8.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.9.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.10.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.11.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.12.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.13.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.14.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.15.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.16.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.17.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.18.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.19.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.20.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.21.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.3.22.0");

            IDictionary<string, string> retrievedData = this.GetSNMPData(getData);

            config.logEnable = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.1.0"]);
            config.logInterval = ConvertFromSNMPInt(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.2.0"]); 
            config.logDifference = ConvertFromSNMPInt(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.3.0"]); 
            config.netservdLog = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.4.0"]);
            config.autonetLog = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.5.0"]);
            config.stdmeterdLog = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.6.0"]); 
            config.groupmeterdLog = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.7.0"]); 
            config.alarmdLog = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.8.0"]);
            config.userlogindLog = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.9.0"]);
            config.usersetupdLog = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.10.0"]);
            config.fwupdatedLog = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.11.0"]);
            config.setupchangedLog = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.12.0"]);
            config.recepchangedLog = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.13.0"]);
            config.pduopchangedLog = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.14.0"]);
            config.dLogCycle = ConvertFromSNMPInt(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.15.0"]);
            config.dLogFullWarnLevel = ConvertFromSNMPInt(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.16.0"]);
            config.netLogServer = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.17.0"];
            config.netdLogPort = ConvertFromSNMPInt(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.18.0"]);
            config.netdLogUser = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.19.0"];
            config.netdLogPasswd = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.20.0"];
            config.netdLogDestdir = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.21.0"];
            config.netdLogOptions = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.3.22.0"];

            this.WriteObject(config);
        }

        private void ProcessRadiusAccessControlConfig()
        {
            RadiusAccessControlConfig config = new RadiusAccessControlConfig();

            List<string> getData = new List<string>();
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.4.2.1.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.4.2.2.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.4.2.3.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.4.2.4.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.4.2.5.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.4.2.6.0");

            IDictionary<string, string> retrievedData = this.GetSNMPData(getData);

            config.enableRadiusAuth = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.4.2.1.0"]); 
            config.radiusServer = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.4.2.2.0"]; 
            config.radiusSHSecret = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.4.2.3.0"]; 
            config.radiusNASServer = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.4.2.4.0"]; 
            config.radiusPort = ConvertFromSNMPInt(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.4.2.5.0"]); 
            config.nasPort = ConvertFromSNMPInt(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.4.2.6.0"]);

            this.WriteObject(config);
        }

        private void ProcessSMTPInterfaceConfig()
        {
            SMTPInterfaceConfig config = new SMTPInterfaceConfig();

            List<string> getData = new List<string>();
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.4.1.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.4.2.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.4.3.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.4.4.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.4.5.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.4.6.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.4.7.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.4.8.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.4.9.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.4.10.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.4.11.0");

            IDictionary<string, string> retrievedData = this.GetSNMPData(getData);

            config.enableSMTP = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.4.1.0"]); 
            config.smtpSendPort = ConvertFromSNMPInt(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.4.2.0"]); 
            config.smtpReceivePort = ConvertFromSNMPInt(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.4.3.0"]); 
            config.smtpServer = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.4.4.0"]; 
            config.smtpUserName = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.4.5.0"]; 
            config.smtpUserPasswd = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.4.6.0"]; 
            config.smtpEmailAddr = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.4.7.0"]; 
            config.smtpEmailDestination1 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.4.8.0"]; 
            config.smtpEmailDestination2 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.4.9.0"]; 
            config.smtpEmailDestination3 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.4.10.0"];
            config.smtpServOptions = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.4.11.0"];

            this.WriteObject(config);
        }

        private void ProcessSNMPInterfaceConfig()
        {
            SNMPInterfaceConfig config = new SNMPInterfaceConfig();

            List<string> getData = new List<string>();
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.1.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.2.1.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.2.2.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.2.3.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.2.4.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.2.5.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.2.6.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.2.7.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.3.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.4.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.5.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.6.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.7.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.8.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.9.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.10.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.11.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.12.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.13.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.14.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.15.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.16.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.17.1.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.17.2.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.17.3.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.17.4.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.17.5.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.1.17.6.0");

            IDictionary<string, string> retrievedData = this.GetSNMPData(getData);

            config.snmpEnable = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.1.0"]);
            config.snmpHostOnly = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.2.1.0"]);
            config.snmpHost1 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.2.2.0"];
            config.snmpHost2 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.2.3.0"];
            config.snmpHost3 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.2.4.0"];
            config.snmpHostIPv61 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.2.5.0"];
            config.snmpHostIPv62 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.2.6.0"];
            config.snmpHostIPv63 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.2.7.0"];
            config.snmpQueryPort = ConvertFromSNMPInt(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.3.0"]);
            config.snmpTrapPort = ConvertFromSNMPInt(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.4.0"]);
            config.snmpReadComm = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.5.0"];
            config.snmpWriteComm = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.6.0"]; 
            config.snmpUSMuserName = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.7.0"];
            config.snmpSecurityLevel = ConvertFromSNMPInt(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.8.0"]);
            config.snmpAuthAlgo = ConvertFromSNMPInt(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.9.0"]);
            config.snmpPrivAlgo = ConvertFromSNMPInt(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.10.0"]);
            config.snmpAuthPasswd = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.11.0"];
            config.snmpPrivPasswd = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.12.0"];
            config.snmpContextName = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.13.0"]; 
            config.snmpEngineID = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.14.0"];
            config.snmpLocAuthKey = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.15.0"]; 
            config.snmpLocPrivKey = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.16.0"];
            config.snmpTrapHost1 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.17.1.0"]; 
            config.snmpTrapHost2 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.17.2.0"];
            config.snmpTrapHost3 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.17.3.0"];
            config.snmpTrapHostIPv61 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.17.4.0"];
            config.snmpTrapHostIPv62 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.17.5.0"];
            config.snmpTrapHostIPv63 = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.1.17.6.0"];

            this.WriteObject(config);
        }

        private void ProcessSSHInterfaceConfig()
        {
            SSHInterfaceConfig config = new SSHInterfaceConfig();

            List<string> getData = new List<string>();
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.3.1.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.3.2.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.3.3.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.2.3.4.0");

            IDictionary<string, string> retrievedData = this.GetSNMPData(getData);

            config.telnetEnable = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.3.1.0"]);
            config.sshEnable = ConvertFromSNMPBool(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.3.2.0"]);
            config.sshPort = ConvertFromSNMPInt(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.3.3.0"]);
            config.telnetPort = ConvertFromSNMPInt(retrievedData["1.3.6.1.4.1.30932.1.10.1.1.2.3.4.0"]);

            this.WriteObject(config);
        }

        private void ProcessTimeNetworkConfig()
        {
            TimeNetworkConfig config = new TimeNetworkConfig();

            List<string> getData = new List<string>();
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.3.1.0");
            getData.Add("1.3.6.1.4.1.30932.1.10.1.1.1.3.2.0");

            IDictionary<string, string> retrievedData = this.GetSNMPData(getData);

            config.rfcTimeServer = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.3.1.0"];
            config.ntpTimeServer = retrievedData["1.3.6.1.4.1.30932.1.10.1.1.1.3.2.0"];

            this.WriteObject(config);
        }

        private bool ConvertFromSNMPBool(string value)
        {
            return value.Length == 0 ? false : (value == "1" ? true : false);
        }

        private int ConvertFromSNMPInt(string value)
        {
            return value.Length == 0 ? 0 : Convert.ToInt32(value);
        }

        private IDictionary<string,string> GetSNMPData(List<string> getData)
        {
            Dictionary<string, string> properties = new Dictionary<string, string>();
            properties.Add("Public.DeviceManager.SNMPVersion", "SNMPv1v2c");
            properties.Add("Public.DeviceManager.SNMPReadCommunity", "public");
            properties.Add("Public.DeviceManager.SNMPWriteCommunity", "private");
            return CPI.Common.SNMP.SNMPDotNetLib.GetSNMPData(this.IPAddress, getData, properties);
        }                
    }
}
