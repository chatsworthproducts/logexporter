﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eConnectPDUTesting
{
    [System.Management.Automation.Cmdlet(System.Management.Automation.VerbsCommon.Split, "FirmwareName")]
    public class Split_FirmwareName : System.Management.Automation.PSCmdlet
    {
        [System.Management.Automation.Parameter(Position = 0, Mandatory = true)]
        public string FirmwareFileName;

        protected override void ProcessRecord()
        {
            base.ProcessRecord();

            string[] parts = this.FirmwareFileName.Split('-', '.');

            DateTime buildDate = new DateTime(int.Parse(parts[1].Substring(0, 4)), int.Parse(parts[1].Substring(4, 2)), int.Parse(parts[1].Substring(6, 2)));
            int svnCommitNumber = int.Parse(parts[2].Substring(3));

            int majorVersion = 0x1;
            int minorVersion = ((svnCommitNumber >> 8) & 0xFF);
            int buildVersion = (svnCommitNumber & 0xFF);
            int patchVersion = 0x0;

            string versionNumber = string.Format("{0}.{1}.{2}.{3}", majorVersion, minorVersion, buildVersion, patchVersion);

            object[] fwData = new object[] { buildDate.ToShortDateString(), svnCommitNumber, majorVersion, minorVersion, buildVersion, patchVersion, versionNumber };

            this.WriteObject(fwData);
        }
    }
}
