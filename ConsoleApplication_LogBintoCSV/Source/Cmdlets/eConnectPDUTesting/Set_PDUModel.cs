﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPI.eConnect.Communications;

namespace eConnectPDUTesting
{
    [System.Management.Automation.Cmdlet(System.Management.Automation.VerbsCommon.Set, "PDUModel")]
    public class Set_PDUModel : System.Management.Automation.PSCmdlet
    {
        [System.Management.Automation.Parameter(Position = 0, Mandatory = true)]
        public string LoginName;

        [System.Management.Automation.Parameter(Position = 1, Mandatory = true)]
        public string LoginPassword;

        [System.Management.Automation.Parameter(Position = 2, Mandatory = true)]
        public string IPAddress;

        [System.Management.Automation.Parameter(Position = 3, Mandatory = true)]
        public string Model;

        protected override void ProcessRecord()
        {
            base.ProcessRecord();

            using (var pdu = new CPI.eConnect.Communications.SSHMCMConsole())
            {
                // establish a connection to the pdu
                pdu.Connect(this.IPAddress, 22, this.LoginName, this.LoginPassword);
                System.Threading.Thread.Sleep(5000);

                IPDUCommand cmd = pdu.SetAdvandedMode;
                cmd.Execute();
                DisposeCmd(cmd);

                cmd = pdu.SetModel;
                cmd.Parameters["model"] = this.Model;
                cmd.Execute();
                DisposeCmd(cmd);
            }
        }

        private void DisposeCmd(IPDUCommand cmd)
        {
            IDisposable dispCmd = (IDisposable)cmd;
            if (dispCmd != null)
            {
                dispCmd.Dispose();
            }
        }
    }
}
