﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.API
{
    public class PDUUpdateEventArgs : EventArgs
    {
        public PDU UpdatedPDU { get; set; }
    }

    public class PDUGatewayEventArgs : EventArgs
    {
        public PDUGateway Gateway { get; set; }
    }

    public class PDUGateway : ObservableCollection<PDU>
    {   
        public bool EnablePeriodicUpdate { get; set; }
        public int UpdatePeriodInSeconds { get; set; }

        public event EventHandler<PDUUpdateEventArgs> OnPDUUpdated;
        public event EventHandler<PDUGatewayEventArgs> OnBeginUpdate;
        public event EventHandler<PDUGatewayEventArgs> OnEndUpdate;
        public event EventHandler<PDUGatewayEventArgs> OnStartUpdate;
        public event EventHandler<PDUGatewayEventArgs> OnStopUpdate;

        public void StopUpdates()
        {

        }

        public void StartUpdates()
        {

        }
    }
}
