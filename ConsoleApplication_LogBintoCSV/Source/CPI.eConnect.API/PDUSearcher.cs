﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.API
{
    public static class PDUSearcher
    {
        private const string SYSNAME_VID = "1.3.6.1.2.1.1.1.0";
        private const string VERSION_VID = "1.3.6.1.4.1.30932.1.1.1.1";
        private const string MODELCODE_VID = "1.3.6.1.4.1.30932.1.1.1.2";
        private const string HWADDRESS_VID = "1.3.6.1.4.1.30932.1.1.1.3";
        private const string NAME_VID = "1.3.6.1.4.1.30932.1.1.2.40";
        private const string DESCRIPTION_VID = "1.3.6.1.4.1.30932.1.1.2.41";        
        private const string PDUROLE_VID = "1.3.6.1.4.1.30932.1.1.2.58.2";
        private const string PDUALTROLE_VID = "1.3.6.1.4.1.30932.1.1.2.58.10";
        private const string PDUCOUNT_VID = "1.3.6.1.4.1.30932.1.1.5.1";
        private const string PDUMAP_VID = "1.3.6.1.4.1.30932.1.1.5.2";
        private const string PDUSELECT_VID = "1.3.6.1.4.1.30932.1.1.5.3";
        private const string UNITYCHECK_VID = "1.3.6.1.4.1.30932.1.10.1.1.1.1.1";
        private const string PDUTABLECOUNT_VID = "1.3.6.1.4.1.30932.1.10.1.2.1";
        private const string PDUTABLE_VID = "1.3.6.1.4.1.30932.1.10.1.2.10";
        private const string PDUTABLE_PDUMAC_VID = "1.3.6.1.4.1.30932.1.10.1.2.10.1.10";
        private const string PDUTABLE_PDUMODEL_VID = "1.3.6.1.4.1.30932.1.10.1.2.10.1.20";
        private const string PDUTABLE_PDUNAME_VID = "1.3.6.1.4.1.30932.1.10.1.2.10.1.30";
        private const string PDUTABLE_PDUDESCRIPTION_VID = "1.3.6.1.4.1.30932.1.10.1.2.10.1.40";
        private const string PDUTABLE_PDUVERSION_VID = "1.3.6.1.4.1.30932.1.10.1.2.10.1.50";
        private const string PDUTABLE_PDUCHAINROLE_VID = "1.3.6.1.4.1.30932.1.10.1.2.10.1.60";

        public static List<PDU> FindWithSNMP(string startAddress, string endAddress, IDictionary<String, String> searchInformation)
        {
            object syncObj = new object();
            List<PDU> foundPdus = new List<PDU>();
            IPAddressEnumerator addressEnumerator = new IPAddressEnumerator(startAddress, endAddress);

            foreach (string deviceAddress in addressEnumerator)
            {
                List<string> oidList = new List<string>()
                    {
                        SYSNAME_VID,
                        VERSION_VID,
                        HWADDRESS_VID,
                        MODELCODE_VID,
                        NAME_VID,
                        DESCRIPTION_VID,
                        PDUCOUNT_VID,
                        PDUROLE_VID,
                        PDUALTROLE_VID,
                        UNITYCHECK_VID
                    };

                IDictionary<string, string> snmpData = CPI.Common.SNMP.SNMPDotNetLib.GetSNMPData(deviceAddress, oidList, searchInformation);
                if(snmpData.Keys.Contains(SYSNAME_VID) == true)
                {
                    if(snmpData[SYSNAME_VID] == "CPI eConnect PDU")
                    {
                        // we've found an econnect pdu...
                        List<PDU> builtPDUs = BuildPDU(snmpData, deviceAddress, searchInformation, snmpData.Keys.Contains(UNITYCHECK_VID));
                        lock (syncObj)
                        {
                            foundPdus.AddRange(builtPDUs);
                        }
                    }
                }
            }

            return foundPdus;
        }

        private static List<PDU> BuildPDU(IDictionary<string, string> snmpData, string ipAddress, IDictionary<String, String> searchInformation, bool supportsUnitySnmp)
        {
            List<PDU> foundPdus = new List<PDU>();

            //if(supportsUnitySnmp == true)
            //{
            //    return BuildUnityPDU(ipAddress, searchInformation);
            //}

            PDU newPDU = new PDU();
            newPDU.Model = snmpData[MODELCODE_VID];
            newPDU.MACAddress = snmpData[HWADDRESS_VID];
            newPDU.Name = snmpData[NAME_VID];
            newPDU.Description = snmpData[DESCRIPTION_VID];            
            newPDU.Version = snmpData[VERSION_VID];
            newPDU.DaisyChainRole = ChainRole.Secondary;
            newPDU.GatewayIP = ipAddress;
            newPDU.GatewaySelector = "0";
            if (snmpData[PDUROLE_VID] =="1")
            {
                newPDU.DaisyChainRole = ChainRole.Primary;
                foundPdus.AddRange(BuildPDUChain(ipAddress, searchInformation));
            }
            else if (snmpData[PDUALTROLE_VID] == "1")
            {
                newPDU.DaisyChainRole = ChainRole.Alternate;
                foundPdus.AddRange(BuildPDUChain(ipAddress, searchInformation));
            }
            else
            {
                foundPdus.Add(newPDU);
            }

            return foundPdus;
        }

        private static List<PDU> BuildUnityPDU(string ipAddress, IDictionary<String, String> searchInformation)
        {
            List<PDU> foundPdus = new List<PDU>();
            // PDU is in a daisy chain...
            List<string> oidList = new List<string>()
                            {
                                PDUTABLECOUNT_VID
                            };
            IDictionary<string, string> snmpData = CPI.Common.SNMP.SNMPDotNetLib.GetSNMPData(ipAddress, oidList, searchInformation);
            int pduCount;
            
            if(Int32.TryParse(snmpData[PDUTABLECOUNT_VID], out pduCount) == false)
            {
                return foundPdus;
            }

            for (int i = 1; i <= pduCount; i++)
            {
                oidList = new List<string>()
                            {
                                string.Format("{0}.{1}", PDUTABLE_VID, i)
                            };

                snmpData = CPI.Common.SNMP.SNMPDotNetLib.GetSNMPData(ipAddress, oidList, searchInformation);

                PDU newPDU = new PDU();
                newPDU.Model = snmpData[PDUTABLE_PDUMODEL_VID];
                newPDU.MACAddress = snmpData[PDUTABLE_PDUMAC_VID];
                newPDU.Name = snmpData[PDUTABLE_PDUNAME_VID];
                newPDU.Description = snmpData[PDUTABLE_PDUDESCRIPTION_VID];
                newPDU.Version = snmpData[PDUTABLE_PDUVERSION_VID];
                newPDU.DaisyChainRole = ChainRole.Secondary;
                if (snmpData[PDUTABLE_PDUCHAINROLE_VID] == "1")
                {
                    newPDU.DaisyChainRole = ChainRole.Primary;
                }
                else if (snmpData[PDUTABLE_PDUCHAINROLE_VID] == "2")
                {
                    newPDU.DaisyChainRole = ChainRole.Alternate;
                }
                foundPdus.Add(newPDU);
            }

            return foundPdus;
        }

        private static List<PDU> BuildPDUChain(string ipAddress, IDictionary<String, String> searchInformation)
        {
            List<PDU> foundPdus = new List<PDU>();
            // PDU is in a daisy chain...
            List<string> oidList = new List<string>()
                            {
                                PDUMAP_VID
                            };
            // Get the PDU map
            IDictionary<string, string> snmpData = CPI.Common.SNMP.SNMPDotNetLib.GetSNMPData(ipAddress, oidList, searchInformation);
            string[] chainedDevices = snmpData[PDUMAP_VID].Split(' ');

            foreach (string chainEntry in chainedDevices)
            {
                List<KeyValuePair<string, object>> setParameters = new List<KeyValuePair<string, object>>();
                setParameters.Add(new KeyValuePair<string, object>(PDUSELECT_VID, chainEntry));

                CPI.Common.SNMP.SNMPDotNetLib.SetSNMPData(ipAddress, setParameters, searchInformation);

                oidList = new List<string>()
                    {
                        SYSNAME_VID,
                        VERSION_VID,
                        HWADDRESS_VID,
                        MODELCODE_VID,
                        NAME_VID,
                        DESCRIPTION_VID,
                        PDUCOUNT_VID,
                        PDUROLE_VID,
                        PDUALTROLE_VID
                    };

                snmpData = CPI.Common.SNMP.SNMPDotNetLib.GetSNMPData(ipAddress, oidList, searchInformation);
                if (snmpData.Count > 0)
                {
                    PDU newPDU = new PDU();
                    newPDU.Model = snmpData[MODELCODE_VID];
                    newPDU.MACAddress = snmpData[HWADDRESS_VID];
                    newPDU.Name = snmpData[NAME_VID];
                    newPDU.Description = snmpData[DESCRIPTION_VID];
                    newPDU.Version = snmpData[VERSION_VID];
                    newPDU.DaisyChainRole = ChainRole.Secondary;
                    newPDU.GatewayIP = ipAddress;
                    newPDU.GatewaySelector = chainEntry;
                    if (snmpData[PDUROLE_VID] == "1")
                    {
                        newPDU.DaisyChainRole = ChainRole.Primary;
                    }
                    else if (snmpData[PDUALTROLE_VID] == "1")
                    {
                        newPDU.DaisyChainRole = ChainRole.Alternate;
                    }
                    foundPdus.Add(newPDU);
                }
            }

            return foundPdus;
        }
    }
}
