﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.API
{
    public enum ChainRole
    {
        Secondary = 0,
        Primary = 1,
        Alternate = 2
    }
}
