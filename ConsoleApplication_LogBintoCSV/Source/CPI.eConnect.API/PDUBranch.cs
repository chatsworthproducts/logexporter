﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.API
{
    public class PDUBranch
    {
        private string[] Phases = new string[]{ "XY", "YZ", "ZX" };

        public int BranchId { get; set; }
        public string Phase { get { return this.Phases[(this.BranchId - 1) % 3]; } }
        public float Current { get; set; }
        public float MaxCurrent { get; set; }
        public float Voltage { get; set; }
        public float Power { get; set; }
        public float PowerFactor { get; set; }
        public float Energy { get; set; }
    }
}
