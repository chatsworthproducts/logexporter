﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.API
{
    public class PDUSensor
    {
        public float Temp { get; set; }
        public float Humifity { get; set; }
    }
}
