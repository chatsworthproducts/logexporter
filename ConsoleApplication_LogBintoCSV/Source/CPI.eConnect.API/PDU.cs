﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.API
{
    public class PDU
    {
        private eConnectDeviceInfo _Info;

        public string Name { get; set; }
        public string Description { get; set; }
        public string Model 
        {
            get
            {
                if (this._Info != null)
                {
                    return this._Info.ModelNumber;
                }
                return string.Empty;
            }

            set
            {
                this._Info = new eConnectDeviceInfo(value);
            }
        }

        public string MACAddress { get; set; }
        public string Attrib { get; set; }        
        public string Version { get; set; }
        public string Config { get; set; }
        public string ModulesVersion { get; set; }
        public ChainRole DaisyChainRole { get; set; }

        public string GatewayIP { get; set; }
        public string GatewaySelector { get; set; }

        // Is capable of monitoring branch current if true
        public bool HasBranchCurrentMonitor 
        { 
            get
            {
                if (this._Info != null)
                {
                    return this._Info.HasBranchCurrentMonitor;
                }
                return false;
            }
        }

        // Is capable of monitoring outlet current if true
        public bool HasOutletCurrentMonitor 
        {
            get
            {
                if (this._Info != null)
                {
                    return this._Info.HasOutletCurrentMonitor;
                }
                return false;
            }
        }

        // Is capable of turning outlets on / off if true
        public bool HasOutletControl 
        { 
            get
            {
                if (this._Info != null)
                {
                    return this._Info.HasOutletControl;
                }
                return false;
            }
        }

        // Total number of outlets for this PDU
        public int TotalOutlets 
        { 
            get
            {
                if (this._Info != null)
                {
                    return this._Info.TotalOutlets;
                }
                return 0;
            }
        }

        // Total number of circuit breakers.  If 6, then a 3 phase unit which maps to xyz on the first 3, and xyz again on the next 3 phases
        public int NumberOfBreakers 
        { 
            get
            {
                if (this._Info != null)
                {
                    return this._Info.NumberOfBreakers;
                }
                return 0;
            }
        }

        // Number of outlets in a section - varies by number of circuit breakers
        public int OutletsPerSection 
        { 
            get
            {
                if (this._Info != null)
                {
                    return this._Info.OutletsPerSection;
                }
                return 0;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures",
            Justification = "Design is simple and users don't need to create entries.")]
        public Dictionary<int, List<int>> BranchOutletMap 
        { 
            get
            {
                if (this._Info != null)
                {
                    return this._Info.BranchOutletMap;
                }
                return new Dictionary<int,List<int>>();
            }
        }
    }
}
