﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.API
{
    public enum PDUOutletControlState
    {
        OFF = 0,
        ON = 1
    }

    public class PDUOutlet
    {
        public int OutletID { get; set; }
        public string OutletName { get; set; }
        public string OutletDescription { get; set; }
        public int BranchID { get; set; }
        public float Current { get; set; }
        public float Voltage { get; set; }
        public float Power { get; set; }
        public float Energy { get; set; }
        public PDUOutletControlState ControlState { get; set; }
    }
}
