﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.API
{
    internal class IPAddressEnumerator : IEnumerator<string>, IEnumerator, IEnumerable<string>
    {
        private uint _StartIntAddress;
        private uint _EndIntAddress;
        private uint _Current;

        public IPAddressEnumerator(string startAddress, string endAddress)
        {
            _StartIntAddress = IPString2IPInt(startAddress);
            _EndIntAddress = IPString2IPInt(endAddress);
            _Current = 0;
        }

        public string Current
        {
            get { return IPInt2IPString(_Current); }
        }

        public void Dispose()
        {
            
        }

        object System.Collections.IEnumerator.Current
        {
            get { return IPInt2IPString(_Current); }
        }

        public bool MoveNext()
        {
            if (_Current == 0)
            {
                _Current = _StartIntAddress;
            }
            else
            {
                _Current++;
            }

            if(_Current > _EndIntAddress)
            {
                return false;
            }

            return true;
        }

        public void Reset()
        {
            _Current = _StartIntAddress;
        }

        public IEnumerator<string> GetEnumerator()
        {
            return this;
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this;
        }

        private static uint IPString2IPInt(string ip)
        {
            string[] numbers = ip.Split('.');

            return (uint)(Convert.ToByte(numbers[0]) << 24) |
                   (uint)(Convert.ToByte(numbers[1]) << 16) |
                   (uint)(Convert.ToByte(numbers[2]) << 8) |
                   (uint)(Convert.ToByte(numbers[3]));
        }

        private static string IPInt2IPString(uint ip)
        {
            return string.Format("{0}.{1}.{2}.{3}",
                                    ((ip & 0xff000000) >> 24).ToString(),
                                    ((ip & 0x00ff0000) >> 16).ToString(),
                                    ((ip & 0x0000ff00) >> 8).ToString(),
                                    (ip & 0x000000ff).ToString());
        }
    }
}
