﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace CPI.eConnect.API
{
    /// <summary>
    /// The eConnedDeviceInfo class is used to determine the information about a PDU.
    /// 
    /// Usage:
    /// 
    /// eConnectDeviceInfo devInfo = new eConnectDeviceInfo("P3-1D0A5-C2C");
    ///   ... access devInfo properties ...
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "e",
        Justification = "Naming is based on product name.")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "eConnect",
        Justification = "Naming is based on product name.")]
    public class eConnectDeviceInfo
    {
        public eConnectDeviceInfo(string modelNumber)
        {
            this.ModelNumber = modelNumber;
            this.BranchOutletMap = new Dictionary<int, List<int>>();
            this.BuildDeviceInfo();
        }

        public string ModelNumber { get; set; }

        // Is capable of monitoring branch current if true
        public bool HasBranchCurrentMonitor { get; set; }

        // Is capable of monitoring outlet current if true
        public bool HasOutletCurrentMonitor { get; set; }

        // Is capable of turning outlets on / off if true
        public bool HasOutletControl { get; set; }

        // Total number of outlets for this PDU
        public int TotalOutlets { get; set; }

        // Total number of circuit breakers.  If 6, then a 3 phase unit which maps to xyz on the first 3, and xyz again on the next 3 phases
        public int NumberOfBreakers { get; set; }

        // Number of outlets in a section - varies by number of circuit breakers
        public int OutletsPerSection { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures",
            Justification = "Design is simple and users don't need to create entries.")]
        public Dictionary<int, List<int>> BranchOutletMap { get; private set; }

        private void BuildDeviceInfo()
        {
            int numberOfSections = 6;       // Always six sections per PDU - used to calculate outlet progessions vs branches
            int outletIsOnSection = 0;      // Which section the outlet of interest is on

            // Initialize the information structure
            this.HasBranchCurrentMonitor = false;
            this.HasOutletCurrentMonitor = false;
            this.HasOutletControl = false;
            this.TotalOutlets = 0;
            this.NumberOfBreakers = 0;
            this.OutletsPerSection = 0;

            this.SetOutletControlInfo();
            this.SetOutletCount();
            this.SetBreakerInfo();

            // More than 1 breaker, now sections come into play
            this.OutletsPerSection = this.TotalOutlets / numberOfSections;

            for (int outlet = 1; outlet <= this.TotalOutlets; outlet++)
            {
                int branch = 0;

                // determine which section this outlet is on is based on the number of outlets and breakers
                // divide to find the section number
                outletIsOnSection = ((outlet - 1) / this.OutletsPerSection) + 1;

                if (this.NumberOfBreakers == 2)
                {
                    // If odd number branch 1.  If even number branch 2.
                    if (outletIsOnSection % 2 != 0)
                    {
                        branch = 1;
                    }
                    else
                    {
                        branch = 2;
                    }
                }
                else if (this.NumberOfBreakers == 3)
                {
                    if (outletIsOnSection <= 3)
                    {
                        branch = outletIsOnSection;
                    }
                    else
                    {
                        branch = outletIsOnSection - 3;
                    }
                }
                else if (this.NumberOfBreakers == 6)
                {
                    branch = outletIsOnSection;
                }

                if ((branch == 0) || ((branch > 3) && this.NumberOfBreakers != 6))
                {
                    return;
                }

                if (this.BranchOutletMap.Keys.Contains(branch) == false)
                {
                    List<int> outlets = new List<int>(1);
                    outlets.Add(outlet);
                    this.BranchOutletMap.Add(branch, outlets);
                }
                else
                {
                    this.BranchOutletMap[branch].Add(outlet);
                }
            }
        }

        private void SetBreakerInfo()
        {
            // Find number of circuit breakers in order to compute outlets per section from 11th character
            this.NumberOfBreakers = 0;
            char testChar = Convert.ToChar(this.ModelNumber.Substring(10, 1), CultureInfo.InvariantCulture);
            switch (testChar)
            {
                case '1': this.NumberOfBreakers = 1;
                    break;
                case '2': this.NumberOfBreakers = 2;
                    break;
                case '3': this.NumberOfBreakers = 3;
                    break;
                case '7': this.NumberOfBreakers = 6;
                    break;
            }

            if (this.NumberOfBreakers == 0)
            {
                return;  // error condition
            }

            if (this.NumberOfBreakers == 1)
            {
                // This is a single branch PDU, thus all outlets are on branch 1                
                this.OutletsPerSection = this.TotalOutlets;
                List<int> outlets = new List<int>(this.TotalOutlets);
                for (int i = 1; i <= this.TotalOutlets; i++)
                {
                    outlets.Add(i);
                }

                this.BranchOutletMap.Add(1, outlets);
                return;
            }
        }

        private void SetOutletCount()
        {
            // Find number of outlets from 7th character in model name
            char testChar = Convert.ToChar(this.ModelNumber.Substring(6, 1).ToUpperInvariant(), CultureInfo.InvariantCulture);
            switch (testChar)
            {
                case 'P':
                case 'Q':
                    this.TotalOutlets = 18;  // new
                    break;

                case 'A':
                case 'C':
                case 'D':
                case 'M':
                case 'N':
                case 'T':
                    this.TotalOutlets = 24;  // new
                    break;

                case 'E':
                case 'V':
                    this.TotalOutlets = 30;
                    break;

                case 'B':
                case 'F':
                case 'G':
                case 'J':
                case 'K':
                case 'L':
                    this.TotalOutlets = 36;  // new
                    break;

                case 'H':
                    this.TotalOutlets = 42;
                    break;
            }

            if (this.TotalOutlets == 0)
            {
                return;  // error condition
            }
        }

        private void SetOutletControlInfo()
        {
            // See if it is a model that monitors current at the outlets from the second character (P4 and P6 models):
            int testInt = Convert.ToInt32(this.ModelNumber.Substring(1, 1), CultureInfo.InvariantCulture);
            switch (testInt)
            {
                case 3:
                case 5:
                    this.HasBranchCurrentMonitor = true;
                    this.HasOutletCurrentMonitor = false;
                    break;
                case 4:
                case 6:
                    this.HasBranchCurrentMonitor = true;
                    this.HasOutletCurrentMonitor = true;
                    break;
            }

            // See if it is a model that can turn power on or off at the outlets from the second character (P5 and P6 models):
            if ((testInt == 5) || (testInt == 6))
            {
                this.HasOutletControl = true;
            }

            // See if it is a basic or invalid model
            if ((testInt < 3) || (testInt > 6))
            {
                return;  // error condition
            }
        }
    }
}
