﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPI.eConnect.Logging.eConnect;
using CPI.Data.PDULog;

namespace CPI.eConnect.Processors.PDULog
{
    public static class ProcessPDULog
    {
        public static void ReadLog(LogReader reader)
        {
            ReadingStateInfo logInfo = new ReadingStateInfo(reader);

            try
            {
                while (true)
                {
                    reader.ReadEntry();
                }
            }
            catch (System.IO.EndOfStreamException)
            {
                // not really an error, we're just done reading the file.
            }
            finally
            {
                reader.Dispose();
            }

            //Process all of the PDUInfo records...
            //  Check to see if the PDU is in the SystemObject table
            CPIPDULogModelContainer c = new CPI.Data.PDULog.CPIPDULogModelContainer();

            foreach (PDUInfoEntry info in logInfo.PDUInfoIndex.Values)
            {
                IEnumerable<SystemObject> logPDUs = from s in c.SystemObjects
                                                    where s.mac == info.MacAddress
                                                    select s;
                SystemObject pduDbObj = logPDUs.FirstOrDefault();

                if (pduDbObj == null)
                {
                    pduDbObj = new SystemObject();
                    pduDbObj.mac = info.MacAddress;
                    pduDbObj.model = info.Model;
                    pduDbObj.type = 0;
                    c.SystemObjects.Add(pduDbObj);
                    c.SaveChanges();
                }

                c.AddAlarms(pduDbObj, info.Alarms.ToList().OrderBy(m => m.TimeStamp));
                c.AddBranchInfo(pduDbObj, info.BranchData);
                c.AddRecepInfo(pduDbObj, info.RecepData);
                c.AddConfigChange(pduDbObj, info.ConfigChanges);
                c.AddEnvironmentalData(pduDbObj, info.EnvironmentData);
                c.AddFirmwareChanges(pduDbObj, info.FirmwareChanges);
                c.AddRecepChange(pduDbObj, info.RecepChanges);
                c.AddOptionChanges(pduDbObj, info.OptionChanges);

                c.SaveChanges();
            }
        } 
    }
}
