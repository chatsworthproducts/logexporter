﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.PDUMaint
{
    public static class RegionNames
    {
        public const string MainRegion = "MainRegion";
        public const string ActionRegion = "ActionRegion";
        public const string StatusRegion = "StatusRegion";
        public const string SecondaryRegion = "DialogRegion";
    }
}
