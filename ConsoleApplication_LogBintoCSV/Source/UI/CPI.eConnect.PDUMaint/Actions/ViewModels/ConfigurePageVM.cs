﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.Prism.Mvvm;
using System.ComponentModel;
using System.Windows.Input;
using CPI.eConnect.PDUMaint.Support.MainContent;

namespace CPI.eConnect.PDUMaint.Actions.ViewModels
{
    class ConfigurePageVM : PDUMaintPageVM
    {
        public ConfigurePageVM() :
            base(new PageInfo {Title = "Configuration", CanClose=true, IsModified=false} )
        {
        }
    }
}
