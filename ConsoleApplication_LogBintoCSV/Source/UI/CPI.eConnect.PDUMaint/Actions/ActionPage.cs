﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.PDUMaint.Actions
{
    public static class ActionPage
    {
        public static string ConfigurationPage = "Page_Configure";
        public static string MCMCalibratePage = "Page_MCMCalibrate";
        public static string MCMMonitorPage = "Page_MCMMonitor";
        public static string DaisyChainPage = "Page_DaisyChain";
        public static string MCMProgrammerPage = "Page_MCMProgrammer";
        public static string PDUTesterPage = "Page_PDUTester";
    }
}
