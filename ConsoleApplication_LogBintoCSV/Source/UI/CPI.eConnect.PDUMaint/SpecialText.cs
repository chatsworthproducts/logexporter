﻿using System;
using System.Linq;
using System.Management;
using System.Reflection;

namespace CPI.eConnect.PDUMaint
{
    public class SpecialText
    {
        private static string _CachedWindowsName = null;
        private static string _OSArchitecture = null;

        public static string OSNameInfo
        {
            get
            {
                return WindowsName + " (" + WindowsVersion + ")";
            }
        }


        public static string WindowsName
        {
            get
            {
                if (_CachedWindowsName == null)
                {
                    var name = (from x in new ManagementObjectSearcher("SELECT * FROM Win32_OperatingSystem").Get().OfType<ManagementObject>()
                                select x.GetPropertyValue("Caption")).First();
                    _CachedWindowsName = name != null ? name.ToString() : "Unknown";
                }

                return _CachedWindowsName;
            }
        }

        public static string OSArchitecture
        {
            get
            {
                if (_OSArchitecture == null)
                {
                    try
                    {
                        System.Management.ManagementObjectSearcher objSearcher = new System.Management.ManagementObjectSearcher("SELECT * FROM Win32_OperatingSystem");
                        System.Management.ManagementObjectCollection objColl = objSearcher.Get();

                        foreach (var mgtObject in objColl)
                        {
                            _OSArchitecture = mgtObject["osarchitecture"].ToString();
                        }
                    }
                    catch (Exception)
                    {
                        _OSArchitecture = null;
                    }
                }

                return _OSArchitecture;
            }
        }

        public static string WindowsVersion
        {
            get
            {
                return Environment.OSVersion.Version.ToString() + " " + Environment.OSVersion.ServicePack;
            }
        }

        public static string AssemblyTitle
        {
            get
            {
                // Get all Title attributes on this assembly
                object[] attributes = Assembly.GetEntryAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                // If there is at least one Title attribute
                if (attributes.Length > 0)
                {
                    // Select the first one
                    AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
                    // If it is not an empty string, return it
                    if (titleAttribute.Title != "")
                        return titleAttribute.Title;
                }
                // If there was no Title attribute, or if the Title attribute was the empty string, return the .exe name
                return System.IO.Path.GetFileNameWithoutExtension(Assembly.GetEntryAssembly().CodeBase);
            }
        }

        public static string AssemblyDescription
        {
            get
            {
                // Get all Description attributes on this assembly
                object[] attributes = Assembly.GetEntryAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
                // If there aren't any Description attributes, return an empty string
                if (attributes.Length == 0)
                    return "";
                // If there is a Description attribute, return its value
                return ((AssemblyDescriptionAttribute)attributes[0]).Description;
            }
        }

        public static string AssemblyProduct
        {
            get
            {
                // Get all Product attributes on this assembly
                object[] attributes = Assembly.GetEntryAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
                // If there aren't any Product attributes, return an empty string
                if (attributes.Length == 0)
                    return "";
                // If there is a Product attribute, return its value
                return ((AssemblyProductAttribute)attributes[0]).Product;
            }
        }

        /// <summary>
        ///    AssemblyInfo file AssemblyCompany: "Luminex Corporation"
        /// <returns></returns>
        /// </summary>
        public static string AssemblyCompany
        {
            get
            {
                // Get all Company attributes on this assembly
                object[] attributes = Assembly.GetEntryAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
                // If there aren't any Company attributes, return an empty string
                if (attributes.Length == 0)
                    return "";
                // If there is a Company attribute, return its value
                return ((AssemblyCompanyAttribute)attributes[0]).Company;
            }
        }

        public static string AssemblyVersion
        {
            get
            {
                return Assembly.GetEntryAssembly().GetName().Version.ToString();
            }
        }
    }
}
