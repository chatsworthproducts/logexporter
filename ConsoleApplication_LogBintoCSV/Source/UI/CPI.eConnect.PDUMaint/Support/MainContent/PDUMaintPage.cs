﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CPI.eConnect.PDUMaint.Support.Commands;
using Microsoft.Practices.Prism;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.ServiceLocation;


namespace CPI.eConnect.PDUMaint.Support.MainContent
{
    public class PDUMaintPage : UserControl
    {
        /// <summary>
        /// Event fired when the content is about to be closed
        /// </summary>
        public event EventHandler<EventArgs> Closing;

        /// <summary>
        /// Event fired when the content has been closed
        /// </summary>
        /// <remarks>Note that when a document is closed property like <see cref="ManagedContent.ContainerPane"/> or <see cref="ManagedContent.Manager"/> returns null.</remarks>
        public event EventHandler Closed;
        
        public virtual bool Close()
        {
            if (Closing != null)
            {
                Closing(this, EventArgs.Empty);
            }

            IRegionManager regionManager = ServiceLocator.Current.GetInstance<IRegionManager>();
            regionManager.Regions[CPI.eConnect.PDUMaint.RegionNames.MainRegion].Remove(this);

            if (Closed != null)
            {
                Closed(this, EventArgs.Empty);
            }

            return true;
        }
        
        protected void OnExecuteCommand(object sender, ExecutedRoutedEventArgs e)
        {
            if (e.Command == ManagedContentCommands.Close)
            {
                e.Handled = Close();
            }
        }        
    }
}
