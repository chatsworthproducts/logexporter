﻿using System.Windows;

namespace CPI.eConnect.PDUMaint.Support.MainContent
{
    public class PageInfo : DependencyObject
    {
        // Using a DependencyProperty as the backing store for Title.  This enables animation, styling, binding, etc…
        public static readonly DependencyProperty TitleProperty =
                DependencyProperty.Register("Title", typeof(string), typeof(PageInfo));
        
        // Using a DependencyProperty as the backing store for CanClose.  This enables animation, styling, binding, etc…
        public static readonly DependencyProperty CanCloseProperty =
                DependencyProperty.Register("CanClose", typeof(bool), typeof(PageInfo));

        // Using a DependencyProperty as the backing store for IsModified.  This enables animation, styling, binding, etc…
        public static readonly DependencyProperty IsModifiedProperty =
                DependencyProperty.Register("IsModified", typeof(bool), typeof(PageInfo));
 
        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        } 
    
        public bool CanClose
        {
            get { return (bool)GetValue(CanCloseProperty); }
            set { SetValue(CanCloseProperty, value); }
        }
 
        public bool IsModified
        {
            get { return (bool)GetValue(IsModifiedProperty); }
            set { SetValue(IsModifiedProperty, value); }
        }
    }
}
