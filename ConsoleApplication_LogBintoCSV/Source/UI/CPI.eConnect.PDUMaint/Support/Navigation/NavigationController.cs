﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.Prism.Mvvm;
using System.ComponentModel;
using System.Windows.Input;

namespace CPI.eConnect.PDUMaint.Support.Navigation
{
    public class NavigationController
    {
        private Queue<string> _WorkAreaQueue = new Queue<string>();
        private string _CurrentView;
        private IRegionManager _RegionManager;

        public NavigationController(IRegionManager manager)
        {
            this._RegionManager = manager;
        }

        public void Push(string newWork)
        {            
            _WorkAreaQueue.Enqueue(_CurrentView);
            _CurrentView = newWork;
            this._RegionManager.Regions[CPI.eConnect.PDUMaint.RegionNames.ActionRegion].RequestNavigate(newWork);
        }

        public void Pop()
        {
            _CurrentView = _WorkAreaQueue.Dequeue();
            this._RegionManager.Regions[CPI.eConnect.PDUMaint.RegionNames.ActionRegion].RequestNavigate(_CurrentView);
        }

        public void Clear()
        {
            this._CurrentView = PDUMaint.Navigation.Navigation.MainNavigation;
            _WorkAreaQueue.Clear();
            this._RegionManager.Regions[CPI.eConnect.PDUMaint.RegionNames.ActionRegion].RequestNavigate(_CurrentView);
        }
    }
}
