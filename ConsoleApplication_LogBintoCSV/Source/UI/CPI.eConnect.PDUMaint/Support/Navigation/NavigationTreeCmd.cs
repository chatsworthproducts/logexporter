﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.Practices.Prism;
using Microsoft.Practices.ServiceLocation;

namespace CPI.eConnect.PDUMaint.Support.Navigation
{
    public class NavigationTreeCmd : ICommand, IActiveAware    
    {
        private string _NextNavigationPoint;
        protected readonly Func<object, bool> _canExecuteMethod;
        private bool _IsActive;

        public NavigationTreeCmd(string nextNavigationPoint)
        {
            this._NextNavigationPoint = nextNavigationPoint;
            this._canExecuteMethod = this.DefaultCanExecute;
        }

        public NavigationTreeCmd(string nextNavigationPoint, Func<object, bool> canExecuteMethod)
        {
            this._NextNavigationPoint = nextNavigationPoint;
            this._canExecuteMethod = canExecuteMethod;
        }

        public event EventHandler CanExecuteChanged;
        public event EventHandler IsActiveChanged;

        public bool IsActive
        {
            get
            {
                return _IsActive;
            }
            set
            {
                this._IsActive = value;
                if(this.IsActiveChanged != null)
                {
                    this.IsActiveChanged(this, new EventArgs());
                }
            }
        }
        
        public bool CanExecute(object parameter)
        {
            return this._canExecuteMethod(parameter);
        }

        public void Execute(object parameter)
        {
            ServiceLocator.Current.GetInstance<NavigationController>().Push(this._NextNavigationPoint);
        }

        public void RaiseCanExecuteChanged()
        { 
            if(this.CanExecuteChanged != null)
            {
                this.CanExecuteChanged(this, new EventArgs());
            }
        }

        private bool DefaultCanExecute(object parameter)
        {
            return true;
        }
    }
}
