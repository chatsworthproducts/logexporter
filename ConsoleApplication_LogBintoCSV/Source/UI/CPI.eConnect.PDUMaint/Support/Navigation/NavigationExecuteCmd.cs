﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.Practices.Prism;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Mvvm;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.ServiceLocation;

namespace CPI.eConnect.PDUMaint.Support.Navigation
{
    public class NavigationExecuteCmd : ICommand, IActiveAware
    {
        private string _PageName;
        protected readonly Func<object, bool> _canExecuteMethod;
        private bool _IsActive;

        public NavigationExecuteCmd(string pageName)
        {
            this._PageName = pageName;
            this._canExecuteMethod = this.DefaultCanExecute;
        }

        public NavigationExecuteCmd(string pageName, Func<object, bool> canExecuteMethod)
        {
            this._PageName = pageName;
            this._canExecuteMethod = canExecuteMethod;
        }

        public event EventHandler CanExecuteChanged;
        public event EventHandler IsActiveChanged;

        public bool IsActive
        {
            get
            {
                return _IsActive;
            }
            set
            {
                this._IsActive = value;
                if (this.IsActiveChanged != null)
                {
                    this.IsActiveChanged(this, new EventArgs());
                }
            }
        }

        public bool CanExecute(object parameter)
        {
            return this._canExecuteMethod(parameter);
        }

        public void Execute(object parameter)
        {
            IRegionManager regionManager = ServiceLocator.Current.GetInstance<IRegionManager>();
            regionManager.Regions[CPI.eConnect.PDUMaint.RegionNames.MainRegion].RequestNavigate(this._PageName);
        }

        public void RaiseCanExecuteChanged()
        {
            if (this.CanExecuteChanged != null)
            {
                this.CanExecuteChanged(this, new EventArgs());
            }
        }

        private bool DefaultCanExecute(object parameter)
        {
            return true;
        }
    }
}
