﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.Practices.Prism;
using Microsoft.Practices.ServiceLocation;

namespace CPI.eConnect.PDUMaint.Support.Navigation
{
    public class NavigationBackCmd : ICommand, IActiveAware
    {        
        protected readonly Func<object, bool> _canExecuteMethod;
        private bool _IsActive;

        public NavigationBackCmd()
        {
            this._canExecuteMethod = this.DefaultCanExecute;
        }

        public NavigationBackCmd(Func<object, bool> canExecuteMethod)
        {            
            this._canExecuteMethod = canExecuteMethod;
        }

        public event EventHandler CanExecuteChanged;
        public event EventHandler IsActiveChanged;

        public bool IsActive
        {
            get
            {
                return _IsActive;
            }
            set
            {
                this._IsActive = value;
                if (this.IsActiveChanged != null)
                {
                    this.IsActiveChanged(this, new EventArgs());
                }
            }
        }

        public bool CanExecute(object parameter)
        {
            return this._canExecuteMethod(parameter);
        }

        public void Execute(object parameter)
        {
            ServiceLocator.Current.GetInstance<NavigationController>().Pop();
        }

        public void RaiseCanExecuteChanged()
        {
            if (this.CanExecuteChanged != null)
            {
                this.CanExecuteChanged(this, new EventArgs());
            }
        }

        private bool DefaultCanExecute(object parameter)
        {
            return true;
        }
    }
}
