﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace CPI.eConnect.PDUMaint.Support.Commands
{
    class ManagedContentCommands
    {
        private static object syncRoot = new object();


        private static RoutedUICommand closeCommand = null;

        /// <summary>
        /// This command closes the content
        /// </summary>
        public static RoutedUICommand Close
        {
            get
            {
                lock (syncRoot)
                {
                    if (null == closeCommand)
                    {
                        closeCommand = new RoutedUICommand("Close", "Close", typeof(ManagedContentCommands));
                    }
                }
                return closeCommand;
            }
        }
    }
}
