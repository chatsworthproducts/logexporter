﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.PDUMaint.Navigation
{
    public static class Navigation
    {
        public static string MainNavigation = "Action_Main";
        public static string JigControlNavigation = "Action_Jig";
        public static string MCMControlNavigation = "Action_MCMControl";
        public static string MCMMonitorNavigation = "Action_MCMMonitor";
        public static string TestAutomationNavigation = "Action_TestAutomation";
    }
}
