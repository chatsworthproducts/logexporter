﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.Prism.Mvvm;
using System.ComponentModel;
using System.Windows.Input;

namespace CPI.eConnect.PDUMaint.Navigation.JigControl
{
    public class JigControlNavigationVM : BindableBase
    {
        private Support.Navigation.NavigationBackCmd _BackPageCmd = new Support.Navigation.NavigationBackCmd();
        private Support.Navigation.NavigationExecuteCmd _MCMProgrammerPageCmd = new Support.Navigation.NavigationExecuteCmd(Actions.ActionPage.MCMProgrammerPage);
        private Support.Navigation.NavigationExecuteCmd _DaisyChainPageCmd = new Support.Navigation.NavigationExecuteCmd(Actions.ActionPage.DaisyChainPage);
        private Support.Navigation.NavigationExecuteCmd _PDUTesterPageCmd = new Support.Navigation.NavigationExecuteCmd(Actions.ActionPage.PDUTesterPage);
        
        public ICommand BackPageCmd
        {
            get { return this._BackPageCmd; }
        }

        public ICommand MCMProgrammerPageCmd
        {
            get { return this._MCMProgrammerPageCmd; }
        }

        public ICommand DaisyChainPageCmd
        {
            get { return this._DaisyChainPageCmd; }
        }

        public ICommand PDUTesterPageCmd
        {
            get { return this._PDUTesterPageCmd; }
        }
    }
}
