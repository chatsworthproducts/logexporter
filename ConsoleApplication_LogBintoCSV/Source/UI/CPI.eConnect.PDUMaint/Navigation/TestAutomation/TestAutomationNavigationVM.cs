﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.Prism.Mvvm;
using System.ComponentModel;
using System.Windows.Input;

namespace CPI.eConnect.PDUMaint.Navigation.TestAutomation
{
    public class TestAutomationNavigationVM
    {
        private Support.Navigation.NavigationBackCmd _BackPageCmd = new Support.Navigation.NavigationBackCmd();

        public ICommand BackPageCmd
        {
            get { return this._BackPageCmd; }
        }
    }
}
