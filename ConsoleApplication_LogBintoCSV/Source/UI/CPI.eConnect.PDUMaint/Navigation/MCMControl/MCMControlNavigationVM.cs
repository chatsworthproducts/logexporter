﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.Prism.Mvvm;
using System.ComponentModel;
using System.Windows.Input;

namespace CPI.eConnect.PDUMaint.Navigation.MCMControl
{
    public class MCMControlNavigationVM : BindableBase
    {
        private Support.Navigation.NavigationBackCmd _BackPageCmd = new Support.Navigation.NavigationBackCmd();
        private Support.Navigation.NavigationExecuteCmd _MonitorPageCmd = new Support.Navigation.NavigationExecuteCmd(PDUMaint.Actions.ActionPage.MCMMonitorPage);
        private Support.Navigation.NavigationExecuteCmd _CalibratePageCmd = new Support.Navigation.NavigationExecuteCmd(PDUMaint.Actions.ActionPage.MCMCalibratePage);

        public ICommand BackPageCmd
        {
            get { return this._BackPageCmd; }
        }

        public ICommand MonitorPageCmd
        {
            get { return this._MonitorPageCmd; }
        }

        public ICommand CalibratePageCmd
        {
            get { return this._CalibratePageCmd; }
        }        
    }
}
