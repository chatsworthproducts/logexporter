﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CPI.eConnect.PDUMaint.Navigation.MCMControl
{
    /// <summary>
    /// Interaction logic for MCMControlNavigation.xaml
    /// </summary>
    public partial class MCMControlNavigation : UserControl
    {
        public MCMControlNavigation()
        {
            InitializeComponent();
        }
    }
}
