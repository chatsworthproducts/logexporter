﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.Prism.Mvvm;
using System.ComponentModel;
using System.Windows.Input;

namespace CPI.eConnect.PDUMaint.Navigation
{
    public class MainNavigationVM : BindableBase
    {
        private Support.Navigation.NavigationExecuteCmd _MCMMonitorPageCmd = new Support.Navigation.NavigationExecuteCmd(Actions.ActionPage.MCMMonitorPage);
        private Support.Navigation.NavigationTreeCmd _MCMControlPageCmd = new Support.Navigation.NavigationTreeCmd(Navigation.MCMControlNavigation);
        private Support.Navigation.NavigationTreeCmd _JigControlPageCmd = new Support.Navigation.NavigationTreeCmd(Navigation.JigControlNavigation);
        private Support.Navigation.NavigationTreeCmd _TestAutoPageCmd = new Support.Navigation.NavigationTreeCmd(Navigation.TestAutomationNavigation);
        private Support.Navigation.NavigationExecuteCmd _ConfigPageCmd = new Support.Navigation.NavigationExecuteCmd(Actions.ActionPage.ConfigurationPage);
        private DelegateCommand<object> _ExitCmd;
        
        public MainNavigationVM()
        {
            _ExitCmd = new DelegateCommand<object>(this.DoExit);
        }

        public ICommand MCMControlPageCmd
        {
            get { return _MCMControlPageCmd; }
        }

        public ICommand MCMMonitorPageCmd
        {
            get { return _MCMMonitorPageCmd; }
        }

        public ICommand JigControlPageCmd
        {
            get { return _JigControlPageCmd; }
        }

        public ICommand TestAutoPageCmd
        {
            get { return _TestAutoPageCmd; }
        }

        public ICommand ConfigPageCmd
        {
            get { return _ConfigPageCmd; }
        }

        public ICommand ExitCmd
        {
            get { return _ExitCmd; }
        }

        private void DoExit(object parameter)
        {
            App.Current.Shutdown();
        }
    }
}
