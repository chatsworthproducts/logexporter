﻿using System;
using System.Configuration;
using System.Management;
using System.Reflection;
using System.Security.Principal;
using System.Windows;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.UnityExtensions;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;

namespace CPI.eConnect.PDUMaint
{
    public partial class PDUMaintBootStrapper : UnityBootstrapper
    {
        private static WindowsIdentity Identity;
        private static WindowsPrincipal Principal;

        protected override Microsoft.Practices.Prism.Logging.ILoggerFacade CreateLogger()
        {
            return new EnterpriseLogAdapter();
        }

        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();
            
            // Navigation Registration
            this.Container.RegisterType<Object, Navigation.MainNavigation>(Navigation.Navigation.MainNavigation);
            this.Container.RegisterType<Object, Navigation.JigControl.JigControlNavigation>(Navigation.Navigation.JigControlNavigation);
            this.Container.RegisterType<Object, Navigation.MCMControl.MCMControlNavigation>(Navigation.Navigation.MCMControlNavigation);
            this.Container.RegisterType<Object, Navigation.TestAutomation.TestAutomationNavigation>(Navigation.Navigation.TestAutomationNavigation);
                        
            // Page Registration
            this.Container.RegisterType<Object, Actions.ConfigurePage>(Actions.ActionPage.ConfigurationPage);
            this.Container.RegisterType<Object, Actions.MCMCalibratePage>(Actions.ActionPage.MCMCalibratePage);
            this.Container.RegisterType<Object, Actions.MCMMonitorPage>(Actions.ActionPage.MCMMonitorPage);
            this.Container.RegisterType<Object, Actions.DaisyChainControl>(Actions.ActionPage.DaisyChainPage);
            this.Container.RegisterType<Object, Actions.MCMProgrammer>(Actions.ActionPage.MCMProgrammerPage);
            this.Container.RegisterType<Object, Actions.PDUTester>(Actions.ActionPage.PDUTesterPage);

            // Support Registration
            this.Container.RegisterType<Support.Navigation.NavigationController, Support.Navigation.NavigationController>(new ContainerControlledLifetimeManager());

            // View-Modle Registration
            this.Container.RegisterType<Navigation.MainNavigationVM, Navigation.MainNavigationVM>();
            this.Container.RegisterType<Navigation.MCMControl.MCMControlNavigationVM, Navigation.MCMControl.MCMControlNavigationVM>();
            this.Container.RegisterType<Navigation.JigControl.JigControlNavigationVM, Navigation.JigControl.JigControlNavigationVM>();
            this.Container.RegisterType<Navigation.TestAutomation.TestAutomationNavigationVM, Navigation.TestAutomation.TestAutomationNavigationVM>();
            
            Identity = WindowsIdentity.GetCurrent();
            Principal = new WindowsPrincipal(Identity);
            this.Container.RegisterInstance<IUnityContainer>(this.Container);
            
            EstablishLog();
        }

        protected override System.Windows.DependencyObject CreateShell()
        {
            return this.Container.Resolve<MainWindow>();
        }

        protected override void InitializeShell()
        {
            base.InitializeShell();            

            Application.Current.MainWindow = (MainWindow)this.Shell;
            Application.Current.MainWindow.Show();
            this.Container.Resolve<Support.Navigation.NavigationController>().Clear();
        }

        private void EstablishLog()
        {               
            Logger.Log("Log intiailizing...", Microsoft.Practices.Prism.Logging.Category.Info, 
                Microsoft.Practices.Prism.Logging.Priority.Low);
            Logger.Log(string.Format("{0} startup log. Build {1}", SpecialText.AssemblyTitle, SpecialText.AssemblyVersion), 
                Microsoft.Practices.Prism.Logging.Category.Info, Microsoft.Practices.Prism.Logging.Priority.Low);
            Logger.Log(string.Format("{0} startup {1} {2}", Assembly.GetEntryAssembly().Location, DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString()),
                Microsoft.Practices.Prism.Logging.Category.Info, Microsoft.Practices.Prism.Logging.Priority.Low);

            // This is the fastest way to determine the user's role, using Directory Services
            // or WMI takes a long time and will add to the total application launch time.
            string userGroup = string.Empty;
            if (Principal.IsInRole(WindowsBuiltInRole.Administrator))
                userGroup = WindowsBuiltInRole.Administrator.ToString();
            else if (Principal.IsInRole(WindowsBuiltInRole.PowerUser))
                userGroup = WindowsBuiltInRole.PowerUser.ToString();
            else if (Principal.IsInRole(WindowsBuiltInRole.BackupOperator))
                userGroup = WindowsBuiltInRole.BackupOperator.ToString();
            else if (Principal.IsInRole(WindowsBuiltInRole.Replicator))
                userGroup = WindowsBuiltInRole.Replicator.ToString();
            else if (Principal.IsInRole(WindowsBuiltInRole.User))
                userGroup = WindowsBuiltInRole.User.ToString();
            else if (Principal.IsInRole(WindowsBuiltInRole.Guest))
                userGroup = WindowsBuiltInRole.Guest.ToString();
            Logger.Log(string.Format("Windows User: {0} Group: {1}", Identity.Name, userGroup),
                Microsoft.Practices.Prism.Logging.Category.Info, Microsoft.Practices.Prism.Logging.Priority.Low);

            Logger.Log(string.Format("OS: {0}", SpecialText.OSNameInfo), 
                Microsoft.Practices.Prism.Logging.Category.Info, Microsoft.Practices.Prism.Logging.Priority.Low);
            Logger.Log(string.Format("OS Architecture: {0}", SpecialText.OSArchitecture), 
                Microsoft.Practices.Prism.Logging.Category.Info, Microsoft.Practices.Prism.Logging.Priority.Low);

            string totalPhysicalMemory;
            string manufacturer;
            string model;
            string processorName;

            GetComputerInfo(out totalPhysicalMemory, out manufacturer, out model);
            GetProcessorName(out processorName);

            Logger.Log(string.Format("Total Physical Memory: {0}", totalPhysicalMemory), 
                Microsoft.Practices.Prism.Logging.Category.Info, Microsoft.Practices.Prism.Logging.Priority.Low);
            Logger.Log(string.Format("Manufacturer: {0} (Model: {1})", manufacturer, model), 
                Microsoft.Practices.Prism.Logging.Category.Info, Microsoft.Practices.Prism.Logging.Priority.Low);
            Logger.Log(string.Format("Processor Name: {0}", processorName), 
                Microsoft.Practices.Prism.Logging.Category.Info, Microsoft.Practices.Prism.Logging.Priority.Low);

            Logger.Log(string.Format("PROCESSOR_ARCHITECTURE: {0}     PROCESSOR_ARCHITEW6432: {1}",
                Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE"), Environment.GetEnvironmentVariable("PROCESSOR_ARCHITEW6432")), 
                Microsoft.Practices.Prism.Logging.Category.Info, Microsoft.Practices.Prism.Logging.Priority.Low);

            Logger.Log(string.Format("Region: {0}", System.Threading.Thread.CurrentThread.CurrentCulture.Name), 
                Microsoft.Practices.Prism.Logging.Category.Info, Microsoft.Practices.Prism.Logging.Priority.Low);
        }

        private static void GetComputerInfo(out string totalPhysicalMemory, out string manufacturer, out string model)
        {
            totalPhysicalMemory = string.Empty;
            manufacturer = string.Empty;
            model = string.Empty;
            try
            {
                System.Management.ManagementObjectSearcher objSearcher = new System.Management.ManagementObjectSearcher("SELECT * FROM Win32_ComputerSystem");
                System.Management.ManagementObjectCollection objColl = objSearcher.Get();

                foreach (var mgtObject in objColl)
                {
                    totalPhysicalMemory = mgtObject["totalphysicalmemory"].ToString();
                    manufacturer = mgtObject["manufacturer"].ToString();
                    model = mgtObject["model"].ToString();
                }
            }
            catch (Exception)
            {
            }
        }

        public static void GetProcessorName(out string processorName)
        {
            processorName = string.Empty;

            try
            {
                System.Management.ManagementObjectSearcher objSearcher = new System.Management.ManagementObjectSearcher("SELECT * FROM Win32_Processor");
                System.Management.ManagementObjectCollection objColl = objSearcher.Get();

                foreach (var mgtObject in objColl)
                {
                    processorName = mgtObject["name"].ToString();
                }
            }
            catch (Exception)
            {
            }
        }
    }
}
