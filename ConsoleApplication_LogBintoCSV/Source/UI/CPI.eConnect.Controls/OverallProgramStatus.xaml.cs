﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace CPI.eConnect.Controls
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class OverallProgramStatus : UserControl, INotifyPropertyChanged
    {
        private StatusController _ProgramSBF;
        private StatusController _ProgramBios;
        private StatusController _BootMCM;
        private StatusController _SetDefaults;
        public OverallProgramStatus()
        {
            InitializeComponent();
            this.rootGrid.DataContext = this;
            this.ProgramSBF = new StatusController("Serial Boot Flash", "Waiting...", Visibility.Hidden);
            this.ProgramBIOS = new StatusController("Programming BIOS", "Waiting...", Visibility.Hidden);
            this.BootMCM = new StatusController("Booting MCM", "Waiting...", Visibility.Hidden);
            this.SetDefaults = new StatusController("Setting Defaults", "Waiting...", Visibility.Hidden);
            this.StatusString = new ObservableCollection<string>();
        }

        public StatusController ProgramSBF
        {
            get
            {
                return this._ProgramSBF;
            }

            set
            {
                this._ProgramSBF = value;
                RaisePropertyChanged("ProgramSBF");
            }
        }

        public StatusController ProgramBIOS
        {
            get
            {
                return this._ProgramBios;
            }

            set
            {
                this._ProgramBios = value;
                RaisePropertyChanged("ProgramBIOS");
            }
        }

        public StatusController BootMCM
        {
            get
            {
                return this._BootMCM;
            }

            set
            {
                this._BootMCM = value;
                RaisePropertyChanged("BootMCM");
            }
        }

        public StatusController SetDefaults
        {
            get
            {
                return this._SetDefaults;
            }

            set
            {
                this._SetDefaults = value;
                RaisePropertyChanged("SetDefaults");
            }
        }

        public ObservableCollection<string> StatusString
        {
            get;
            set;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
