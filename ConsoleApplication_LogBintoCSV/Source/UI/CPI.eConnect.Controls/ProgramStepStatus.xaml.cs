﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace CPI.eConnect.Controls
{
    public enum StepStatus
    {
        Waiting = 0,
        Downloading = 1,
        Error = 2,
        Complete = 3
    }

    /// <summary>
    /// Interaction logic for ProgramStepStatus.xaml
    /// </summary>
    public partial class ProgramStepStatus : UserControl
    {
        public static readonly DependencyProperty StatusProperty =
            DependencyProperty.Register("Status", typeof(StepStatus), typeof(ProgramStepStatus), 
            new PropertyMetadata(new PropertyChangedCallback(Status_PropertyChanged)));
        public static readonly DependencyProperty StatusImgProperty =
            DependencyProperty.Register("StatusImg", typeof(string), typeof(ProgramStepStatus), new PropertyMetadata("Resources/Wait.png"));
        public static readonly DependencyProperty NumberStepsProperty =
            DependencyProperty.Register("NumberSteps", typeof(int), typeof(ProgramStepStatus), new PropertyMetadata(10));
        public static readonly DependencyProperty ProgressVisibilityProperty =
            DependencyProperty.Register("ProgressVisibility", typeof(Visibility), typeof(ProgramStepStatus), new PropertyMetadata(Visibility.Visible));
        public static readonly DependencyProperty CurrentStepProperty =
            DependencyProperty.Register("CurrentStep", typeof(int), typeof(ProgramStepStatus), new PropertyMetadata(1));
        public static readonly DependencyProperty StepNameProperty =
            DependencyProperty.Register("StepName", typeof(string), typeof(ProgramStepStatus), new PropertyMetadata("Step Name"));
        public static readonly DependencyProperty StepTextProperty =
            DependencyProperty.Register("StepText", typeof(string), typeof(ProgramStepStatus), new PropertyMetadata("Step Info"));

        public ProgramStepStatus()
        {            
            InitializeComponent();
            rootGrid.DataContext = this;
            this.ProgressVisibility = Visibility.Hidden;
            this.NumberSteps = 10;
            this.Status = StepStatus.Waiting;
            this.CurrentStep = 0;
            this.StepName = "Step Name";
            this.StepText = "Step Info";
        }

        public StepStatus Status
        {
            get
            {
                return (StepStatus)GetValue(StatusProperty);
            }

            set
            {
                SetValue(StatusProperty, value);
            }
        }

        public string StatusImg
        {
            get
            {
                return (string)GetValue(StatusImgProperty);
            }

            set
            {
                SetValue(StatusImgProperty, value);
            }
        }

        public int NumberSteps
        {
            get
            {
                return (int)GetValue(NumberStepsProperty);
            }

            set
            {
                SetValue(NumberStepsProperty, value);
            }
        }

        public Visibility ProgressVisibility
        {
            get
            {
                return (Visibility)GetValue(ProgressVisibilityProperty);
            }

            set
            {
                SetValue(ProgressVisibilityProperty, value);
            }
        }

        public int CurrentStep
        {
            get
            {
                return (int)GetValue(CurrentStepProperty);
            }

            set
            {
                SetValue(CurrentStepProperty, value);
            }
        }

        public string StepName
        {
            get
            {
                return (string)GetValue(StepNameProperty);
            }

            set
            {
                SetValue(StepNameProperty, value);
            }
        }

        public string StepText
        {
            get
            {
                return (string)GetValue(StepTextProperty);
            }

            set
            {
                SetValue(StepTextProperty, value);
            }
        }

        private static void Status_PropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ProgramStepStatus cntrl = (ProgramStepStatus)obj;

            switch((StepStatus)e.NewValue)
            {
                case StepStatus.Complete:
                    cntrl.StatusImg = "Resources/Ok.png";
                    cntrl.ProgressVisibility = Visibility.Hidden;
                    break;
                case StepStatus.Downloading:
                    cntrl.StatusImg = "Resources/Download.png";
                    cntrl.ProgressVisibility = Visibility.Visible;
                    break;
                case StepStatus.Error:
                    cntrl.StatusImg = "Resources/Minus.png";
                    cntrl.ProgressVisibility = Visibility.Visible;
                    break;
                case StepStatus.Waiting:
                    cntrl.StatusImg = "Resources/Wait.png";
                    cntrl.ProgressVisibility = Visibility.Hidden;
                    break;
            }
        }
    }
}
