﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace CPI.eConnect.Controls
{
    public class StatusController : DependencyObject, INotifyPropertyChanged
    {
        public static readonly DependencyProperty StatusProperty =
            DependencyProperty.Register("Status", typeof(StepStatus), typeof(StatusController),
            new PropertyMetadata(StepStatus.Waiting));
        public static readonly DependencyProperty NumberStepsProperty =
            DependencyProperty.Register("NumberSteps", typeof(int), typeof(StatusController), new PropertyMetadata(10));
        public static readonly DependencyProperty CurrentStepProperty =
            DependencyProperty.Register("CurrentStep", typeof(int), typeof(StatusController), new PropertyMetadata(1));
        public static readonly DependencyProperty StepNameProperty =
            DependencyProperty.Register("StepName", typeof(string), typeof(StatusController), new PropertyMetadata("Step Name"));
        public static readonly DependencyProperty StepTextProperty =
            DependencyProperty.Register("StepText", typeof(string), typeof(StatusController), new PropertyMetadata("Step Info"));
        public static readonly DependencyProperty VisibleProperty =
            DependencyProperty.Register("Visible", typeof(Visibility), typeof(StatusController), new PropertyMetadata(Visibility.Collapsed));

        public StatusController(string step, string text, Visibility vis)
        {
            this.StepName = step;
            this.StepText = text;
            this.Visible = vis;
        }

        public Visibility Visible
        {
            get
            {
                return (Visibility)GetValue(VisibleProperty);
            }

            set
            {
                SetValue(VisibleProperty, value);
                RaisePropertyChanged("Visible");
            }
        }

        public StepStatus Status
        {
            get
            {
                return (StepStatus)GetValue(StatusProperty);
            }

            set
            {
                SetValue(StatusProperty, value);
                RaisePropertyChanged("Status");
            }
        }

        public int NumberSteps
        {
            get
            {
                return (int)GetValue(NumberStepsProperty);
            }

            set
            {
                SetValue(NumberStepsProperty, value);
                RaisePropertyChanged("NumberSteps");
            }
        }

        public int CurrentStep
        {
            get
            {
                return (int)GetValue(CurrentStepProperty);
            }

            set
            {
                SetValue(CurrentStepProperty, value);
                RaisePropertyChanged("CurrentStep");
            }
        }

        public string StepName
        {
            get
            {
                return (string)GetValue(StepNameProperty);
            }

            set
            {
                SetValue(StepNameProperty, value);
                RaisePropertyChanged("StepName");
            }
        }

        public string StepText
        {
            get
            {
                return (string)GetValue(StepTextProperty);
            }

            set
            {
                SetValue(StepTextProperty, value);
                RaisePropertyChanged("StepText");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
