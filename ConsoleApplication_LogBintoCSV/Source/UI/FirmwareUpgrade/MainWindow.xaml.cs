﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CPI.eConnect.FirmwareUpgrade
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IChangeWorkArea
    {
        private Queue<object> _WorkAreaQueue = new Queue<object>();

        public MainWindow()
        {
            InitializeComponent();
            this.Clear();
            App MCMProgrammerApp = ((App)Application.Current);
            MCMProgrammerApp.WorkArea = this;
        }

        private void OnCloseApp(object sender, RoutedEventArgs e)
        {
            App.Current.Shutdown();
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            // Begin dragging the window
            this.DragMove();
        }

        public void Push(object newWork)
        {
            _WorkAreaQueue.Enqueue(this.WorkArea.Content);
            this.WorkArea.Content = newWork;
        }

        public void Pop()
        {
            this.WorkArea.Content = _WorkAreaQueue.Dequeue();
        }

        public void Clear()
        {
            this.WorkArea.Content = new MainMenu();
            _WorkAreaQueue.Clear();
        }
    }
}
