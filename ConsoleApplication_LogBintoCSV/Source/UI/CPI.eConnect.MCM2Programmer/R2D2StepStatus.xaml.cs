﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CPI.eConnect.MCM2Programmer
{
    /// <summary>
    /// Interaction logic for R2D2StepStatus.xaml
    /// </summary>
    public partial class R2D2StepStatus : UserControl, IMessage
    {
        private R2D2Programmer _Programmer;
        private object _RootContent;
        
        public R2D2StepStatus(int jigid, string comport)
        {
            _Programmer = new R2D2Programmer(jigid, comport, this.Dispatcher, this);
            InitializeComponent();
            this.rootContent.DataContext = _Programmer;
            this.DataContext = _Programmer;
            this._RootContent = this.rootContent.Content;
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            _Programmer.Start();
        }

        private void OnAbortProgramming(object sender, RoutedEventArgs e)
        {
            _Programmer.Abort();
        }

        public bool ActiveMessage { get; set; }
        
        public void DisplayMessage(string message)
        {
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => this.rootContent.Content = new Message(message)));
            this.ActiveMessage = true;
        }

        public void DismissMessage()
        {
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => this.rootContent.Content = this._RootContent));
            this.ActiveMessage = false;
        }
    }
}
