﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPI.eConnect.Programmer;

namespace CPI.eConnect.MCM2Programmer
{
    class R2D2Stage1Programmer : MCMWatcher
    {
        public const int TOTAL_STEPS = 51;

        public int Execute()
        {
            _Sequence.Add(new SerialSequence("SF: Done to load image", new ProgressStatusEventArgs("Loading Uboot...", 1, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("CPU: SAMA5D34", new ProgressStatusEventArgs("Running Uboot...", 2, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Hit any key to stop autoboot: ", new ProgressStatusEventArgs("Running Uboot...", 3, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("USB0:   USB EHCI 1.00", new ProgressStatusEventArgs("Running Uboot...", 4, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("scanning bus 0 for devices...", new ProgressStatusEventArgs("Scanning for USB...", 5, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("scanning usb for storage devices...", new ProgressStatusEventArgs("Scanning for USB...", 6, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("reading kr", new ProgressStatusEventArgs("USB Found, loading...", 7, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("reading dtb", new ProgressStatusEventArgs("USB Found, loading...", 8, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("## Booting kernel from Legacy Image at 2a000000 ...", new ProgressStatusEventArgs("Booting kernel...", 9, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("## Flattened Device Tree blob at 20f00000", new ProgressStatusEventArgs("Booting kernel...", 10, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Starting kernel ...", new ProgressStatusEventArgs("Booting kernel...", 11, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Uncompressing Linux... done, booting the kernel.", new ProgressStatusEventArgs("Booting kernel...", 12, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Booting Linux on physical CPU 0x0", new ProgressStatusEventArgs("Booting kernel...", 13, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Console: colour dummy device 80x30", new ProgressStatusEventArgs("Booting kernel...", 14, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("DMA: preallocated 256 KiB pool for atomic coherent allocations", new ProgressStatusEventArgs("Booting kernel...", 15, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("RPC: Registered tcp NFSv4.1 backchannel transport module.", new ProgressStatusEventArgs("Booting kernel...", 16, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("NET: Registered protocol family 10", new ProgressStatusEventArgs("Booting kernel...", 17, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Waiting 5 sec before mounting root device...", new ProgressStatusEventArgs("Booting kernel...", 18, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("scsi 0:0:0:0: Direct-Access     Lexar    USB Flash Drive  1100 PQ: 0 ANSI: 4", new ProgressStatusEventArgs("Booting kernel...", 19, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("sd 0:0:0:0: [sda] Attached SCSI removable disk", new ProgressStatusEventArgs("Booting kernel...", 20, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("EXT2-fs (sda2): warning: mounting unchecked fs, running e2fsck is recommended", new ProgressStatusEventArgs("Booting kernel...", 21, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("INIT: version 2.88 booting", new ProgressStatusEventArgs("Booting kernel...", 22, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Waiting for /dev to be fully pop", new ProgressStatusEventArgs("Booting kernel...", 23, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Activating swap", new ProgressStatusEventArgs("Booting kernel...", 24, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Cleaning up temporary files", new ProgressStatusEventArgs("Booting kernel...", 25, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Loading kernel module g_serial.", new ProgressStatusEventArgs("Booting kernel...", 26, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Checking file systems...fsck from util-linux 2.20.1", new ProgressStatusEventArgs("Booting kernel...", 27, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Mounting local filesystems", new ProgressStatusEventArgs("Booting kernel...", 28, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Activating swapfile swap", new ProgressStatusEventArgs("Booting kernel...", 29, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Cleaning up temporary files", new ProgressStatusEventArgs("Booting kernel...", 30, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Setting kernel variables", new ProgressStatusEventArgs("Booting kernel...", 31, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Configuring network interfaces", new ProgressStatusEventArgs("Booting kernel...", 32, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Cleaning up temporary files", new ProgressStatusEventArgs("Booting kernel...", 33, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("INIT: Entering runlevel: 2", new ProgressStatusEventArgs("Booting kernel...", 34, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Using makefile-style concurrent boot in runlevel 2.", new ProgressStatusEventArgs("Booting kernel...", 35, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Test of MCM-R2D2 Board Software Load Script", new ProgressStatusEventArgs("Programming eMMC...", 36, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("/boot", new ProgressStatusEventArgs("Programming eMMC...", 37, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("%This will overwrite any existing data on the device", new ProgressStatusEventArgs("Programming eMMC...", 38, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Checking that no-one is using this disk right now ...", new ProgressStatusEventArgs("Programming eMMC...", 39, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Disk /dev/mmcblk0: 118016 cylinders, 4 heads, 16 sectors/track", new ProgressStatusEventArgs("Programming eMMC...", 40, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Old situation:", new ProgressStatusEventArgs("Programming eMMC...", 41, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("New situation:", new ProgressStatusEventArgs("Programming eMMC...", 42, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Successfully wrote the new partition table", new ProgressStatusEventArgs("Programming eMMC...", 43, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Re-reading the partition table ...", new ProgressStatusEventArgs("Programming eMMC...", 44, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("%Creating DOS file system on partition 1 of the eMMC device", new ProgressStatusEventArgs("Programming eMMC...", 45, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("%Creating mount point for eMMC partition 1 as /media/kernel", new ProgressStatusEventArgs("Programming eMMC...", 46, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("%Attempting to mount eMMC partition 1 as /media/kernel", new ProgressStatusEventArgs("Programming eMMC...", 47, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("%Copying boot files to partition 1 of eMMC", new ProgressStatusEventArgs("Programming eMMC...", 48, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("%Flushing buffers to complete file copy", new ProgressStatusEventArgs("Programming eMMC...", 49, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("%Unmounting partition 1 of the eMMC", new ProgressStatusEventArgs("Programming eMMC...", 50, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("%Init of MCM-R2D2 board complete", new ProgressStatusEventArgs("Programming eMMC...", 51, R2D2Stage1Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("%Turn test jig power off and remove board", new ProgressStatusEventArgs("Restart.", 52, R2D2Stage1Programmer.TOTAL_STEPS)));

            this.NotifyStatus("Waiting for power up...", 0, R2D2Stage1Programmer.TOTAL_STEPS);

            return this.Watch();
        }        
    }
}
