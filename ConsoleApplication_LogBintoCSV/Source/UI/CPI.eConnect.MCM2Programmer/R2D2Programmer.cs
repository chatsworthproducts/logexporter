﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.ComponentModel;
using CPI.eConnect.Communications;
using CPI.eConnect.Programmer;

namespace CPI.eConnect.MCM2Programmer
{
    public class R2D2Programmer : INotifyPropertyChanged
    {
        private string _JigID;
        private string _StepTitle;
        private int _NumberSteps;
        private int _CurrentStep;
        private string _StepText;

        public const int PROGRAMMER_TIMEOUT = 1;

        public string JigID 
        { 
            get
            {
                return this._JigID;
            }

            set
            {
                this._JigID = value;
                RaisePropertyChanged("JigID");
            }
        }

        public string StepTitle
        {
            get
            {
                return this._StepTitle;
            }

            set
            {
                this._StepTitle = value;
                RaisePropertyChanged("StepTitle");
            }
        }
        
        public int NumberSteps
        {
            get
            {
                return this._NumberSteps;
            }

            set
            {
                this._NumberSteps = value;
                RaisePropertyChanged("NumberSteps");
            }
        }

        public int CurrentStep
        {
            get
            {
                return this._CurrentStep;
            }

            set
            {
                this._CurrentStep = value;
                RaisePropertyChanged("CurrentStep");
            }
        }

        public string StepText
        {
            get
            {
                return this._StepText;
            }

            set
            {
                this._StepText = value;
                RaisePropertyChanged("StepText");
            }
        }

        private System.Threading.Thread _ProgrammingThread;
        private string ComPort;
        private System.Threading.ManualResetEvent _AbortEvent = new System.Threading.ManualResetEvent(false);
        private System.Threading.ManualResetEvent _CloseEvent = new System.Threading.ManualResetEvent(false);
        private IMessage _MessageService;
        public Dispatcher Dispatcher { get; private set; }

        public R2D2Programmer(int jigid, string comport, Dispatcher disp, IMessage msgSvc)
        {
            this.JigID = jigid.ToString();
            this.ComPort = comport;
            this.Dispatcher = disp;
            this._MessageService = msgSvc;
            AppNotifications.Instance.AppClosing += AppClosing;
        }

        void AppClosing(object sender, object e)
        {
            Close();
        }

        public void Start()
        {
            _ProgrammingThread = new System.Threading.Thread(new System.Threading.ThreadStart(this.Program));
            _ProgrammingThread.Start();
        }

        public void Close()
        {
            this._CloseEvent.Set();
            this._AbortEvent.Set();
            this._ProgrammingThread.Join();
        }

        public void Abort()
        {
            this._AbortEvent.Set();            
        }

        public void Program()
        {
            MCMBiosController controller;

            controller = new MCMBiosController();
            controller.PortName = this.ComPort;
            controller.NewData += new EventHandler<OnSerialDataEventArgs>(this.MCMResponseHandler);
            controller.Connect();

            this._MessageService.DisplayMessage("Place board on jig, plugin serial port cable and USB stick. Flip programming switch and then power on.");
            while (this._CloseEvent.WaitOne(0) == false)
            {
                // do stage 1 programming
                int programmingReturn = DoStage1Programming(controller);
                if (programmingReturn == R2D2Programmer.PROGRAMMER_TIMEOUT)
                {
                    this._AbortEvent.Reset();
                    continue;
                }
                else if ((programmingReturn != 0) && (this._AbortEvent.WaitOne(0) == true))
                {
                    this._MessageService.DisplayMessage("Programming aborted. Place board on jig, plugin serial port cable and USB stick. Flip programming switch and then power on.");
                    this._AbortEvent.Reset();
                    continue;
                }
                else if (programmingReturn != 0)
                {
                    // display a message box indicating that they need to restart
                    //  the operation.
                    this._MessageService.DisplayMessage("Programming failed, retry the programming operation. Ensure the board is secure in the jig, the serial port cable is plugged in, the USB stick is seated, and programming switch is up.");
                    continue;
                }

                this._MessageService.DisplayMessage("Power off the jig, flip the programming switch, and remove the USB stick.  Then power back on.");
                while (this._AbortEvent.WaitOne(0) == false)
                {
                    // do stage 2 programming
                    programmingReturn = DoStage2Programming(controller);
                    if ((programmingReturn == R2D2Programmer.PROGRAMMER_TIMEOUT) && (this._AbortEvent.WaitOne(0) == true))
                    {
                        break;
                    }
                    else if (programmingReturn == R2D2Programmer.PROGRAMMER_TIMEOUT)
                    {
                        continue;
                    }
                    else if ((programmingReturn != 0) && (this._AbortEvent.WaitOne(0) == true))
                    {
                        break;
                    }
                    else if (programmingReturn != 0)
                    {
                        // display a message box indicating that they need to restart
                        //  the operation.
                        this._MessageService.DisplayMessage("Programming failed. Ensure the board is secure in the jig, the serial port cable is plugged in, the USB stick is removed, and programming switch is down. Try the operation again.");
                        continue;
                    }
                    else
                    {
                        // everything went ok, so make like a tree and leave...
                        break;
                    }
                }

                if ((programmingReturn != 0) && (this._AbortEvent.WaitOne(0) == true))
                {
                    this._MessageService.DisplayMessage("Programming aborted. Place board on jig, plugin serial port cable and USB stick. Flip programming switch and then power on.");
                    this._AbortEvent.Reset();
                    continue;
                }

                // do stage 3 programming
                programmingReturn = DoStage3Programming(controller);
                if (programmingReturn == R2D2Programmer.PROGRAMMER_TIMEOUT)
                {
                    this._AbortEvent.Reset();
                    continue;
                }
                else if ((programmingReturn != 0) && (this._AbortEvent.WaitOne(0) == true))
                {
                    this._MessageService.DisplayMessage("Programming aborted. Place board on jig, plugin serial port cable and USB stick. Flip programming switch and then power on.");
                    this._AbortEvent.Reset();
                    continue;
                }
                else if (programmingReturn != 0)
                {
                    // display a message box indicating that they need to restart
                    //  the operation.
                    this._MessageService.DisplayMessage("Programming failed, retry the programming operation. Ensure the board is secure in the jig, the serial port cable is plugged in, the USB stick is seated, and programming switch is up.");
                    continue;
                }

                // do stage 4 programming
                programmingReturn = DoStage4Programming(controller);
                if (programmingReturn == R2D2Programmer.PROGRAMMER_TIMEOUT)
                {
                    this._AbortEvent.Reset();
                    continue;
                }
                else if ((programmingReturn != 0) && (this._AbortEvent.WaitOne(0) == true))
                {
                    this._MessageService.DisplayMessage("Programming aborted. Place board on jig, plugin serial port cable and USB stick. Flip programming switch and then power on.");
                    this._AbortEvent.Reset();
                    continue;
                }
                else if (programmingReturn != 0)
                {
                    // display a message box indicating that they need to restart
                    //  the operation.
                    this._MessageService.DisplayMessage("Programming failed, retry the programming operation. Ensure the board is secure in the jig, the serial port cable is plugged in, the USB stick is seated, and programming switch is up.");
                    continue;
                }

                this._MessageService.DisplayMessage("Programming complete. Power off, remove board, and place next board on jig, plugin serial port cable and USB stick. Flip programming switch and then power on.");
            }

            controller.Close();            
        }

        private int DoStage1Programming(MCMBiosController controller)
        {
            int returnValue = 0;
            R2D2Stage1Programmer programmer = new R2D2Stage1Programmer();
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => this.StepTitle = "Stage 1 Programming"));
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => this.NumberSteps = R2D2Stage1Programmer.TOTAL_STEPS));
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => this.CurrentStep = 0));
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => this.StepText = string.Empty));
            programmer.AbortEvent = this._AbortEvent;
            programmer.StatusEvent += new EventHandler<ProgressStatusEventArgs>(ProgressHandler);
            programmer.Setup(controller);
            returnValue = programmer.Execute();
            if ((returnValue != 0) &&
                (this.CurrentStep == 0))
            {
                returnValue = PROGRAMMER_TIMEOUT;
            }
            
            programmer.StatusEvent -= new EventHandler<ProgressStatusEventArgs>(ProgressHandler);
            programmer.Dispose();
            
            return returnValue;
        }

        private int DoStage2Programming(MCMBiosController controller)
        {
            int returnValue = 0;
            R2D2Stage2Programmer programmer = new R2D2Stage2Programmer();
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => this.StepTitle = "Stage 2 Programming"));
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => this.NumberSteps = R2D2Stage2Programmer.TOTAL_STEPS));
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => this.CurrentStep = 0));
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => this.StepText = string.Empty));
            programmer.AbortEvent = this._AbortEvent;
            programmer.StatusEvent += new EventHandler<ProgressStatusEventArgs>(ProgressHandler);
            programmer.Setup(controller);
            returnValue = programmer.Execute();
            if ((returnValue != 0) &&
                (this.CurrentStep == 0))
            {
                returnValue = PROGRAMMER_TIMEOUT;
            }

            programmer.StatusEvent -= new EventHandler<ProgressStatusEventArgs>(ProgressHandler);
            programmer.Dispose();

            return returnValue;
        }

        private int DoStage3Programming(MCMBiosController controller)
        {
            int returnValue = 0;
            R2D2Stage3Programmer programmer = new R2D2Stage3Programmer();
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => this.StepTitle = "Stage 3 Programming"));
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => this.NumberSteps = R2D2Stage3Programmer.TOTAL_STEPS));
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => this.CurrentStep = 0));
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => this.StepText = string.Empty));
            programmer.AbortEvent = this._AbortEvent;
            programmer.StatusEvent += new EventHandler<ProgressStatusEventArgs>(ProgressHandler);
            programmer.Setup(controller);
            returnValue = programmer.Execute();
            if ((returnValue != 0) &&
                (this.CurrentStep == 0))
            {
                returnValue = PROGRAMMER_TIMEOUT;
            }

            programmer.StatusEvent -= new EventHandler<ProgressStatusEventArgs>(ProgressHandler);
            programmer.Dispose();

            return returnValue;
        }

        private int DoStage4Programming(MCMBiosController controller)
        {
            int returnValue = 0;
            FactoryDefaultsProgrammer programmer = new FactoryDefaultsProgrammer();
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => this.StepTitle = "Stage 4 Programming"));
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => this.NumberSteps = FactoryDefaultsProgrammer.TOTAL_STEPS));
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => this.CurrentStep = 0));
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => this.StepText = string.Empty));
            programmer.AbortEvent = this._AbortEvent;
            programmer.StatusEvent += new EventHandler<ProgressStatusEventArgs>(ProgressHandler);
            programmer.Setup(controller);
            returnValue = programmer.Execute();
            if ((returnValue != 0) &&
                (this.CurrentStep == 0))
            {
                returnValue = PROGRAMMER_TIMEOUT;
            }

            programmer.StatusEvent -= new EventHandler<ProgressStatusEventArgs>(ProgressHandler);
            programmer.Dispose();

            return returnValue;
        }

        private void ProgressHandler(object sender, ProgressStatusEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(string.Format("{0}/{1} - {2}", e.CurrentStep, e.TotalSteps, e.Message));
            
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => this.CurrentStep = e.CurrentStep));
            
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => this.StepText = e.Message));

            if((e.CurrentStep > 0) && (this._MessageService.ActiveMessage == true))
            {
                this._MessageService.DismissMessage();
            }
        }

        private void MCMResponseHandler(object sender, OnSerialDataEventArgs e)
        {
            string msg = e.Data.Trim().Trim('-').Trim('>').TrimEnd('\n').TrimEnd('\r');
            System.Diagnostics.Debug.WriteLine(msg);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
