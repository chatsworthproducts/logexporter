﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.MCM2Programmer
{
    public interface IMessage
    {
        bool ActiveMessage { get; set; }
        
        void DisplayMessage(string message);        
        void DismissMessage();
    }
}
