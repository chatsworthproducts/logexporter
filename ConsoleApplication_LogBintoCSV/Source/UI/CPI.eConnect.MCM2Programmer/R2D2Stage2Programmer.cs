﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPI.eConnect.Communications;
using CPI.eConnect.Programmer;

namespace CPI.eConnect.MCM2Programmer
{
    public class R2D2Stage2Programmer : MCMProgrammer
    {
        public const int TOTAL_STEPS = 4;

        public int Execute()
        {
            this.NotifyStatus("Power up the MCM.", 0, R2D2Stage2Programmer.TOTAL_STEPS);
            if (_Controller.StopAutoboot(this.AbortEvent) != 0)
            {                
                // Display a message to reboot...
                // try again...
                return 1;
            }
            _Controller.Flush();
            this.NotifyStatus("Auto-boot sequence interrupted successfully.", 1, R2D2Stage2Programmer.TOTAL_STEPS);

            // set the MCM ID....
            //  Get the next MCM ID...
            using (IDManagerTransaction mcmTransaction = IDManager.Instance.GetNext())
            {
                //  Set the MCM ID...
                this.NotifyStatus("Setting MCM Id...", 2, R2D2Stage2Programmer.TOTAL_STEPS);
                if (this.SendCmdAndWait(string.Format("set mcmid {0}\r\n", mcmTransaction.ID), "U-Boot", 5000) != 0)
                {
                    return 1;
                }

                _Controller.Flush();
                this.NotifyStatus("Saving...", 3, R2D2Stage2Programmer.TOTAL_STEPS);
                //  Save the UBoot Environment...
                if (this.SendCmdAndWait(string.Format("saveenv\r\n"), "U-Boot", 5000) != 0)
                {
                    return 1;
                }

                _Controller.Flush();
                mcmTransaction.Commit();
            }

            this.NotifyStatus("Resetting...", 4, R2D2Stage2Programmer.TOTAL_STEPS);
            //  Restart the MCM...
            _Controller.Flush();
            while( this.SendCmdAndWait("reset\r\n", "resetting", 1000) != 0 )
            {
                System.Threading.Thread.Sleep(2);
            }
            return 0;
        }
    }
}
