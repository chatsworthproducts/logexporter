﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.MCM2Programmer
{
    public class AppNotifications
    {
        private static AppNotifications instance;

        public static AppNotifications Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AppNotifications();
                }
                return instance;
            }
        }

        public event EventHandler<object> AppClosing;

        public void FireAppClosing()
        {
            if(AppClosing != null)
            {
                this.AppClosing(null,null);
            }
        }
    }
}
