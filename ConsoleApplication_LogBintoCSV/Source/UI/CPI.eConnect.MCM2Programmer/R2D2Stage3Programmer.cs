﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPI.eConnect.Programmer;

namespace CPI.eConnect.MCM2Programmer
{
    public class R2D2Stage3Programmer : MCMWatcher
    {
        public const int TOTAL_STEPS = 29;

        public int Execute()
        {
            _Sequence.Add(new SerialSequence("CPU: SAMA5D34", new ProgressStatusEventArgs("Booting...", 1, R2D2Stage3Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("In:    serial", new ProgressStatusEventArgs("Booting...", 2, R2D2Stage3Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("reading bootseq", new ProgressStatusEventArgs("Booting...", 3, R2D2Stage3Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("ERROR: can't get kernel image!", new ProgressStatusEventArgs("Booting...", 4, R2D2Stage3Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("resetting ...", new ProgressStatusEventArgs("Booting...", 5, R2D2Stage3Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("reading kr", new ProgressStatusEventArgs("Booting...", 6, R2D2Stage3Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("reading fs", new ProgressStatusEventArgs("Booting...", 7, R2D2Stage3Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("reading dtb", new ProgressStatusEventArgs("Booting...", 8, R2D2Stage3Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("## Booting kernel from Legacy Image at 2a000000 ...", new ProgressStatusEventArgs("Booting...", 9, R2D2Stage3Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("## Loading init Ramdisk from Legacy Image at 2b000000 ...", new ProgressStatusEventArgs("Booting...", 10, R2D2Stage3Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("## Flattened Device Tree blob at 20f00000", new ProgressStatusEventArgs("Booting...", 11, R2D2Stage3Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Starting kernel ...", new ProgressStatusEventArgs("Booting...", 12, R2D2Stage3Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Uncompressing Linux...", new ProgressStatusEventArgs("Booting...", 13, R2D2Stage3Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Booting Linux on physical CPU 0x0", new ProgressStatusEventArgs("Booting...", 14, R2D2Stage3Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("CPU: Testing write buffer coherency: ok", new ProgressStatusEventArgs("Booting...", 15, R2D2Stage3Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("DMA: preallocated 256 KiB pool for atomic coherent allocations", new ProgressStatusEventArgs("Booting...", 16, R2D2Stage3Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("pinctrl-at91 pinctrl.5: initialized AT91 pinctrl driver", new ProgressStatusEventArgs("Booting...", 17, R2D2Stage3Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Unpacking initramfs...", new ProgressStatusEventArgs("Booting...", 18, R2D2Stage3Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Freeing initrd memory: 10088K (cf165000 - cfb3f000)", new ProgressStatusEventArgs("Booting...", 19, R2D2Stage3Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("at91_ohci 600000.ohci: new USB bus registered, assigned bus number 2", new ProgressStatusEventArgs("Booting...", 20, R2D2Stage3Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("can: raw protocol (rev 20120528)", new ProgressStatusEventArgs("Booting...", 21, R2D2Stage3Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("trying to adjust system clock...", new ProgressStatusEventArgs("Booting...", 22, R2D2Stage3Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("extracting /usr/cpi/luabc/cpiluabc.tgz...", new ProgressStatusEventArgs("Booting...", 23, R2D2Stage3Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("ip_tables: (C) 2000-2006 Netfilter Core Team", new ProgressStatusEventArgs("Booting...", 24, R2D2Stage3Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Adding static route for default gateway to 192.168.123.1:", new ProgressStatusEventArgs("Booting...", 25, R2D2Stage3Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Setting up IPv6 networking on eth0:", new ProgressStatusEventArgs("Booting...", 26, R2D2Stage3Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("=== CPI app timestamps:", new ProgressStatusEventArgs("Booting...", 27, R2D2Stage3Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("CPI console 3.1.103", new ProgressStatusEventArgs("Booting...", 28, R2D2Stage3Programmer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("CPI Event ID", new ProgressStatusEventArgs("Booting...", 29, R2D2Stage3Programmer.TOTAL_STEPS)));

            this.NotifyStatus("Waiting for reset...", 0, R2D2Stage1Programmer.TOTAL_STEPS);

            return this.Watch();
        }        
    }
}
