﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CPI.eConnect.MCM2Programmer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private R2D2StepStatus _Programmer1;
        private R2D2StepStatus _Programmer2;
        private R2D2StepStatus _Programmer3;
        private R2D2StepStatus _Programmer4;
        private R2D2StepStatus _Programmer5;
        
        public MainWindow()
        {
            InitializeComponent();
            this.Programmer1Area.Content = new NotConfigured("Jig 1");
            this.Programmer2Area.Content = new NotConfigured("Jig 2");
            this.Programmer3Area.Content = new NotConfigured("Jig 3");
            this.Programmer4Area.Content = new NotConfigured("Jig 4");
            this.Programmer5Area.Content = new NotConfigured("Jig 5");
        }
                
        public R2D2StepStatus Programmer1 
        { 
            get
            {
                return _Programmer1;
            }

            set
            {
                _Programmer1 = value;
                this.Programmer1Area.Content = _Programmer1;
            }
        }

        public R2D2StepStatus Programmer2 
        {
            get
            {
                return _Programmer2;
            }

            set
            {
                _Programmer2 = value;
                this.Programmer2Area.Content = _Programmer2;
            } 
        }

        public R2D2StepStatus Programmer3 
        {
            get
            {
                return _Programmer3;
            }

            set
            {
                _Programmer3 = value;
                this.Programmer3Area.Content = _Programmer3;
            } 
        }

        public R2D2StepStatus Programmer4 
        {
            get
            {
                return _Programmer4;
            }

            set
            {
                _Programmer4 = value;
                this.Programmer4Area.Content = _Programmer4;
            } 
        }

        public R2D2StepStatus Programmer5 
        {
            get
            {
                return _Programmer5;
            }

            set
            {
                _Programmer5 = value;
                this.Programmer5Area.Content = _Programmer5;
            } 
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            // Begin dragging the window
            this.DragMove();
        }

        private void OnCloseApp(object sender, RoutedEventArgs e)
        {
            App.Current.Shutdown();
        }
    }
}
