﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace CPI.eConnect.MCM2Programmer
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            IDManager.Instance.Load();
            AppDomain.CurrentDomain.UnhandledException += AppDomainUnhandledException;
            base.OnStartup(e);

            MainWindow win = new MainWindow();
            this.MainWindow = win;
            if(CPI.eConnect.MCM2Programmer.Properties.Settings.Default.Jig1Port.CompareTo("?") != 0)
            {
                win.Programmer1 = new R2D2StepStatus(1, CPI.eConnect.MCM2Programmer.Properties.Settings.Default.Jig1Port);
            }
            if (CPI.eConnect.MCM2Programmer.Properties.Settings.Default.Jig2Port.CompareTo("?") != 0)
            {
                win.Programmer2 = new R2D2StepStatus(2, CPI.eConnect.MCM2Programmer.Properties.Settings.Default.Jig2Port);
            }
            if (CPI.eConnect.MCM2Programmer.Properties.Settings.Default.Jig3Port.CompareTo("?") != 0)
            {
                win.Programmer3 = new R2D2StepStatus(3, CPI.eConnect.MCM2Programmer.Properties.Settings.Default.Jig3Port);
            }
            if (CPI.eConnect.MCM2Programmer.Properties.Settings.Default.Jig4Port.CompareTo("?") != 0)
            {
                win.Programmer4 = new R2D2StepStatus(4, CPI.eConnect.MCM2Programmer.Properties.Settings.Default.Jig4Port);
            }
            if (CPI.eConnect.MCM2Programmer.Properties.Settings.Default.Jig5Port.CompareTo("?") != 0)
            {
                win.Programmer5 = new R2D2StepStatus(5, CPI.eConnect.MCM2Programmer.Properties.Settings.Default.Jig5Port);
            }
            win.Show();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            IDManager.Instance.Save();
            AppNotifications.Instance.FireAppClosing();
            base.OnExit(e);
        }

        private static void AppDomainUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            HandleException(e.ExceptionObject as Exception);
        }

        private static void HandleException(Exception ex)
        {
            if (ex == null)
                return;
            
            MessageBox.Show("An unhandled exception occurred, and the application is terminating. For more information, see your Application log..");
            Environment.Exit(1);
        }
    }
}
