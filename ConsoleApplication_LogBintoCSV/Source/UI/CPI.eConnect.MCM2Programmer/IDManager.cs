﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace CPI.eConnect.MCM2Programmer
{
    public class IDManagerTransaction : IDisposable
    {
        private bool _Committed;

        public IDManager Manager { get; set; }
        public int ID { get; set; }

        public IDManagerTransaction(IDManager manager, int id)
        {
            _Committed = false;
            this.Manager = manager;
            this.ID = id;
        }

        public void Commit()
        {
            _Committed = true;
        }

        public void Abort()
        {
            Manager.AbortTransaction(this.ID);
        }

        public void Dispose()
        {
            if(_Committed == false)
            {
                Abort();
            }
        }
    }

    public class IDManager
    {
        private object _ManagerLock = new object();
        private Queue<string> _AvailableIDs = new Queue<string>();
        private int _StartingId;
        private int _EndingId;
        private int _CurrentId;
        private static IDManager instance;

        public static IDManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new IDManager();
                }
                return instance;
            }
        }

        public void Load()
        {
            // read the ID settings file...
            string settingsPath = System.IO.Path.Combine(Properties.Settings.Default.DataDir, "id_settings.txt");
            if (File.Exists(settingsPath) == true)
            {
                using (StreamReader sr = new StreamReader(settingsPath))
                {
                    String line = sr.ReadLine();
                    // line is a comma delimeted file...
                    //  start, end, current
                    line.TrimEnd('\n', '\r', ' ');
                    string[] lineParts = line.Split(',');
                    _StartingId = Convert.ToInt32(lineParts[0]);
                    _EndingId = Convert.ToInt32(lineParts[1]);
                    _CurrentId = Convert.ToInt32(lineParts[2]);
                }
            }
            else
            {
                throw new Exception(string.Format("Missing settings file {0}", settingsPath));
            }

            // read the Available IDs...
            string availablePath = System.IO.Path.Combine(Properties.Settings.Default.DataDir, "available_ids.txt");
            if (File.Exists(availablePath) == true)
            {
                using (StreamReader sr = new StreamReader(availablePath))
                {
                    while (sr.Peek() >= 0)
                    {
                        string line = sr.ReadLine();
                        _AvailableIDs.Enqueue(line);
                    }
                }
            }
        }

        public void Save()
        {
            // write out the ID settings file...
            string settingsPath = System.IO.Path.Combine(Properties.Settings.Default.DataDir, "id_settings.txt");
            using (StreamWriter sr = new StreamWriter(settingsPath,false))
            {
                sr.WriteLine("{0},{1},{2}", _StartingId, _EndingId, _CurrentId);                
            }

            // write out the Available IDs...
            string availablePath = System.IO.Path.Combine(Properties.Settings.Default.DataDir, "available_ids.txt");
            using (StreamWriter sr = new StreamWriter(availablePath, false))
            {
                string[] queuedIds = _AvailableIDs.ToArray();
                foreach(string s in queuedIds)
                {
                    sr.WriteLine(s);
                }
            }
        }

        public IDManagerTransaction GetNext()
        {
            int id = 0;
            lock(_ManagerLock)
            {
                if(_AvailableIDs.Count != 0 )
                {
                    string newId = _AvailableIDs.Dequeue();
                    id = Convert.ToInt32(newId);
                }
                else
                {
                    id = _CurrentId;
                    _CurrentId++;
                }

                this.Save();
            }

            return new IDManagerTransaction(this, id);
        }

        public void AbortTransaction(int id)
        {
            lock (_ManagerLock)
            {
                _AvailableIDs.Enqueue(id.ToString());
                this.Save();
            }
        }
    }
}
