﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CPI.eConnect.Communications;
using CPI.eConnect.Programmer;
using CPI.eConnect.Controls;

namespace CPI.eConnect.MCMProgrammer
{
    /// <summary>
    /// Interaction logic for ProgrammerControl.xaml
    /// </summary>
    public partial class ProgrammerControl : UserControl
    {
        private StatusController _CurrentController;
        private System.Threading.Thread _ProgrammingThread;
        private System.Threading.ManualResetEvent _AbortEvent = new System.Threading.ManualResetEvent(false);

        public ProgrammerControl()
        {
            InitializeComponent();
        }

        public void Start()
        {
            _ProgrammingThread = new System.Threading.Thread(new System.Threading.ThreadStart(this.Program));
            _ProgrammingThread.Start();
        }

        public void Abort()
        {
            this._AbortEvent.Set();
            this._ProgrammingThread.Join();
        }

        public void Program()
        {
            MCMBiosController controller;

            controller = new MCMBiosController();
            controller.PortName = Properties.Settings.Default.COMPort;
            controller.NewData += new EventHandler<OnSerialDataEventArgs>(this.MCMResponseHandler);
            controller.Connect();

            if (ProgramSBF(controller) != 0)
            {
                controller.Close();
                return;
            }

            if (ProgramBIOS(controller) != 0)
            {
                controller.Close();
                return;
            }

            if (ProgramBoot(controller) != 0)
            {
                controller.Close();
                return;
            }

            if (ProgramFactoryDefaults(controller) != 0)
            {
                controller.Close();
                return;
            }

            controller.Close();

            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => AbortBtn.Visibility = Visibility.Hidden));
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => FinishBtn.Visibility = Visibility.Visible));
        }

        private int ProgramFactoryDefaults(MCMBiosController controller)
        {
            int returnValue = 0;
            FactoryDefaultsProgrammer programmer = new FactoryDefaultsProgrammer();
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => this.ProgramStatusDisplay.StatusString.Clear())); 
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => _CurrentController = this.ProgramStatusDisplay.SetDefaults));
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => _CurrentController.Visible = Visibility.Visible));
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => _CurrentController.Status = StepStatus.Downloading));
            programmer.AbortEvent = this._AbortEvent;
            programmer.StatusEvent += new EventHandler<ProgressStatusEventArgs>(ProgressHandler);
            programmer.Setup(controller);
            returnValue = programmer.Execute();
            if( returnValue != 0 )
            {                
                this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                        (Action)(() => _CurrentController.Status = StepStatus.Error));
            }
            else
            {
                this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                        (Action)(() => _CurrentController.Status = StepStatus.Complete));
            }
            programmer.StatusEvent -= new EventHandler<ProgressStatusEventArgs>(ProgressHandler);
            programmer.Dispose();
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => _CurrentController.Visible = Visibility.Hidden));
            
            return returnValue;
        }

        private int ProgramBoot(MCMBiosController controller)
        {
            int returnValue = 0;
            BootProgrammer programmer = new BootProgrammer();
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => this.ProgramStatusDisplay.StatusString.Clear())); 
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => _CurrentController = this.ProgramStatusDisplay.BootMCM));
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => _CurrentController.Visible = Visibility.Visible)); 
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => _CurrentController.Status = StepStatus.Downloading));
            programmer.AbortEvent = this._AbortEvent;
            programmer.StatusEvent += new EventHandler<ProgressStatusEventArgs>(ProgressHandler);
            programmer.Setup(controller);
            returnValue = programmer.Execute(true);
            if (returnValue != 0)
            {
                this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                        (Action)(() => _CurrentController.Status = StepStatus.Error));
            }
            else
            {
                this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                        (Action)(() => _CurrentController.Status = StepStatus.Complete));
            }
            programmer.StatusEvent -= new EventHandler<ProgressStatusEventArgs>(ProgressHandler);
            programmer.Dispose();
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => _CurrentController.Visible = Visibility.Hidden));

            return returnValue;
        }

        private int ProgramSBF(MCMBiosController controller)
        {
            int returnValue = 0;
            SerialBootProgrammer programmer = new SerialBootProgrammer();
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => this.ProgramStatusDisplay.StatusString.Clear())); 
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => _CurrentController = this.ProgramStatusDisplay.ProgramSBF));
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => _CurrentController.Visible = Visibility.Visible)); 
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => _CurrentController.Status = StepStatus.Downloading));
            programmer.AbortEvent = this._AbortEvent;
            programmer.StatusEvent += new EventHandler<ProgressStatusEventArgs>(ProgressHandler);
            programmer.Setup(controller);
            returnValue = programmer.Execute();
            if (returnValue != 0)
            {
                this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                        (Action)(() => _CurrentController.Status = StepStatus.Error));
            }
            else
            {
                this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                        (Action)(() => _CurrentController.Status = StepStatus.Complete));
            }
            programmer.StatusEvent -= new EventHandler<ProgressStatusEventArgs>(ProgressHandler);
            programmer.Dispose();
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => _CurrentController.Visible = Visibility.Hidden));

            return returnValue;
        }

        private int ProgramBIOS(MCMBiosController controller)
        {
            int returnValue = 0;
            BiosProgrammer programmer = new BiosProgrammer();
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => this.ProgramStatusDisplay.StatusString.Clear()));
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => _CurrentController = this.ProgramStatusDisplay.ProgramBIOS));
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => _CurrentController.Visible = Visibility.Visible)); 
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => _CurrentController.Status = StepStatus.Downloading));
            programmer.ServerIP = Properties.Settings.Default.ServerIP;
            programmer.IPAddress = Properties.Settings.Default.MCMIP;
            programmer.MCMID = Properties.Settings.Default.MCMID;
            programmer.AbortEvent = this._AbortEvent;
            programmer.StatusEvent += new EventHandler<ProgressStatusEventArgs>(ProgressHandler);
            programmer.Setup(controller);
            returnValue = programmer.Execute();
            if (returnValue != 0)
            {
                this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                        (Action)(() => _CurrentController.Status = StepStatus.Error));
            }
            else
            {
                this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                        (Action)(() => _CurrentController.Status = StepStatus.Complete));
            }
            programmer.StatusEvent -= new EventHandler<ProgressStatusEventArgs>(ProgressHandler);
            programmer.Dispose();
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => _CurrentController.Visible = Visibility.Hidden));

            return returnValue;
        }

        private void ProgressHandler(object sender, ProgressStatusEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(string.Format("{0}/{1} - {2}", e.CurrentStep, e.TotalSteps, e.Message));

            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => _CurrentController.CurrentStep = e.CurrentStep));
            
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => _CurrentController.NumberSteps = e.TotalSteps));
            
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => _CurrentController.StepText = e.Message));
        }

        private void MCMResponseHandler(object sender, OnSerialDataEventArgs e)
        {
            string msg = e.Data.Trim().Trim('-').Trim('>').TrimEnd('\n').TrimEnd('\r');

            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                (Action)(() => this.ProgramStatusDisplay.StatusString.Insert(0,msg)));
            System.Diagnostics.Debug.WriteLine(msg);
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            this.Start();
        }

        private void OnAbort(object sender, RoutedEventArgs e)
        {
            this.Abort();
            App MCMProgrammerApp = ((App)Application.Current);
            MCMProgrammerApp.WorkArea.Pop();
        }

        private void OnFinish(object sender, RoutedEventArgs e)
        {
            this.Abort();
            App MCMProgrammerApp = ((App)Application.Current);
            MCMProgrammerApp.WorkArea.Pop();
        }
    }
}
