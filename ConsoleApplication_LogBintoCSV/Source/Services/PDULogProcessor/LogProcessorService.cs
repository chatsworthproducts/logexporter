﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using CPI.eConnect.Logging.eConnect;
using CPI.eConnect.Processors.PDULog;

namespace PDULogProcessor
{
    partial class LogProcessorService : ServiceBase
    {
        System.Timers.Timer _PollingTimer = new System.Timers.Timer();

        public LogProcessorService()
        {
            InitializeComponent();
            _PollingTimer.AutoReset = true;
            _PollingTimer.Elapsed += _PollingTimer_Elapsed;
        }
                
        protected override void OnStart(string[] args)
        {
            base.OnStart(args);
            foreach (string arg in args)
            {
                if (arg == "DEBUG_SERVICE")
                    DebugMode();

            }

#if DEBUG
            DebugMode();
#endif

            _PollingTimer.Interval = 3000000;
            _PollingTimer.Start();            
        }

        protected override void OnStop()
        {
            base.OnStop();
            _PollingTimer.Stop();
        }

        protected override void OnPause()
        {
            base.OnPause();
            _PollingTimer.Stop();
        }

        protected override void OnContinue()
        {
            base.OnContinue();
            _PollingTimer.Start();
        }

        protected override void OnShutdown()
        {
            base.OnShutdown();
            _PollingTimer.Stop();
        }

        protected override bool OnPowerEvent(PowerBroadcastStatus powerStatus)
        {
            return base.OnPowerEvent(powerStatus);
        }

        private static void DebugMode()
        {
            Debugger.Break();
        }

        private void _PollingTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            string[] files = System.IO.Directory.GetFiles(Properties.Settings.Default.WatchDirectory, 
                "*", System.IO.SearchOption.TopDirectoryOnly);

            foreach (string file in files)
            {
                System.Threading.ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(ReadLog), file);
            }

            _PollingTimer.Start();
        }

        public static void ReadLog(object state)
        {
            LogReader reader = (LogReader)state;
            ProcessPDULog.ReadLog(reader);
        }
    }
}
