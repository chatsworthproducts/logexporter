﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDULogProcessor
{
    public static class DictionaryExtensions
    {
        public static TValue GetValueOrAdd<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
        {
            TValue ret = default(TValue);
            if (dictionary.TryGetValue(key, out ret) == false)
            {
                ret = (TValue)Activator.CreateInstance(typeof(TValue));
                dictionary.Add(key, ret);
            }
            return ret;
        }
    }
}
