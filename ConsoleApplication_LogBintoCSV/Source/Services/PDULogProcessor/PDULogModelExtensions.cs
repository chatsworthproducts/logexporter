﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPI.eConnect.Logging.eConnect;
using CPI.Data.PDULog;

namespace PDULogProcessor
{
    public static class PDULogModelExtensions
    {
        public static void AddOptionChanges(this CPIPDULogModelContainer container, SystemObject sysObj, IEnumerable<PDUOptionChange> optionInfoList)
        {
            foreach(PDUOptionChange oc in optionInfoList)
            {
                SystemObjectStateChange sc = new SystemObjectStateChange();
                sc.systemObjectId = sysObj.id;
                sc.timestamp = oc.TimeStamp;
                sc.action = (int)oc.OPAction;
            }
        }

        public static void AddRecepChange(this CPIPDULogModelContainer container, SystemObject sysObj, IEnumerable<RecepChange> changeInfoList)
        {
            CPI.eConnect.API.eConnectDeviceInfo devInfo = new CPI.eConnect.API.eConnectDeviceInfo(sysObj.model);

            foreach(RecepChange rc in changeInfoList)
            {
                UInt64 stateMap = (UInt64)(rc.StatusMap[5] << 40) +
                                  (UInt64)(rc.StatusMap[4] << 32) +
                                  (UInt64)(rc.StatusMap[3] << 24) +
                                  (UInt64)(rc.StatusMap[2] << 16) +
                                  (UInt64)(rc.StatusMap[1] << 8) +
                                  (UInt64)rc.StatusMap[0];

                UInt64 resetMap = (UInt64)(rc.ResetMap[5] << 40) +
                                  (UInt64)(rc.ResetMap[4] << 32) +
                                  (UInt64)(rc.ResetMap[3] << 24) +
                                  (UInt64)(rc.ResetMap[2] << 16) +
                                  (UInt64)(rc.ResetMap[1] << 8) +
                                  (UInt64)rc.ResetMap[0];
                
                for (int i = 0; i < devInfo.TotalOutlets; i++)
                {
                    UInt64 recepBit = (UInt64)1 << i;
                    OutletChangeHistory och = new OutletChangeHistory();
                    och.systemObjectId = sysObj.id;
                    och.timestamp = rc.TimeStamp;
                    och.state = (stateMap & recepBit) == recepBit;
                    och.reset = (resetMap & recepBit) == recepBit;

                    container.OutletChangeHistories.Add(och);
                }
            }
        }

        public static void AddFirmwareChanges(this CPIPDULogModelContainer container, SystemObject sysObj, IEnumerable<LogInOut> logioInfoList)
        {
            foreach(LogInOut li in logioInfoList)
            {
                LogInOutHistory lih = new LogInOutHistory();
                lih.systemObjectId = sysObj.id;
                lih.timestamp = li.TimeStamp;
                lih.user = li.User;
                lih.action = (int)li.Action;
            }
        }

        public static void AddFirmwareChanges(this CPIPDULogModelContainer container, SystemObject sysObj, IEnumerable<FirmwareChange> fwInfoList)
        {
            foreach(FirmwareChange fc in fwInfoList)
            {
                FWChangeHistory fwh = new FWChangeHistory();
                fwh.systemObjectId = sysObj.id;
                fwh.biosRev = (int)fc.BiosRev;
                fwh.kernelRev = (int)fc.KernelRev;
                fwh.fileSysRev = (int)fc.FileSysRev;
                fwh.patchRev = (int)fc.PatchRev;
                fwh.timestamp = fc.TimeStamp;
                fwh.result = fc.UpdateResult;

                container.FWChangeHistories.Add(fwh);
            }
        }

        public static void AddEnvironmentalData(this CPIPDULogModelContainer container, SystemObject sysObj, IEnumerable<EnvironInfo> environInfoList)
        {
            foreach(EnvironInfo ei in environInfoList)
            {
                SensorHistory sh = new SensorHistory();
                sh.systemObjectId = sysObj.id;
                sh.sensor = ei.EnvironType;
                sh.value = ei.Value;
                sh.timestamp = ei.TimeStamp;

                if(sh.value != 65535)
                {
                    container.SensorHistories.Add(sh);
                }
            }
        }

        public static void AddConfigChange(this CPIPDULogModelContainer container, SystemObject sysObj, IEnumerable<ConfigChange> changeInfoList)
        {
            foreach (ConfigChange ci in changeInfoList)
            {
                ConfigChangeHistory ch = new ConfigChangeHistory();
                ch.timestamp = ci.TimeStamp;
                ch.systemObjectId = sysObj.id;
                ch.variable = (int)ci.ConfigVariable;
                ch.occurance = ci.Occurance;
                ch.size = ci.Size;
                ch.dataType = (int)ci.DataType;
                ch.pduInterface = (int)ci.PDUInterface;
                ch.value = ci.NewValue;

                container.ConfigChangeHistories.Add(ch);
            }
        }

        public static void AddBranchInfo(this CPIPDULogModelContainer container, SystemObject sysObj, IEnumerable<BranchInfo> branchInfoList)
        {
            foreach(BranchInfo bi in branchInfoList)
            {
                BranchHistory bh = new BranchHistory();
                bh.branch = bi.Branch;
                bh.systemObjectId = sysObj.id;
                bh.timestamp = bi.TimeStamp;
                bh.current = bi.Current;
                bh.energy = (long)bi.Energy;
                bh.power = bi.Power;
                bh.powerfactor = bi.PowerFactor;
                bh.systemObjectId = sysObj.id;
                bh.voltage = bi.Voltage;

                container.BranchHistories.Add(bh);
            }
        }

        public static void AddRecepInfo(this CPIPDULogModelContainer container, SystemObject sysObj, IEnumerable<RecepInfo> recepInfoList)
        {
            foreach (RecepInfo oi in recepInfoList)
            {
                OutletHistory oh = new OutletHistory();
                oh.outlet = oi.Outlet;
                oh.systemObjectId = sysObj.id;
                oh.timestamp = oi.TimeStamp;
                oh.current = oi.Current;
                oh.energy = (long)oi.Energy;
                oh.power = oi.Power;
                oh.powerfactor = oi.PowerFactor;
                oh.systemObjectId = sysObj.id;
                oh.voltage = oi.Voltage;

                container.OutletHistories.Add(oh);
            }
        }

        public static void AddAlarms(this CPIPDULogModelContainer container, SystemObject sysObj, IEnumerable<AlarmInfo> alarms)
        {
            foreach(AlarmInfo ai in alarms)
            {                
                container.AddBranchAlarms(sysObj, ai);
                container.AddRecepAlarms(sysObj, ai);                
            }
        }

        public static void AddBranchAlarms(this CPIPDULogModelContainer container, SystemObject sysObj, AlarmInfo alarm)
        {
            CPI.eConnect.API.eConnectDeviceInfo devInfo = new CPI.eConnect.API.eConnectDeviceInfo(sysObj.model);
            for (int i = 0; i < devInfo.NumberOfBreakers; i++)
            {
                UInt32 branchBit = (UInt32)1 << i;
                BranchAlarm dbAlarm = new BranchAlarm();
                dbAlarm.systemObjectId = sysObj.id;
                dbAlarm.timestamp = alarm.TimeStamp;
                dbAlarm.branch = i + 1;
                dbAlarm.alarmHigh = (alarm.AlarmsHigh & branchBit) == branchBit;
                dbAlarm.alarmLow = (alarm.AlarmsLow & branchBit) == branchBit;
                dbAlarm.warningHigh = (alarm.WarningsHigh & branchBit) == branchBit;
                dbAlarm.warningLow = (alarm.WarnsingLow & branchBit) == branchBit;

                container.BranchAlarms.Add(dbAlarm);
            }
        }

        public static void AddRecepAlarms(this CPIPDULogModelContainer container, SystemObject sysObj, AlarmInfo alarm)
        {
            CPI.eConnect.API.eConnectDeviceInfo devInfo = new CPI.eConnect.API.eConnectDeviceInfo(sysObj.model);
            for (int i = 0; i < devInfo.TotalOutlets; i++)
            {
                UInt64 recepBit = (UInt64)1 << i;
                OutletAlarm dbAlarm = new OutletAlarm();
                dbAlarm.systemObjectId = sysObj.id;
                dbAlarm.timestamp = alarm.TimeStamp;
                dbAlarm.outlet = i + 1;

                dbAlarm.alarmHigh = (alarm.RecepAlarmsHigh & recepBit) == recepBit;
                dbAlarm.alarmLow = (alarm.RecepAlarmsLow & recepBit) == recepBit;
                dbAlarm.warningHigh = (alarm.RecepWarningHigh & recepBit) == recepBit;
                dbAlarm.warningLow = (alarm.RecepWarningLow & recepBit) == recepBit;

                container.OutletAlarms.Add(dbAlarm);
            }
        }
    }
}
