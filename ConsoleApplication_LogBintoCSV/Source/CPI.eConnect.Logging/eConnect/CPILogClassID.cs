﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Logging.eConnect
{
    public enum CPILogClassID
    {
        Info = 0,
        Meter,
        Group,
        Alarm,
        LogIO,
        ConfigChange,
        RecepChange,
        FirmwareUpdate,
        PDUOptionChange,
        UserChange,
    };
}
