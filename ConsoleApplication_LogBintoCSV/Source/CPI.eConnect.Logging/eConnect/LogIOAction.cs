﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Logging.eConnect
{
    public enum LogIOAction
    {
        In = 0,
        Out = 1,
        Timeout = 2,
        Faile = 3
    }
}
