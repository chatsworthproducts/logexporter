﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Logging.eConnect
{
    public class MeterInfo// : IRecordReader
    {
        public DateTime TimeStamp { get; set; }
        public string MAC { get; set; }
        public EnvironInfo[] Environmental { get; set; }
        public BranchInfo[] Branch { get; set; }
        public RecepInfo[] Recep { get; set; }

        public void ReadRecord(EndianBinaryLogReader reader, LogRecord record)
        {
            int recepCount = 24;
            if (record.SubClassId == 1)
            {
                recepCount = 36;
            }
            else if (record.SubClassId == 2)
            {
                recepCount = 48;
            }

            TimeStamp = record.TimeStamp;
            MAC = record.MAC;

            Environmental = new EnvironInfo[4]; // 0 == Temp, 1 == Humidity, 2 == Temp, 3 == Humidity
            Environmental[0] = new EnvironInfo(0);
            Environmental[0].TimeStamp = TimeStamp;
            Environmental[0].MAC = MAC;
            Environmental[0].Value = reader.ReadUInt16();
            
            Environmental[1] = new EnvironInfo(1);
            Environmental[1].TimeStamp = TimeStamp;
            Environmental[1].MAC = MAC;
            Environmental[1].Value = reader.ReadUInt16();
            
            Environmental[2] = new EnvironInfo(0);
            Environmental[2].TimeStamp = TimeStamp;
            Environmental[2].MAC = MAC;
            Environmental[2].Value = reader.ReadUInt16();
            
            Environmental[3] = new EnvironInfo(1);
            Environmental[3].TimeStamp = TimeStamp;
            Environmental[3].MAC = MAC;
            Environmental[3].Value = reader.ReadUInt16();

            Branch = new BranchInfo[6];
            for (int i = 0; i < 6; i++)
            {
                Branch[i] = new BranchInfo();
                Branch[i].TimeStamp = TimeStamp;
                Branch[i].MAC = MAC;
                Branch[i].Current = reader.ReadUInt16();
                Branch[i].Branch = i + 1;
            }
            
            for (int i = 0; i < 6; i++)
            {
                Branch[i].Voltage = reader.ReadUInt16();
            }

            for (int i = 0; i < 6; i++)
            {
                Branch[i].Power = reader.ReadUInt16();
            }

            for (int i = 0; i < 6; i++)
            {
                Branch[i].PowerFactor = reader.ReadByte();
            }

            for (int i = 0; i < 6; i++)
            {
                Branch[i].Energy = reader.ReadUInt64();
            }

            Recep = new RecepInfo[recepCount];

            for (int i = 0; i < recepCount; i++)
            {
                Recep[i] = new RecepInfo();
                Recep[i].TimeStamp = TimeStamp;
                Recep[i].Outlet = i + 1;
                Recep[i].MAC = MAC;
                Recep[i].Current = reader.ReadUInt16();
            }

            for (int i = 0; i < recepCount; i++)
            {
                Recep[i].Voltage = reader.ReadUInt16();
            }

            for (int i = 0; i < recepCount; i++)
            {
                Recep[i].Power = reader.ReadUInt16();
            }

            for (int i = 0; i < recepCount; i++)
            {
                Recep[i].PowerFactor = reader.ReadByte();
            }

            for (int i = 0; i < recepCount; i++)
            {
                Recep[i].Energy = reader.ReadUInt64();
            }
        }
    }
}
