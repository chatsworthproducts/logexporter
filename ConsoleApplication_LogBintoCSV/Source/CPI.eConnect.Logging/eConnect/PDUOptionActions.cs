﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Logging.eConnect
{
    public enum PDUOptionActions
    {
        ColdStart = 0,
        WarmStart = 1,
        AppStart = 2,
        AppDown = 3,
        Reboot = 4,
        Restart = 5,
        Recover = 6,
        Online = 7
    }
}
