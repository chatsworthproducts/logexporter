﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Logging.eConnect
{
    public class GroupEntry// : IRecordReader
    {
        public DateTime TimeStamp { get; set; }
        public string MAC { get; set; }
        public byte GroupID { get; set; }
        public GroupDetails[] Details { get; set; }

        public void ReadRecord(EndianBinaryLogReader reader, LogRecord record)
        {
            TimeStamp = record.TimeStamp;
            MAC = record.MAC;

            Details = new GroupDetails[32];
            for (int i = 0; i < 32; i++)
            {
                Details[i] = new GroupDetails();
            }

            GroupID = reader.ReadByte();

            for (int i = 0; i < 32; i++)
            {
                Details[i].PDUId = reader.ReadBytes(3);
            }

            for (int i = 0; i < 32; i++)
            {
                Details[i].RecepNumber = reader.ReadByte();
            }

            for (int i = 0; i < 32; i++)
            {
                Details[i].Current = reader.ReadUInt16();
            }

            for (int i = 0; i < 32; i++)
            {
                Details[i].Voltage = reader.ReadUInt16();
            }

            for (int i = 0; i < 32; i++)
            {
                Details[i].Power = reader.ReadUInt16();
            }

            for (int i = 0; i < 32; i++)
            {
                Details[i].PowerFactor = reader.ReadByte();
            }

            for (int i = 0; i < 32; i++)
            {
                Details[i].Energy = reader.ReadUInt64();
            }
        }
    }
}
