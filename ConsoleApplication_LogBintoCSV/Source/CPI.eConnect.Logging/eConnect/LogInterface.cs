﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Logging.eConnect
{
    public enum LogInterface
    {
        Serial = 0,
        Telnet = 1,
        SSH = 2,
        WebGUI = 3,
        SNMP = 4,
        SEMA = 5,
        LCD = 6,
        Internal = 7
    }
}
