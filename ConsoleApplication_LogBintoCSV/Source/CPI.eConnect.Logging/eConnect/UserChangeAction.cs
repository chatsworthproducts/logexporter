﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Logging.eConnect
{
    public enum UserChangeAction
    {
        AddUser = 0,
        DeleteUser = 1,
        ChangeName = 2,
        ChangePassword = 3,
        ChangePermission = 4
    }
}
