﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

/*
 HeaderCrc = reader.ReadByte();
            DataCrc = reader.ReadByte();
            DataLength = FixEndianness_16Bit(reader.ReadUInt16());
            uint dateTime = FixEndianness_32Bit(reader.ReadUInt32());
            TimeStamp = Time_T2DateTime(dateTime);
            PDUID = reader.ReadBytes(3);
            MAC = string.Format("00:0E:D3:{0:X2}:{1:X2}:{2:X2}", PDUID[0], PDUID[1], PDUID[2]);
            ClassId = (CPILogClassID)reader.ReadByte();
            SubClassId = reader.ReadByte();
            reader.ReadBytes(3);
*/

namespace CPI.eConnect.Logging.eConnect
{
    public class EndianBinaryLogReader
    {
        private BinaryReader _Reader;        

        public EndianBinaryLogReader(string path)
        {
            _Reader = new BinaryReader(File.Open(path, FileMode.Open));
        }

        // Summary:
        //     Closes the current reader and the underlying stream.
        public virtual void Close()
        {
            _Reader.Close();
        }
        //
        // Summary:
        //     Releases all resources used by the current instance of the System.IO.BinaryReader
        //     class.
        public void Dispose() 
        {
            _Reader.Dispose();
        }
        //
        // Summary:
        //     Returns the next available character and does not advance the byte or character
        //     position.
        //
        // Returns:
        //     The next available character, or -1 if no more characters are available or
        //     the stream does not support seeking.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     An I/O error occurs.
        //
        //   System.ArgumentException:
        //     The current character cannot be decoded into the internal character buffer
        //     by using the System.Text.Encoding selected for the stream.
        public virtual int PeekChar() 
        {
            return _Reader.PeekChar();
        }
        //
        // Summary:
        //     Reads characters from the underlying stream and advances the current position
        //     of the stream in accordance with the Encoding used and the specific character
        //     being read from the stream.
        //
        // Returns:
        //     The next character from the input stream, or -1 if no characters are currently
        //     available.
        //
        // Exceptions:
        //   System.IO.IOException:
        //     An I/O error occurs.
        //
        //   System.ObjectDisposedException:
        //     The stream is closed.
        public virtual int Read()
        {
            return _Reader.Read();
        }
        //
        // Summary:
        //     Reads the specified number of bytes from the stream, starting from a specified
        //     point in the byte array.
        //
        // Parameters:
        //   buffer:
        //     The buffer to read data into.
        //
        //   index:
        //     The starting point in the buffer at which to begin reading into the buffer.
        //
        //   count:
        //     The number of bytes to read.
        //
        // Returns:
        //     The number of bytes read into buffer. This might be less than the number
        //     of bytes requested if that many bytes are not available, or it might be zero
        //     if the end of the stream is reached.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The buffer length minus index is less than count. -or-The number of decoded
        //     characters to read is greater than count. This can happen if a Unicode decoder
        //     returns fallback characters or a surrogate pair.
        //
        //   System.ArgumentNullException:
        //     buffer is null.
        //
        //   System.ArgumentOutOfRangeException:
        //     index or count is negative.
        //
        //   System.ObjectDisposedException:
        //     The stream is closed.
        //
        //   System.IO.IOException:
        //     An I/O error occurs.
        public virtual int Read(byte[] buffer, int index, int count)
        {
            return _Reader.Read(buffer, index, count);
        }
        //
        // Summary:
        //     Reads the specified number of characters from the stream, starting from a
        //     specified point in the character array.
        //
        // Parameters:
        //   buffer:
        //     The buffer to read data into.
        //
        //   index:
        //     The starting point in the buffer at which to begin reading into the buffer.
        //
        //   count:
        //     The number of characters to read.
        //
        // Returns:
        //     The total number of characters read into the buffer. This might be less than
        //     the number of characters requested if that many characters are not currently
        //     available, or it might be zero if the end of the stream is reached.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The buffer length minus index is less than count. -or-The number of decoded
        //     characters to read is greater than count. This can happen if a Unicode decoder
        //     returns fallback characters or a surrogate pair.
        //
        //   System.ArgumentNullException:
        //     buffer is null.
        //
        //   System.ArgumentOutOfRangeException:
        //     index or count is negative.
        //
        //   System.ObjectDisposedException:
        //     The stream is closed.
        //
        //   System.IO.IOException:
        //     An I/O error occurs.
        public virtual int Read(char[] buffer, int index, int count)
        {
            return _Reader.Read(buffer, index, count);
        }
        //
        // Summary:
        //     Reads a Boolean value from the current stream and advances the current position
        //     of the stream by one byte.
        //
        // Returns:
        //     true if the byte is nonzero; otherwise, false.
        //
        // Exceptions:
        //   System.IO.EndOfStreamException:
        //     The end of the stream is reached.
        //
        //   System.ObjectDisposedException:
        //     The stream is closed.
        //
        //   System.IO.IOException:
        //     An I/O error occurs.
        public virtual bool ReadBoolean()
        {
            return _Reader.ReadBoolean();
        }
        //
        // Summary:
        //     Reads the next byte from the current stream and advances the current position
        //     of the stream by one byte.
        //
        // Returns:
        //     The next byte read from the current stream.
        //
        // Exceptions:
        //   System.IO.EndOfStreamException:
        //     The end of the stream is reached.
        //
        //   System.ObjectDisposedException:
        //     The stream is closed.
        //
        //   System.IO.IOException:
        //     An I/O error occurs.
        public virtual byte ReadByte()
        {
            return _Reader.ReadByte();
        }
        //
        // Summary:
        //     Reads the specified number of bytes from the current stream into a byte array
        //     and advances the current position by that number of bytes.
        //
        // Parameters:
        //   count:
        //     The number of bytes to read.
        //
        // Returns:
        //     A byte array containing data read from the underlying stream. This might
        //     be less than the number of bytes requested if the end of the stream is reached.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The number of decoded characters to read is greater than count. This can
        //     happen if a Unicode decoder returns fallback characters or a surrogate pair.
        //
        //   System.IO.IOException:
        //     An I/O error occurs.
        //
        //   System.ObjectDisposedException:
        //     The stream is closed.
        //
        //   System.ArgumentOutOfRangeException:
        //     count is negative.
        public virtual byte[] ReadBytes(int count)
        {
            return _Reader.ReadBytes(count);
        }
        //
        // Summary:
        //     Reads the next character from the current stream and advances the current
        //     position of the stream in accordance with the Encoding used and the specific
        //     character being read from the stream.
        //
        // Returns:
        //     A character read from the current stream.
        //
        // Exceptions:
        //   System.IO.EndOfStreamException:
        //     The end of the stream is reached.
        //
        //   System.ObjectDisposedException:
        //     The stream is closed.
        //
        //   System.IO.IOException:
        //     An I/O error occurs.
        //
        //   System.ArgumentException:
        //     A surrogate character was read.
        public virtual char ReadChar()
        {
            return _Reader.ReadChar();
        }
        //
        // Summary:
        //     Reads the specified number of characters from the current stream, returns
        //     the data in a character array, and advances the current position in accordance
        //     with the Encoding used and the specific character being read from the stream.
        //
        // Parameters:
        //   count:
        //     The number of characters to read.
        //
        // Returns:
        //     A character array containing data read from the underlying stream. This might
        //     be less than the number of characters requested if the end of the stream
        //     is reached.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The number of decoded characters to read is greater than count. This can
        //     happen if a Unicode decoder returns fallback characters or a surrogate pair.
        //
        //   System.ObjectDisposedException:
        //     The stream is closed.
        //
        //   System.IO.IOException:
        //     An I/O error occurs.
        //
        //   System.ArgumentOutOfRangeException:
        //     count is negative.
        public virtual char[] ReadChars(int count)
        {
            return _Reader.ReadChars(count);
        }
        //
        // Summary:
        //     Reads a decimal value from the current stream and advances the current position
        //     of the stream by sixteen bytes.
        //
        // Returns:
        //     A decimal value read from the current stream.
        //
        // Exceptions:
        //   System.IO.EndOfStreamException:
        //     The end of the stream is reached.
        //
        //   System.ObjectDisposedException:
        //     The stream is closed.
        //
        //   System.IO.IOException:
        //     An I/O error occurs.
        public virtual decimal ReadDecimal()
        {
            return _Reader.ReadDecimal();
        }
        //
        // Summary:
        //     Reads an 8-byte floating point value from the current stream and advances
        //     the current position of the stream by eight bytes.
        //
        // Returns:
        //     An 8-byte floating point value read from the current stream.
        //
        // Exceptions:
        //   System.IO.EndOfStreamException:
        //     The end of the stream is reached.
        //
        //   System.ObjectDisposedException:
        //     The stream is closed.
        //
        //   System.IO.IOException:
        //     An I/O error occurs.
        public virtual double ReadDouble()
        {
            return _Reader.ReadDouble();
        }
        //
        // Summary:
        //     Reads a 2-byte signed integer from the current stream and advances the current
        //     position of the stream by two bytes.
        //
        // Returns:
        //     A 2-byte signed integer read from the current stream.
        //
        // Exceptions:
        //   System.IO.EndOfStreamException:
        //     The end of the stream is reached.
        //
        //   System.ObjectDisposedException:
        //     The stream is closed.
        //
        //   System.IO.IOException:
        //     An I/O error occurs.
        public virtual short ReadInt16()
        {
            byte[] temp = BitConverter.GetBytes(_Reader.ReadInt16());
            Array.Reverse(temp);
            return BitConverter.ToInt16(temp, 0);
        }
        //
        // Summary:
        //     Reads a 4-byte signed integer from the current stream and advances the current
        //     position of the stream by four bytes.
        //
        // Returns:
        //     A 4-byte signed integer read from the current stream.
        //
        // Exceptions:
        //   System.IO.EndOfStreamException:
        //     The end of the stream is reached.
        //
        //   System.ObjectDisposedException:
        //     The stream is closed.
        //
        //   System.IO.IOException:
        //     An I/O error occurs.
        public virtual int ReadInt32()
        {
            byte[] temp = BitConverter.GetBytes(_Reader.ReadInt32());
            Array.Reverse(temp);
            return BitConverter.ToInt32(temp, 0);
        }
        //
        // Summary:
        //     Reads an 8-byte signed integer from the current stream and advances the current
        //     position of the stream by eight bytes.
        //
        // Returns:
        //     An 8-byte signed integer read from the current stream.
        //
        // Exceptions:
        //   System.IO.EndOfStreamException:
        //     The end of the stream is reached.
        //
        //   System.ObjectDisposedException:
        //     The stream is closed.
        //
        //   System.IO.IOException:
        //     An I/O error occurs.
        public virtual long ReadInt64()
        {
            byte[] temp = BitConverter.GetBytes(_Reader.ReadInt64());
            Array.Reverse(temp);
            return BitConverter.ToInt64(temp, 0);
        }
        //
        // Summary:
        //     Reads a signed byte from this stream and advances the current position of
        //     the stream by one byte.
        //
        // Returns:
        //     A signed byte read from the current stream.
        //
        // Exceptions:
        //   System.IO.EndOfStreamException:
        //     The end of the stream is reached.
        //
        //   System.ObjectDisposedException:
        //     The stream is closed.
        //
        //   System.IO.IOException:
        //     An I/O error occurs.
        public virtual sbyte ReadSByte()
        {
            return _Reader.ReadSByte();
        }
        //
        // Summary:
        //     Reads a 4-byte floating point value from the current stream and advances
        //     the current position of the stream by four bytes.
        //
        // Returns:
        //     A 4-byte floating point value read from the current stream.
        //
        // Exceptions:
        //   System.IO.EndOfStreamException:
        //     The end of the stream is reached.
        //
        //   System.ObjectDisposedException:
        //     The stream is closed.
        //
        //   System.IO.IOException:
        //     An I/O error occurs.
        public virtual float ReadSingle()
        {
            return _Reader.ReadSingle();
        }
        //
        // Summary:
        //     Reads a string from the current stream. The string is prefixed with the length,
        //     encoded as an integer seven bits at a time.
        //
        // Returns:
        //     The string being read.
        //
        // Exceptions:
        //   System.IO.EndOfStreamException:
        //     The end of the stream is reached.
        //
        //   System.ObjectDisposedException:
        //     The stream is closed.
        //
        //   System.IO.IOException:
        //     An I/O error occurs.
        public virtual string ReadString()
        {
            return _Reader.ReadString();
        }
        //
        // Summary:
        //     Reads a 2-byte unsigned integer from the current stream using little-endian
        //     encoding and advances the position of the stream by two bytes.
        //
        // Returns:
        //     A 2-byte unsigned integer read from this stream.
        //
        // Exceptions:
        //   System.IO.EndOfStreamException:
        //     The end of the stream is reached.
        //
        //   System.ObjectDisposedException:
        //     The stream is closed.
        //
        //   System.IO.IOException:
        //     An I/O error occurs.
        public virtual ushort ReadUInt16()
        {
            byte[] temp = BitConverter.GetBytes(_Reader.ReadUInt16());
            Array.Reverse(temp);
            return BitConverter.ToUInt16(temp, 0);
        }
        //
        // Summary:
        //     Reads a 4-byte unsigned integer from the current stream and advances the
        //     position of the stream by four bytes.
        //
        // Returns:
        //     A 4-byte unsigned integer read from this stream.
        //
        // Exceptions:
        //   System.IO.EndOfStreamException:
        //     The end of the stream is reached.
        //
        //   System.ObjectDisposedException:
        //     The stream is closed.
        //
        //   System.IO.IOException:
        //     An I/O error occurs.
        public virtual uint ReadUInt32()
        {
            byte[] temp = BitConverter.GetBytes(_Reader.ReadUInt32());
            Array.Reverse(temp);
            return BitConverter.ToUInt32(temp, 0);
        }
        //
        // Summary:
        //     Reads an 8-byte unsigned integer from the current stream and advances the
        //     position of the stream by eight bytes.
        //
        // Returns:
        //     An 8-byte unsigned integer read from this stream.
        //
        // Exceptions:
        //   System.IO.EndOfStreamException:
        //     The end of the stream is reached.
        //
        //   System.IO.IOException:
        //     An I/O error occurs.
        //
        //   System.ObjectDisposedException:
        //     The stream is closed.
        public virtual ulong ReadUInt64()
        {
            byte[] temp = BitConverter.GetBytes(_Reader.ReadUInt64());
            Array.Reverse(temp);
            return BitConverter.ToUInt64(temp, 0);
        }
    }
}
