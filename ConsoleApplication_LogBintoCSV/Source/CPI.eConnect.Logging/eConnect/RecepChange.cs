﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Logging.eConnect
{
    public class RecepChange// : IRecordReader
    {
        public DateTime TimeStamp { get; set; }        
        public string MAC { get; set; }
        public ushort UserId { get; set; }
        public LogInterface PDUInterface { get; set; }
        public RecepChangeAction Action { get; set; }
        public byte[] StatusMap { get; set; }
        public byte[] ResetMap { get; set; }

        public void ReadRecord(EndianBinaryLogReader reader, LogRecord record)
        {
            TimeStamp = record.TimeStamp;
            MAC = record.MAC;
            UserId = reader.ReadUInt16();
            PDUInterface = (LogInterface)reader.ReadByte();
            Action = (RecepChangeAction)reader.ReadByte();
            StatusMap = reader.ReadBytes(6);
            ResetMap = reader.ReadBytes(6);
        }
    }
}
