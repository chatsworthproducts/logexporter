﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Logging.eConnect
{
    public class PDUInfo// : IRecordReader
    {
        public DateTime TimeStamp { get; set; }
        public string MAC { get; set; }
        public PDUInfoEntry[] Entries { get; set; }

        public void ReadRecord(EndianBinaryLogReader reader, LogRecord record)
        {
            // if it doesn't have a subclass id of 0 then
            //  we don't know how to process it.
            if (record.SubClassId != 0)
            {
                return;
            }

            TimeStamp = record.TimeStamp;

            Entries = new PDUInfoEntry[48];

            for (int i = 0; i < 48; i++)
            {
                Entries[i] = new PDUInfoEntry();
                Entries[i].TimeStamp = record.TimeStamp;
            }

            for (int i = 0; i < 48; i++)
            {
                byte[] mac = reader.ReadBytes(8);
                Entries[i].MacAddress = string.Format("{0:X2}:{1:X2}:{2:X2}:{3:X2}:{4:X2}:{5:X2}",
                    mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]); 
            }

            for (int i = 0; i < 48; i++)
            {
                byte[] modelBytes = reader.ReadBytes(20);
                Entries[i].Model = System.Text.Encoding.Default.GetString(modelBytes);
            }

            for (int i = 0; i < 48; i++)
            {
                Entries[i].userNumber = reader.ReadBytes(20);
            }

            for (int i = 0; i < 48; i++)
            {
                Entries[i].MCMIdNumber = reader.ReadBytes(20);
            }

            Entries[0].DChainType = reader.ReadByte();
        }
    }
}
