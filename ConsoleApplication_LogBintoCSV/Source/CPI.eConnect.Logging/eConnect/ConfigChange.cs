﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Logging.eConnect
{
    public class ConfigChange// : IRecordReader
    {
        public DateTime TimeStamp { get; set; }
        public string MAC { get; set; }
        public ushort UserId { get; set; }
        public LogConfigVariable ConfigVariable { get; set; }
        public byte Occurance { get; set; }
        public byte Size { get; set; }
        public ConfigDataTypes DataType { get; set; }
        public byte MultiOCC { get; set; }
        public LogInterface PDUInterface { get; set; }
        public byte[] NewValue { get; set; }

        public void ReadRecord(EndianBinaryLogReader reader, LogRecord record)
        {
            TimeStamp = record.TimeStamp;
            MAC = record.MAC;
            UserId = reader.ReadUInt16();
            ConfigVariable = (LogConfigVariable)reader.ReadUInt16();
            Occurance = reader.ReadByte();
            Size = reader.ReadByte();
            DataType = (ConfigDataTypes)reader.ReadByte();
            MultiOCC = reader.ReadByte();
            PDUInterface = (LogInterface)reader.ReadByte();
            NewValue = reader.ReadBytes(64);
        }

        public System.IO.BinaryReader Reader
        {
            get
            {                
                return new System.IO.BinaryReader(new System.IO.MemoryStream(NewValue));
            }
        }
    }
}
