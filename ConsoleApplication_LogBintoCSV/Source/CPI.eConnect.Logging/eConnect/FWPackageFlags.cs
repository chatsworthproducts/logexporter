﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Logging.eConnect
{
    [Flags]
    public enum FWPackageFlags
    {
        Bios = 0x01,
        Kernel = 0x02,
        Filesystem = 0x04,
        Patch = 0x08
    }
}
