﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Logging.eConnect
{
    public class ReadingStateInfo
    {
        public Dictionary<string, PDUInfoEntry> PDUInfoIndex { get; private set; }
        public List<AlarmInfo> AlarmInfoHist { get; private set; }
        public List<MeterInfo> MeterInfoHist { get; private set; }
        public List<GroupEntry> GroupEntryHist { get; private set; }
        public List<LogInOut> LogInOutHist { get; private set; }
        public List<ConfigChange> ConfigChangeHist { get; private set; }
        public List<RecepChange> RecepChangeHist { get; private set; }
        public List<FirmwareChange> FirmwareChangeHist { get; private set; }
        public List<PDUOptionChange> PDUOptionChangeHist { get; private set; }
        public List<UserChange> UserChangeHist { get; private set; }

        public ReadingStateInfo(LogReader reader)
        {
            this.PDUInfoIndex = new Dictionary<string, PDUInfoEntry>();
            this.AlarmInfoHist = new List<AlarmInfo>();            
            this.MeterInfoHist = new List<MeterInfo>();
            this.GroupEntryHist = new List<GroupEntry>();
            this.LogInOutHist = new List<LogInOut>();
            this.ConfigChangeHist = new List<ConfigChange>();
            this.RecepChangeHist = new List<RecepChange>();
            this.FirmwareChangeHist = new List<FirmwareChange>();
            this.PDUOptionChangeHist = new List<PDUOptionChange>();
            this.UserChangeHist = new List<UserChange>();

            reader.OnAlarmInfo += OnAlarmInfo;
            reader.OnConfigChange += OnConfigChange;
            reader.OnFirmwareChange += OnFirmwareChange;
            reader.OnGroupEntry += OnGroupEntry;
            //reader.OnLogInOut += OnLogInOut;
            reader.OnMeterInfo += OnMeterInfo;
            reader.OnPDUInfo += OnPDUInfo;
            reader.OnPDUOptionChange += OnPDUOptionChange;
            reader.OnRecepChange += OnRecepChange;
            reader.OnUserChange += OnUserChange;
        }

        public void OnUserChange(object sender, UserChange e)
        {            
            if (this.PDUInfoIndex.Keys.Contains(e.MAC))
            {
                this.PDUInfoIndex[e.MAC].UserChanges.Add(e);
            }
            else
            {
                this.UserChangeHist.Add(e);
            }
        }

        public void OnRecepChange(object sender, RecepChange e)
        {            
            if (this.PDUInfoIndex.Keys.Contains(e.MAC))
            {
                this.PDUInfoIndex[e.MAC].RecepChanges.Add(e);
            }
            else
            {
                this.RecepChangeHist.Add(e);
            }
        }

        public void OnPDUOptionChange(object sender, PDUOptionChange e)
        {            
            if (this.PDUInfoIndex.Keys.Contains(e.MAC))
            {
                this.PDUInfoIndex[e.MAC].OptionChanges.Add(e);
            }
            else
            {
                this.PDUOptionChangeHist.Add(e);
            }
        }

        public void OnPDUInfo(object sender, PDUInfo e)
        {            
            foreach (PDUInfoEntry i in e.Entries)
            {
                if ((i.MacAddress.Equals("00:00:00:00:00:00:00:00") == false) &&
                    (this.PDUInfoIndex.Keys.Contains(i.MacAddress) == false))
                {
                    this.PDUInfoIndex.Add(i.MacAddress, i);
                }
            }
        }

        public void OnMeterInfo(object sender, MeterInfo e)
        {            
            if (this.PDUInfoIndex.Keys.Contains(e.MAC))
            {
                foreach (BranchInfo b in e.Branch)
                {
                    this.PDUInfoIndex[e.MAC].BranchData.Add(b);
                }

                foreach(EnvironInfo ev in e.Environmental)
                {
                    this.PDUInfoIndex[e.MAC].EnvironmentData.Add(ev);
                }

                foreach(RecepInfo r in e.Recep)
                {
                    this.PDUInfoIndex[e.MAC].RecepData.Add(r);
                }
            }
            else
            {
                this.MeterInfoHist.Add(e);
            }
        }

        public void OnLogInOut(object sender, LogInOut e)
        {            
            if (this.PDUInfoIndex.Keys.Contains(e.MAC))
            {
                this.PDUInfoIndex[e.MAC].LogInOutInfo.Add(e);
            }
            else
            {
                this.LogInOutHist.Add(e);
            }
        }

        public void OnGroupEntry(object sender, GroupEntry e)
        {            
            this.GroupEntryHist.Add(e);
        }

        public void OnFirmwareChange(object sender, FirmwareChange e)
        {            
            if (this.PDUInfoIndex.Keys.Contains(e.MAC))
            {
                this.PDUInfoIndex[e.MAC].FirmwareChanges.Add(e);
            }
            else
            {
                this.FirmwareChangeHist.Add(e);
            }
        }

        public void OnConfigChange(object sender, ConfigChange e)
        {            
            if (this.PDUInfoIndex.Keys.Contains(e.MAC))
            {
                this.PDUInfoIndex[e.MAC].ConfigChanges.Add(e);
            }
            else
            {
                this.ConfigChangeHist.Add(e);
            }
        }

        public void OnAlarmInfo(object sender, AlarmInfo e)
        {            
            if (this.PDUInfoIndex.Keys.Contains(e.MAC))
            {
                this.PDUInfoIndex[e.MAC].Alarms.Add(e);
            }
            else
            {
                this.AlarmInfoHist.Add(e);
            }
        }
    }
}
