﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Logging.eConnect
{
    public enum FWChangeAction
    {
        UpdateMaster = 0,
        UpdateSecondary = 1,
        AbortMaster = 2
    }
}
