﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Logging.eConnect
{
    public class LogInOut// : IRecordReader
    {
        public DateTime TimeStamp { get; set; }
        public string MAC { get; set; }
        public ushort UserId { get; set; }
        public string User { get; set; }
        public LogInterface PDUInterface { get; set; }
        public LogIOAction Action { get; set; }

        public void ReadRecord(EndianBinaryLogReader reader, LogRecord record)
        {
            UserId = reader.ReadUInt16();
            byte[] userBytes = reader.ReadBytes(64);
            User = System.Text.Encoding.Default.GetString(userBytes);
            PDUInterface = (LogInterface)reader.ReadByte();
            Action = (LogIOAction)reader.ReadByte();
        }
    }
}
