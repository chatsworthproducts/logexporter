﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Logging.eConnect
{
    public class GroupDetails
    {
        public byte[] PDUId { get; set; }
        public byte RecepNumber { get; set; }
        public ushort Current { get; set; }
        public ushort Voltage { get; set; }
        public ushort Power { get; set; }
        public byte PowerFactor { get; set; }
        public UInt64 Energy { get; set; }
    }
}
