﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Logging.eConnect
{
    public class PDUInfoEntry
    {
        public DateTime TimeStamp { get; set; }
        public string MacAddress { get; set; }
        public string Model { get; set; }
        public byte[] userNumber { get; set; }
        public byte[] MCMIdNumber { get; set; }
        public byte DChainType { get; set; }

        public List<AlarmInfo> Alarms { get; set; }
        public List<BranchInfo> BranchData { get; set; }
        public List<RecepInfo> RecepData { get; set; }        
        public List<ConfigChange> ConfigChanges { get; set; }
        public List<EnvironInfo> EnvironmentData { get; set; }
        public List<FirmwareChange> FirmwareChanges { get; set; }
        public List<LogInOut> LogInOutInfo { get; set; }
        public List<RecepChange> RecepChanges { get; set; }
        public List<PDUOptionChange> OptionChanges { get; set; }

        public List<UserChange> UserChanges { get; set; }

        public PDUInfoEntry()
        {
            Alarms = new List<AlarmInfo>();
            BranchData = new List<BranchInfo>();
            ConfigChanges = new List<ConfigChange>();
            EnvironmentData = new List<EnvironInfo>();
            FirmwareChanges = new List<FirmwareChange>();
            LogInOutInfo = new List<LogInOut>();
            RecepData = new List<RecepInfo>();
            RecepChanges = new List<RecepChange>();
            OptionChanges = new List<PDUOptionChange>();
            UserChanges = new List<UserChange>();
        }
    }
}
