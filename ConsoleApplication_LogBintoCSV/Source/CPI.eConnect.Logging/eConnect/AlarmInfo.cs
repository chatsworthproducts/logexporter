﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Logging.eConnect
{
    public class AlarmInfo// : IRecordReader
    {
        public DateTime TimeStamp { get; set; }
        public string MAC { get; set; }
        public UInt32 AlarmsHigh { get; set; }
        public UInt32 AlarmsLow { get; set; }
        public UInt32 WarningsHigh { get; set; }
        public UInt32 WarnsingLow { get; set; }
        public UInt64 RecepAlarmsHigh { get; set; }
        public UInt64 RecepAlarmsLow { get; set; }
        public UInt64 RecepWarningHigh { get; set; }
        public UInt64 RecepWarningLow { get; set; }

        public void ReadRecord(EndianBinaryLogReader reader, LogRecord record)
        {
            TimeStamp = record.TimeStamp;
            MAC = record.MAC;
            AlarmsHigh = reader.ReadUInt32();
            AlarmsLow = reader.ReadUInt32();
            WarningsHigh = reader.ReadUInt32();
            WarnsingLow = reader.ReadUInt32();
            RecepAlarmsHigh = reader.ReadUInt64();
            RecepAlarmsLow = reader.ReadUInt64();
            RecepWarningHigh = reader.ReadUInt64();
            RecepWarningLow = reader.ReadUInt64();
        }
    }
}
