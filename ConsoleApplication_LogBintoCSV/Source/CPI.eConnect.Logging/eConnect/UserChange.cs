﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Logging.eConnect
{
    public class UserChange// : IRecordReader
    {
        public DateTime TimeStamp { get; set; }
        public string MAC { get; set; }
        public ushort UserID { get; set; }
        public ushort ChangeUserID { get; set; }
        public ushort Permission { get; set; }
        public LogInterface PDUInterface { get; set; }
        public UserChangeAction Action { get; set; }
        public string UserName { get; set; }

        public void ReadRecord(EndianBinaryLogReader reader, LogRecord record)
        {
            TimeStamp = record.TimeStamp;
            MAC = record.MAC;
            UserID = reader.ReadUInt16();
            ChangeUserID = reader.ReadUInt16();
            Permission = reader.ReadUInt16();
            PDUInterface = (LogInterface)reader.ReadByte();
            Action = (UserChangeAction)reader.ReadByte();
            byte[] userBytes = reader.ReadBytes(64);
            UserName = System.Text.Encoding.Default.GetString(userBytes);  
        }
    }
}
