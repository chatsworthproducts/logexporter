﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CPI.eConnect.Logging.eConnect
{   
    public class LogReader : IDisposable
    {
        private EndianBinaryLogReader _Reader;        

        public LogReader(string path)
        {
            _Reader = new EndianBinaryLogReader(path);
        }

        public event EventHandler<AlarmInfo> OnAlarmInfo;
        public event EventHandler<PDUInfo> OnPDUInfo;
        public event EventHandler<MeterInfo> OnMeterInfo;
        public event EventHandler<GroupEntry> OnGroupEntry;
        public event EventHandler<LogInOut> OnLogInOut;
        public event EventHandler<ConfigChange> OnConfigChange;
        public event EventHandler<RecepChange> OnRecepChange;
        public event EventHandler<FirmwareChange> OnFirmwareChange;
        public event EventHandler<PDUOptionChange> OnPDUOptionChange;
        public event EventHandler<UserChange> OnUserChange;        

        public void ReadEntry()
        {
            LogRecord record = new LogRecord();
            record.ReadLogRecord(_Reader);

            switch(record.ClassId)
            {
                case CPILogClassID.Info:
                    PDUInfo info = new PDUInfo();
                    info.ReadRecord(_Reader, record);
                    if(OnPDUInfo != null )
                    {
                        OnPDUInfo(this, info);
                    }
                    break;
                case CPILogClassID.Meter:
                    MeterInfo meterinfo = new MeterInfo();
                    meterinfo.ReadRecord(_Reader, record);
                    if(OnMeterInfo != null)
                    {
                        OnMeterInfo(this, meterinfo);
                    }
                    break;
                case CPILogClassID.Group:
                    GroupEntry groupinfo = new GroupEntry();
                    groupinfo.ReadRecord(_Reader, record);
                    if(OnGroupEntry != null)
                    {
                        OnGroupEntry(this, groupinfo);
                    }
                    break;
                case CPILogClassID.Alarm:
                    AlarmInfo alarminfo = new AlarmInfo();
                    alarminfo.ReadRecord(_Reader, record);
                    if(OnAlarmInfo != null)
                    {
                        OnAlarmInfo(this, alarminfo);
                    }
                    break;
                case CPILogClassID.LogIO:
                    LogInOut logioinfo = new LogInOut();
                    logioinfo.ReadRecord(_Reader, record);
                    if(OnLogInOut != null)
                    {
                        OnLogInOut(this, logioinfo);
                    }
                    break;
                case CPILogClassID.ConfigChange:
                    ConfigChange configinfo = new ConfigChange();
                    configinfo.ReadRecord(_Reader, record);
                    if(OnConfigChange != null)
                    {
                        OnConfigChange(this, configinfo);
                    }
                    break;
                case CPILogClassID.RecepChange:
                    RecepChange recepinfo = new RecepChange();
                    recepinfo.ReadRecord(_Reader, record);
                    if(OnRecepChange != null)
                    {
                        OnRecepChange(this, recepinfo);
                    }
                    break;
                case CPILogClassID.FirmwareUpdate:
                    FirmwareChange fwinfo = new FirmwareChange();
                    fwinfo.ReadRecord(_Reader, record);
                    if(OnFirmwareChange != null)
                    {
                        OnFirmwareChange(this, fwinfo);
                    }
                    break;
                case CPILogClassID.PDUOptionChange:
                    PDUOptionChange optioninfo = new PDUOptionChange();
                    optioninfo.ReadRecord(_Reader, record);
                    if(OnPDUOptionChange != null)
                    {
                        OnPDUOptionChange(this, optioninfo);
                    }
                    break;
                case CPILogClassID.UserChange:
                    UserChange userinfo = new UserChange();
                    userinfo.ReadRecord(_Reader, record);
                    if(OnUserChange != null)
                    {
                        OnUserChange(this, userinfo);
                    }
                    break;
            }
        }

        public void Dispose()
        {
            _Reader.Dispose();
        }
    }
}
