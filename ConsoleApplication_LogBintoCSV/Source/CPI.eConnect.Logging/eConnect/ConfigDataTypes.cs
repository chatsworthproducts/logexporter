﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Logging.eConnect
{
    public enum ConfigDataTypes
    {
        Byte = 0,
        Word = 1,
        DWord = 2,
        String = 3,
        IP = 4,
        MAC = 5,
        IPV6 = 6,
        QWord = 7,
        Password = 8
    }
}
