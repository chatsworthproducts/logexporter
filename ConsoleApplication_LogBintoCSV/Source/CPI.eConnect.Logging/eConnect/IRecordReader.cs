﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CPI.eConnect.Logging.eConnect
{
    public interface IRecordReader
    {
        void ReadRecord(BinaryReader reader, LogRecord record);
    }
}
