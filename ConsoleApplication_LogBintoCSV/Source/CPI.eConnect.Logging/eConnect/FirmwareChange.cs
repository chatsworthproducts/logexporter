﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Logging.eConnect
{
    public class FirmwareChange //: IRecordReader
    {
        public DateTime TimeStamp { get; set; }
        public string MAC { get; set; }
        public ushort UserID { get; set; }
        public LogInterface PDUInterface { get; set; }
        public FWChangeAction Action { get; set; }
        public FWPackageFlags UpdateMap { get; set; }
        public FWPackageFlags PackageMap { get; set; }
        public UInt32 BiosRev { get; set; }
        public UInt32 KernelRev { get; set; }
        public UInt32 FileSysRev { get; set; }
        public UInt32 PatchRev { get; set; }
        public byte UpdateResult { get; set; }

        public void ReadRecord(EndianBinaryLogReader reader, LogRecord record)
        {
            TimeStamp = record.TimeStamp;
            MAC = record.MAC;

            if (record.DataLength == 5)
            {
                UserID = reader.ReadUInt16();
                PDUInterface = (LogInterface)reader.ReadByte();
                UpdateMap = (FWPackageFlags)reader.ReadByte();
                UpdateResult = reader.ReadByte();
            }
            else
            {
                UserID = reader.ReadUInt16();
                PDUInterface = (LogInterface)reader.ReadByte();
                Action = (FWChangeAction)reader.ReadByte();
                UpdateMap = (FWPackageFlags)reader.ReadByte();
                PackageMap = (FWPackageFlags)reader.ReadByte();
                BiosRev = reader.ReadUInt32();
                KernelRev = reader.ReadUInt32();
                FileSysRev = reader.ReadUInt32();
                PatchRev = reader.ReadUInt32();
                UpdateResult = reader.ReadByte();
            }
        }
    }
}
