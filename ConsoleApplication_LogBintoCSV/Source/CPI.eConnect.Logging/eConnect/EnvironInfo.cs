﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Logging.eConnect
{
    public class EnvironInfo
    {
        public EnvironInfo(int type)
        {
            this.EnvironType = type;
        }

        public DateTime TimeStamp { get; set; }
        public string MAC { get; set; }
        public int EnvironType { get; set; }
        public ushort Value { get; set; }
    }
}
