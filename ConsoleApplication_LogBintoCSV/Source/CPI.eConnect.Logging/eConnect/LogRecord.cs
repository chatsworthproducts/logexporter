﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CPI.eConnect.Logging.eConnect
{
    public class LogRecord// : ILogRecordReader
    {
        public byte HeaderCrc { get; set; }
        public byte DataCrc { get; set; }
        public int DataLength { get; set; }
        public DateTime TimeStamp { get; set; }
        public byte[] PDUID { get; set; }
        public string MAC { get; set; }
        public CPILogClassID ClassId { get; set; }
        public int SubClassId { get; set; }

        static ushort FixEndianness_16Bit(ushort value)
        {
            byte[] temp = BitConverter.GetBytes(value);
            Array.Reverse(temp);
            return BitConverter.ToUInt16(temp, 0);
        }

        static uint FixEndianness_32Bit(uint value)
        {
            byte[] temp = BitConverter.GetBytes(value);
            Array.Reverse(temp);
            return BitConverter.ToUInt32(temp, 0);
        }

        public void ReadLogRecord(EndianBinaryLogReader reader)
        {
            HeaderCrc = reader.ReadByte();
            DataCrc = reader.ReadByte();
            DataLength = reader.ReadUInt16();
            uint dateTime = reader.ReadUInt32();
            TimeStamp = Time_T2DateTime(dateTime);
            PDUID = reader.ReadBytes(3);
            MAC = string.Format("00:0E:D3:{0:X2}:{1:X2}:{2:X2}", PDUID[0], PDUID[1], PDUID[2]);
            ClassId = (CPILogClassID)reader.ReadByte();
            SubClassId = reader.ReadByte();
            reader.ReadBytes(3); // reserved bytes;            
        }

        
        private DateTime Time_T2DateTime(uint time_t)
        {
            return new DateTime(1970, 1, 1).AddSeconds(time_t);
            //long win32FileTime = 10000000 * (long)time_t + 116444736000000000;
            //return DateTime.FromFileTimeUtc(win32FileTime);
        }
        
    }
}
