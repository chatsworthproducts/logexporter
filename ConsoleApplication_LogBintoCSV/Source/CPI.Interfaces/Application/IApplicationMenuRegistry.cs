﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.Interfaces.Application
{
    public interface IApplicationMenuRegistry
    {
        void RegisterMenuItem(string menuItemName, string description, Type viewType);
    }
}
