﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CPI.Interfaces.Application
{
    public interface IApplicationCommands
    {
        void CloseView(object view);
        void OpenView(string title, Type viewType);
        void OpenView(string title, UIElement viewToOpen);
    }
}
