//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CPI.Data.PDULog
{
    using System;
    using System.Collections.Generic;
    
    public partial class ConfigChangeHistory
    {
        public long id { get; set; }
        public long systemObjectId { get; set; }
        public System.DateTime timestamp { get; set; }
        public int variable { get; set; }
        public int occurance { get; set; }
        public int size { get; set; }
        public int dataType { get; set; }
        public int pduInterface { get; set; }
        public byte[] value { get; set; }
    
        public virtual SystemObject SystemObject { get; set; }
    }
}
