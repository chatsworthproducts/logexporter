﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Renci.SshNet;

namespace CPI.eConnect.Communications
{
    public class SSHMCMConsole : IMCMChannel, IDisposable
    {
        private string _IPAddress;
        private int _Port;
        private string _User;
        private string _Password;
        private Renci.SshNet.SshClient _Client;
        private Renci.SshNet.ShellStream _Stream;

        public IPDUCommand FirmwareUpgradeFromUSB { get { return new StreamCommands.FirmwareUpgradeFromUSBCmd(_Stream); } }

        public IPDUCommand FirmwareUpgradeFromHTTP { get { return new StreamCommands.FirmwareUpgradeFromHTTPCmd(_Stream); } }

        public IPDUCommand FirmwareUpgradeFromTFTP { get { return new StreamCommands.FirmwareUpgradeFromTFTPCmd(_Stream); } }

        public IPDUCommand FirmwareUpgradeChain { get { return new StreamCommands.FirmwareUpgradeChainCmd(_Stream); } }

        public IPDUCommand Login { get { return new StreamCommands.LoginStreamCmd(_Stream); } }

        public IPDUCommand Logout { get { return new StreamCommands.LogoutStreamCmd(_Stream); } }

        public IPDUCommand SetModel { get { return new StreamCommands.SetModelCmd(_Stream); } }

        public IPDUCommand SetMACAddress { get { return new StreamCommands.SetMACAddressCmd(_Stream); } }

        public IPDUCommand SetAttrib { get { return new StreamCommands.SetAttribCmd(_Stream); } }

        public IPDUCommand SetSerialNumber { get { return new StreamCommands.SetSerialNumberCmd(_Stream); } }

        public IPDUCommand SetMCMIdNum { get { return new StreamCommands.SetMCMIdNumCmd(_Stream); } }

        public IPDUCommand DefManualConfigVar { get { return new StreamCommands.DefManualConfigVarCmd(_Stream); } }

        public IPDUCommand SetConfigVar { get { return new StreamCommands.SetConfigVarCmd(_Stream); } }

        public IPDUCommand RebootPDU { get { return new StreamCommands.RebootPDUCmd(_Stream); } }

        public IPDUCommand RestartModule { get { return new StreamCommands.RestartModuleCmd(_Stream); } }

        public IPDUCommand ResetAlarmStatus { get { return new StreamCommands.ResetAlarmStatusCmd(_Stream); } }

        public IPDUCommand GetMetrics { get { return new StreamCommands.GetMetricsCmd(_Stream); } }

        public IPDUCommand GetEnergy { get { return new StreamCommands.GetEnergyCmd(_Stream); } }

        public IPDUCommand LockCalibrationMetricsMode { get { return new StreamCommands.LockCalibrationMetricsModeCmd(_Stream); } }

        public IPDUCommand UnlockCalibrationMetricsMode { get { return new StreamCommands.UnlockCalibrationMetricsModeCmd(_Stream); } }

        public IPDUCommand GetCalibrationMetric { get { return new StreamCommands.GetCalibrationMetricCmd(_Stream); } }

        public IPDUCommand GetPowerStatus { get { return new StreamCommands.GetPowerStatusCmd(_Stream); } }

        public IPDUCommand GetResetStatus { get { return new StreamCommands.GetResetStatusCmd(_Stream); } }

        public IPDUCommand GetAlarmStatus { get { return new StreamCommands.GetAlarmStatusCmd(_Stream); } }

        public IPDUCommand SetPowerCntrl { get { return new StreamCommands.SetPowerCntrlCmd(_Stream); } }

        public IPDUCommand SetPowerReset { get { return new StreamCommands.SetPowerResetCmd(_Stream); } }

        public IPDUCommand GetEnvironmental { get { return new StreamCommands.GetEnvironmentalCmd(_Stream); } }

        public IPDUCommand SetCalibration { get { return new StreamCommands.SetCalibrationCmd(_Stream); } }

        public IPDUCommand GetCalibration { get { return new StreamCommands.GetCalibrationCmd(_Stream); } }

        public IPDUCommand SaveCalibration { get { return new StreamCommands.SaveCalibrationCmd(_Stream); } }

        public IPDUCommand GetChainPDUList { get { return new StreamCommands.GetChainPDUListCmd(_Stream); } }

        public IPDUCommand SetAdvandedMode { get { return new StreamCommands.SetAdvandedModeCmd(_Stream); } }

        public IPDUCommand GetConfig { get { return new StreamCommands.GetConfigCmd(_Stream); } }

        public IPDUCommand GetModel { get { return new StreamCommands.GetModelCmd(_Stream); } }

        public IPDUCommand GetMACAddress { get { return new StreamCommands.GetMACAddressCmd(_Stream); } }

        public IPDUCommand GetAttrib { get { return new StreamCommands.GetAttribCmd(_Stream); } }

        public IPDUCommand GetSerialNumber { get { return new StreamCommands.GetSerialNumberCmd(_Stream); } }

        public IPDUCommand GetMCMIdNumber { get { return new StreamCommands.GetMCMIdNumberCmd(_Stream); } }

        public IPDUCommand GetVersion { get { return new StreamCommands.GetVersionCmd(_Stream); } }

        public IPDUCommand GetModulesVersion { get { return new StreamCommands.GetModulesVersionCmd(_Stream); } }

        public IPDUCommand GetConfigVar { get { return new StreamCommands.GetConfigVarCmd(_Stream); } }

        public void Connect(string ipAddress, int port, string user, string password)
        {
            _Client = new SshClient(ipAddress, port, user, password);
            _Client.Connect();
            _Stream = _Client.CreateShellStream("mcmcontrol", 80, 256, 80, 80, 1024);
            bool ready = false;
            
            while(ready == false)
            {
                string data = _Stream.Read();
                System.Diagnostics.Debug.Write(data);
                ready = data.Contains("?");
            }
            _Stream.Flush();
        }

        public void Dispose()
        {
            _Stream.Close();
            _Client.Disconnect();
        }
    }
}
