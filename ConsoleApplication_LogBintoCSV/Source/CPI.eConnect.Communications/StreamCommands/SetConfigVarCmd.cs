﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Communications.StreamCommands
{
    public class SetConfigVarCmd : BaseStreamCommand
    {
        public SetConfigVarCmd(System.IO.Stream commandStream) :
            base(commandStream)
        {
        }

        override public bool Execute()
        {
            AssertParameter("pdu index");
            AssertParameter("var id");
            AssertParameter("data");
            AssertParameter("var index");

            string cmd = string.Format("{0} {1:x} {2:x} {3} {4:x}\r", PDU_SET_CFGVAR_CMDSTR, GetParameterAsString("pdu index"),
                GetParameterAsString("var id"), GetParameterAsString("data"), GetParameterAsString("var index"));
            int response = 0;
            SendCmdAndWait(cmd, new string[] { "OK", "Bad Command", "FAILED!" }, 2000, out response);

            if (response != 0)
            {
                return false;
            }

            return true;
        }
    }
}
