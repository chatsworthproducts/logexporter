﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Communications.StreamCommands
{
    public class SetSerialNumberCmd : BaseStreamCommand
    {
        public SetSerialNumberCmd(System.IO.Stream commandStream) :
            base(commandStream)
        {
        }

        override public bool Execute()
        {
            AssertParameter("serial number");

            string cmd = string.Format("{0} {1} {2}\r", PDU_SET_SERIALNUM_CMDSTR, GetParameterAsString("serial number"), PDU_MAGIC_WORD_STR);
            int response = 0;
            SendCmdAndWait(cmd, new string[] { "OK", "Bad Command", "FAILED!" }, 2000, out response);

            if (response != 0)
            {
                return false;
            }

            return true;
        }
    }
}
