﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Communications.StreamCommands
{
    public class GetEnergyCmd : BaseStreamCommand
    {
        public GetEnergyCmd(System.IO.Stream commandStream) :
            base(commandStream)
        {
        }

        override public bool Execute()
        {
            AssertParameter("pdu index");
            AssertParameter("outlet flag");
            AssertParameter("port");
            
            string cmd = string.Format("{0} {1:x} {2} {3:x}\r", PDU_GET_ENERGY_CMDSTR, 
                GetParameterAsString("pdu index"), GetParameterAsString("outlet flag"), GetParameterAsString("port"));
            ResponseBuilder builder = new ResponseBuilder();
            this.Flush();
            this.NewData += new EventHandler<OnSerialDataEventArgs>(builder.MCMResponseHandler);
            SendCmd(cmd);
            if (builder.WaitForResponse(5000) == true)
            {
                string returnData = builder.ToString();
            }

            return true;
        }
    }
}
