﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Communications.StreamCommands
{
    public class LogoutStreamCmd : BaseStreamCommand
    {
        public LogoutStreamCmd(System.IO.Stream commandStream) :
            base(commandStream)
        {
        }

        override public bool Execute()
        {
            SendCmdAndWait(ADMIN_LOGOUT_CMDSTR, "CPI Event ID", 2000);
            return true;
        }
    }
}
