﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Communications.StreamCommands
{
    public class GetEnvironmentalCmd : BaseStreamCommand
    {
        public GetEnvironmentalCmd(System.IO.Stream commandStream) :
            base(commandStream)
        {
        }

        override public bool Execute()
        {
            AssertParameter("pdu index");
            AssertParameter("raw flag");

            string cmd = string.Format("{0} {1:x} {2}\r", PDU_GET_EVALUE_TEMPHUMID_CMDSTR, 
                GetParameterAsString("pdu index"), GetParameterAsInt("raw flag"));
            ResponseBuilder builder = new ResponseBuilder();
            this.Flush();
            this.NewData += new EventHandler<OnSerialDataEventArgs>(builder.MCMResponseHandler);
            SendCmd(cmd);

            if (builder.WaitForResponse(5000) == true)
            {
                string returnData = builder.ToString();
            }

            return true;
        }
    }
}
