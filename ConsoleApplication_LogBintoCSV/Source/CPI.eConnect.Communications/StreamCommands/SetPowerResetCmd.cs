﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Communications.StreamCommands
{
    public class SetPowerResetCmd : BaseStreamCommand
    {
        public SetPowerResetCmd(System.IO.Stream commandStream) :
            base(commandStream)
        {
        }

        override public bool Execute()
        {
            AssertParameter("pdu index");
            AssertParameter("outlet number");
            AssertParameter("reset flag");

            string cmd = string.Format("{0} {1:x} {2:x} {3}\r", PDU_SET_RECEPRESET_CMDSTR, GetParameterAsString("pdu index"),
                GetParameterAsString("outlet number"), GetParameterAsInt("reset flag"));
            int response = 0;
            SendCmdAndWait(cmd, new string[] { "OK", "Bad Command", "FAILED!" }, 2000, out response);

            if (response != 0)
            {
                return false;
            }

            return true;
        }
    }
}
