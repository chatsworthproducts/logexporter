﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPI.eConnect.Communications;

namespace CPI.eConnect.Communications.StreamCommands
{
    public abstract class BaseStreamCommand : IPDUCommand, IDisposable
    {
        protected const string PDU_MAGIC_WORD_STR = "95132408";
        protected const string ADMIN_LOGOUT_CMDSTR = "!q\r";
        protected const string PDU_ADVANCED_MODE_CMDSTR = "#)(*&^\r";
        protected const string PDU_GET_MODEL_CMDSTR = "u";
        protected const string PDU_SET_MODEL_CMDSTR = "#S";
        protected const string PDU_GET_ATTRIB_CMDSTR = "q";
        protected const string PDU_SET_ATTRIB_CMDSTR = "#Q";
        protected const string PDU_GET_MCMIDNUM_CMDSTR = "z";
        protected const string PDU_SET_MCMIDNUM_CMDSTR = "#Z";
        protected const string PDU_GET_SERIALNUM_CMDSTR = "y";
        protected const string PDU_SET_SERIALNUM_CMDSTR = "#Y";
        protected const string PDU_GET_MACADDR_CMDSTR = "M";
        protected const string PDU_SET_MACADDR_CMDSTR = "#M";
        protected const string PDU_GET_VERSION_CMDSTR = "v";
        protected const string PDU_GET_CONFIG_CMDSTR = "V";
        protected const string PDU_GET_CFGVAR_CMDSTR = "h";
        protected const string PDU_SET_CFGVAR_CMDSTR = "H";
        protected const string PDU_MANUFDEF_CFGVAR_CMDSTR = "#D";
        protected const string PDU_RESTART_APP_CMDSTR = "#x";
        protected const string PDU_REBOOT_PDU_CMDSTR = "#X";
        protected const string PDU_RESET_ALARMS_CMDSTR = "N";
        protected const string PDU_GET_MODSVERSION_CMDSTR = "U";
        protected const string PDU_SAVE_CALIB_CMDSTR = "#W";
        protected const string PDU_ERASE_CALIB_CMDSTR = "#E";
        protected const string PDU_SET_CALIB_CMDSTR = "#C";
        protected const string PDU_GET_CALIB_CMDSTR = "#c";
        protected const string PDU_LOCK_CALIBMETR_CMDSTR = "#L";
        protected const string PDU_UNLOCK_CALIBMETR_CMDSTR = "#l";
        protected const string PDU_GET_CALIBMETRIC_CMDSTR = "#e";
        protected const string PDU_GET_METRICS_CMDSTR = "G";
        protected const string PDU_SET_RECEPPWRCTRL_CMDSTR = "R";
        protected const string PDU_GET_RECEPPWRSTATUS_CMDSTR = "P";
        protected const string PDU_GET_RECEPRESSTATUS_CMDSTR = "p";
        protected const string PDU_GET_ALARMSTATUS_CMDSTR = "n";
        protected const string PDU_SET_RECEPRESET_CMDSTR = "o";
        protected const string PDU_GET_EVALUE_TEMPHUMID_CMDSTR = "J";
        protected const string PDU_GET_CHAINEDPDULIST_CMDSTR = "a";
        protected const string PDU_SET_UPDATEFW_CMDSTR = "#U";
        protected const string PDU_SET_UPDATECHAIN_CMDSTR = "#u";
        protected const string PDU_GET_ENERGY_CMDSTR = "E";
        protected const string PDU_TEST_XLOG_CMDSTR = "#b logupload";
        protected const string PDU_TEST_EMAIL_CMDSTR = "#b email";

        protected const string PDU_METRIC_CURRONLY_TYPESTR = "I";
        protected const string PDU_METRIC_VOLTONLY_TYPESTR = "V";
        protected const string PDU_METRIC_POWERONLY_TYPESTR = "P";
        protected const string PDU_METRIC_PFACTONLY_TYPESTR = "F";
        protected const string PDU_METRIC_LINEONLY_TYPESTR = "L";
        protected const string PDU_METRIC_IVPFALLY_TYPESTR = "A";
        protected const string PDU_METRIC_CURRVOLT_TYPESTR = "B";
        protected const string PDU_METRIC_SOCKCURR_TYPESTR = "i";
        protected const string PDU_METRIC_SOCKPOWER_TYPESTR = "p";
        protected const string PDU_METRIC_CIRCENER_TYPESTR = "E 0";
        protected const string PDU_METRIC_SOCKENER_TYPESTR = "E 1";

        private System.IO.Stream _CommandStream;
        private Queue<string> _Responses = new Queue<string>();
        private object _SyncObject = new object();
        private string _BufferString = string.Empty;
        private char _Terminator = '\n';
        private System.Threading.ManualResetEvent _ResponseRecv = new System.Threading.ManualResetEvent(false);
        private System.Threading.ManualResetEvent _HoldResponses = new System.Threading.ManualResetEvent(false);
        private System.Threading.ManualResetEvent _Shutdown = new System.Threading.ManualResetEvent(false);

        private string[] _WaitData;
        private int _WaitID = 0;
        private System.Threading.ManualResetEvent _WaitResponseRecv = new System.Threading.ManualResetEvent(false);

        private System.Threading.Thread _WorkerThread;
        private System.Threading.Thread _ReaderThread;

        public BaseStreamCommand(System.IO.Stream commandStream)
        {
            this._CommandStream = commandStream;
            this.Parameters = new Dictionary<string, object>();
        }

        public Dictionary<string, object> Parameters { get; private set; }

        public Dictionary<string, object> CommandData { get; private set; }

        protected EventHandler<OnSerialDataEventArgs> NewData;

        abstract public bool Execute();

        protected void Start()
        {            
            this._WorkerThread = new System.Threading.Thread(new System.Threading.ThreadStart(this.Worker));
            this._WorkerThread.Start();

            this._ReaderThread = new System.Threading.Thread(new System.Threading.ThreadStart(this.streamPortDataReceived));
            this._ReaderThread.Start();            
        }

        protected int SendCmdAndWait(string cmd, string response, int timeout)
        {
            SetWaitRespone(response);
            SendCmd(cmd);
            if (_WaitResponseRecv.WaitOne(timeout) == false)
            {
                return 1;
            }
            Flush();
            return 0;
        }

        protected int SendCmdAndWait(string cmd, string[] response, int timeout, out int responseId)
        {
            responseId = -1;
            SetWaitRespone(response);
            SendCmd(cmd);
            if (_WaitResponseRecv.WaitOne(timeout) == false)
            {
                return 1;
            }
            Flush();
            responseId = _WaitID;
            return 0;
        }

        protected void SetWaitRespone(string wait)
        {
            _WaitResponseRecv.Reset();
            _WaitData = new string[] { wait };
            _WaitID = 0;
        }

        protected void SetWaitRespone(string[] wait)
        {
            _WaitResponseRecv.Reset();
            _WaitData = wait;
            _WaitID = 0;
        }

        protected void SendCmd(string cmd)
        {
            byte[] cmdbytes = System.Text.Encoding.UTF8.GetBytes(cmd);// ASCIIEncoding.ASCII.GetBytes(cmd);
            for (int i = 0; i < cmdbytes.Length; )
            {
                this._CommandStream.Write(cmdbytes, i, Math.Min(60, cmdbytes.Length - i));
                i += Math.Min(60, cmdbytes.Length - i);
            }
        }

        protected void Flush()
        {
            while (this._Responses.Count() > 0)
            {
                System.Threading.Thread.Sleep(1);
            }
            System.Threading.Thread.Sleep(100);
        }

        protected void AssertParameter(string paramName)
        {
            if(this.Parameters.Keys.Contains(paramName) == false)
            {
                throw new ArgumentException("", paramName);
            }
        }

        protected string GetParameterAsString(string paramName)
        {
            return this.Parameters[paramName].ToString();
        }

        protected int GetParameterAsInt(string paramName)
        {
            int integer = 0;

            int.TryParse(this.Parameters[paramName].ToString(), out integer);
            return integer;
        }

        private void Worker()
        {
            System.Threading.ManualResetEvent[] events = new System.Threading.ManualResetEvent[] {
                this._Shutdown,
                this._ResponseRecv,
                this._HoldResponses
            };

            while (true)
            {
                int triggeredHandle = System.Threading.ManualResetEvent.WaitAny(events);
                switch (triggeredHandle)
                {
                    case 0: // shutdown
                        System.Diagnostics.Debug.WriteLine("Goodbye");
                        return;
                    case 1: // response
                        string response;
                        while (this._Responses.Count() > 0)
                        {
                            lock (this._SyncObject)
                            {
                                response = this._Responses.Dequeue();
                                this._ResponseRecv.Reset();
                            }

                            System.Diagnostics.Debug.WriteLine("DATA: {0}", response);

                            if (_WaitData != null)
                            {
                                for (int i = 0; i < _WaitData.Length; i++)
                                {
                                    if (response.Contains(_WaitData[i]) == true)
                                    {
                                        _WaitID = i;
                                        _WaitResponseRecv.Set();
                                    }
                                }
                            }

                            if (this.NewData != null)
                            {
                                this.NewData(this, new OnSerialDataEventArgs(response));
                            }
                        }
                        break;
                    case 2: // hold
                        while (this._HoldResponses.WaitOne(1) == true)
                        {
                            System.Threading.Thread.Sleep(1);
                        }
                        break;
                }
            }
        }

        private void streamPortDataReceived()
        {
            System.Threading.ManualResetEvent[] events = new System.Threading.ManualResetEvent[] {
                this._Shutdown
            };

            while (true)
            {
                if(this._Shutdown.WaitOne(0) == true )
                {
                    return;
                }

                // Initialize a buffer to hold the received data
                byte[] buffer = new byte[256];

                // There is no accurate method for checking how many bytes are read
                // unless you check the return from the Read method
                int bytesRead = this._CommandStream.Read(buffer, 0, buffer.Length);

                // For the example assume the data we are received is ASCII data.
                this._BufferString += Encoding.ASCII.GetString(buffer, 0, bytesRead);

                // Check if string contains the terminator 
                while (this._BufferString.IndexOf(this._Terminator) > -1)
                {
                    // If tString does contain terminator we cannot assume that it is the last character received
                    string workingString = this._BufferString.Substring(0, this._BufferString.IndexOf(this._Terminator));

                    // Remove the data up to the terminator from tString
                    this._BufferString = this._BufferString.Substring(this._BufferString.IndexOf(this._Terminator) + 1);

                    lock (this._SyncObject)
                    {
                        this._Responses.Enqueue(workingString);
                        this._ResponseRecv.Set();
                    }
                }
            }
        }

        public void Dispose()
        {
            if (this._Shutdown != null)
            {
                this._Shutdown.Set();
                this._ReaderThread.Join();
                this._WorkerThread.Join();

                this._Shutdown = null;
            }
        }
    }
}
