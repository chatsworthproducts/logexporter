﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Communications.StreamCommands
{
    public class FirmwareUpgradeChainCmd : BaseStreamCommand
    {
        public FirmwareUpgradeChainCmd(System.IO.Stream commandStream) :
            base(commandStream)
        {
        }

        override public bool Execute()
        {
            string cmd = string.Format("{0} {1} 1\r", PDU_SET_UPDATECHAIN_CMDSTR, PDU_MAGIC_WORD_STR);
            int response = 0;
            SendCmdAndWait(cmd, new string[] { "OK", "Bad Command", "FAILED!" }, 30000, out response);

            if (response != 0)
            {
                return false;
            }

            return true;
        }
    }
}
