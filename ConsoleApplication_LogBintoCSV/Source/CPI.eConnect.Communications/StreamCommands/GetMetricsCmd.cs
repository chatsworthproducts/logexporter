﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPI.eConnect.Communications;

namespace CPI.eConnect.Communications.StreamCommands
{
    public class GetMetricsCmd : BaseStreamCommand
    {
        public GetMetricsCmd(System.IO.Stream commandStream) :
            base(commandStream)
        {
        }

        override public bool Execute()
        {
            string metricString = string.Empty;

            AssertParameter("metric");
            AssertParameter("pdu index");
            AssertParameter("port");

            switch ((Metric)GetParameterAsInt("metric"))
            {
                case Metric.Current:
                    metricString = PDU_METRIC_CURRONLY_TYPESTR;
                    break;
                case Metric.Voltage:
                    metricString = PDU_METRIC_VOLTONLY_TYPESTR;
                    break;
                case Metric.Power:
                    metricString = PDU_METRIC_POWERONLY_TYPESTR;
                    break;
                case Metric.PowerFactor:
                    metricString = PDU_METRIC_PFACTONLY_TYPESTR;
                    break;
                case Metric.CurrentVolt:
                    metricString = PDU_METRIC_CURRVOLT_TYPESTR;
                    break;
                case Metric.ALL:
                    metricString = PDU_METRIC_IVPFALLY_TYPESTR;
                    break;
                case Metric.SocketCurrent:
                    metricString = PDU_METRIC_SOCKCURR_TYPESTR;
                    break;
                case Metric.SocketPower:
                    metricString = PDU_METRIC_SOCKPOWER_TYPESTR;
                    break;
                case Metric.LineOnly:
                    metricString = PDU_METRIC_LINEONLY_TYPESTR;
                    break;
            }

            string cmd = string.Format("{0} {1:x} {2} {3:x}\r", PDU_GET_METRICS_CMDSTR, 
                GetParameterAsString("pdu index"), metricString, GetParameterAsString("port"));
            ResponseBuilder builder = new ResponseBuilder();
            this.Flush();
            this.NewData += new EventHandler<OnSerialDataEventArgs>(builder.MCMResponseHandler);
            SendCmd(cmd);
            if (builder.WaitForResponse(5000) == true)
            {
                string returnData = builder.ToString();
            }

            return true;
        }
    }
}
