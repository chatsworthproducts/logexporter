﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Communications.StreamCommands
{
    public class LockCalibrationMetricsModeCmd : BaseStreamCommand
    {
        public LockCalibrationMetricsModeCmd(System.IO.Stream commandStream) :
            base(commandStream)
        {
        }

        override public bool Execute()
        {
            AssertParameter("outlet flag");
            AssertParameter("port");

            string cmd = string.Format("{0} {1} {2:x}\r", PDU_LOCK_CALIBMETR_CMDSTR, 
                GetParameterAsString("outlet flag"), GetParameterAsString("port"));
            int response = 0;
            SendCmdAndWait(cmd, new string[] { "OK", "Bad Command", "FAILED!" }, 2000, out response);

            if (response != 0)
            {
                return false;
            }

            return true;
        }
    }
}
