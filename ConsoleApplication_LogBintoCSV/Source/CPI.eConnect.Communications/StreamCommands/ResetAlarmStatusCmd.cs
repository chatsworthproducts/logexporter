﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Communications.StreamCommands
{
    public class ResetAlarmStatusCmd : BaseStreamCommand
    {
        public ResetAlarmStatusCmd(System.IO.Stream commandStream) :
            base(commandStream)
        {
        }

        override public bool Execute()
        {
            AssertParameter("pdu index");

            string cmd = string.Format("{0} {1}\r", PDU_RESET_ALARMS_CMDSTR, GetParameterAsString("pdu index"));
            int response = 0;
            SendCmdAndWait(cmd, new string[] { "OK", "Bad Command", "FAILED!" }, 2000, out response);

            if (response != 0)
            {
                return false;
            }

            return true;
        }
    }
}
