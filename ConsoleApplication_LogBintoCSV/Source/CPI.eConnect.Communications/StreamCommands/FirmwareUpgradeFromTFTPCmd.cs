﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Communications.StreamCommands
{
    public class FirmwareUpgradeFromTFTPCmd : BaseStreamCommand
    {
        public FirmwareUpgradeFromTFTPCmd(System.IO.Stream commandStream) :
            base(commandStream)
        {
        }

        override public bool Execute()
        {
            AssertParameter("path");
            this.Start();

            string cmd = string.Format("{0} {1} force tftp {2}\r", PDU_SET_UPDATEFW_CMDSTR, PDU_MAGIC_WORD_STR,
                GetParameterAsString("path"));
            int response = 0;
            SendCmdAndWait(cmd, new string[] { "OK", "Bad Command", "FAILED!" }, 30000, out response);

            if (response != 0)
            {
                this.Dispose();
                return false;
            }

            this.Dispose();
            return true;
        }
    }
}
