﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Communications.StreamCommands
{
    public class GetCalibrationMetricCmd : BaseStreamCommand
    {
        public GetCalibrationMetricCmd(System.IO.Stream commandStream) :
            base(commandStream)
        {
        }

        override public bool Execute()
        {
            string cmd = string.Format("{0}\r", PDU_GET_CALIBMETRIC_CMDSTR);
            ResponseBuilder builder = new ResponseBuilder();
            this.Flush();
            this.NewData += new EventHandler<OnSerialDataEventArgs>(builder.MCMResponseHandler);
            SendCmd(cmd);

            if (builder.WaitForResponse(5000) == true)
            {
                string returnData = builder.ToString();
            }

            return true;
        }
    }
}
