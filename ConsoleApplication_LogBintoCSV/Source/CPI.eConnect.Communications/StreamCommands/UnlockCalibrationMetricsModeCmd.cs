﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Communications.StreamCommands
{
    public class UnlockCalibrationMetricsModeCmd : BaseStreamCommand
    {
        public UnlockCalibrationMetricsModeCmd(System.IO.Stream commandStream) :
            base(commandStream)
        {
        }

        override public bool Execute()
        {
            string cmd = string.Format("{0}\r", PDU_UNLOCK_CALIBMETR_CMDSTR);
            int response = 0;
            SendCmdAndWait(cmd, new string[] { "OK", "Bad Command", "FAILED!" }, 2000, out response);

            if (response != 0)
            {
                return false;
            }

            return true;
        }
    }
}
