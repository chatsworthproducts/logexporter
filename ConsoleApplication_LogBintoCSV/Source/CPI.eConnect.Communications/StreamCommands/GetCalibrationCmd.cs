﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Communications.StreamCommands
{
    public class GetCalibrationCmd : BaseStreamCommand
    {
        public GetCalibrationCmd(System.IO.Stream commandStream) :
            base(commandStream)
        {
        }

        override public bool Execute()
        {
            AssertParameter("outlet flag");
            AssertParameter("type");
            AssertParameter("port");

            string cmd = string.Format("{0} {1:x} {1}\r", PDU_GET_CALIB_CMDSTR, 
                GetParameterAsString("outlet flag"), GetParameterAsString("type"), 
                GetParameterAsString("port"));
            ResponseBuilder builder = new ResponseBuilder();
            this.Flush();
            this.NewData += new EventHandler<OnSerialDataEventArgs>(builder.MCMResponseHandler);
            SendCmd(cmd);

            if (builder.WaitForResponse(5000) == true)
            {
                string returnData = builder.ToString();
            }

            return true;
        }
    }
}
