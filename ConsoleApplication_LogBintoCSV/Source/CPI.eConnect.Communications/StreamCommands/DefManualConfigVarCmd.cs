﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Communications.StreamCommands
{
    public class DefManualConfigVarCmd : BaseStreamCommand
    {
        public DefManualConfigVarCmd(System.IO.Stream commandStream) :
            base(commandStream)
        {
        }

        override public bool Execute()
        {
            AssertParameter("pdu index");
            AssertParameter("options map");

            string cmd = string.Format("{0} {1:x} {2} {3}\r", PDU_MANUFDEF_CFGVAR_CMDSTR, 
                GetParameterAsString("pdu index"), GetParameterAsString("options map"), PDU_MAGIC_WORD_STR);
            int response = 0;
            SendCmdAndWait(cmd, new string[] { "OK", "Bad Command", "FAILED!" }, 2000, out response);

            if (response != 0)
            {
                return false;
            }

            return true;
        }
    }
}
