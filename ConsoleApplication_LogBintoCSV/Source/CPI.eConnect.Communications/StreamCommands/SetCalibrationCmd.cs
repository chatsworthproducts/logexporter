﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Communications.StreamCommands
{
    public class SetCalibrationCmd : BaseStreamCommand
    {
        public SetCalibrationCmd(System.IO.Stream commandStream) :
            base(commandStream)
        {
        }

        override public bool Execute()
        {
            AssertParameter("outlet flag");
            AssertParameter("type");
            AssertParameter("port");
            AssertParameter("cal buffer");

            string cmd = string.Format("{0} {1} {2:x} {3:x} {4}\r", PDU_SET_CALIB_CMDSTR, GetParameterAsString("outlet flag"), 
                GetParameterAsString("type"), GetParameterAsString("port"), GetParameterAsString("cal buffer"));
            
            int response = 0;
            SendCmdAndWait(cmd, new string[] { "OK", "Bad Command", "FAILED!" }, 2000, out response);

            if (response != 0)
            {
                return false;
            }

            return true;
        }
    }
}
