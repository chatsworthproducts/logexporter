﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Communications.StreamCommands
{
    public class LoginStreamCmd : BaseStreamCommand
    {        
        public LoginStreamCmd(System.IO.Stream commandStream) :
            base(commandStream)
        {
        }

        override public bool Execute()
        {
            AssertParameter("name");
            AssertParameter("password");

            // determine where we are at..
            int responseId = 0;
            SendCmdAndWait("test\rtest\r", new string[] { "Bad command!", "Incorrect", "Password:", "cpi #" }, 30000, out responseId);

            switch (responseId)
            {
                case 1: // enter the name
                    if (SendCmdAndWait(string.Format("{0}\r",GetParameterAsString("name")),
                            "Name: admin", 30000) != 0)
                    {
                        return false;
                    }
                    goto case 2;
                case 2: // enter the password
                    if (SendCmdAndWait(string.Format("{0}\r", GetParameterAsString("password")),
                            "Password:", 30000) != 0)
                    {
                        return false;
                    }
                    goto case 0;
                case 3: // exit the shell and get back into the cpi app
                    if (SendCmdAndWait("exit\r", "?", 30000) != 0)
                    {
                        return false;
                    }
                    break;
                case 0: // logged in and ready to go                    
                    break;
            }

            return true;
        }
    }
}
