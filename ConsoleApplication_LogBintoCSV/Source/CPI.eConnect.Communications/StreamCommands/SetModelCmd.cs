﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Communications.StreamCommands
{
    public class SetModelCmd : BaseStreamCommand
    {
        public SetModelCmd(System.IO.Stream commandStream) :
            base(commandStream)
        {
        }
        
        override public bool Execute()
        {
            AssertParameter("model");

            string cmd = string.Format("{0} {1} {2}\r", PDU_SET_MODEL_CMDSTR, GetParameterAsString("model"), PDU_MAGIC_WORD_STR);
            int response = 0;
            this.Start();
            SendCmdAndWait(cmd, new string[] { "OK", "Bad Command", "FAILED!" }, 2000, out response);
            this.Dispose();

            if (response != 0)
            {
                return false;
            }

            return true;
        }
    }
}
