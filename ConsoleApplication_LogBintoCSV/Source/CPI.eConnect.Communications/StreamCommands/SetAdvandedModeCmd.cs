﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Communications.StreamCommands
{
    public class SetAdvandedModeCmd : BaseStreamCommand
    {
        public SetAdvandedModeCmd(System.IO.Stream commandStream) :
            base(commandStream)
        {
        }

        override public bool Execute()
        {
            this.Start();
            SendCmdAndWait("#)(*&^\r", "OK", 30000);
            this.Dispose();
            return true;
        }
    }
}
