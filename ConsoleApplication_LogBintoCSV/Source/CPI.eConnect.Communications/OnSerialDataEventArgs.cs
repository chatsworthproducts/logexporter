﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Communications
{
    public class OnSerialDataEventArgs : EventArgs
    {
        public OnSerialDataEventArgs(string data)
        {
            this.Data = data;
        }

        public string Data { get; set; }
    }
}
