﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

namespace CPI.eConnect.Communications
{
    public class MCMBiosController
    {
        private const string MCMP_SET_STOPAUTOBOOT_CMDSTR = "\r\n\r\n";
        private const string MCMP_AUTOBOOT_PROMPT_STRING = "Hit any key to stop autoboot:";

        private SerialPort _Port = new SerialPort();

        /// <summary>
        /// Holds data received until we get a terminator.
        /// </summary>
        private string _BufferString = string.Empty;

        private char _Terminator = '\n';

        private object _SyncObject = new object();

        private System.Threading.ManualResetEvent _ResponseRecv = new System.Threading.ManualResetEvent(false);
        private System.Threading.ManualResetEvent _HoldResponses = new System.Threading.ManualResetEvent(false);
        private System.Threading.ManualResetEvent _Shutdown = new System.Threading.ManualResetEvent(false);

        private System.Threading.Thread _WorkerThread;

        private Queue<string> _Responses = new Queue<string>();

        public MCMBiosController()
        {
            this.BaudRate = 9600;
            this.DataBits = 8;
            this.StopBits = System.IO.Ports.StopBits.One;
            this.Parity = System.IO.Ports.Parity.None;            
            this.Handshake = System.IO.Ports.Handshake.XOnXOff;
        }

        public EventHandler<OnSerialDataEventArgs> NewData;

        /// <summary>
        /// Gets or sets BaudRate (Default: 9600)
        /// </summary>
        public int BaudRate { get; set; }

        /// <summary>
        /// Gets or sets DataBits (Default: 8)
        /// </summary>
        public int DataBits { get; set; }

        /// <summary>
        /// Gets or sets Handshake (Default: None)
        /// </summary>
        public Handshake Handshake { get; set; }

        /// <summary>
        /// Gets or sets Parity (Default: None)
        /// </summary>
        public Parity Parity { get; set; }

        /// <summary>
        /// Gets or sets PortName (Default: COM1)
        /// </summary>
        public string PortName { get; set; }

        /// <summary>
        /// Gets or sets StopBits (Default: One}
        /// </summary>
        public StopBits StopBits { get; set; }

        public bool Connect()
        {
            try
            {
                this._Port.BaudRate = this.BaudRate;
                this._Port.DataBits = this.DataBits;
                this._Port.Handshake = this.Handshake;
                this._Port.Parity = this.Parity;
                
                this._Port.PortName = this.PortName;
                this._Port.StopBits = this.StopBits;
                this._Port.DataReceived += new SerialDataReceivedEventHandler(this.serialPortDataReceived);
                this._Port.ReadTimeout = SerialPort.InfiniteTimeout;
                this._Port.WriteTimeout = SerialPort.InfiniteTimeout;
                this._Port.Open();

                this._WorkerThread = new System.Threading.Thread(new System.Threading.ThreadStart(this.Worker));
                this._WorkerThread.Start();
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                return false;
            }

            return true;
        }

        public void Close()
        {
            this._Shutdown.Set();
            this._WorkerThread.Join();
            this._Port.DataReceived -= new SerialDataReceivedEventHandler(this.serialPortDataReceived);
            this._Port.Close();
        }

        public int StopAutoboot()
        {
            int returnValue = -1;
            try
            {
                this._HoldResponses.Set();
                int retries = 1;
                while (retries < 30000)
                {
                    // look for the interupt string in the buffer, since it won't complete until
                    //  the countdown is complete.
                    if (this._BufferString.Contains(MCMP_AUTOBOOT_PROMPT_STRING) == true)
                    {
                        byte[] stringbytes = ASCIIEncoding.ASCII.GetBytes(MCMP_SET_STOPAUTOBOOT_CMDSTR);
                        this._Port.Write(stringbytes, 0, stringbytes.Length);
                        retries = 300000;
                        returnValue = 0;
                        break;
                    }

                    retries++;
                    System.Threading.Thread.Sleep(5);
                }
            }
            finally
            {
                // flush the response buffer
                lock (this._SyncObject)
                {
                    this._Responses.Clear();
                    this._ResponseRecv.Reset();
                }

                this._HoldResponses.Reset();
            }

            return returnValue;
        }

        public int StopAutoboot(System.Threading.ManualResetEvent abort)
        {
            int returnValue = -1;
            try
            {                
                this._HoldResponses.Set();
                int retries = 1;
                while((retries < 30000) && (abort.WaitOne(5) == false))
                {
                    // look for the interupt string in the buffer, since it won't complete until
                    //  the countdown is complete.
                    if (this._BufferString.Contains(MCMP_AUTOBOOT_PROMPT_STRING) == true)
                    {
                        byte[] stringbytes = ASCIIEncoding.ASCII.GetBytes(MCMP_SET_STOPAUTOBOOT_CMDSTR);
                        this._Port.Write(stringbytes, 0, stringbytes.Length);
                        retries = 300000;
                        returnValue = 0;
                        break;
                    }

                    retries++;                
                }
            }
            finally
            {
                // flush the response buffer
                lock (this._SyncObject)
                {
                    this._Responses.Clear();
                    this._ResponseRecv.Reset();
                }

                this._HoldResponses.Reset();
            }

            return returnValue;
        }

        public void SendBiosCmd(string cmd)
        {
            byte[] cmdbytes = ASCIIEncoding.ASCII.GetBytes(cmd);
            for (int i = 0; i < cmdbytes.Length;)
            {
                this._Port.Write(cmdbytes, i, Math.Min(60,cmdbytes.Length - i));
                i += Math.Min(60, cmdbytes.Length - i);
            }
        }

        public void Flush()
        {
            while (this._Responses.Count() > 0)
            {                
                System.Threading.Thread.Sleep(1);
            }
            System.Threading.Thread.Sleep(100);
        }

        private void Worker()
        {
            System.Threading.ManualResetEvent[] events = new System.Threading.ManualResetEvent[] {
                this._Shutdown,
                this._ResponseRecv,
                this._HoldResponses
            };

            while(true)
            {
                int triggeredHandle = System.Threading.ManualResetEvent.WaitAny(events);
                switch(triggeredHandle)
                {
                    case 0: // shutdown
                        System.Diagnostics.Debug.WriteLine("Goodbye");
                        return;
                    case 1: // response
                        string response;
                        while(this._Responses.Count() > 0)
                        {
                            lock (this._SyncObject)
                            {
                                response = this._Responses.Dequeue();
                                this._ResponseRecv.Reset();
                            }

                            if (this.NewData != null)
                            {
                                this.NewData(this, new OnSerialDataEventArgs(response));
                            }
                        }                        
                        break;
                    case 2: // hold
                        while( this._HoldResponses.WaitOne(1) == true )
                        {
                            System.Threading.Thread.Sleep(1);
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// Handles DataReceived Event from SerialPort
        /// </summary>
        /// <param name="sender">Serial Port</param>
        /// <param name="e">Event arguments</param>
        private void serialPortDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            // Initialize a buffer to hold the received data
            byte[] buffer = new byte[this._Port.ReadBufferSize];

            // There is no accurate method for checking how many bytes are read
            // unless you check the return from the Read method
            int bytesRead = this._Port.Read(buffer, 0, buffer.Length);

            // For the example assume the data we are received is ASCII data.
            this._BufferString += Encoding.ASCII.GetString(buffer, 0, bytesRead);

            // Check if string contains the terminator 
            while (this._BufferString.IndexOf(this._Terminator) > -1)
            {
                // If tString does contain terminator we cannot assume that it is the last character received
                string workingString = this._BufferString.Substring(0, this._BufferString.IndexOf(this._Terminator));

                // Remove the data up to the terminator from tString
                this._BufferString = this._BufferString.Substring(this._BufferString.IndexOf(this._Terminator) + 1);

                lock (this._SyncObject)
                {
                    this._Responses.Enqueue(workingString);
                    this._ResponseRecv.Set();
                }
            }
        }
    }
}
