﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Communications
{
    public interface IMCMChannel
    {
        IPDUCommand Login { get; }
        IPDUCommand Logout { get; }

        IPDUCommand SetModel { get; }
        IPDUCommand SetMACAddress { get; }
        IPDUCommand SetAttrib { get; }
        IPDUCommand SetSerialNumber { get; }
        IPDUCommand SetMCMIdNum { get; }

        IPDUCommand GetCalibration { get; }
        IPDUCommand GetCalibrationMetric { get; }
        IPDUCommand SetCalibration { get; }
        IPDUCommand LockCalibrationMetricsMode { get; }
        IPDUCommand UnlockCalibrationMetricsMode { get; }
        IPDUCommand SaveCalibration { get; }

        IPDUCommand GetChainPDUList { get; }
        IPDUCommand SetAdvandedMode { get; }
        IPDUCommand FirmwareUpgradeFromUSB { get; }
        IPDUCommand FirmwareUpgradeFromHTTP { get; }
        IPDUCommand FirmwareUpgradeFromTFTP { get; }
        IPDUCommand FirmwareUpgradeChain { get; }

        IPDUCommand GetModel { get; }
        IPDUCommand GetMACAddress { get; }
        IPDUCommand GetAttrib { get; }
        IPDUCommand GetSerialNumber { get; }
        IPDUCommand GetMCMIdNumber { get; }
        IPDUCommand GetVersion { get; }
        IPDUCommand GetConfig { get; }
        IPDUCommand GetModulesVersion { get; }
        IPDUCommand GetConfigVar { get; }
        IPDUCommand GetMetrics { get; }
        IPDUCommand GetEnergy { get; }

        IPDUCommand SetConfigVar { get; }
        IPDUCommand GetPowerStatus { get; }
        IPDUCommand GetResetStatus { get; }
        IPDUCommand GetAlarmStatus { get; }
        IPDUCommand ResetAlarmStatus { get; }
        IPDUCommand SetPowerCntrl { get; }
        IPDUCommand SetPowerReset { get; }
        IPDUCommand GetEnvironmental { get; }
        IPDUCommand DefManualConfigVar { get; }

        IPDUCommand RebootPDU { get; }
        IPDUCommand RestartModule { get; }
    }
}
