﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Communications
{
    public interface IPDUCommand
    {
        Dictionary<string, object> Parameters { get; }
        Dictionary<string, object> CommandData { get; }
        bool Execute();
    }
}
