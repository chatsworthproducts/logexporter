﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

namespace CPI.eConnect.Communications
{
    public class MCMSerialController : IChannel, IMCMChannel
    {
        private const string PDU_MAGIC_WORD_STR = "95132408";
        private const string ADMIN_LOGOUT_CMDSTR                = "!q\r";
        private const string PDU_ADVANCED_MODE_CMDSTR           = "#)(*&^\r";
        private const string PDU_GET_MODEL_CMDSTR		        = "u";
        private const string PDU_SET_MODEL_CMDSTR		        = "#S";
        private const string PDU_GET_ATTRIB_CMDSTR		        = "q";
        private const string PDU_SET_ATTRIB_CMDSTR		        = "#Q";
        private const string PDU_GET_MCMIDNUM_CMDSTR	        = "z";
        private const string PDU_SET_MCMIDNUM_CMDSTR	        = "#Z";
        private const string PDU_GET_SERIALNUM_CMDSTR	        = "y";
        private const string PDU_SET_SERIALNUM_CMDSTR	        = "#Y";
        private const string PDU_GET_MACADDR_CMDSTR		        = "M";
        private const string PDU_SET_MACADDR_CMDSTR		        = "#M";
        private const string PDU_GET_VERSION_CMDSTR		        = "v";
        private const string PDU_GET_CONFIG_CMDSTR		        = "V";
        private const string PDU_GET_CFGVAR_CMDSTR		        = "h";
        private const string PDU_SET_CFGVAR_CMDSTR		        = "H";
        private const string PDU_MANUFDEF_CFGVAR_CMDSTR	        = "#D";
        private const string PDU_RESTART_APP_CMDSTR		        = "#x";
        private const string PDU_REBOOT_PDU_CMDSTR		        = "#X";
        private const string PDU_RESET_ALARMS_CMDSTR	        = "N";
        private const string PDU_GET_MODSVERSION_CMDSTR	        = "U";
        private const string PDU_SAVE_CALIB_CMDSTR		        = "#W";
        private const string PDU_ERASE_CALIB_CMDSTR		        = "#E";
        private const string PDU_SET_CALIB_CMDSTR		        = "#C";
        private const string PDU_GET_CALIB_CMDSTR		        = "#c";
        private const string PDU_LOCK_CALIBMETR_CMDSTR	        = "#L";
        private const string PDU_UNLOCK_CALIBMETR_CMDSTR        = "#l";
        private const string PDU_GET_CALIBMETRIC_CMDSTR	        = "#e";
        private const string PDU_GET_METRICS_CMDSTR		        = "G";
        private const string PDU_SET_RECEPPWRCTRL_CMDSTR        = "R";
        private const string PDU_GET_RECEPPWRSTATUS_CMDSTR      = "P";
        private const string PDU_GET_RECEPRESSTATUS_CMDSTR      = "p";
        private const string PDU_GET_ALARMSTATUS_CMDSTR	        = "n";
        private const string PDU_SET_RECEPRESET_CMDSTR	        = "o";
        private const string PDU_GET_EVALUE_TEMPHUMID_CMDSTR    = "J";
        private const string PDU_GET_CHAINEDPDULIST_CMDSTR      = "a";
        private const string PDU_SET_USBUPDATE_CMDSTR	        = "#U";
        private const string PDU_GET_ENERGY_CMDSTR		        = "E";
        private const string PDU_TEST_XLOG_CMDSTR		        = "#b logupload";
        private const string PDU_TEST_EMAIL_CMDSTR		        = "#b email";
            
        private const string PDU_METRIC_CURRONLY_TYPESTR	    = "I";
        private const string PDU_METRIC_VOLTONLY_TYPESTR	    = "V";
        private const string PDU_METRIC_POWERONLY_TYPESTR	    = "P";
        private const string PDU_METRIC_PFACTONLY_TYPESTR	    = "F";
        private const string PDU_METRIC_LINEONLY_TYPESTR	    = "L";
        private const string PDU_METRIC_IVPFALLY_TYPESTR	    = "A";
        private const string PDU_METRIC_CURRVOLT_TYPESTR	    = "B";
        private const string PDU_METRIC_SOCKCURR_TYPESTR	    = "i";
        private const string PDU_METRIC_SOCKPOWER_TYPESTR	    = "p";
        private const string PDU_METRIC_CIRCENER_TYPESTR	    = "E 0";
        private const string PDU_METRIC_SOCKENER_TYPESTR        = "E 1";
        
        private SerialPort _Port = new SerialPort();

        /// <summary>
        /// Holds data received until we get a terminator.
        /// </summary>
        private string _BufferString = string.Empty;

        private char _Terminator = '\n';

        private object _SyncObject = new object();

        private System.Threading.ManualResetEvent _ResponseRecv = new System.Threading.ManualResetEvent(false);
        private System.Threading.ManualResetEvent _HoldResponses = new System.Threading.ManualResetEvent(false);
        private System.Threading.ManualResetEvent _Shutdown = new System.Threading.ManualResetEvent(false);

        private string[] _WaitData;
        private int _WaitID = 0;
        private System.Threading.ManualResetEvent _WaitResponseRecv = new System.Threading.ManualResetEvent(false);

        private System.Threading.Thread _WorkerThread;

        private Queue<string> _Responses = new Queue<string>();

        public MCMSerialController()
        {
            this.BaudRate = 9600;
            this.DataBits = 8;
            this.StopBits = System.IO.Ports.StopBits.One;
            this.Parity = System.IO.Ports.Parity.None;            
            this.Handshake = System.IO.Ports.Handshake.XOnXOff;
        }

        #region IChannel region
        public EventHandler<OnSerialDataEventArgs> NewData;

        /// <summary>
        /// Gets or sets BaudRate (Default: 9600)
        /// </summary>
        public int BaudRate { get; set; }

        /// <summary>
        /// Gets or sets DataBits (Default: 8)
        /// </summary>
        public int DataBits { get; set; }

        /// <summary>
        /// Gets or sets Handshake (Default: None)
        /// </summary>
        public Handshake Handshake { get; set; }

        /// <summary>
        /// Gets or sets Parity (Default: None)
        /// </summary>
        public Parity Parity { get; set; }

        /// <summary>
        /// Gets or sets PortName (Default: COM1)
        /// </summary>
        public string PortName { get; set; }

        /// <summary>
        /// Gets or sets StopBits (Default: One}
        /// </summary>
        public StopBits StopBits { get; set; }

        public bool Connect()
        {
            try
            {
                this._Port.BaudRate = this.BaudRate;
                this._Port.DataBits = this.DataBits;
                this._Port.Handshake = this.Handshake;
                this._Port.Parity = this.Parity;
                
                this._Port.PortName = this.PortName;
                this._Port.StopBits = this.StopBits;
                this._Port.DataReceived += new SerialDataReceivedEventHandler(this.serialPortDataReceived);
                this._Port.ReadTimeout = SerialPort.InfiniteTimeout;
                this._Port.WriteTimeout = SerialPort.InfiniteTimeout;
                this._Port.Open();

                this._WorkerThread = new System.Threading.Thread(new System.Threading.ThreadStart(this.Worker));
                this._WorkerThread.Start();
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                return false;
            }

            return true;
        }

        public void Close()
        {
            this._Shutdown.Set();
            this._WorkerThread.Join();
            this._Port.DataReceived -= new SerialDataReceivedEventHandler(this.serialPortDataReceived);
            this._Port.Close();
        }

        public void SendCmd(string cmd)
        {
            byte[] cmdbytes = ASCIIEncoding.ASCII.GetBytes(cmd);
            for (int i = 0; i < cmdbytes.Length; )
            {
                this._Port.Write(cmdbytes, i, Math.Min(60, cmdbytes.Length - i));
                i += Math.Min(60, cmdbytes.Length - i);
            }
        }

        public void Flush()
        {
            while (this._Responses.Count() > 0)
            {                
                System.Threading.Thread.Sleep(1);
            }
            System.Threading.Thread.Sleep(100);
        }
        #endregion

        private void Worker()
        {
            System.Threading.ManualResetEvent[] events = new System.Threading.ManualResetEvent[] {
                this._Shutdown,
                this._ResponseRecv,
                this._HoldResponses
            };

            while(true)
            {
                int triggeredHandle = System.Threading.ManualResetEvent.WaitAny(events);
                switch(triggeredHandle)
                {
                    case 0: // shutdown
                        System.Diagnostics.Debug.WriteLine("Goodbye");
                        return;
                    case 1: // response
                        string response;
                        while(this._Responses.Count() > 0)
                        {
                            lock (this._SyncObject)
                            {
                                response = this._Responses.Dequeue();
                                this._ResponseRecv.Reset();
                            }

                            if (_WaitData != null)
                            {
                                for (int i = 0; i < _WaitData.Length; i++)
                                {
                                    if (response.Contains(_WaitData[i]) == true)
                                    {
                                        _WaitID = i;
                                        _WaitResponseRecv.Set();
                                    }
                                }
                            }

                            if (this.NewData != null)
                            {
                                this.NewData(this, new OnSerialDataEventArgs(response));
                            }
                        }                        
                        break;
                    case 2: // hold
                        while( this._HoldResponses.WaitOne(1) == true )
                        {
                            System.Threading.Thread.Sleep(1);
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// Handles DataReceived Event from SerialPort
        /// </summary>
        /// <param name="sender">Serial Port</param>
        /// <param name="e">Event arguments</param>
        private void serialPortDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            // Initialize a buffer to hold the received data
            byte[] buffer = new byte[this._Port.ReadBufferSize];

            // There is no accurate method for checking how many bytes are read
            // unless you check the return from the Read method
            int bytesRead = this._Port.Read(buffer, 0, buffer.Length);

            // For the example assume the data we are received is ASCII data.
            this._BufferString += Encoding.ASCII.GetString(buffer, 0, bytesRead);

            // Check if string contains the terminator 
            while (this._BufferString.IndexOf(this._Terminator) > -1)
            {
                // If tString does contain terminator we cannot assume that it is the last character received
                string workingString = this._BufferString.Substring(0, this._BufferString.IndexOf(this._Terminator));

                // Remove the data up to the terminator from tString
                this._BufferString = this._BufferString.Substring(this._BufferString.IndexOf(this._Terminator) + 1);

                lock (this._SyncObject)
                {
                    this._Responses.Enqueue(workingString);
                    this._ResponseRecv.Set();
                }
            }
        }

        private int SendCmdAndWait(string cmd, string response, int timeout)
        {
            SetWaitRespone(response);
            SendCmd(cmd);
            if (_WaitResponseRecv.WaitOne(timeout) == false)
            {
                return 1;
            }
            Flush();
            return 0;
        }

        private int SendCmdAndWait(string cmd, string[] response, int timeout, out int responseId)
        {
            responseId = -1;
            SetWaitRespone(response);
            SendCmd(cmd);
            if (_WaitResponseRecv.WaitOne(timeout) == false)
            {
                return 1;
            }
            Flush();
            responseId = _WaitID;
            return 0;
        }

        protected void SetWaitRespone(string wait)
        {
            _WaitResponseRecv.Reset();
            _WaitData = new string[] { wait };
            _WaitID = 0;
        }

        protected void SetWaitRespone(string[] wait)
        {
            _WaitResponseRecv.Reset();
            _WaitData = wait;
            _WaitID = 0;
        }

        public IPDUCommand FirmwareUpgradeFromUSB { get { return new StreamCommands.FirmwareUpgradeFromUSBCmd(_Port.BaseStream); } }

        public IPDUCommand FirmwareUpgradeFromHTTP { get { return new StreamCommands.FirmwareUpgradeFromHTTPCmd(_Port.BaseStream); } }

        public IPDUCommand FirmwareUpgradeFromTFTP { get { return new StreamCommands.FirmwareUpgradeFromTFTPCmd(_Port.BaseStream); } }

        public IPDUCommand FirmwareUpgradeChain { get { return new StreamCommands.FirmwareUpgradeChainCmd(_Port.BaseStream); } }

        public IPDUCommand Login { get { return new StreamCommands.LoginStreamCmd(_Port.BaseStream); } }

        public IPDUCommand Logout { get { return new StreamCommands.LogoutStreamCmd(_Port.BaseStream); } }

        public IPDUCommand SetModel { get { return new StreamCommands.SetModelCmd(_Port.BaseStream); } }

        public IPDUCommand SetMACAddress { get { return new StreamCommands.SetMACAddressCmd(_Port.BaseStream); } }

        public IPDUCommand SetAttrib { get { return new StreamCommands.SetAttribCmd(_Port.BaseStream); } }

        public IPDUCommand SetSerialNumber { get { return new StreamCommands.SetSerialNumberCmd(_Port.BaseStream); } }

        public IPDUCommand SetMCMIdNum { get { return new StreamCommands.SetMCMIdNumCmd(_Port.BaseStream); } }

        public IPDUCommand DefManualConfigVar { get { return new StreamCommands.DefManualConfigVarCmd(_Port.BaseStream); } }

        public IPDUCommand SetConfigVar { get { return new StreamCommands.SetConfigVarCmd(_Port.BaseStream); } }

        public IPDUCommand RebootPDU { get { return new StreamCommands.RebootPDUCmd(_Port.BaseStream); } }

        public IPDUCommand RestartModule { get { return new StreamCommands.RestartModuleCmd(_Port.BaseStream); } }

        public IPDUCommand ResetAlarmStatus { get { return new StreamCommands.ResetAlarmStatusCmd(_Port.BaseStream); } }

        public IPDUCommand GetMetrics { get { return new StreamCommands.GetMetricsCmd(_Port.BaseStream); } }

        public IPDUCommand GetEnergy { get { return new StreamCommands.GetEnergyCmd(_Port.BaseStream); } }

        public IPDUCommand LockCalibrationMetricsMode { get { return new StreamCommands.LockCalibrationMetricsModeCmd(_Port.BaseStream); } }

        public IPDUCommand UnlockCalibrationMetricsMode { get { return new StreamCommands.UnlockCalibrationMetricsModeCmd(_Port.BaseStream); } }

        public IPDUCommand GetCalibrationMetric { get { return new StreamCommands.GetCalibrationMetricCmd(_Port.BaseStream); } }

        public IPDUCommand GetPowerStatus { get { return new StreamCommands.GetPowerStatusCmd(_Port.BaseStream); } }

        public IPDUCommand GetResetStatus { get { return new StreamCommands.GetResetStatusCmd(_Port.BaseStream); } }

        public IPDUCommand GetAlarmStatus { get { return new StreamCommands.GetAlarmStatusCmd(_Port.BaseStream); } }

        public IPDUCommand SetPowerCntrl { get { return new StreamCommands.SetPowerCntrlCmd(_Port.BaseStream); } }

        public IPDUCommand SetPowerReset { get { return new StreamCommands.SetPowerResetCmd(_Port.BaseStream); } }

        public IPDUCommand GetEnvironmental { get { return new StreamCommands.GetEnvironmentalCmd(_Port.BaseStream); } }

        public IPDUCommand SetCalibration { get { return new StreamCommands.SetCalibrationCmd(_Port.BaseStream); } }

        public IPDUCommand GetCalibration { get { return new StreamCommands.GetCalibrationCmd(_Port.BaseStream); } }

        public IPDUCommand SaveCalibration { get { return new StreamCommands.SaveCalibrationCmd(_Port.BaseStream); } }

        public IPDUCommand GetChainPDUList { get { return new StreamCommands.GetChainPDUListCmd(_Port.BaseStream); } }

        public IPDUCommand SetAdvandedMode { get { return new StreamCommands.SetAdvandedModeCmd(_Port.BaseStream); } }

        public IPDUCommand GetConfig { get { return new StreamCommands.GetConfigCmd(_Port.BaseStream); } }

        public IPDUCommand GetModel { get { return new StreamCommands.GetModelCmd(_Port.BaseStream); } }

        public IPDUCommand GetMACAddress { get { return new StreamCommands.GetMACAddressCmd(_Port.BaseStream); } }

        public IPDUCommand GetAttrib { get { return new StreamCommands.GetAttribCmd(_Port.BaseStream); } }

        public IPDUCommand GetSerialNumber { get { return new StreamCommands.GetSerialNumberCmd(_Port.BaseStream); } }

        public IPDUCommand GetMCMIdNumber { get { return new StreamCommands.GetMCMIdNumberCmd(_Port.BaseStream); } }

        public IPDUCommand GetVersion { get { return new StreamCommands.GetVersionCmd(_Port.BaseStream); } }

        public IPDUCommand GetModulesVersion { get { return new StreamCommands.GetModulesVersionCmd(_Port.BaseStream); } }

        public IPDUCommand GetConfigVar { get { return new StreamCommands.GetConfigVarCmd(_Port.BaseStream); } }

        public bool ExecuteCommand(IPDUCommand cmd, params string[] parameterValues)
        {
            foreach(string str in parameterValues)
            {
                string[] stringBits = str.Split('=');
                cmd.SetParameter(stringBits[0], stringBits[1]);
            }
            
            return cmd.Execute();
        }
    }
}
