﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Communications
{
    public interface IChannel
    {
        //EventHandler<OnSerialDataEventArgs> NewData;

        bool Connect();
        void Close();
        void SendCmd(string cmd);
        void Flush();
    }
}
