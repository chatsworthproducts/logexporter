﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Communications
{
    public enum Metric
    {
        Current = 0,
        Voltage = 1,
        Power = 2,
        PowerFactor = 3,
        CurrentVolt = 4,
        ALL = 5,
        SocketCurrent = 6,
        SocketPower = 7,
        LineOnly = 8
    }
}
