﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Communications
{
    public static class PDUCommandExt
    {
        public static IPDUCommand SetParameter(this IPDUCommand cmd, string paramName, object param)
        {
            cmd.Parameters[paramName] = paramName.ToString();
            return cmd;
        }

        public static IPDUCommand Execute(this IPDUCommand cmd)
        {
            bool ret = cmd.Execute();
            if( ret == false)
            {
                throw new Exception("Execute exception");
            }
            return cmd;
        }
    }
}
