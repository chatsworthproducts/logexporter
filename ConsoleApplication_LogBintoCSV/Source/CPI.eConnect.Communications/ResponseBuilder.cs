﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Communications
{
    internal class ResponseBuilder
    {
        StringBuilder _ResponseString = new StringBuilder();
        private System.Threading.ManualResetEvent _ResponseRecv = new System.Threading.ManualResetEvent(false);

        public bool WaitForResponse(int timeout)
        {
            return _ResponseRecv.WaitOne(timeout);
        }

        public override string ToString()
        {
            return _ResponseString.ToString();
        }

        public void MCMResponseHandler(object sender, OnSerialDataEventArgs e)
        {
            if (e.Data.StartsWith("?") == false)
            {
                _ResponseString.Append(e.Data);
                _ResponseRecv.Set();
            }
        }
    }
}
