﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

namespace CPI.eConnect.Communications
{
    public delegate void DataReceived(object sender, SerialPortEventArgs arg);
    public class SerialPortEventArgs : EventArgs
    {
        public string ReceivedData { get; private set; }
        public SerialPortEventArgs(string data)
        {
            ReceivedData = data;
        }
    }

    public enum Fluke8808ASampleRate
    {
        Slow = 0,
        Medium = 1,
        Fast = 2
    }

    public enum Fluke8808AOption
    {
        ACVolt = 0,
        ACAMP = 1,
        Ohms = 2
    }

    /// <summary>
    /// The Fluke8808A class encodes the communications to and from the Fluke 8808A.
    ///     See the documentation on the meter in the documentation folder.
    /// </summary>
    public class Fluke8808A
    {
        private const string INST_OK_PROMPT_STRING = "=>\r\n";
        private const string INST_CMDERR_PROMPT_STRING = "?>\r\n";
        private const string INST_EXECERR_PROMPT_STRING = "!>\r\n";
        private const string INST_CMD_IDENTIFY = "*IDN?";
        private const string INST_IDENT_PROMPT_STRING = "FLUKE, 8808A, ";
        private const string INST_PWRUP_RESET_CMDSTR = "*RST";
        private const string INST_REMOTE_LOCKLOC_CMDSTR = "RWLS";
        private const string INST_REMOTE_UNLOCKLOC_CMDSTR ="LOCS";
        private const string INST_CLEAR_STATUS_CMDSTR = "*CLS";
        private const string INST_SET_RANGE_CMDSTR = "RANGE";
        private const string INST_GET_RANGE_CMDSTR = "RANGE1?";
        private const string INST_SET_FORMAT_CMDSTR = "FORMAT 2";
        private const string INST_SET_SAMPRATE_CMDSTR = "RATE";
        private const string INST_GET_SAMPRATE_CMDSTR = "RATE?";
        private const string INST_SET_FUNCACVOLT_CMDSTR = "VAC";
        private const string INST_SET_FUNCACCURR_CMDSTR = "AAC";
        private const string INST_SET_FUNCOHMS_CMDSTR = "OHMS";
        private const string INST_GET_FUNCTION_CMDSTR = "FUNC1?";
        private const string INST_GET_SAMPLE_CMDSTR = "MEAS1?";
        private const string INST_GET_VALUE_CMDSTR = "VAL1?";
        private const string INST_MEASUNITS_VOLT_STR = "VAC";
        private const string INST_MEASUNITS_CURR_STR = "AAC";
        private const string INST_MEASUNITS_OHMS_STR = "OHM";
        private const string INST_SAMPRATE_SLOW_STR = "S";
        private const string INST_SAMPRATE_MEDIUM_STR = "M";
        private const string INST_SAMPRATE_FAST_STR = "F";

        private SerialPort _Port = new SerialPort();

        /// <summary>
        /// Holds data received until we get a terminator.
        /// </summary>
        private string _BufferString = string.Empty;

        private char _Terminator = '\n';

        private object _SyncObject = new object();

        private System.Threading.ManualResetEvent _ResponseRecv = new System.Threading.ManualResetEvent(false);

        private Queue<string> _Responses = new Queue<string>();

        public Fluke8808A()
        {
            this.BaudRate = 9600;
            this.DataBits = 8;
            this.Parity = System.IO.Ports.Parity.None;
            this.StopBits = System.IO.Ports.StopBits.None;
        }

        /// <summary>
        /// Gets or sets BaudRate (Default: 9600)
        /// </summary>
        public int BaudRate { get; set; }

        /// <summary>
        /// Gets or sets DataBits (Default: 8)
        /// </summary>
        public int DataBits { get; set; }

        /// <summary>
        /// Gets or sets Handshake (Default: None)
        /// </summary>
        public Handshake Handshake { get; set; }

        /// <summary>
        /// Gets or sets Parity (Default: None)
        /// </summary>
        public Parity Parity { get; set; }

        /// <summary>
        /// Gets or sets PortName (Default: COM1)
        /// </summary>
        public string PortName { get; set; }

        /// <summary>
        /// Gets or sets StopBits (Default: One}
        /// </summary>
        public StopBits StopBits { get; set; }        
        
        public bool Connect(int comport)
        {
            try
            {
                this._Port.BaudRate = this.BaudRate;
                this._Port.DataBits = this.DataBits;
                this._Port.Handshake = this.Handshake;
                this._Port.Parity = this.Parity;
                this._Port.PortName = this.PortName;
                this._Port.StopBits = this.StopBits;
                this._Port.DataReceived += new SerialDataReceivedEventHandler(this.serialPortDataReceived);
                this._Port.ReadTimeout = 10;
                this._Port.WriteTimeout = 10;
                this._Port.Open();
            }
            catch
            {
                return false;
            }

            return true;
        }

        public void Close()
        {
            this._Port.DataReceived -= new SerialDataReceivedEventHandler(this.serialPortDataReceived);
            this._Port.Close();
        }

        public bool Login()
        {
            Tuple<int, string> reply = XmitWaitForReply("", new string[] { INST_OK_PROMPT_STRING, INST_CMDERR_PROMPT_STRING, INST_EXECERR_PROMPT_STRING });

            if ((reply.Item1 >= 0) && (reply.Item1 <= 1))
            {
                return true;
            }

            return false;
        }

        public Tuple<int, string> GetIdentifier()
        {
            return GetStatus(string.Format("{0}\r", INST_CMD_IDENTIFY));            
        }

        public Tuple<int, string> GetFunctionType()
        {
            return GetStatus(string.Format("{0}\r", INST_GET_FUNCTION_CMDSTR));
        }

        public Tuple<int, string> GetRange()
        {
            return GetStatus(string.Format("{0}\r", INST_GET_RANGE_CMDSTR));
        }

        public Tuple<int, string> GetSample()
        {
            return GetStatus(string.Format("{0}\r", INST_GET_VALUE_CMDSTR));
        }

        public Tuple<int, string> PutRemoteLock(bool lockFlag)
        {
            Tuple<int, string> returnValue = new Tuple<int,string>(-1, string.Empty);
            if( lockFlag )
            {
                returnValue = GetStatus(string.Format("{0}\r", INST_REMOTE_LOCKLOC_CMDSTR));
            }
            else
            {
                returnValue = GetStatus(string.Format("{0}\r", INST_REMOTE_UNLOCKLOC_CMDSTR));
            }
            return GetStatus(string.Format("{0}\r", INST_GET_FUNCTION_CMDSTR));
        }

        public Tuple<int, string> SetPowerupRest()
        {
            return GetStatus(string.Format("{0}\r", INST_PWRUP_RESET_CMDSTR));
        }

        public Tuple<int, string> SetRange(int range)
        {
            return GetStatus(string.Format("{0} {1}\r", INST_SET_RANGE_CMDSTR, range));
        }

        public Tuple<int, string> SetFormat()
        {
            return GetStatus(string.Format("{0}\r", INST_SET_FORMAT_CMDSTR));
        }

        public Tuple<int, string> SetSampleRate(Fluke8808ASampleRate sampleRate)
        {
            Tuple<int, string> returnValue = new Tuple<int, string>(-1, string.Empty);
            switch(sampleRate)
            {
                case Fluke8808ASampleRate.Slow:
                    returnValue = GetStatus(string.Format("{0} {1}\r", INST_SET_SAMPRATE_CMDSTR, INST_SAMPRATE_SLOW_STR));
                    break;
                case Fluke8808ASampleRate.Medium:
                    returnValue = GetStatus(string.Format("{0} {1}\r", INST_SET_SAMPRATE_CMDSTR, INST_SAMPRATE_MEDIUM_STR));
                    break;
                case Fluke8808ASampleRate.Fast:
                    returnValue = GetStatus(string.Format("{0} {1}\r", INST_SET_SAMPRATE_CMDSTR, INST_SAMPRATE_FAST_STR));
                    break;                    
            }
            return returnValue;
        }

        public Tuple<int, string> SetFunctionType(Fluke8808AOption option)
        {
            Tuple<int, string> returnValue = new Tuple<int, string>(-1, string.Empty);
            switch(option)
            {
                case Fluke8808AOption.ACAMP:
                    returnValue = GetStatus(string.Format("{0}\r", INST_SET_FUNCACCURR_CMDSTR));
                    break;
                case Fluke8808AOption.ACVolt:
                    returnValue = GetStatus(string.Format("{0}\r", INST_SET_FUNCACVOLT_CMDSTR));
                    break;
                case Fluke8808AOption.Ohms:
                    returnValue = GetStatus(string.Format("{0}\r", INST_SET_FUNCOHMS_CMDSTR));
                    break;
            }
            return returnValue;
        }

        private Tuple<int, string> GetStatus(string commandString)
        {
            Tuple<int,string> reply = XmitWaitForReply("", new string[] { INST_OK_PROMPT_STRING });
            if (reply.Item1 != 0)
            {                
                return new Tuple<int,string>(-1, string.Empty); // error?
            }

            reply = XmitWaitForReply(commandString, new string[] { INST_OK_PROMPT_STRING, INST_CMDERR_PROMPT_STRING, INST_EXECERR_PROMPT_STRING });
            if( reply.Item1 != 0)
            {
                return new Tuple<int, string>(-1, string.Empty); // error?
            }

            int index = reply.Item2.IndexOf(INST_OK_PROMPT_STRING);
            if(index == -1)
            {
                return new Tuple<int, string>(-1, string.Empty); // error?
            }

            string status = reply.Item2.Substring(index + INST_OK_PROMPT_STRING.Length);
            return new Tuple<int, string>(0, status);
        }

        private Tuple<int,string> XmitWaitForReply(string cmd, string[]potentialResponses)
        {
            Tuple<int, string> returnValue = new Tuple<int, string>(-1, string.Empty);
            int retries = 20;
            int result = -1;
            bool loop = true;

            result = GetPrompt(new string[] { INST_OK_PROMPT_STRING, INST_CMDERR_PROMPT_STRING, INST_EXECERR_PROMPT_STRING });

            if (result != 0 )
            {
                return new Tuple<int,string>(-1, string.Empty);
            }

            this.ResetBuffer();
            this._Port.Write(cmd);

            while ((retries-- > 0) && (loop == true))
            {
                bool triggered = this._ResponseRecv.WaitOne(20);
                if ((!triggered) && (retries > 0))
                {
                    continue;
                }

                if (retries <= 0)
                {
                    break;
                }

                string response;
                lock (this._SyncObject)
                {
                    response = this._Responses.Dequeue();
                }

                for (int i = 0; i < potentialResponses.Count(); i++)
                {
                    if (potentialResponses[i].CompareTo(response) == 0)
                    {
                        returnValue = new Tuple<int, string>(i, response);                        
                        loop = false;
                        break;
                    }
                }
            }

            return returnValue;
        }

        private int GetPrompt(string[] prompts)
        {
            int retries = 5;
            int result = -1;
            bool loop = true;

            while ((retries-- > 0) && (loop == true))
            {
                this.ResetBuffer();
                this._Port.Write("\r");
                bool triggered = this._ResponseRecv.WaitOne(20);
                if ((!triggered) && (retries > 0))
                {
                    continue;
                }

                if (retries <= 0)
                {
                    break;
                }

                string response;
                lock (this._SyncObject)
                {
                    response = this._Responses.Dequeue();
                }

                for (int i = 0; i < prompts.Count(); i++)
                {
                    if (prompts[i].CompareTo(response) == 0)
                    {
                        result = i;
                        loop = false;
                        break;
                    }
                }
            }

            return result;
        }

        private void ResetBuffer()
        {
            this._BufferString = string.Empty;
            this._ResponseRecv.Reset();
            this._Responses.Clear();
        }

        /// <summary>
        /// Handles DataReceived Event from SerialPort
        /// </summary>
        /// <param name="sender">Serial Port</param>
        /// <param name="e">Event arguments</param>
        private void serialPortDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            // Initialize a buffer to hold the received data
            byte[] buffer = new byte[this._Port.ReadBufferSize];

            // There is no accurate method for checking how many bytes are read
            // unless you check the return from the Read method
            int bytesRead = this._Port.Read(buffer, 0, buffer.Length);

            // For the example assume the data we are received is ASCII data.
            this._BufferString += Encoding.ASCII.GetString(buffer, 0, bytesRead);

            // Check if string contains the terminator 
            if (this._BufferString.IndexOf(this._Terminator) > -1)
            {
                // If tString does contain terminator we cannot assume that it is the last character received
                string workingString = this._BufferString.Substring(0, this._BufferString.IndexOf(this._Terminator));

                // Remove the data up to the terminator from tString
                this._BufferString = this._BufferString.Substring(this._BufferString.IndexOf(this._Terminator));

                lock (this._SyncObject)
                {
                    this._Responses.Enqueue(workingString);
                    this._ResponseRecv.Set();
                }
            }
        }
    }
}
