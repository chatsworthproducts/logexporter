﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

namespace CPI.eConnect.Communications
{
    public class BaseCOMInerface
    {
        protected SerialPort _Port = new SerialPort();

        /// <summary>
        /// Holds data received until we get a terminator.
        /// </summary>
        private string _BufferString = string.Empty;

        private char _Terminator = '\n';

        protected object _SyncObject = new object();

        protected System.Threading.ManualResetEvent _ResponseRecv = new System.Threading.ManualResetEvent(false);

        protected Queue<string> _Responses = new Queue<string>();

        /// <summary>
        /// Gets or sets BaudRate (Default: 9600)
        /// </summary>
        public int BaudRate { get; set; }

        /// <summary>
        /// Gets or sets DataBits (Default: 8)
        /// </summary>
        public int DataBits { get; set; }

        /// <summary>
        /// Gets or sets Handshake (Default: None)
        /// </summary>
        public Handshake Handshake { get; set; }

        /// <summary>
        /// Gets or sets Parity (Default: None)
        /// </summary>
        public Parity Parity { get; set; }

        /// <summary>
        /// Gets or sets PortName (Default: COM1)
        /// </summary>
        public string PortName { get; set; }

        /// <summary>
        /// Gets or sets StopBits (Default: One}
        /// </summary>
        public StopBits StopBits { get; set; }

        public bool Connect(int comport)
        {
            try
            {
                this._Port.BaudRate = this.BaudRate;
                this._Port.DataBits = this.DataBits;
                this._Port.Handshake = this.Handshake;
                this._Port.Parity = this.Parity;
                this._Port.PortName = this.PortName;
                this._Port.StopBits = this.StopBits;
                this._Port.DataReceived += new SerialDataReceivedEventHandler(this.serialPortDataReceived);
                this._Port.ReadTimeout = 10;
                this._Port.WriteTimeout = 10;
                this._Port.Open();
            }
            catch
            {
                return false;
            }

            return true;
        }

        public void Close()
        {
            this._Port.DataReceived -= new SerialDataReceivedEventHandler(this.serialPortDataReceived);
            this._Port.Close();
        }

        protected void ResetBuffer()
        {
            this._BufferString = string.Empty;
            this._ResponseRecv.Reset();
            this._Responses.Clear();
        }

        /// <summary>
        /// Handles DataReceived Event from SerialPort
        /// </summary>
        /// <param name="sender">Serial Port</param>
        /// <param name="e">Event arguments</param>
        private void serialPortDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            // Initialize a buffer to hold the received data
            byte[] buffer = new byte[this._Port.ReadBufferSize];

            // There is no accurate method for checking how many bytes are read
            // unless you check the return from the Read method
            int bytesRead = this._Port.Read(buffer, 0, buffer.Length);

            // For the example assume the data we are received is ASCII data.
            this._BufferString += Encoding.ASCII.GetString(buffer, 0, bytesRead);

            // Check if string contains the terminator 
            if (this._BufferString.IndexOf(this._Terminator) > -1)
            {
                // If tString does contain terminator we cannot assume that it is the last character received
                string workingString = this._BufferString.Substring(0, this._BufferString.IndexOf(this._Terminator));

                // Remove the data up to the terminator from tString
                this._BufferString = this._BufferString.Substring(this._BufferString.IndexOf(this._Terminator));

                lock (this._SyncObject)
                {
                    this._Responses.Enqueue(workingString);
                    this._ResponseRecv.Set();
                }
            }
        }
    }
}
