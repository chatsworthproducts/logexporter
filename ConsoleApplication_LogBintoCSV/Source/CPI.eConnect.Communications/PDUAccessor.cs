﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Communications
{
    public enum AccessorType
    {
        SNMP = 0,
        SSH = 1,
        Telnet = 2,
        Any = 3,
        UseGateway = 4
    }

    public class PDUAccessorInfo
    {
        public AccessorType Type { get; set; }
        public string Address { get; set; }        
    }
}
