﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace CPI.eConnect.UpdateManager
{
    public class UpdateOpManager
    {
        private static UpdateOpManager _Instance = new UpdateOpManager();

        private bool _Dirty = true;
        private List<UpdateOp> _Ops;

        public UpdateOpManager Instance { get { return _Instance; } }

        public IEnumerable<UpdateOp> PDUs
        {
            get
            {
                return this.LoadOps();
            }
        }

        public UpdateOp CreateOP(string firmwarePath)
        {
            DateTime createTime = DateTime.Now;
            string sql = string.Format("insert into update_op (create_time, start_time, end_time, path, status) values ('{0}','{1}','{2}','{3}',{4}); SELECT last_insert_rowid()",
                createTime.ToUniversalTime(), string.Empty, string.Empty, firmwarePath, (int)OpStatus.Created);

            using (SQLiteConnection conn = this.GetConnection())
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sql))
                {
                    int rowId = (int)cmd.ExecuteScalar();
                    _Dirty = true;

                    UpdateOp op = new UpdateOp();
                    op.Id = rowId;
                    op.CreateDate = createTime;
                    op.StartDate = DateTime.MinValue;
                    op.EndDate = DateTime.MinValue;
                    op.FirmwarePath = firmwarePath;
                    op.Status = OpStatus.Created;

                    return op;
                }
            }
        }

        public void SaveOp(UpdateOp op)
        {
            string sql = string.Format("update update_op set start_time='{0}', end_time='{1}', path='{2}', status={3} where id={4}",
                op.StartDate, op.EndDate, op.FirmwarePath, op.Status, op.Id);

            using (SQLiteConnection conn = this.GetConnection())
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sql))
                {
                    cmd.ExecuteNonQuery();
                    _Dirty = true;
                }
            }
        }

        public void DeleteOp(UpdateOp op)
        {
            string sql = string.Format("delete from update_op where id={0}",
                op.Id);

            using (SQLiteConnection conn = this.GetConnection())
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sql))
                {
                    cmd.ExecuteNonQuery();
                    _Dirty = true;
                }
            }
        }

        private SQLiteConnection GetConnection()
        {
            return new SQLiteConnection(System.Configuration.ConfigurationManager.ConnectionStrings["UpdateSvrDb"].ConnectionString);
        }

        private IEnumerable<UpdateOp> LoadOps()
        {
            if (_Dirty == true)
            {
                _Ops = new List<UpdateOp>();
                using (SQLiteConnection conn = this.GetConnection())
                {
                    conn.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(GetSelect()))
                    {
                        using (SQLiteDataReader rdr = cmd.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                _Ops.Add(UpdateOpBuilder.BuildUpdateOp(rdr));
                            }
                            _Dirty = false;
                        }
                    }
                }
            }

            return _Ops;
        }

        private string GetSelect()
        {
            return string.Format("Select {0} from update_op", UpdatePDUBuilder.UPDATE_PDU_FIELDS);
        }
    }
}
