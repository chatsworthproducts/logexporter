﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.UpdateManager
{
    public class UpdatePDU
    {
        public int Id { get; set; }
        public int GatewayId { get; set; }
        public string IP { get; set; }
        public string IPv6 { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Model { get; set; }
    }
}
