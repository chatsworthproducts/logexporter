﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace CPI.eConnect.UpdateManager
{
    public class UpdateChannel
    {
        private bool _Dirty = false;
        private List<UpdatePDU> _PDUs;

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public IEnumerable<UpdatePDU> AssociatedPDUs
        {
            get
            {
                return this.LoadAssociatedPDUs();
            }
        }

        public void AddPDUtoChannel(UpdatePDU pdu)
        {
            string sql = string.Format("insert into pdu_channel (pduId, channelId) values ({0},{1})",
                pdu.Id, this.Id);

            using (SQLiteConnection conn = this.GetConnection())
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sql))
                {
                    int rowId = (int)cmd.ExecuteNonQuery();
                    _Dirty = true;
                }
            }
        }

        public void RemovePDUFromChannel(UpdatePDU pdu)
        {
            string sql = string.Format("delete from pdu where pduId={0} and channelId={1}",
                pdu.Id, this.Id);

            using (SQLiteConnection conn = this.GetConnection())
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sql))
                {
                    cmd.ExecuteNonQuery();
                    _Dirty = true;
                }
            }
        }

        private IEnumerable<UpdatePDU> LoadAssociatedPDUs()
        {
            if (_Dirty == true)
            {
                _PDUs = new List<UpdatePDU>();
                using (SQLiteConnection conn = this.GetConnection())
                {
                    conn.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(GetSelect()))
                    {
                        using (SQLiteDataReader rdr = cmd.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                _PDUs.Add(UpdatePDUBuilder.UpdatePDU(rdr));
                            }
                            _Dirty = false;
                        }
                    }
                }
            }

            return _PDUs;
        }

        private SQLiteConnection GetConnection()
        {
            return new SQLiteConnection(System.Configuration.ConfigurationManager.ConnectionStrings["UpdateSvrDb"].ConnectionString);
        }

        private string GetSelect()
        {
            return string.Format("Select {0} from pdu join pdu_channels on pdu.id=pdu_channels.pduId where pdu_channels.channeId={1}", 
                UpdatePDUBuilder.UPDATE_PDU_FIELDS, this.Id);
        }
    }
}
