﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace CPI.eConnect.UpdateManager
{
    internal static class UpdateOpBuilder
    {
        public const string UPDATE_OP_FIELDS = "id, create_time, start_time, end_time, path, status";

        public static UpdateOp BuildUpdateOp(SQLiteDataReader rdr)
        {
            UpdateOp op = new UpdateOp();
            op.Id = rdr.GetInt32(0);
            op.CreateDate = DateTime.Parse(rdr.GetString(1));
            op.StartDate = ConvertStringToDate(rdr.GetString(2));
            op.EndDate = ConvertStringToDate(rdr.GetString(3));
            op.FirmwarePath = rdr.GetString(4);
            op.Status = (OpStatus)rdr.GetInt32(5);

            return op;
        }

        public static DateTime ConvertStringToDate(string strDate)
        {
            if(strDate.Length > 0)
            {
                return DateTime.Parse(strDate);
            }

            return DateTime.MinValue;
        }
    }
}
