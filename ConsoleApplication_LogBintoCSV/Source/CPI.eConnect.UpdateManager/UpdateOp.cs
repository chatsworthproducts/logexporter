﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.UpdateManager
{
    public enum OpStatus
    {
        Created = 0,
        Running = 1,
        Complete = 2
    }

    public class UpdateOp
    {
        public int Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string FirmwarePath { get; set; }
        public OpStatus Status { get; set; }
        public IEnumerable<UpdateChannel> AssociatedChannels { get; set; }
    }
}
