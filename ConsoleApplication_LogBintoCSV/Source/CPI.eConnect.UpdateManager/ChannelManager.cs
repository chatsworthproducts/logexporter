﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace CPI.eConnect.UpdateManager
{
    public class ChannelManager
    {
        private static ChannelManager _Instance = new ChannelManager();
        
        private bool _Dirty = true;
        private List<UpdateChannel> _Channels;

        public static ChannelManager Instance { get { return _Instance; } }
        public IEnumerable<UpdateChannel> Channels 
        { 
            get
            {
                return this.LoadChannels();
            }
        }

        public UpdateChannel CreateChannel(string name, string description)
        {
            string sql = string.Format("insert into Channels (name, description) values ('{0}','{1}'); SELECT last_insert_rowid()",
                name, description);

            using (SQLiteConnection conn = this.GetConnection())
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sql))
                {
                    int rowId = (int)cmd.ExecuteScalar();
                    _Dirty = true;

                    UpdateChannel chn = new UpdateChannel();
                    chn.Id = rowId;
                    chn.Name = name;
                    chn.Description = description;

                    return chn;
                }
            }
        }
        
        public void SaveChannel(UpdateChannel channel)
        {
            string sql = string.Format("update Channels set name='{0}', description='{1}' where id={2}",
                channel.Name, channel.Description, channel.Id);

            using (SQLiteConnection conn = this.GetConnection())
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sql))
                {
                    cmd.ExecuteNonQuery();
                    _Dirty = true;
                }
            }
        }

        public void DeleteChannel(UpdateChannel channel)
        {
            string sql = string.Format("delete from Channels where id={0}",
                channel.Id);

            using (SQLiteConnection conn = this.GetConnection())
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sql))
                {
                    cmd.ExecuteNonQuery();
                    _Dirty = true;
                }
            }
        }

        private SQLiteConnection GetConnection()
        {
            return new SQLiteConnection(System.Configuration.ConfigurationManager.ConnectionStrings["UpdateSvrDb"].ConnectionString);
        }

        private IEnumerable<UpdateChannel> LoadChannels()
        {
            if(_Dirty == true)
            {
                _Channels = new List<UpdateChannel>();
                using(SQLiteConnection conn = this.GetConnection())
                {
                    conn.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand("Select * from Channel"))
                    {
                        using(SQLiteDataReader rdr = cmd.ExecuteReader())
                        {
                            while(rdr.Read())
                            {
                                UpdateChannel chn = new UpdateChannel();
                                chn.Id = rdr.GetInt32(0);
                                chn.Name = rdr.GetString(1);
                                chn.Description = rdr.GetString(2);
                                _Channels.Add(chn);
                            }
                            _Dirty = false;
                        }
                    }
                }
            }

            return _Channels;
        }
    }
}
