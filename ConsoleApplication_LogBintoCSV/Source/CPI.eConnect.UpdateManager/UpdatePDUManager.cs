﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace CPI.eConnect.UpdateManager
{
    class UpdatePDUManager
    {
        private static UpdatePDUManager _Instance = new UpdatePDUManager();

        private bool _Dirty = true;
        private List<UpdatePDU> _PDUs;

        public static UpdatePDUManager Instance { get { return _Instance; } }

        public IEnumerable<UpdatePDU> PDUs
        {
            get
            {
                return this.LoadPDUs();
            }
        }

        public UpdatePDU CreatePDU(int gatewayId, string model, string name, string description, string ip, string ipv6)
        {
            string sql = string.Format("insert into pdu (gatewayId, ip, ipv6, name, description, model) values ({0},'{1}','{2}','{3}','{4}','{5}'); SELECT last_insert_rowid()",
                gatewayId, ip, ipv6, name, description, model);

            using (SQLiteConnection conn = this.GetConnection())
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sql))
                {
                    int rowId = (int)cmd.ExecuteScalar();
                    _Dirty = true;

                    UpdatePDU pdu = new UpdatePDU();
                    pdu.Id = rowId;
                    pdu.GatewayId = gatewayId;
                    pdu.Model = model;
                    pdu.IP = ip;
                    pdu.IPv6 = ipv6;
                    pdu.Name = name;
                    pdu.Description = description;

                    return pdu;
                }
            }
        }

        public void SavePDU(UpdatePDU pdu)
        {
            string sql = string.Format("update pdu set gatewayId={0}, ip='{1}', ipv6='{2}', name='{3}', description='{4}', model='{5}' where id={6}",
                pdu.GatewayId, pdu.IP, pdu.IPv6, pdu.Name, pdu.Description, pdu.Model, pdu.Id);

            using (SQLiteConnection conn = this.GetConnection())
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sql))
                {
                    cmd.ExecuteNonQuery();
                    _Dirty = true;
                }
            }
        }

        public void DeletePDU(UpdatePDU pdu)
        {
            string sql = string.Format("delete from pdu where id={0}",
                pdu.Id);

            using (SQLiteConnection conn = this.GetConnection())
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sql))
                {
                    cmd.ExecuteNonQuery();
                    _Dirty = true;
                }
            }
        }

        private SQLiteConnection GetConnection()
        {
            return new SQLiteConnection(System.Configuration.ConfigurationManager.ConnectionStrings["UpdateSvrDb"].ConnectionString);
        }

        private IEnumerable<UpdatePDU> LoadPDUs()
        {
            if (_Dirty == true)
            {
                _PDUs = new List<UpdatePDU>();
                using (SQLiteConnection conn = this.GetConnection())
                {
                    conn.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(GetSelect()))
                    {
                        using (SQLiteDataReader rdr = cmd.ExecuteReader())
                        {
                            while (rdr.Read())
                            {                                
                                _PDUs.Add(UpdatePDUBuilder.UpdatePDU(rdr));
                            }
                            _Dirty = false;
                        }
                    }
                }
            }

            return _PDUs;
        }

        private string GetSelect()
        {
            return string.Format("Select {0} from pdu", UpdatePDUBuilder.UPDATE_PDU_FIELDS);
        }
    }
}
