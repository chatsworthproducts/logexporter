﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace CPI.eConnect.UpdateManager
{
    internal class UpdatePDUBuilder
    {
        public const string UPDATE_PDU_FIELDS = "id, gatewayId, ip, ipv6, name, description, model";

        public static UpdatePDU UpdatePDU(SQLiteDataReader rdr)
        {
            UpdatePDU pdu = new UpdatePDU();
            pdu.Id = rdr.GetInt32(0);
            pdu.GatewayId = rdr.GetInt32(1);
            pdu.IP = rdr.GetString(2);
            pdu.IPv6 = rdr.GetString(3);
            pdu.Name = rdr.GetString(4);
            pdu.Description = rdr.GetString(5);
            pdu.Model = rdr.GetString(6);

            return pdu;
        }
    }
}
