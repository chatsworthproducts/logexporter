﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPI.eConnect.Communications;

namespace CPI.eConnect.Programmer
{
    public class MCMProgrammer : IDisposable
    {
        protected MCMBiosController _Controller;
        private string[] _WaitData;
        private int _WaitID = 0;
        private System.Threading.ManualResetEvent _ResponseRecv = new System.Threading.ManualResetEvent(false);

        public EventHandler<ProgressStatusEventArgs> StatusEvent;

        public MCMProgrammer()
        {
            this.AbortEvent = new System.Threading.ManualResetEvent(false);
        }

        public System.Threading.ManualResetEvent AbortEvent 
        {
            get;
            set;
        }
        
        public void Setup(MCMBiosController controller)
        {
            _Controller = controller;
            _Controller.NewData += new EventHandler<OnSerialDataEventArgs>(this.MCMResponseHandler);
        }

        public void Setup(string port)
        {
            _Controller = new MCMBiosController();
            _Controller.PortName = port;
            _Controller.NewData += new EventHandler<OnSerialDataEventArgs>(this.MCMResponseHandler);
            _Controller.Connect();
        }

        public void Dispose()
        {
            _Controller.NewData -= new EventHandler<OnSerialDataEventArgs>(this.MCMResponseHandler);
        }

        protected int SendCmdAndWait(string cmd, string response, int timeout)
        {
            SetWaitRespone(response);
            _Controller.SendBiosCmd(cmd);
            int waitReturn = System.Threading.WaitHandle.WaitAny(new System.Threading.WaitHandle[] { _ResponseRecv, this.AbortEvent }, timeout);
            if (waitReturn == 0)
            {            
                return 0;
            }
            _Controller.Flush();

            if (waitReturn == 1)
            {
                return -1;
            }
            return 2;
        }

        protected int SendCmdAndWait(string cmd, string[] response, int timeout, out int responseId)
        {
            responseId = -1;
            SetWaitRespone(response);
            _Controller.SendBiosCmd(cmd);
            int waitReturn = System.Threading.WaitHandle.WaitAny(new System.Threading.WaitHandle[] { _ResponseRecv, this.AbortEvent }, timeout);
            if ( waitReturn == 0)
            {
                responseId = _WaitID;
                return 0;
            }
            _Controller.Flush();
            
            if (waitReturn == 1)
            {
                return -1;
            }            
            return 2;
        }

        protected void NotifyStatus(string statusMsg, int currentStep, int totalSteps)
        {
            if (this.StatusEvent != null)
            {
                this.StatusEvent(this, new ProgressStatusEventArgs(statusMsg, currentStep, totalSteps));
            }
        }

        protected void SetWaitRespone(string wait)
        {
            _ResponseRecv.Reset();
            _WaitData = new string[] { wait };
            _WaitID = 0;
        }

        protected void SetWaitRespone(string[] wait)
        {
            _ResponseRecv.Reset();
            _WaitData = wait;
            _WaitID = 0;
        }

        private void MCMResponseHandler(object sender, OnSerialDataEventArgs e)
        {
            if (_WaitData != null)
            {
                for (int i = 0; i < _WaitData.Length; i++)
                {
                    if (e.Data.Contains(_WaitData[i]) == true)
                    {
                        _WaitID = i;
                        _ResponseRecv.Set();
                    }
                }
            }
        }
    }
}
