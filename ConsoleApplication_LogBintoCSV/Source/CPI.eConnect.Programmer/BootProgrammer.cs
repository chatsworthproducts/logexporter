﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPI.eConnect.Communications;

namespace CPI.eConnect.Programmer
{
    public class BootProgrammer : MCMWatcher
    {
        private const int TOTAL_STEPS = 15;

        public int Execute(bool inProgramingMode)
        {
            _Sequence.Add(new SerialSequence("CPU:   Freescale MCF54418 (Mask:", new ProgressStatusEventArgs("MCM booting...", 1, BootProgrammer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("## Booting kernel from Legacy Image", new ProgressStatusEventArgs("MCM booting...", 2, BootProgrammer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("## Loading init Ramdisk from Legacy Image", new ProgressStatusEventArgs("MCM booting...", 3, BootProgrammer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("starting up linux startmem", new ProgressStatusEventArgs("MCM booting...", 4, BootProgrammer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("checking if image is initramfs", new ProgressStatusEventArgs("MCM booting...", 5, BootProgrammer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("NAND device: Manufacturer ID:", new ProgressStatusEventArgs("MCM booting...", 6, BootProgrammer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("cpi: Chatsworth Products, Inc., Version", new ProgressStatusEventArgs("MCM booting...", 7, BootProgrammer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("RAMDISK: Compressed image found at block", new ProgressStatusEventArgs("MCM booting...", 8, BootProgrammer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("init started: BusyBox", new ProgressStatusEventArgs("MCM booting...", 9, BootProgrammer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("#config partition is tested", new ProgressStatusEventArgs("MCM booting...", 10, BootProgrammer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("#patch partition is tested", new ProgressStatusEventArgs("MCM booting...", 11, BootProgrammer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("#log partition is tested", new ProgressStatusEventArgs("MCM booting...", 12, BootProgrammer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("Setting up networking on loopback device", new ProgressStatusEventArgs("MCM booting...", 13, BootProgrammer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("CPI app timestamps:", new ProgressStatusEventArgs("MCM booting...", 14, BootProgrammer.TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("CPI Event ID", new ProgressStatusEventArgs("MCM booting...", 15, BootProgrammer.TOTAL_STEPS)));

            if (inProgramingMode == true)
            {
                this.NotifyStatus("Restarting the MCM.", 0, BootProgrammer.TOTAL_STEPS);                
                _Controller.Flush();
                _Controller.SendBiosCmd("reset\r");
            }

            return this.Watch();
        }        
    }
}
