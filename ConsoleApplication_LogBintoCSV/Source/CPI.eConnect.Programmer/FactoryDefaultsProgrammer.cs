﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPI.eConnect.Communications;

namespace CPI.eConnect.Programmer
{
    public sealed class FactoryDefaultsProgrammer : MCMProgrammer
    {
        public const int TOTAL_STEPS = 3;

        public int Execute()
        {
            this.NotifyStatus("Logging into MCM.", 0, FactoryDefaultsProgrammer.TOTAL_STEPS);
            _Controller.Flush();
            if(Login() != 0)
            {
                this.NotifyStatus("Failed to login to MCM.", 0, FactoryDefaultsProgrammer.TOTAL_STEPS);
                return 1;
            }

            this.NotifyStatus("Setting Advanced Mode.", 1, FactoryDefaultsProgrammer.TOTAL_STEPS);
            _Controller.Flush();
            if(this.SendCmdAndWait("#)(*&^\r", "OK", 30000) != 0 )
            {
                this.NotifyStatus("Failed to set advanced mode.", 1, FactoryDefaultsProgrammer.TOTAL_STEPS);
                return 1;
            }
                        
            // Set Factory Defaults
            this.NotifyStatus("Setting factory defaults for MCM.", 2, FactoryDefaultsProgrammer.TOTAL_STEPS);
            _Controller.Flush();
            if (this.SendCmdAndWait("#D 0 7 95132408\r", "OK", 30000) != 0)
            {
                this.NotifyStatus("Failed to set factory defaults for MCM.", 2, FactoryDefaultsProgrammer.TOTAL_STEPS);
                return 1;
            }
            this.NotifyStatus("Factory defaults set for MCM.", 3, FactoryDefaultsProgrammer.TOTAL_STEPS); 

            return 0;
        }

        private int Login()
        {
            // determine where we are at..
            int responseId = 0;
            SendCmdAndWait("test\rtest\r", new string[] { "Bad command!", "Incorrect", "cpi #" }, 30000, out responseId);
            
            switch(responseId)
            {
                case 1: // enter the name
                    if( SendCmdAndWait("admin\r", "Name: admin", 30000) != 0 )
                    {
                        return 1;
                    }                    
                    goto case 2;
                case 2: // enter the password
                    if( SendCmdAndWait("admin\r", "Password:", 30000) != 0 )
                    {
                        return 1;
                    }
                    goto case 0;
                case 3: // exit the shell and get back into the cpi app
                    if( SendCmdAndWait("exit\r", "?", 30000) != 0 )
                    {
                        return 1;
                    }
                    break;
                case 0: // logged in and ready to go                    
                    break;
            }            

            return 0;
        }
    }
}
