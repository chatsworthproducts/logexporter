﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Programmer
{
    public class SerialSequence
    {
        public SerialSequence(string inWaitData, ProgressStatusEventArgs eventArgs)
        {
            this.CurrentWaitData = inWaitData;
            this.EventData = eventArgs;
        }
        public string CurrentWaitData { get; set; }
        public ProgressStatusEventArgs EventData { get; set; }
    }
}
