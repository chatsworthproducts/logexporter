﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPI.eConnect.Programmer
{
    public class ProgressStatusEventArgs : EventArgs
    {
        public ProgressStatusEventArgs(string msg, int currStep, int totalSteps)
        {
            this.Message = msg;
            this.CurrentStep = currStep;
            this.TotalSteps = totalSteps;   
        }

        public string Message { get; set; }
        public int CurrentStep { get; set; }
        public int TotalSteps { get; set; }
    }
}
