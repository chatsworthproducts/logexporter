﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPI.eConnect.Communications;

namespace CPI.eConnect.Programmer
{
    public class MCMWatcher : IDisposable
    {
        protected List<SerialSequence> _Sequence = new List<SerialSequence>();
        protected MCMBiosController _Controller;        
        private int _CurrentSequenceEntry = 0;
        private Queue<ProgressStatusEventArgs> _EventQueue = new Queue<ProgressStatusEventArgs>();
        private System.Threading.ManualResetEvent _ResponseRecv = new System.Threading.ManualResetEvent(false);

        public EventHandler<ProgressStatusEventArgs> StatusEvent;

        public MCMWatcher()
        {
            this.AbortEvent = new System.Threading.ManualResetEvent(false);
        }

        public System.Threading.ManualResetEvent AbortEvent
        {
            get;
            set;
        }

        public void Setup(MCMBiosController controller)
        {
            _Controller = controller;
            _Controller.NewData += new EventHandler<OnSerialDataEventArgs>(this.MCMResponseHandler);
        }

        public void Setup(string port)
        {
            _Controller = new MCMBiosController();
            _Controller.PortName = port;
            _Controller.NewData += new EventHandler<OnSerialDataEventArgs>(this.MCMResponseHandler);
            _Controller.Connect();
        }

        public void Dispose()
        {
            _Controller.NewData -= new EventHandler<OnSerialDataEventArgs>(this.MCMResponseHandler);
        }

        protected int Watch()
        {
            bool bLoop = true;
            int timeOutCounter = 0;
            while ((bLoop == true) && (timeOutCounter < 150000))
            {
                if (_ResponseRecv.WaitOne(1) == true)
                {
                    timeOutCounter = 0;
                    ProgressStatusEventArgs eventData;

                    while (this._EventQueue.Count > 0)
                    {
                        lock (this._EventQueue)
                        {
                            eventData = this._EventQueue.Dequeue();
                        }

                        if (StatusEvent != null)
                        {
                            this.StatusEvent(this, eventData);
                        }

                        if (_CurrentSequenceEntry >= _Sequence.Count)
                        {
                            bLoop = false;
                        }
                    }
                    this._ResponseRecv.Reset();
                }
                else
                {
                    if (this.AbortEvent.WaitOne(0) == true)
                    {
                        return -1;
                    }
                    timeOutCounter++;
                }
            }

            if (bLoop == true)
            {
                return 1;
            }

            return 0;
        }

        protected void NotifyStatus(string statusMsg, int currentStep, int totalSteps)
        {
            if (this.StatusEvent != null)
            {
                this.StatusEvent(this, new ProgressStatusEventArgs(statusMsg, currentStep, totalSteps));
            }
        }

        private void MCMResponseHandler(object sender, OnSerialDataEventArgs e)
        {
            if (_Sequence != null)
            {
                for (int i = _CurrentSequenceEntry; i < _Sequence.Count; i++)
                {
                    if (e.Data.Contains(_Sequence[i].CurrentWaitData) == true)
                    {
                        lock (this._EventQueue)
                        {
                            this._EventQueue.Enqueue(_Sequence[i].EventData);
                        }
                        _ResponseRecv.Set();
                        _CurrentSequenceEntry=_Sequence[i].EventData.CurrentStep;

                        // these are the droids we're looking for,  head back out.
                        break;
                    }
                }
            }
        }
    }
}
