﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPI.eConnect.Communications;

namespace CPI.eConnect.Programmer
{
    public sealed class SerialBootProgrammer : MCMWatcher
    {
        private const int TOTAL_STEPS = 5;

        public int Execute()
        {
            this.NotifyStatus("Power up the MCM, with the SBF switch set to ON.", 0, TOTAL_STEPS);            

            _Sequence.Add(new SerialSequence("cpisbfub", new ProgressStatusEventArgs("MCM booting...", 0, TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("### INITIALIZING", new ProgressStatusEventArgs("Initializing NAND flash.", 1, TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("### ERASING NANDFLASH", new ProgressStatusEventArgs("Erasing NAND flash.", 2, TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("### READING SPIFLASH UBOOT", new ProgressStatusEventArgs("Reading from SPI flash.", 3, TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("### WRITING NANDFLASH UBOOT", new ProgressStatusEventArgs("Writing to NAND flash.", 4, TOTAL_STEPS)));
            _Sequence.Add(new SerialSequence("### OK, please Reset!", new ProgressStatusEventArgs("Set the SBF switch set to OFF.", 5, TOTAL_STEPS)));

            return this.Watch();
        }
    }
}
