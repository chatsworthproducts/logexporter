﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPI.eConnect.Communications;

namespace CPI.eConnect.Programmer
{
    public sealed class BiosProgrammer : MCMProgrammer
    {
        private const int TOTAL_STEPS = 11;
        private const int STEP_STOP_AUTOBOOT = 1;
        private const int STEP_APPLY_ENVIRONMENT = 2;
        private const int STEP_FETCHING_KERNEL = 3;
        private const int STEP_WRITING_KERNEL1 = 4;
        private const int STEP_WRITING_KERNEL2 = 5;
        private const int STEP_FETCHING_FILESYSTEM = 6;
        private const int STEP_WRITING_FILESYSTEM1 = 7;
        private const int STEP_WRITING_FILESYSTEM2 = 8;
        private const int STEP_FETCHING_BOOTSCRIPT = 9;
        private const int STEP_WRITING_BOOTSCRIPT1 = 10;
        private const int STEP_WRITING_BOOTSCRIPT2 = 11;

        public string IPAddress
        {
            get;
            set;
        }

        public string ServerIP
        {
            get;
            set;
        }

        public int MCMID
        {
            get;
            set;
        }

        public bool Abort
        {
            get;
            set;
        }

        public int Execute()
        {
            this.NotifyStatus("Power up the MCM.", 0, BiosProgrammer.TOTAL_STEPS);
            this.NotifyStatus("Attempting to stop auto-boot sequence.", BiosProgrammer.STEP_STOP_AUTOBOOT, BiosProgrammer.TOTAL_STEPS);
            if (_Controller.StopAutoboot() != 0)
            {
                this.NotifyStatus("Unable to stop the MCM from booting. Restart the software and try again.", BiosProgrammer.STEP_STOP_AUTOBOOT, BiosProgrammer.TOTAL_STEPS);
                return 1;
            }
            this.NotifyStatus("Auto-boot sequence interrupted successfully.", BiosProgrammer.STEP_STOP_AUTOBOOT, BiosProgrammer.TOTAL_STEPS);

            this.NotifyStatus("Attempting to configure BIOS environment.", BiosProgrammer.STEP_APPLY_ENVIRONMENT, BiosProgrammer.TOTAL_STEPS);
            if(this.ApplyEnvironment() != 0)
            {
                this.NotifyStatus("Unable to apply the correct environment to the MCM. Restart the software and try again.", BiosProgrammer.STEP_APPLY_ENVIRONMENT, BiosProgrammer.TOTAL_STEPS);
                return 1;
            }
            this.NotifyStatus("BIOS environment successfully configured.", BiosProgrammer.STEP_APPLY_ENVIRONMENT, BiosProgrammer.TOTAL_STEPS);

            
            if( this.ApplyKernel() != 0 )
            {
                this.NotifyStatus("Unable to apply the correct kernel to the MCM. Restart the software and try again.", 0, BiosProgrammer.TOTAL_STEPS);
                return 1;
            }

            if(this.ApplyFileSystem() != 0)
            {
                this.NotifyStatus("Unable to apply the correct file system to the MCM. Restart the software and try again.", 0, BiosProgrammer.TOTAL_STEPS);
                return 1;
            }

            if( this.ApplyBootScript() != 0)
            {
                this.NotifyStatus("Unable to apply the correct boot script to the MCM. Restart the software and try again.", 0, BiosProgrammer.TOTAL_STEPS);
                return 1;
            }

            this.NotifyStatus("Complete.", BiosProgrammer.TOTAL_STEPS, BiosProgrammer.TOTAL_STEPS);
            return 0;
        }


        public int ApplyEnvironment()
        {   
            if (this.SendCmdAndWait("\r\n", "->", 5000) != 0)
            {
                return 1;
            }

            if (this.SendCmdAndWait("set bootargs 'root=/dev/ram0 rw ramdisk_size=49152 mtdparts=NAND:1m(u),16m(k),50m(f),8m(c),30m(p),135m(l),1m(s) console=ttyS0,9600'\r\n", 
                "->", 5000) != 0)
            {
                return 1;
            }

            if (this.SendCmdAndWait(string.Format("set ipaddr {0}\r\n", this.IPAddress), "->", 5000) != 0)
            {
                return 1;
            }

            if (this.SendCmdAndWait(string.Format("set serverip {0}\r\n", this.ServerIP), "->", 5000) != 0)
            {
                return 1;
            }

            if (this.SendCmdAndWait(string.Format("set mcmid {0}\r\n", this.MCMID), "->", 5000) != 0)
            {
                return 1;
            }

            if (this.SendCmdAndWait("save\r\n", "done", 5000) != 0)
            {
                return 1;
            }

            return 0;
        }

        public int ApplyKernel()
        {
            // Get and write the kernel
            this.NotifyStatus("Attempting to fetch the kernel from the TFPT site.", BiosProgrammer.STEP_FETCHING_KERNEL, BiosProgrammer.TOTAL_STEPS);
            if( TFTPFecth("46000000", "k") == false )
            {
                this.NotifyStatus("Failed to fetch the kernel from the TFPT site.", BiosProgrammer.STEP_FETCHING_KERNEL, BiosProgrammer.TOTAL_STEPS);
                return 1;
            }
            this.NotifyStatus("Successfully fetched the kernel from the TFPT site.", BiosProgrammer.STEP_FETCHING_KERNEL, BiosProgrammer.TOTAL_STEPS);
            if (this.Abort)
            {
                return -1;
            }

            this.NotifyStatus("Attempting to write first kernel image to NAND.", BiosProgrammer.STEP_WRITING_KERNEL1, BiosProgrammer.TOTAL_STEPS);
            // write the primary kernel image
            if( WriteAndCheck("46000000", "100000", "700000") != 0 )
            {
                this.NotifyStatus("Failed to write first kernel image to NAND.", BiosProgrammer.STEP_WRITING_KERNEL1, BiosProgrammer.TOTAL_STEPS);
                return 1;
            }
            this.NotifyStatus("Successfully wrote first kernel image to NAND.", BiosProgrammer.STEP_WRITING_KERNEL1, BiosProgrammer.TOTAL_STEPS);
            if (this.Abort)
            {
                return -1;
            }

            // write the secondary kernel image
            this.NotifyStatus("Attempting to write second kernel image to NAND.", BiosProgrammer.STEP_WRITING_KERNEL2, BiosProgrammer.TOTAL_STEPS);
            if (WriteAndCheck("46000000", "900000", "700000") != 0)
            {
                this.NotifyStatus("Failed to write second kernel image to NAND.", BiosProgrammer.STEP_WRITING_KERNEL2, BiosProgrammer.TOTAL_STEPS);
                return 1;
            }
            this.NotifyStatus("Successfully wrote second kernel image to NAND.", BiosProgrammer.STEP_WRITING_KERNEL2, BiosProgrammer.TOTAL_STEPS);
            if (this.Abort)
            {
                return -1;
            }

            return 0;
        }

        public int ApplyFileSystem()
        {
            // Get and write the filesystem.
            this.NotifyStatus("Attempting to fetch file system from TFPT site.", BiosProgrammer.STEP_FETCHING_FILESYSTEM, BiosProgrammer.TOTAL_STEPS);
            if( TFTPFecth("46400000", "fs") == false )
            {
                this.NotifyStatus("Failed to fetch file system from TFPT site.", BiosProgrammer.STEP_FETCHING_FILESYSTEM, BiosProgrammer.TOTAL_STEPS);
                return 1;
            }
            this.NotifyStatus("Successfully fetched file system from TFPT site.", BiosProgrammer.STEP_FETCHING_FILESYSTEM, BiosProgrammer.TOTAL_STEPS);
            if (this.Abort)
            {
                return -1;
            }

            this.NotifyStatus("Attempting to write first file system to NAND.", BiosProgrammer.STEP_WRITING_FILESYSTEM1, BiosProgrammer.TOTAL_STEPS);
            if( WriteAndCheck("46400000", "1100000", "1800000") != 0 )
            {
                this.NotifyStatus("Failed to write first file system to NAND.", BiosProgrammer.STEP_WRITING_FILESYSTEM1, BiosProgrammer.TOTAL_STEPS);
                return 1;
            }
            this.NotifyStatus("Successfully wrote first file system to NAND.", BiosProgrammer.STEP_WRITING_FILESYSTEM1, BiosProgrammer.TOTAL_STEPS);
            if (this.Abort)
            {
                return -1;
            }

            this.NotifyStatus("Attempting to write second file system to NAND.", BiosProgrammer.STEP_WRITING_FILESYSTEM2, BiosProgrammer.TOTAL_STEPS);
            if (WriteAndCheck("46400000", "2a00000", "1800000") != 0)
            {
                this.NotifyStatus("Failed to write second file system to NAND.", BiosProgrammer.STEP_WRITING_FILESYSTEM2, BiosProgrammer.TOTAL_STEPS);
                return 1;
            }
            this.NotifyStatus("Successfully wrote second file system to NAND.", BiosProgrammer.STEP_WRITING_FILESYSTEM2, BiosProgrammer.TOTAL_STEPS);
            if (this.Abort)
            {
                return -1;
            }

            return 0;
        }

        public int ApplyBootScript()
        {
            // Get and write the filesystem.
            this.NotifyStatus("Attempting to fetch the boot script from the TFTP site.", BiosProgrammer.STEP_FETCHING_BOOTSCRIPT, BiosProgrammer.TOTAL_STEPS);
            if (TFTPFecth("45000000", "bs") == false)
            {
                this.NotifyStatus("Failed to fetch the boot script from the TFTP site.", BiosProgrammer.STEP_FETCHING_BOOTSCRIPT, BiosProgrammer.TOTAL_STEPS);
                return 1;
            }
            this.NotifyStatus("Successfully fetched the boot script from the TFTP site.", BiosProgrammer.STEP_FETCHING_BOOTSCRIPT, BiosProgrammer.TOTAL_STEPS);
            if (this.Abort)
            {
                return -1;
            }

            this.NotifyStatus("Attempting to write the boot script to NAND.", BiosProgrammer.STEP_WRITING_BOOTSCRIPT1, BiosProgrammer.TOTAL_STEPS);
            if (WriteAndCheck("45000000", "f000000", "20000") != 0)
            {
                this.NotifyStatus("Failed to write the boot script to NAND.", BiosProgrammer.STEP_WRITING_BOOTSCRIPT1, BiosProgrammer.TOTAL_STEPS);
                return 1;
            }
            this.NotifyStatus("Successfully wrote the boot script to NAND.", BiosProgrammer.STEP_WRITING_BOOTSCRIPT1, BiosProgrammer.TOTAL_STEPS);
            if (this.Abort)
            {
                return -1;
            }

            this.NotifyStatus("Attempting to write the boot script to NAND.", BiosProgrammer.STEP_WRITING_BOOTSCRIPT2, BiosProgrammer.TOTAL_STEPS);
            if (WriteAndCheck("45000000", "f020000", "20000") != 0)
            {
                this.NotifyStatus("Failed to write the boot script to NAND.", BiosProgrammer.STEP_WRITING_BOOTSCRIPT2, BiosProgrammer.TOTAL_STEPS);
                return 1;
            }
            this.NotifyStatus("Successfully wrote the boot script to NAND.", BiosProgrammer.STEP_WRITING_BOOTSCRIPT2, BiosProgrammer.TOTAL_STEPS);
            if (this.Abort)
            {
                return -1;
            }

            return 0;
        }
 
        private int WriteAndCheck(string sourceAddress, string targetAddress, string length)
        {
            // erase the target location in NAND
            if (this.SendCmdAndWait(string.Format("nand erase {0} {1}\r", targetAddress, length), "OK", 60000) != 0)
            {
                return 1;
            }

            // write out the image to NAND
            if (this.SendCmdAndWait(string.Format("nand write {0} {1} {2}\r", sourceAddress, targetAddress, length), "OK", 60000) != 0)
            {
                return 1;
            }
            
            // read the image back for verification
            if (this.SendCmdAndWait(string.Format("nand read 43800000 {0} {1}\r", targetAddress, length), "OK", 60000) != 0)
            {
                return 1;
            }
            
            // verify images
            string response = string.Format("Total of {0} bytes were the same", Int32.Parse(length, System.Globalization.NumberStyles.HexNumber));
            if (this.SendCmdAndWait(string.Format("cmp.b {0} 43800000 {1}\r", sourceAddress, length), response, 60000) != 0)
            {
                return 1;
            }
            
            return 0;
        }

        private bool TFTPFecth(string targetAddress, string fetchFile)
        {
            if (this.SendCmdAndWait(string.Format("tftp {0} {1}\r", targetAddress, fetchFile), "Bytes transferred", 30000) != 0)
            {
                return false;
            }
            return true;
        }
    }
}
