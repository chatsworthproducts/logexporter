﻿#
# Script.ps1
#
#
# MCM/Jenkins interaction script
#

$TFTPAddr = ""
$TFTPDirectory = "c:\CPI\TFTP\"
$JenkinsServerAddr = "gt-dev-tfs:8080"
$TargetTestPDU = "192.168.136.32"
$PDUAdminLoginID = "admin"
$PDUAdminLoginPWD = "admin"

Function DeployLastMCMBuildToTFTP()
{
	Param($JenkinsServer = $null,
		  $Project = "MCM_R2D1_main",
		  $TempDir = $null,
		  $TFTPDir = $TFTPDirectory)

	Process {
		$FWBinFile = GetCPIBinFile $JenkinsServer $Project
		Copy-Item -Path $FWBinFile -Destination $TFTPDir -Force

		$PathParts = split-path $FWBinFile -leaf
		return $PathParts[1]
	}
}

Function GetCPIBinFile()
{
	Param($JenkinsServer = $null,
		  $Project = "MCM_R2D1_main",
		  $TempDir = $null)

	Process {
		$PackageFile = GetCPIPackageFile $JenkinsServer $Project

		if( $PackageFile -eq $null )
		{
			return $null;
		}

		if($TempDir -eq $null)
		{
			$TempDir = $env:temp + "\cpipack"
			if( (Test-Path $TempDir) -eq $true )
			{
				Remove-Item $TempDir -Recurse
			}

			New-Item $TempDir -type directory
		}

		Expand-ZIPFile $PackageFile $TempDir
				
		$binFile = (Get-ChildItem $TempDir -force | Where-Object {$_.Extension -match ".bin"}).FullName

		return $binFile
	}
}

Function GetCPIPackageFile()
{
	Param($JenkinsServer = $null,
		  $Project = "MCM_R2D1_main")
	
	Process{
		# Get the URL of the last successfully built project
		$JenkinsProject = "http://" + $JenkinsServer + "/job/" + $Project + "/api/xml?xpath=/freeStyleProject/lastCompletedBuild/url"
		$ProjectList = "last_successful_url_" + $Project + ".xml"

		Invoke-WebRequest $JenkinsProject -OutFile $ProjectList

		$LastSuccessfulURL = [xml](get-content $ProjectList)
		$LastSuccessfulURL = $LastSuccessfulURL.FirstChild.InnerText
		Remove-Item $ProjectList

		$JenkinsProject = $LastSuccessfulURL + "api/xml"
		$ProjectList = "last_successful_" + $Project + ".xml"

		Invoke-WebRequest $JenkinsProject -OutFile $ProjectList

		$LastSuccessfulXMLData = [xml](get-content $ProjectList)
		
		$artifactList = $LastSuccessfulXMLData.GetElementsByTagName("artifact")        

		for($i=0; $i -lt $artifactList.Count; $i++)        
		{
			$artifactName = $artifactList[$i].SelectSingleNode("relativePath").InnerText
			if( $artifactName.StartsWith("cpipackage-") -eq $true )
			{
				break;
			}
			$artifactName = $null
		}

		Remove-Item $ProjectList

		if( $artifactName -ne $null )
		{
			$JenkinsProject = $LastSuccessfulURL + "artifact/" + $artifactName
			$ProjectList = $artifactName

			Invoke-WebRequest $JenkinsProject -OutFile $ProjectList
		}
		
		return (Get-Item -Path ".\" -Verbose).FullName + "\" + $artifactName
	}
}

function Expand-ZIPFile($file, $destination)
{
	$shell = new-object -com shell.application
	$zip = $shell.NameSpace($file)
	foreach($item in $zip.items())
	{
		$shell.Namespace($destination).copyhere($item)
	}
}

function Wait-PDUReboot($pduIp)
{
	while( (Test-Connection -ComputerName $pduIp -Quiet -Count 2) -eq $true )
	{
		Start-Sleep -Seconds 1
	}

	while( (Test-Connection -ComputerName $pduIp -Quiet -Count 2) -eq $false )
	{
		Start-Sleep -Seconds 1
	}

	Invoke-WebRequest -Uri http://$pduip/login.lp -Method Get
}

Write-Host "Starting Testing of new firmware."

# Get the version number of the FW on the PDU now.
$beforeVersion = Get_PDUSNMP $TargetTestPDU "0" "1.3.6.1.4.1.30932.1.1.1.1"

# Upgrade the FW on the PDU
$binFile = DeployLastMCMBuildToTFTP $JenkinsServerAddr MCM_R2D1_main
$fwVersionInfo = Split-FirmwareName $binFile

Write-Host "Upgrading from $beforeVersion to $fwVersionInfo[6]"

Install-PDUFirmware $PDUAdminLoginID $PDUAdminLoginPWD $TargetTestPDU $TFTPAddr $binFile
Wait-PDUReboot 192.168.10.5

# Verify FW upgraded successfully on the PDU.

# Stamp the Test Model on the PDU

# Reboot the PDU

# Sync the time on the PDU

# Touch all screens and verify no errors pop

# Check the SNMP APIs
#	- Check Original SNMP operation
#	- Check Fieldview Patch SNMP Operation
#	- Check Unity SNMP Operation



