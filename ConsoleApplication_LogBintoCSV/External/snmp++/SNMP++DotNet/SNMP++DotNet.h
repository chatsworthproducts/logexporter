// SNMP++DotNet.h

#pragma once

#include <string>
#include <list>
#include <libsnmp.h>
#include <msclr\marshal_cppstd.h>
#include "snmp_pp/snmp_pp.h"
#include "SNMPAdapter.h"
#include "SNMP2Adapter.h"
#include "SNMP3Adapter.h"

using namespace System;
using namespace System::Collections::Generic;
using namespace msclr::interop;

namespace CPI {
    namespace Common {
        namespace SNMP {

            public ref class SNMPDotNetLib
            {
            public:
                static IDictionary<String^, String^> ^GetSNMPData(String^ ipAddress, 
                                                                 IList<String^>^ getOids, 
                                                                 IDictionary<String^, String^> ^propertyInformation)
                {
                    Snmp_pp::Snmp::socket_startup();  // Initialize socket subsystem

                    marshal_context^ context = gcnew marshal_context();
                    Snmp_pp::UdpAddress address(context->marshal_as<const char *>(ipAddress));      // make a SNMP++ Generic address                    
                    Snmp_pp::Pdu pdu;                              // construct a Pdu object                    
                    BuildPDU(pdu, getOids);
                    delete context;                    
                    
                    InternalDictionary configProperties;
                    InternalDictionary internalReturnValue;
                    configProperties.FromManaged(propertyInformation);

                    SNMPAdapter *adapter = BuildAdpater(configProperties);
                    if (adapter == NULL)
                    {
                        Snmp_pp::Snmp::socket_cleanup();
                        return internalReturnValue.ToManaged();
                    }
                    
                    int successCode = adapter->Fetch(address, pdu);
                    if (successCode == 0)
                    {
                        for (int i = 0; i < pdu.get_vb_count(); i++) 
                        {
                            Snmp_pp::Vb vb;
                            pdu.get_vb(vb, i);                            
                            internalReturnValue.insert(InternalDictionary::value_type(vb.get_printable_oid(), vb.get_printable_value()));                            
                        }
                    }
                    delete adapter;
                    
                    Snmp_pp::Snmp::socket_cleanup();
                    return internalReturnValue.ToManaged();
                }

                static IDictionary<String^, String^> ^BulkGetSNMPData(String^ ipAddress,
                                                                      IList<String^>^ getOids,
                                                                      IDictionary<String^, String^> ^propertyInformation)
                {
                    Snmp_pp::Snmp::socket_startup();  // Initialize socket subsystem

                    marshal_context^ context = gcnew marshal_context();
                    Snmp_pp::UdpAddress address(context->marshal_as<const char *>(ipAddress));      // make a SNMP++ Generic address                    
                    Snmp_pp::Pdu pdu;                              // construct a Pdu object                    
                    BuildPDU(pdu, getOids);
                    delete context;

                    InternalDictionary configProperties;
                    InternalDictionary internalReturnValue;
                    configProperties.FromManaged(propertyInformation);

                    SNMPAdapter *adapter = BuildAdpater(configProperties);
                    if (adapter == NULL)
                    {
                        Snmp_pp::Snmp::socket_cleanup();
                        return internalReturnValue.ToManaged();
                    }

                    int successCode = adapter->BulkFetch(address, pdu);
                    if (successCode == 0)
                    {
                        for (int i = 0; i < pdu.get_vb_count(); i++)
                        {
                            Snmp_pp::Vb vb;
                            pdu.get_vb(vb, i);
                            internalReturnValue.insert(InternalDictionary::value_type(vb.get_printable_oid(), vb.get_printable_value()));
                        }
                    }
                    delete adapter;

                    Snmp_pp::Snmp::socket_cleanup();
                    return internalReturnValue.ToManaged();
                }

                static void SetSNMPData(String^ ipAddress, 
                                        IList<KeyValuePair<String^, Object^>> ^setData, 
                                        IDictionary<String^, String^> ^propertyInformation)
                {
                    Snmp_pp::Snmp::socket_startup();  // Initialize socket subsystem

                    marshal_context^ context = gcnew marshal_context();
                    Snmp_pp::UdpAddress address(context->marshal_as<const char *>(ipAddress));      // make a SNMP++ Generic address                    
                    Snmp_pp::Pdu pdu;                              // construct a Pdu object                    
                    BuildPDU(pdu, setData);
                    delete context;


                    InternalDictionary configProperties;
                    InternalDictionary internalReturnValue;
                    configProperties.FromManaged(propertyInformation);

                    SNMPAdapter *adapter = BuildAdpater(configProperties);
                    if (adapter == NULL)
                    {
                        Snmp_pp::Snmp::socket_cleanup();                        
                    }

                    int successCode = adapter->Set(address, pdu);
                    if (successCode < 0)
                    {
                        throw gcnew Exception();
                    }

                    Snmp_pp::Snmp::socket_cleanup();
                }

            private:
                static SNMPAdapter *BuildAdpater(const InternalDictionary &settings)
                {
                    InternalDictionary::const_iterator it = settings.find("Public.DeviceManager.SNMPVersion");
                    if (it == settings.end())
                    {
                        return NULL;
                    }

                    SNMPAdapter *adapter;
                    if (it->second.compare("SNMPv1v2c") == 0)
                    {                    
                        adapter = new SNMP2Adapter();
                    }
                    else if (it->second.compare("SNMPv3") == 0)
                    {
                        adapter = new SNMP3Adapter();
                    }
                    adapter->Configure(settings);

                    return adapter;
                }

                static void BuildPDU(Snmp_pp::Pdu &pdu, IList<String^>^ getOids)
                {
                    marshal_context^ context = gcnew marshal_context();

                    for (int i = 0; i < getOids->Count; i++)
                    {
                        // construct a Oid object
                        Snmp_pp::Oid oid(context->marshal_as<const char *>(getOids[i]));

                        // construct a Vb object
                        Snmp_pp::Vb vb(oid);

                        // add it to the pdu object
                        pdu += vb;
                    }

                    delete context;
                }

                static void BuildPDU(Snmp_pp::Pdu &pdu, IList<KeyValuePair<String^, Object^>> ^getOids)
                {
                    marshal_context^ context = gcnew marshal_context();

                    for (int i = 0; i < getOids->Count; i++)
                    {
                        // construct a Oid object
                        Snmp_pp::Oid oid(context->marshal_as<const char *>(getOids[i].Key));

                        // construct a Vb object
                        Object^ data = getOids[i].Value;
                        Snmp_pp::Vb vb(oid);
                        if ((data->GetType() == Int32::typeid) ||
                            (data->GetType() == int::typeid))
                        {
                            int intData = (int)data;
                            vb.set_value(intData);
                        }
                        else if (data->GetType() == String::typeid)
                        {
                            String^ strData = (String^)data;
                            vb.set_value(context->marshal_as<const char *>(strData));
                        }

                        // add it to the pdu object
                        pdu += vb;
                    }

                    delete context;
                }
            };
        }
    }
}