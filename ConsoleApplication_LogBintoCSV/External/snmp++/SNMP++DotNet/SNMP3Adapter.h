#pragma once
#include "SNMPAdapter.h"

class SNMP3Adapter : public SNMPAdapter
{
public:
    SNMP3Adapter()
    {

    }

    virtual void Configure(const InternalDictionary &properties)
    {

    }

    virtual int Fetch(Snmp_pp::UdpAddress &address, Snmp_pp::Pdu &workingpdu)
    {
        return 1;
    }

    virtual int BulkFetch(Snmp_pp::UdpAddress &address, Snmp_pp::Pdu &workingpdu)
    {
        return 1;
    }

    virtual int Set(Snmp_pp::UdpAddress &address, Snmp_pp::Pdu &workingpdu)
    {
        return 1;
    }
};

