#pragma once
#include <string>
#include <libsnmp.h>
#include <msclr\marshal_cppstd.h>
#include "snmp_pp/snmp_pp.h"
#include "SNMPAdapter.h"

class SNMP2Adapter : public SNMPAdapter
{
    std::string _RCommunityString;
    std::string _WCommunityString;

public:
    SNMP2Adapter()
    {
        _RCommunityString = "public";
        _WCommunityString = "private";
    }

    virtual void Configure(const InternalDictionary &properties)
    {
        InternalDictionary::const_iterator it = properties.find("Public.DeviceManager.SNMPReadCommunity");
        if (it != properties.end())
        {            
            _RCommunityString = it->second;
        }

        it = properties.find("Public.DeviceManager.SNMPWriteCommunity");
        if (it != properties.end())
        {
            _WCommunityString = it->second;
        }
    }

    virtual int Fetch(Snmp_pp::UdpAddress &address, Snmp_pp::Pdu &workingpdu)
    {
        int status;
        Snmp_pp::Snmp snmpSession(status, 0, (address.get_ip_version() == Snmp_pp::Address::version_ipv6));
        if (status != SNMP_CLASS_SUCCESS)
        {
            return 1;
        }

        Snmp_pp::snmp_version version = Snmp_pp::version2c;         // default is v2c
        int retries = 1;                          // default retries is 1
        int timeout = 1000;                        // default is 1 second
        u_short port = 161;                       // default snmp port is 161
        int non_reps = 0;                         // non repeaters default is 0
        int max_reps = 10;                        // maximum repetitions default is 10

        address.set_port(port);
        Snmp_pp::CTarget ctarget(address);        // make a target using the address
        ctarget.set_version(version);         // set the SNMP version SNMPV1 or V2
        ctarget.set_retry(retries);           // set the number of auto retries
        ctarget.set_timeout(timeout);         // set timeout
        ctarget.set_readcommunity(_RCommunityString.c_str()); // set the read community name
        ctarget.set_writecommunity(_WCommunityString.c_str());

        Snmp_pp::SnmpTarget *target = &ctarget;

        status = snmpSession.get(workingpdu, *target);
        if (status == SNMP_CLASS_SUCCESS)
        {
            return 0;
        }

        return 2;
    }
    
    virtual int BulkFetch(Snmp_pp::UdpAddress &address, Snmp_pp::Pdu &workingpdu)
    {
        int status;
        Snmp_pp::Snmp snmpSession(status, 0, (address.get_ip_version() == Snmp_pp::Address::version_ipv6));
        if (status != SNMP_CLASS_SUCCESS)
        {
            return 1;
        }

        Snmp_pp::snmp_version version = Snmp_pp::version2c;         // default is v2c
        int retries = 1;                          // default retries is 1
        int timeout = 1000;                        // default is 1 second
        u_short port = 161;                       // default snmp port is 161
        int non_reps = 0;                         // non repeaters default is 0
        int max_reps = 10;                        // maximum repetitions default is 10

        address.set_port(port);
        Snmp_pp::CTarget ctarget(address);        // make a target using the address
        ctarget.set_version(version);         // set the SNMP version SNMPV1 or V2
        ctarget.set_retry(retries);           // set the number of auto retries
        ctarget.set_timeout(timeout);         // set timeout
        ctarget.set_readcommunity(_RCommunityString.c_str()); // set the read community name
        ctarget.set_writecommunity(_WCommunityString.c_str());

        Snmp_pp::SnmpTarget *target = &ctarget;

        status = snmpSession.get_bulk(workingpdu, *target, 0, 100 );
        if (status == SNMP_CLASS_SUCCESS)
        {
            return 0;
        }

        return 2;
    }

    virtual int Set(Snmp_pp::UdpAddress &address, Snmp_pp::Pdu &workingpdu)
    {
        int status;
        Snmp_pp::Snmp snmpSession(status, 0, (address.get_ip_version() == Snmp_pp::Address::version_ipv6));
        if (status != SNMP_CLASS_SUCCESS)
        {
            return 1;
        }

        Snmp_pp::snmp_version version = Snmp_pp::version2c;         // default is v2c
        int retries = 1;                          // default retries is 1
        int timeout = 100;                        // default is 1 second
        u_short port = 161;                       // default snmp port is 161
        int non_reps = 0;                         // non repeaters default is 0
        int max_reps = 10;                        // maximum repetitions default is 10

        address.set_port(port);
        Snmp_pp::CTarget ctarget(address);        // make a target using the address
        ctarget.set_version(version);         // set the SNMP version SNMPV1 or V2
        ctarget.set_retry(retries);           // set the number of auto retries
        ctarget.set_timeout(timeout);         // set timeout
        ctarget.set_readcommunity(_RCommunityString.c_str()); // set the read community name
        ctarget.set_writecommunity(_WCommunityString.c_str());

        Snmp_pp::SnmpTarget *target = &ctarget;

        status = snmpSession.set(workingpdu, *target);
        if (status == SNMP_CLASS_SUCCESS)
        {
            return 0;
        }

        return 1;
    }
};

