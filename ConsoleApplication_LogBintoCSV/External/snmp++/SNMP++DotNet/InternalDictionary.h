#pragma once
#include <string>
#include <list>
#include <map>
#include <msclr\marshal_cppstd.h>

using namespace System;
using namespace System::Collections::Generic;
using namespace msclr::interop;

class InternalDictionary : public std::map<std::string, std::string>
{

public:    
    InternalDictionary() :
        std::map<std::string, std::string>()
    {
    }

    void FromManaged(IDictionary<String^, String^> ^sourceDictionary)
    {
        marshal_context^ context = gcnew marshal_context();

        for each (KeyValuePair<String^, String^>^ var in sourceDictionary)
        {
            this->insert(InternalDictionary::value_type(context->marshal_as<const char *>(var->Key), 
                context->marshal_as<const char *>(var->Value)));
        }

        delete context;
    }

    IDictionary<String^, String^> ^ToManaged()
    {
        Dictionary<String^, String^> ^returnDictionary = gcnew Dictionary<String^, String^>();

        for (InternalDictionary::iterator it = this->begin(); it != this->end(); it++)
        {
            returnDictionary->Add(gcnew String(it->first.c_str()), gcnew String(it->second.c_str()));
        }

        return returnDictionary;
    }
};

