#pragma once
#include <libsnmp.h>
#include "InternalDictionary.h"
#include "snmp_pp/snmp_pp.h"

class SNMPAdapter
{
public:
    virtual void Configure(const InternalDictionary &properties) = 0;

    virtual int Fetch(Snmp_pp::UdpAddress &address, Snmp_pp::Pdu &workingpdu) = 0;
    virtual int BulkFetch(Snmp_pp::UdpAddress &address, Snmp_pp::Pdu &workingpdu) = 0;
    virtual int Set(Snmp_pp::UdpAddress &address, Snmp_pp::Pdu &workingpdu) = 0;
};
