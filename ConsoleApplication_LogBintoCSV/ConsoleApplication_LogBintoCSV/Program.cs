﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPI.eConnect.Logging.eConnect;
using CPI.Data.PDULog;
using ConsoleApplication_LogBintoCSV;

namespace ConsoleApplication_LogBintoCSV
{
    class Program
    {
        //static string logFilePath = "C:\\Users\\baldwin_m\\Documents\\Random Notes\\Feature and Bug Testing\\BinToCSV\\003702-201509291800.log.dat";
        //static string logFilePath = "C:\\Users\\baldwin_m\\Documents\\Random Notes\\Feature and Bug Testing\\BinToCSV\\001404+201508210600.log.dat";
        static string logFilePath = "C:\\Users\\baldwin_m\\Documents\\Random Notes\\Feature and Bug Testing\\BinToCSV\\00102E-201511101800.log.dat";
        //static string logFilePath = "C:\\Users\\baldwin_m\\Documents\\Random Notes\\Feature and Bug Testing\\BinToCSV\\00246A-201509211800.log.dat";
        //static string logFilePath = "C:\\Users\\baldwin_m\\Documents\\Random Notes\\Feature and Bug Testing\\BinToCSV\\003702-201510021201.log (5).dat";
        //static string logFilePath = "C:\\Users\\baldwin_m\\Documents\\Random Notes\\Feature and Bug Testing\\BinToCSV\\003702-201510061800.log.dat";

        static string outputFilePath = "C:\\Users\\baldwin_m\\Documents\\Random Notes\\Feature and Bug Testing\\BinToCSV\\outputFile.csv";
       
        static string AlarmTestOutput = "C:\\Users\\baldwin_m\\Documents\\Random Notes\\Feature and Bug Testing\\BinToCSV\\AlarmOutputTest.csv";
        static string BranchTestOutput = "C:\\Users\\baldwin_m\\Documents\\Random Notes\\Feature and Bug Testing\\BinToCSV\\BranchOutputTest.csv";
        static string RecepInfoTestOutput = "C:\\Users\\baldwin_m\\Documents\\Random Notes\\Feature and Bug Testing\\BinToCSV\\RecepInfoOutputTest.csv";
        static string ConfigChangeTestOutput = "C:\\Users\\baldwin_m\\Documents\\Random Notes\\Feature and Bug Testing\\BinToCSV\\ConfigChangeOutputTest.csv";
        static string EnvironTestOutput = "C:\\Users\\baldwin_m\\Documents\\Random Notes\\Feature and Bug Testing\\BinToCSV\\EnvironOutputTest.csv";
        static string FirmwareChangeTestOutput = "C:\\Users\\baldwin_m\\Documents\\Random Notes\\Feature and Bug Testing\\BinToCSV\\FirmwareChangeOutputTest.csv";
        static string LogInOutTestOutput = "C:\\Users\\baldwin_m\\Documents\\Random Notes\\Feature and Bug Testing\\BinToCSV\\LogInOutOutputTest.csv";
        static string RecepChangeTestOutput = "C:\\Users\\baldwin_m\\Documents\\Random Notes\\Feature and Bug Testing\\BinToCSV\\RecepChangeOutputTest.csv";
        static string PDUOptionChangeTestOutput = "C:\\Users\\baldwin_m\\Documents\\Random Notes\\Feature and Bug Testing\\BinToCSV\\PDUOptionChangeOutputTest.csv";
        static string UserChangeTestOutput = "C:\\Users\\baldwin_m\\Documents\\Random Notes\\Feature and Bug Testing\\BinToCSV\\UserChangeOutputTest.csv";

        static void Main(string[] args)
        {

            LogReader reader = new LogReader(logFilePath);
            ReadingStateInfo logInfo = new ReadingStateInfo(reader);

            ///////////////////////////////////////////////////////////////////////////////
            // Populating the objects in memory with Log Information
            try
            {
                while (true)
                {
                    reader.ReadEntry();
                }
            }
            catch (System.IO.EndOfStreamException)
            {
                // not really an error, we're just done reading the file.
            }
            finally
            {
                reader.Dispose();
            }
            // Done Populating objects in memory
            ////////////////////////////////////////////////////////////////////////////////
            
            AlarmOutput.AlarmCSVOutput(logInfo, AlarmTestOutput, "Environmental");
            
            BranchOutput.BranchCSVOutput(logInfo, BranchTestOutput);
           
            RecepInfoOutput.RecepInfoCSVOutput(logInfo, RecepInfoTestOutput);
            
            ConfigChangeOutput.ConfigChangeCSVOutput(logInfo, ConfigChangeTestOutput);
            
            EnvironInfoOutput.EnvironInfoCSVOutput(logInfo, EnvironTestOutput);
            
            FirmwareChangeOutput.FirmwareChangeCSVOutput(logInfo, FirmwareChangeTestOutput);
            
            LogInOutOutput.LogInOutCSVOutput(logInfo, LogInOutTestOutput);
            
            RecepChangeOutput.RecepChangeCSVOutput(logInfo, RecepChangeTestOutput);
            
            PDUOptionChangeOutput.PDUOptionChangeCSVOutput(logInfo, PDUOptionChangeTestOutput);
            
            UserChangeOutput.UserChangeCSVOutput(logInfo, UserChangeTestOutput);
        }
    }
}
