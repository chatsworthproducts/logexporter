﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using CPI.eConnect.Logging.eConnect;
using CPI.Data.PDULog;

namespace ConsoleApplication_LogBintoCSV
{
    public class PDUOptionChangeOutput
    {
        public static void PDUOptionChangeCSVOutput(ReadingStateInfo logInfo, string outputFilePath)
        {
            foreach (KeyValuePair<string, PDUInfoEntry> entry in logInfo.PDUInfoIndex)
            {
                PDUInfoEntry test = logInfo.PDUInfoIndex[entry.Key];

                try
                {
                    using (StreamWriter w = new StreamWriter(outputFilePath, true))
                    {
                        w.WriteLine("TimeStamp, MAC, User ID, PDU Interface, OP Action");
                        //w.Flush();

                        for (int i = 0; i < test.OptionChanges.Count; i++)
                        {
                            PDUOptionChange currentEntry = test.OptionChanges[i];
                            string line = currentEntry.TimeStamp + ", " + currentEntry.MAC + ", " + currentEntry.UserId + ", " +
                                currentEntry.PDUInterface + ", " + currentEntry.OPAction;
                            w.WriteLine(line);
                            //w.Flush();         
                        }
                    }
                }
                catch (Exception e)
                {
                    //System.Console.WriteLine("SOMETHING WENT WRONG");
                }
            }
        }
    }
}
