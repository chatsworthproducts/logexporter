﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using CPI.eConnect.Logging.eConnect;
using CPI.Data.PDULog;

namespace ConsoleApplication_LogBintoCSV
{
    public class FirmwareChangeOutput
    {

        private static string Action(FirmwareChange currentEntry)
        {
            string action = "";

            switch (currentEntry.Action.ToString())
            {
                case("UpdateMaster"):
                    action = "Updated PDU";
                    break;
            }

            return action;
        }

        public static void FirmwareChangeCSVOutput(ReadingStateInfo logInfo, string outputFilePath)
        {
            foreach (KeyValuePair<string, PDUInfoEntry> entry in logInfo.PDUInfoIndex)
            {
                PDUInfoEntry test = logInfo.PDUInfoIndex[entry.Key];

                try
                {
                    using (StreamWriter w = new StreamWriter(outputFilePath, true))
                    {
                        //w.WriteLine("TimeStamp, MAC, User ID, PDU Interface, Action, Update Map, Package Map, Bios Rev, Kernel Rev, File Sys Rev, Patch Rev, Update Result");
                        //w.Flush();
                        w.WriteLine("TimeStamp, MAC, User ID, PDU Interface, Action, New Value");

                        for (int i = 0; i < test.FirmwareChanges.Count; i++)
                        {
                            FirmwareChange currentEntry = test.FirmwareChanges[i];
                            /*string line = currentEntry.TimeStamp + ", " + currentEntry.MAC + ", " + currentEntry.UserID + ", " + currentEntry.PDUInterface + ", " +
                                currentEntry.Action + ", " + (short)currentEntry.UpdateMap + ", " + (short)currentEntry.PackageMap + ", " + currentEntry.BiosRev + ", " +
                                currentEntry.KernelRev + ", " + currentEntry.FileSysRev + ", " + currentEntry.PatchRev + ", " + currentEntry.UpdateResult;*/

                            string line = currentEntry.TimeStamp + ", " + currentEntry.MAC + ", " + currentEntry.UserID + ", " + currentEntry.PDUInterface + ", " +
                                Action(currentEntry) + ", " + currentEntry.FileSysRev;

                            w.WriteLine(line);
                            //w.Flush();         
                        }
                    }
                }
                catch (Exception e)
                {
                    //System.Console.WriteLine("SOMETHING WENT WRONG");
                }
            }
        }
    }
}
