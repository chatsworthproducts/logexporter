﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using CPI.eConnect.Logging.eConnect;
using CPI.Data.PDULog;

namespace ConsoleApplication_LogBintoCSV
{
    public class UserChangeOutput
    {

        private static string NewValue(UserChange currentEntry)
        {
            string newValue = "";

            switch (currentEntry.Action.ToString())
            {
                case ("DeleteUser"):
                    newValue = "N/A";
                    break;
                case ("ChangePassword"):
                    newValue = "N/A";
                    break;
                case ("ChangePermission"):
                    switch (currentEntry.Permission.ToString()) 
                    {
                        case ("7"):
                            newValue = "Admin";
                            break;
                        case ("5"):
                            newValue = "User";
                            break;
                        case ("3"):
                            newValue = "Viewer";
                            break;
                        case ("1"):
                            newValue = "Disabled";
                            break;
                    }
                    break;
                case ("ChangeName"):
                    for (int j = 0; j < 64; j++)
                    {
                        if (currentEntry.UserName[j] != '\0')
                            newValue += currentEntry.UserName[j];
                        else
                            break;
                    }
                    break;
            }
            return newValue;
        }

        private static string Action(UserChange currentEntry)
        {
            string action = "";

            switch (currentEntry.Action.ToString())
            {
                case ("DeleteUser"):
                    action = "Deleted UserID " + currentEntry.ChangeUserID.ToString();
                    break;
                case ("ChangePassword"):
                    action = "Set Password for UserID " + currentEntry.ChangeUserID.ToString();
                    break;
                case ("ChangePermission"):
                    action = "Set Permission for UserID " + currentEntry.ChangeUserID.ToString();
                    break;
                case ("ChangeName"):
                    action = "Set Username for UserID " + currentEntry.ChangeUserID.ToString();
                    break;
            }
            return action;
        }

        public static void UserChangeCSVOutput(ReadingStateInfo logInfo, string outputFilePath)
        {
            foreach (KeyValuePair<string, PDUInfoEntry> entry in logInfo.PDUInfoIndex)
            {
                PDUInfoEntry test = logInfo.PDUInfoIndex[entry.Key];

                try
                {
                    using (StreamWriter w = new StreamWriter(outputFilePath, true))
                    {
                        w.WriteLine("TimeStamp, MAC, User ID, Change User ID, Permission, PDU Interface, Action, UserName");
                        //w.Flush();

                        for (int i = 0; i < test.UserChanges.Count; i++)
                        {
                            UserChange currentEntry = test.UserChanges[i];

                            string newValue = NewValue(currentEntry);

                            if (newValue != "")
                            {
                                string line = currentEntry.TimeStamp + ", " + currentEntry.MAC + ", " + currentEntry.UserID + ", " + currentEntry.ChangeUserID + ", " +
                                currentEntry.Permission + ", " + currentEntry.PDUInterface + ", " + Action(currentEntry) + ", " + NewValue(currentEntry);

                                w.WriteLine(line);
                            }

                            if (currentEntry.Action.ToString() == "DeleteUser")
                                i++;
                            
                            //w.Flush();         
                        }
                    }
                }
                catch (Exception e)
                {
                    //System.Console.WriteLine("SOMETHING WENT WRONG");
                }
            }
        } 
    }
}
