﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using CPI.eConnect.Logging.eConnect;
using CPI.Data.PDULog;

namespace ConsoleApplication_LogBintoCSV
{
    public class ConfigChangeOutput
    {
        public static void ConfigChangeCSVOutput(ReadingStateInfo logInfo, string outputFilePath)
        {
            foreach (KeyValuePair<string, PDUInfoEntry> entry in logInfo.PDUInfoIndex)
            {
                PDUInfoEntry test = logInfo.PDUInfoIndex[entry.Key];

                try
                {
                    using (StreamWriter w = new StreamWriter(outputFilePath, true))
                    {
                        w.WriteLine("TimeStamp, MAC, User ID, Config Variable, Occurance, Size, DataType, MultiOCC, PDU Interface, New Value");
                        //w.Flush();

                        for (int i = 0; i < test.ConfigChanges.Count; i++)
                        {
                            ConfigChange currentEntry = test.ConfigChanges[i];

                            string newValue = "";

                            for (int j = 0; j < currentEntry.NewValue.Length; j++)
                            {
                                newValue += currentEntry.NewValue[j] + ",";
                            }

                            string line = currentEntry.TimeStamp + ", " + currentEntry.MAC + ", " + currentEntry.UserId + ", " + currentEntry.ConfigVariable + ", " +
                                currentEntry.Occurance + ", " + currentEntry.Size + ", " + currentEntry.DataType + ", " + currentEntry.MultiOCC + ", " +
                                currentEntry.PDUInterface + ", " + newValue;
                            w.WriteLine(line);
                            //w.Flush();         
                        }
                    }
                }
                catch (Exception e)
                {
                    //System.Console.WriteLine("SOMETHING WENT WRONG");
                }
            }
        }
    }
}
