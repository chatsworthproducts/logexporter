﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPI.eConnect.Logging.eConnect;
using CPI.Data.PDULog;

namespace ConsoleApplication_LogBintoCSV
{
    public class AlarmOutput
    {

        private static string EnvironmentalAlarmLineOutput(AlarmInfo currentEntry, string probe)
        {
            string line = "";

            ushort mask = 0x0000;

            switch (probe)
            {
                case "T1":
                    mask = 0x1000;
                    break;
                case "T2":
                    mask = 0x2000;
                    break;
                case "H1":
                    mask = 0x4000;
                    break;
                case "H2":
                    mask = 0x8000;
                    break;
            }

            if ((currentEntry.AlarmsHigh & mask) != 0 ||
                (currentEntry.AlarmsLow & mask) != 0)
            {
                line += currentEntry.TimeStamp + ", " + currentEntry.MAC + ", " + probe + ", ";
                //High Voltage Alarm
                line += ((currentEntry.AlarmsHigh & mask) != 0) ? "1, " : ", ";
                //Low Voltage Alarm
                line += ((currentEntry.AlarmsLow & mask) != 0) ? "1, " : ", ";
            }
            else
                return "";

            return line;
        }

        private static string ReceptacleAlarmLineOutput(AlarmInfo currentEntry, short receptacle)
        {
            string line = "";

            //Get the receptacle Bit Mask
            ulong mask = (ulong)0x00000001 << (receptacle - 1);

            if ((currentEntry.RecepAlarmsHigh & mask) != 0 ||
                (currentEntry.RecepWarningHigh & mask) != 0 ||
                (currentEntry.RecepWarningLow & mask) != 0)
            {
                line += currentEntry.TimeStamp + ", " + currentEntry.MAC + ", " + receptacle + ", ";
                //High Voltage Alarm
                line += ((currentEntry.RecepAlarmsHigh & mask) != 0) ? "1, " : ", ";
                //Low Voltage Alarm
                line += ((currentEntry.RecepWarningHigh & mask) != 0) ? "1, " : ", ";
                //High Current Alarm
                line += ((currentEntry.RecepWarningLow & mask) != 0) ? "1, " : " ";
            }
            else
                return "";

            return line;
        }

        private static string BranchAlarmLineOutput(AlarmInfo currentEntry, string branch)
        {
            string line = "";
            ushort voltMask = 0x0000;
            ushort curMask = 0x0000;
            //Set the BitMask to use
            switch (branch)
            {
                case "XY1":
                    voltMask = 0x0040;
                    curMask = 0x0001;
                    break;
                case "YZ1":
                    voltMask = 0x0080;
                    curMask = 0x0002;
                    break;
                case "ZX1":
                    voltMask = 0x0100;
                    curMask = 0x0004;
                    break;
                case "XY2":
                    voltMask = 0x0200;
                    curMask = 0x0008;
                    break;
                case "YZ2":
                    voltMask = 0x0400;
                    curMask = 0x0010;
                    break;
                case "ZX2":
                    voltMask = 0x0800;
                    curMask = 0x0020;
                    break;
            }

            //Check if there are any alarms present
            if ((currentEntry.AlarmsHigh & voltMask) != 0 ||
                (currentEntry.AlarmsHigh & curMask) != 0 ||
                (currentEntry.AlarmsLow & voltMask) != 0 ||
                (currentEntry.AlarmsLow & curMask) != 0 ||
                (currentEntry.WarningsHigh & voltMask) != 0 ||
                (currentEntry.WarningsHigh & curMask) != 0 ||
                (currentEntry.WarnsingLow & voltMask) != 0 ||
                (currentEntry.WarnsingLow & curMask) != 0)
            //We have Alarms. Need to build the line for the CSV file
            {
                line += currentEntry.TimeStamp + ", " + currentEntry.MAC + ", " + branch + ", ";
                //High Voltage Alarm
                line += ((currentEntry.AlarmsHigh & voltMask) != 0) ? "1, " : ", ";
                //Low Voltage Alarm
                line += ((currentEntry.AlarmsLow & voltMask) != 0) ? "1, " : ", ";
                //High Current Alarm
                line += ((currentEntry.AlarmsHigh & curMask) != 0) ? "1, " : ", ";
                //Warning High Current
                line += ((currentEntry.WarningsHigh & curMask) != 0) ? "1, " : ", ";
                //Warning Low Current
                line += ((currentEntry.WarnsingLow & curMask) != 0) ? "1 " : " ";
            }
            //No Alarms
            else
                return "";
            
            return line;
        }

        public static void AlarmCSVOutput(ReadingStateInfo logInfo, string outputFilePath, string report)
        {
            foreach (KeyValuePair<string, PDUInfoEntry> entry in logInfo.PDUInfoIndex)
            {
                PDUInfoEntry test = logInfo.PDUInfoIndex[entry.Key];

                try
                {
                    switch (report)
                    {
                        case "Branch":
                            using (StreamWriter w = new StreamWriter(outputFilePath, true))
                            {
                                w.WriteLine("TimeStamp, MAC, Branch, High Voltage Alarm, Low Voltage Alarm, Critical High Current, Warning High Current, Warning Low Current");

                                string line = "";

                                for (int i = 0; i < test.Alarms.Count; i++)
                                {
                                    AlarmInfo currentEntry = test.Alarms[i];
                                    //XY1
                                    line = BranchAlarmLineOutput(currentEntry, "XY1");
                                    if (line != "")
                                        w.WriteLine(line);
                                    //YZ1
                                    line = BranchAlarmLineOutput(currentEntry, "YZ1");
                                    if (line != "")
                                        w.WriteLine(line);
                                    //ZX1
                                    line = BranchAlarmLineOutput(currentEntry, "ZX1");
                                    if (line != "")
                                        w.WriteLine(line);
                                    //XY2
                                    line = BranchAlarmLineOutput(currentEntry, "XY2");
                                    if (line != "")
                                        w.WriteLine(line);
                                    //YZ2
                                    line = BranchAlarmLineOutput(currentEntry, "YZ2");
                                    if (line != "")
                                        w.WriteLine(line);
                                    //ZX2
                                    line = BranchAlarmLineOutput(currentEntry, "ZX2");
                                    if (line != "")
                                        w.WriteLine(line);
                                }
                            }
                            break;
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        case "Receptacle":
                            using (StreamWriter w = new StreamWriter(outputFilePath, true))
                            {
                                w.WriteLine("TimeStamp, MAC, Receptacle, High Current Alarm, High Current Warning, Low Current Warning");

                                for (int i = 0; i < test.Alarms.Count; i++)
                                {
                                    AlarmInfo currentEntry = test.Alarms[i];
                                    for (short r = 0; r < 64; r++)
                                    {
                                        string line = ReceptacleAlarmLineOutput(currentEntry, (short)(r+1));
                                        if (line != "")
                                            w.WriteLine(line);
                                    }
                                }
                            }
                            break;
                        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        case "Environmental":
                            using (StreamWriter w = new StreamWriter(outputFilePath, true))
                            {
                                w.WriteLine("TimeStamp, MAC, Probe, Max Threshold Alarm, Min Threshold Alarm");

                                for (int i = 0; i < test.Alarms.Count; i++)
                                {
                                    AlarmInfo currentEntry = test.Alarms[i];
                                    //Temperature 1 Probe
                                    string line = EnvironmentalAlarmLineOutput(currentEntry, "T1");
                                    if (line != "")
                                        w.WriteLine(line);
                                    //Temperature 2 Probe
                                    line = EnvironmentalAlarmLineOutput(currentEntry, "T2");
                                    if (line != "")
                                        w.WriteLine(line);
                                    //Humidity 1 Probe
                                    line = EnvironmentalAlarmLineOutput(currentEntry, "H1");
                                    if (line != "")
                                        w.WriteLine(line);
                                    //Humidity 2 Probe
                                    line = EnvironmentalAlarmLineOutput(currentEntry, "H2");
                                    if (line != "")
                                        w.WriteLine(line);
                                }
                            }
                            break;
                            
                    }
                }
                catch (Exception e)
                {
                    //System.Console.WriteLine("SOMETHING WENT WRONG");
                }
            }
        }
    }
}
