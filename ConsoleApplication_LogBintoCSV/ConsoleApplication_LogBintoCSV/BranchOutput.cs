﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPI.eConnect.Logging.eConnect;
using CPI.Data.PDULog;

namespace ConsoleApplication_LogBintoCSV
{
    public class BranchOutput
    {
        public static void BranchCSVOutput(ReadingStateInfo logInfo, string outputFilePath)
        {
            foreach (KeyValuePair<string, PDUInfoEntry> entry in logInfo.PDUInfoIndex)
            {
                PDUInfoEntry test = logInfo.PDUInfoIndex[entry.Key];

                try
                {
                    using (StreamWriter w = new StreamWriter(outputFilePath, true))
                    {
                        //w.WriteLine("TimeStamp, MAC, Branch, Current, Voltage, Power, PowerFactor, Energy");
                        //w.Flush();
                        /*
                        for (int i = 0; i < test.BranchData.Count; i++)
                        {
                            BranchInfo currentEntry = test.BranchData[i];
                            string line = currentEntry.TimeStamp + ", " + currentEntry.MAC + ", " + currentEntry.Branch + ", " + 
                                String.Format("{0:0.00}", currentEntry.Current) + " A, " + String.Format("{0:0.0}", currentEntry.Voltage/10.0)+ " V, " + 
                                String.Format("{0:0.00}", currentEntry.Power) + " kW, " + String.Format("{0:0.00}", currentEntry.PowerFactor) + ", " + 
                                String.Format("{0:0.00}", currentEntry.Energy/360000.0) + " kWh";
                            
                            if( currentEntry.Current != 65535)
                                w.WriteLine(line);

                            if (currentEntry.Branch == 6)
                                w.WriteLine();
                            //w.Flush();         
                        }*/
                    }
                }
                catch (Exception e)
                {
                    //System.Console.WriteLine("SOMETHING WENT WRONG");
                }
            }
        }
    }
}
