﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using CPI.eConnect.Logging.eConnect;
using CPI.Data.PDULog;

namespace ConsoleApplication_LogBintoCSV
{
    public class RecepInfoOutput
    {
        public static void RecepInfoCSVOutput(ReadingStateInfo logInfo, string outputFilePath) 
        {
            foreach (KeyValuePair<string, PDUInfoEntry> entry in logInfo.PDUInfoIndex)
            {
                PDUInfoEntry test = logInfo.PDUInfoIndex[entry.Key];

                try
                {
                    using (StreamWriter w = new StreamWriter(outputFilePath, true))
                    {
                        w.WriteLine("TimeStamp, Outlet, MAC, Current, Voltage, Power, Energy");
                        //w.Flush();

                        for (int i = 0; i < test.RecepData.Count; i++)
                        {
                            RecepInfo currentEntry = test.RecepData[i];
                            string line = currentEntry.TimeStamp + ", " + currentEntry.Outlet + ", " + currentEntry.MAC + ", " +
                                String.Format("{0:0.00}", currentEntry.Current) + " A, " + String.Format("{0:0.0}", currentEntry.Voltage / 10.0) + " V, " +
                                String.Format("{0:0.00}", currentEntry.Power) + " kW, " + String.Format("{0:0.00}", currentEntry.Energy / 360000.0) + " kWh";
                            w.WriteLine(line);
                            //w.Flush();         
                            if (currentEntry.Outlet == 24)
                                w.WriteLine();
                        }
                    }
                }
                catch (Exception e)
                {
                    //System.Console.WriteLine("SOMETHING WENT WRONG");
                }
            }
        }
    }
}
