﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using CPI.eConnect.Logging.eConnect;
using CPI.Data.PDULog;

namespace ConsoleApplication_LogBintoCSV
{
    public class LogInOutOutput
    {
        public static void LogInOutCSVOutput(ReadingStateInfo logInfo, string outputFilePath)
        {
            foreach (KeyValuePair<string, PDUInfoEntry> entry in logInfo.PDUInfoIndex)
            {
                PDUInfoEntry test = logInfo.PDUInfoIndex[entry.Key];

                try
                {
                    using (StreamWriter w = new StreamWriter(outputFilePath, true))
                    {
                        w.WriteLine("TimeStamp, MAC, User ID, User, PDU Interface, Action");
                        //w.Flush();

                        for (int i = 0; i < test.LogInOutInfo.Count; i++)
                        {
                            LogInOut currentEntry = test.LogInOutInfo[i];
                            string line = currentEntry.TimeStamp + ", " + currentEntry.MAC + ", " + currentEntry.UserId + ", " + currentEntry.User + ", " +
                                currentEntry.PDUInterface + ", " + currentEntry.Action;
                            w.WriteLine(line);
                            //w.Flush();         
                        }
                    }
                }
                catch (Exception e)
                {
                    //System.Console.WriteLine("SOMETHING WENT WRONG");
                }
            }
        }
    }
}
